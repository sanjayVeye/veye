<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Subscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->int('reports_id');
            $table->string('subscribe_title');
            $table->string('yearly_description');
            $table->string('red_strap_text');
            $table->string('plan_information');
            $table->string('plan_price');
            $table->string('sale_price');
            $table->string('report_type');
            $table->string('display_order');
            $table->enum('status',[1,0])->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscriptions');
    }
}
