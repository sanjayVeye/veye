<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VeyeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('veye_details', function (Blueprint $table) {
            $table->increments('id');
            $table->int('veye_acn_no');
            $table->int('veye_abn_no');
            $table->int('veye_afsl_no');
            $table->int('veye_ar_no');
            $table->int('global_acn_no');
            $table->int('global_abn_no');
            $table->int('global_afsl_no');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('state');
            $table->string('country');
            $table->string('post_code');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('veye_details');
    }
}
