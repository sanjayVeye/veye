<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->integer('post_code');
            $table->boolean('payment')->default(1)->nullable();
            $table->enum('client_type', ['regular', 'premium']);

            $table->string('plans1',20)->default(null);
            $table->string('plans2',20)->default(null);
            $table->string('plans3',20)->default(null);
            $table->string('plans4',20)->default(null);
            $table->string('plans5',20)->default(null);
            $table->string('plans6',20)->default(null);

            $table->string('duration1',20)->default(null);
            $table->string('duration2',20)->default(null);
            $table->string('duration3',20)->default(null);
            $table->string('duration4',20)->default(null);
            $table->string('duration5',20)->default(null);
            $table->string('duration6',20)->default(null);

            $table->string('sale_type',80)->default(null);
            $table->string('sale_price',80)->default(null);
            
            $table->softDeletes();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
