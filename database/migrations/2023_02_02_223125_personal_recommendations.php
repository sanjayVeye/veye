<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PersonalRecommendations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personal_recommendations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company');
            $table->string('buy_price');
            $table->string('buy_date');
            $table->string('report_no1');
            $table->string('sell_price');
            $table->string('sell_date');
            $table->string('report_no_2');
            $table->string('gain_losses');
            $table->enum('status',[1,0])->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_recommendations');
    }
}
