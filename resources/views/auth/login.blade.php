@extends('layouts.app')
@section('content')
<?php

use App\Models\LogoEdit;
?>
@php $logodata = LogoEdit::first(); @endphp
<div class="container-xxl  position-relative bg-white d-flex p-0">
    <div class="container-fluid smalBgs" style=" background-image: url('{{asset('assets/img/bullsBgs.jpg')}}');">
        <div class="row row h-100 align-items-center justify-content-center">
            <!-- <div class="col-12 col-sm-8 d-none d-sm-block col-md-6 col-lg-5 col-xl-6">
                  <div class="rounded p-4 p-sm-5 my-4 mx-3">
                     <img class="img-fluid" src="img/adminImage.gif">
                      
                  </div>
               </div> -->
            <style>
                img.imgLlgst {
                    width: 100%;
                }
            </style>
            <div class="marginCsTop col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                <div class="bg-light  rounded p-4 p-sm-5 my-4 mx-3">
                    <div class=" align-items-center justify-content-between mb-3">
                        <center>
                            <img class="imgLlgst" src="{{asset('frontend/assets/images')}}/{{$logodata->header_logo}}">
                        </center>
                        <h3 class="text-primary text-center"> VEYE :: ADMIN PANEL </h3>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-floating mb-3 ">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <label for="floatingInput">Email address</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <label for="floatingPassword">Password</label>
                        </div>
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>

                            </div>
                            @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary py-3 w-100 mb-4">
                            {{ __('Login') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection