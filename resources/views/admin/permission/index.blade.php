@extends('layouts.admin')
@section('title') Manage Permission @endsection
@section('head')
    <link href="{{asset('assets/global/plugins/jstree/dist/themes/default/style.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Manage Permission
            <small>Sync Permission</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('roles.index')}}">Roles</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Permission-Manage</span></li>
            </ul>
            <div class="page-toolbar">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="row">
                    <marquee style="color:red;">Role :<b>{{$role->name}}</b></marquee>
                    <div id="panel" style="position:absolute;top:50%;left:50%;"></div>
                    <div class="col-md-12"><div id="jstree"></div></div>
                    <div class="col-md-12 form-group"><br><button class="btn btn-success" onclick="funSync()">Save</button></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('assets/global/plugins/jstree/dist/jstree.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
		$(function () {
                $('#jstree').jstree({
                    "plugins" : ["checkbox" ],
                    'core' : {
                        'data' : <?php echo json_encode($tree) ?>
                    }
                });
        });

        function funSync(){
			let selectedElmsIds=[];
            var selectedElms = $('#jstree').jstree("get_checked", true);
            $.each(selectedElms, function(a,b) {
				console.log(b);
                if((this.children).length == 0){
                    selectedElmsIds.push(this.id);
                }
            });
            $('#panel').html('<div class="row"><div class="col-md-12" style="color:red">Loading........</div></div>');
            $('#panel').show();
            $.ajax({
                url: '{{url("admin/permission")}}/{{$role->id}}',
                type: 'PATCH',
                data: {
                    _token:'{{csrf_token()}}',
                    permission:selectedElmsIds
                },
                cache: false,
                success: function (data) {
                    $('#panel').html('<div class="row"><div class="col-md-12" style="color:green">Success</div></div>');
                    $('#panel').hide(1000);
                    window.location='{{url("admin/home")}}';
                },
                error: function () {
                    $('#panel').hide(500);
                }
            });
        }
    </script>
@endsection

