@extends('layouts.admin')
@section('title') Add Report @endsection
@section('js')
<script>
    function makeSlug(_this) {
        let slug = (_this.value).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
        $('#slug').val(slug);
    }
</script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add New Report
        <small>Add New Report</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('allreports.index')}}">Reports</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('allreports.store')}}">
                @csrf
                <input type="hidden" name="rid" value="<?php echo @$_GET['rid']; ?>">
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" onchange="makeSlug(this)" placeholder="enter title here" value="{{old('title')}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Slug</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="slug" type="text" id="slug" placeholder="enter slug here" value="{{old('slug')}}">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report No.</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="report_no" type="text" id="report_no" placeholder="enter report no here" value="{{old('report_no')}}">
                        @if ($errors->has('report_no'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_no') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">ASX Code</label>
                    </div>
                    <div class="col-lg-8">
                        <select required="" name="asx_code[]" id="asx_code" multiple class="form-control selectpicker" data-live-search="true">
                            <option value="">Choose One</option>
                            @foreach($asxCodes as $data)
                            <option value="{{$data->asx_code}}">{{$data->asx_code }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('asx_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('asx_code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="report_id" id="report_id" style="text-align:right;">
                            <option value="">Choose One</option>
                            @foreach($reports as $data)
                            <option value="{{$data->id}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('report_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report Category</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="reportcategory" id="reportcategory" style="text-align:right;">
                            <option value="">Choose One</option>
                            @foreach($reportsCategory as $data)
                            <option value="{{$data->id}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('reportcategory'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reportcategory') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Sector</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control"  name="sector" id="reportcategory">
                            <option value="">Choose One</option>
                            @foreach($sector as $data)
                            <option value="{{$data->id}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('sector'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sector') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Upload Report </label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="attachment" type="file">
                        @if ($errors->has('attachment'))
                        <span class="help-block">
                            <strong>{{ $errors->first('attachment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report Thumbnail</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="thumbnail" type="file">
                        @if ($errors->has('thumbnail'))
                        <span class="help-block">
                            <strong>{{ $errors->first('thumbnail') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="schedule" type="date" placeholder="enter date here" value="{{old('schedule')}}">
                        @if ($errors->has('schedule'))
                        <span class="help-block">
                            <strong>{{ $errors->first('schedule') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Time</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="report_time" type="time" placeholder="enter time here" value="{{old('report_time')}}">
                        @if ($errors->has('report_time'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_time') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Recommendation</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" name="recommendation" id="recommendation" style="text-align:right;">
                            <option value="">Choose One</option>
                            @foreach($recommendationData as $data)
                            <option value="{{$data->id}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('recommendation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('recommendation') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Current Price</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="current_price" type="text" placeholder="Enter Current Price" value="{{old('current_price')}}">
                        @if ($errors->has('current_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('current_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report Content</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="reporthtml" id="description" class="form-control"></textarea>
                        @if ($errors->has('reporthtml'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reporthtml') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<script>
    $('select').selectpicker();
    CKEDITOR.replace('description');
</script>
<script>
    CKEDITOR.replace('description');
</script>
@endsection