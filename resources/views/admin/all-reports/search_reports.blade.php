@extends('layouts.admin')
@section('title') All Reports @endsection
@section('head')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{
            width: 100%!important;
        }
        .select2-search--dropdown .select2-search__field {
            width: 98%;
        }
        .table-scrollable{
            overflow-x: inherit !important;
            overflow-y: inherit !important;
        }
    </style>
@endsection
@section('js')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
    $('#example').DataTable();
});
    </script>
   
@endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> All Reports
            <small>All Reports</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li><span>Report</span></li>
            </ul>
        </div>
        <div class="row">
        <form action="" method="get">
<div class="col-lg-4 col-xs-4 col-sm-4">
<input type="text" name="title" class="form-control" placeholder="Enter Title" value="{{$title}}">
</div>
<div class="col-lg-4 col-xs-4 col-sm-4">
<input type="text" name="asx_code" class="form-control" placeholder="Enter ASX COde" value="{{$asx_code}}">
</div>
<div class="col-lg-2 col-xs-2 col-sm-2">
<button class="btn" type="submit">Search</button>
</div>
</form>
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="table-responsive">
					<table id="example" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Report Title</th>
                                <th>Description</th>
                                <th>thumbnail</th>
                                <th>ASX Codes</th>
                                <th>Recommendation</th>
                                <th>Current Price</th>
                                <th>Add Date</th>
                                <th>Updated Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                  
                        @php $i=0; @endphp
                        @foreach($users as $data)
                        @php $i++; @endphp
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $data->title }}</td>
                                <td>{{ $data->description }}</td>
                                <td><img src="{{ asset('uploads/reports/')}}{{ $data->thumbnail }}</" alt=""></td>
                                <td>{{ $data->asx_code }}</td>
                                <td>{{ $data->recommendation }}</td>
                                <td>{{ $data->current_price }}</td>
                                <td>{{ $data->created_at }}</td>
                                <td>{{ $data->updated_at }}</td>
                                 <td><a href="{{route('allreports.edit',$data->id)}}"><i class="fa fa-edit" style="font-size: 24px;"></i></a>
                               
                            </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="bd-example-snippet bd-code-snippet">
                    <div class="bd-example">
                        <nav aria-label="Another pagination example">
                            <ul class="pagination pagination-lg flex-wrap">
                                <li class="page-item disabled">
                                    {{ $users->links( "pagination::bootstrap-4") }}
                                    <!-- <a class="page-link"><i class="fas fa-long-arrow-alt-left"></i></a> -->
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
 
@endsection

