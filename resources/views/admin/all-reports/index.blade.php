@extends('layouts.admin')
@section('title') All Reports @endsection
@section('head')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{
            width: 100%!important;
        }
        .select2-search--dropdown .select2-search__field {
            width: 98%;
        }
        .table-scrollable{
            overflow-x: inherit !important;
            overflow-y: inherit !important;
        }
    </style>
@endsection
@section('js')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
    $('#example1').DataTable();
});
    </script>
    <!-- <script type="text/javascript">
		$(document).ready(function(){
			$('#dataTableExample').dataTable({
					"aLengthMenu": [
						[10, 30, 50, -1],
						[10, 30, 50, "All"]
					],
					"pagingType": "full_numbers",
					"iDisplayLength": 10,
					"language": {
						search: ""
					},
					"serverSide": true,
					"processing": true,
					"paging": true,
					"ajax":{
						url:'{{route("allreports.index")}}',
						data:function(e){

						}
					},
					'createdRow': function( row, data, dataIndex ) {
						$(row).attr('id', 'tr_'+data.id);
					},
					columns: [
						{ data: "sno" ,name: 'sno',searchable: false, sortable : false, visible:true},
						{ data: "title" ,name:"title"},
						{ data: "description" ,name:"description"},
                        { data: "thumbnail" ,name: 'thumbnail',searchable: false, sortable : false, visible:true},
						{ data: "asx_code" ,name:"asx_code"},
						{ data: "recommendation" ,name:"recommendation"},
						{ data: "current_price" ,name:"current_price"},
						{ data: "action" ,name: 'action',searchable: false, sortable : false, visible:true},
					],
                    "fnInitComplete": function (oSettings, json) {

                    }
				});



		});
    </script> -->
@endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> All Reports
            <small>All Reports</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li><span>Report</span></li>
            </ul>
            <div class="page-toolbar">
                <a class="btn btn-primary" href="{{route('allreports.create')}}?rid=<?php echo @$_GET['rid']; ?>">Add Report</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="table-responsive">
					<table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sl No</th>
								 <th>Report No</th>
                                <th>Report Title</th>
                                <th>Description</th>
                                <th>thumbnail</th>
                                <th>ASX Codes</th>
                                <th>Recommendation</th>
                                <th>Current Price</th>
                                <th>Add Date</th>
                                <th>Updated Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                  
                        @php $i=0; @endphp
                        @foreach($users as $data)
                        @php $i++; @endphp
                            <tr>
                                <td>{{ $i }}</td>
								<td>{{ $data->report_no }}</td>
                                <td>{{ $data->title }}</td>
                                <td>{{ $data->description }}</td>
                                <td><img src="{{ asset('/')}}{{ $data->thumbnail }}" style="max-width: 80px;" alt=""></td>
                                <td>{{ $data->asx_code }}</td>
                                <td>@if($data->recommendation==1) Sell @elseif($data->recommendation==2) Buy @endif</td>
                                <td>{{ $data->current_price }}</td>
                                <td>{{ $data->created_at }}</td>
                                <td>{{ $data->updated_at }}</td>
                                <td><a href="{{route('allreports.edit',$data->id)}}"><i class="fa fa-edit" style="font-size: 24px;"></i></a>
                                <a href="{{route('allreports.destroy',$data->id)}}" onclick="if (confirm('Are you sure to delete?')){return true;}else{event.stopPropagation(); event.preventDefault();};"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="bd-example-snippet bd-code-snippet">
                    <div class="bd-example">
                        <nav aria-label="Another pagination example">
                            <ul class="pagination pagination-lg flex-wrap">
                                {{--<li class="page-item disabled">
                                    {{ $users->links( "pagination::bootstrap-4") }}
                                    <!-- <a class="page-link"><i class="fas fa-long-arrow-alt-left"></i></a> -->
                                </li>--}}

                            </ul>
                        </nav>
                    </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>
 
@endsection

