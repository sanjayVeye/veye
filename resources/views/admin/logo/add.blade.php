@extends('layouts.admin')
@section('title') Add Logo @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Logo
        <small>Add Banner</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>AdminiLogo</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('logo.index')}}">Logo</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('logo.store')}}">
                @csrf
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Header Logo</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="header_logo" type="file">
                        @if ($errors->has('header_logo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('header_logo') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Footer Logo</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="footer_logo" type="file">
                        @if ($errors->has('footer_logo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('footer_logo') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection