@extends('layouts.admin')
@section('title') Edit Subscribe Page @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Subscribe Page
        <small>Edit Subscribe Page</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('subscriptions.index')}}">Subscribe Page</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('subscriptions.update',$subscriptions->id)}}">
                @csrf
                {{ method_field('PATCH') }}


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="Enter title here" value="{{$subscriptions->title}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">/offer/slug_path</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="slug" type="text" placeholder="Enter offer/slug path" value="{{$subscriptions->slug}}">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Reports</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="reports_id[]" id="" class="form-control" multiple data-live-search="true">
                            <option value="">Latest Report</option>
                            @foreach($reports as $data)
                            @php $reportsId = explode(",", $subscriptions->reports_id); @endphp

                            <option value="{{$data->id}}" <?php foreach ($reportsId as $v) { ?> <?php if ($data->id == $v) { ?> selected <?php } ?><?php } ?>>{{$data->title}}</option>

                            @endforeach
                        </select>
                        @if ($errors->has('reports_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reports_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Subscribe Box Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="subscribe_title" type="text" placeholder="Enter Title (Ex. : YEARLY)..." value="{{$subscriptions->subscribe_title}}">
                        @if ($errors->has('subscribe_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('subscribe_title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Yearly Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="yearly_description" id="" class="form-control">{{$subscriptions->yearly_description}}</textarea>
                        @if ($errors->has('yearly_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('yearly_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Plan Information</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="plan_information" type="text" placeholder="Ex. : $60/Month" value="{{$subscriptions->plan_information}}">
                        @if ($errors->has('plan_information'))
                        <span class="help-block">
                            <strong>{{ $errors->first('plan_information') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Offer Price</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="offer_price" type="text" placeholder="Enter Offer Price" value="{{$subscriptions->offer_price}}">
                        @if ($errors->has('offer_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('offer_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Plan Price Yearly ( please enter here price, which program will send to stripe payment gateway. )</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="plan_price" type="number" placeholder="For Ex. : 900" value="{{$subscriptions->plan_price}}">
                        @if ($errors->has('plan_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('plan_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Sale Information</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="sale_information" type="text" placeholder="Ex. : $60/Month" value="{{$subscriptions->sale_information}}">
                        @if ($errors->has('sale_information'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sale_information') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Sale Cut Price</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="plan_cut_price" type="text" placeholder="Enter Sale Cut Price" value="{{$subscriptions->plan_cut_price}}">
                        @if ($errors->has('plan_cut_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('plan_cut_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>



                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Sale Price</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="sale_price" type="text" placeholder="Enter Sale Price" value="{{$subscriptions->sale_price}}">
                        @if ($errors->has('sale_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sale_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Red Strap Text</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="red_strap_text" type="text" placeholder="Please Enter Red Strap Special Text Ex.: You Save 20% .." value="{{$subscriptions->red_strap_text}}">
                        @if ($errors->has('red_strap_text'))
                        <span class="help-block">
                            <strong>{{ $errors->first('red_strap_text') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Report Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="report_type" id="" class="form-control">
                            <option value="1" @if($subscriptions->report_type=='1') selected @endif>Active</option>
                            <option value="0" @if($subscriptions->report_type=='0') selected @endif>In active</option>
                        </select>
                        @if ($errors->has('report_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Display Order</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="display_order" type="text" placeholder="Enter Display Order" value="{{$subscriptions->display_order}}">
                        @if ($errors->has('display_order'))
                        <span class="help-block">
                            <strong>{{ $errors->first('display_order') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Offer Start Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="offer_start_date" type="datetime-local" placeholder="offer-end-date Order" value="{{$subscriptions->offer_start}}">
                        @if ($errors->has('offer_start_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('offer_start_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Offer End Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="offer_end_date" type="datetime-local" placeholder="offer-end-date Order" value="{{$subscriptions->offer_end}}">
                        @if ($errors->has('offer_end_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('offer_end_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Icon Image</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="icon_image" type="file">
                        @if ($errors->has('icon_image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('icon_image') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Upload Image</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="image" type="file">
                        @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Plan Content</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="description" id="description" class="form-control" placeholder="Plan Content">{{$subscriptions->description}}</textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<script>
    $('select').selectpicker();
</script>
<script>
    $('select').selectpicker();
    CKEDITOR.replace('description');
</script>
<script>
    CKEDITOR.replace('description');
</script>
@endsection