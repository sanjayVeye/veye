@extends('layouts.admin')
@section('title') Edit Menues @endsection
@section('js')
<script>
    function makeSlug(_this) {
        let slug = (_this.value).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
        $('#slug').val(slug);
    }
</script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Menues
        <small>Edit Menues</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="{{route('menues.index')}}">Menues</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{ route('menues.update', $menu->id) }}">
                @csrf
                {{method_field('PATCH')}}
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="myTitle" name="title" onchange="makeSlug(this)" placeholder="Enter title here" value="{{ $menu->title }}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Slug</label>
                    </div>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug here" value="{{ $menu->slug }}">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Parent</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="parent" class="form-control">
                            <option value="0">-Parent-</option>
                            @foreach($parents as $rows)
                            @php
                            $sel=$menu->parent==$rows->id ? 'selected':'';
                            @endphp
                            <option value="{{ $rows->id }}" {{$sel}}>{{ $rows->title }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('parent'))
                        <span class="help-block">
                            <strong>{{ $errors->first('parent') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Order</label>
                    </div>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="order" placeholder="Enter order here" value="{{ $menu->order }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Title</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title">{{ $menu->meta_title }}</textarea>
                        @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea class="form-control" id="meta" name="meta" placeholder="Meta Tags">{{ $menu->meta }}</textarea>
                        @if ($errors->has('meta'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Descriptions</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea class="form-control" id="meta_desc" name="meta_desc" placeholder="Meta Desc Tags">{{ $menu->meta_desc }}</textarea>
                        @if ($errors->has('meta_desc'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_desc') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection