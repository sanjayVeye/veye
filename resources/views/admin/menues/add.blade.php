@extends('layouts.admin')
@section('title') Add Menu @endsection
@section('js')
    <script>
        function makeSlug(_this){
            let slug=(_this.value).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
            $('#slug').val(slug);
        }
    </script>
@endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Add Menues
            <small>Add Menues</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{route('menues.index')}}">Menues</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Add</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{ route('menues.store') }}">
					@csrf
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Title</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="myTitle" name="title" onchange="makeSlug(this)" placeholder="Enter title here" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Slug</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug here" value="{{ old('slug') }}">
                            @if ($errors->has('slug'))
                                <span class="help-block">
                                <strong>{{ $errors->first('slug') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Parent</label>
                        </div>
                        <div class="col-lg-8">
                            <select name="parent"  class="form-control">
                                <option value="0">-Parent-</option>
                                @foreach($menues as $rows)
                                    @php
                                        $sel=old('parent')==$rows->id ? 'selected':'';
                                    @endphp
                                    <option value="{{ $rows->id }}" {{$sel}}>{{ $rows->title }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('parent'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('parent') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Style</label>
                        </div>
                        
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Order</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="order" placeholder="Enter order here" value="{{ old('order') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Meta</label>
                        </div>
                        <div class="col-lg-8">
                            <textarea  class="form-control" id="meta" name="meta" placeholder="Meta Tags">{{ old('meta') }}</textarea>
                            @if ($errors->has('meta'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('meta') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Meta Descriptions</label>
                        </div>
                        <div class="col-lg-8">
                            <textarea  class="form-control" id="meta_desc" name="meta_desc" placeholder="Meta Desc Tags" >{{ old('meta_desc') }}</textarea>
                            @if ($errors->has('meta_desc'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('meta_desc') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Save</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

