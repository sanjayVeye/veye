@extends('layouts.admin')
@section('title') Add Subscribe Page @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Add Subscribe Page 
            <small>Add Subscribe Page </small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('subscriptions.index')}}">Company</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Add</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('subscriptions.store')}}">
					@csrf
                    
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Title</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="title" type="text" placeholder="Enter title here" value="{{old('title')}}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Select Reports</label>
                        </div>
                        <div class="col-lg-8">
                           <select name="reports_id" id="" class="form-control">
                            <option value="">Latest Report</option>
                            @foreach($reports as $data)
                                    <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                           </select>
                            @if ($errors->has('reports_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('reports_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Subscribe Box Title</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="subscribe_title" type="text" placeholder="Enter Title (Ex. : YEARLY)..." value="{{old('subscribe_title')}}">
                            @if ($errors->has('subscribe_title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('subscribe_title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Yearly Description</label>
                        </div>
                        <div class="col-lg-8">
                            <textarea name="yearly_description" id="" class="form-control"></textarea>
                            @if ($errors->has('yearly_description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('yearly_description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Red Strap Text</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="red_strap_text" type="text" placeholder="Please Enter Red Strap Special Text Ex.: You Save 20% .." value="{{old('red_strap_text')}}">
                            @if ($errors->has('red_strap_text'))
                                <span class="help-block">
                                <strong>{{ $errors->first('red_strap_text') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Plan Information</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="plan_information" type="text" placeholder="Ex. : $60/Month" value="{{old('plan_information')}}">
                            @if ($errors->has('plan_information'))
                                <span class="help-block">
                                <strong>{{ $errors->first('plan_information') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Plan Price Yearly   ( please enter here price, which program will send to stripe payment gateway. )</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="plan_price" type="number" placeholder="For Ex. : 900" value="{{old('plan_price')}}">
                            @if ($errors->has('plan_price'))
                                <span class="help-block">
                                <strong>{{ $errors->first('plan_price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Sale Price</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="sale_price" type="text" placeholder="Enter Sale Price" value="{{old('sale_price')}}">
                            @if ($errors->has('sale_price'))
                                <span class="help-block">
                                <strong>{{ $errors->first('sale_price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Select Report Type</label>
                        </div>
                        <div class="col-lg-8">
                           <select name="report_type" id="" class="form-control">
                            <option value="">Latest Report</option>
                            @foreach($reports as $data)
                                    <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                           </select>
                            @if ($errors->has('report_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('report_type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Display Order</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="display_order" type="text" placeholder="Enter Display Order" value="{{old('display_order')}}">
                            @if ($errors->has('display_order'))
                                <span class="help-block">
                                <strong>{{ $errors->first('display_order') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                 
                  				
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

