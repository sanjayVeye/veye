@extends('layouts.admin')
@section('title') View Watchlist @endsection
@section('head')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{
            width: 100%!important;
        }
        .select2-search--dropdown .select2-search__field {
            width: 98%;
        }
        .table-scrollable{
            overflow-x: inherit !important;
            overflow-y: inherit !important;
        }
    </style>
@endsection
@section('js')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
		$(document).ready(function(){
			$('#dataTableExample').dataTable({
					"aLengthMenu": [
						[10, 30, 50, -1],
						[10, 30, 50, "All"]
					],
					// "pagingType": "full_numbers",
					"iDisplayLength": 10,
					"language": {
						search: ""
					},
					"serverSide": true,
					"processing": true,
					"paging": true,
					"ajax":{
						url:'{{route("watchlist.index")}}',
						data:function(e){

						}
					},
					'createdRow': function( row, data, dataIndex ) {
						$(row).attr('id', 'tr_'+data.id);
					},
					columns: [
						{ data: "sno" ,name: 'sno',searchable: false, sortable : false, visible:true},
						{ data: "user_id" ,name:"user_id"},
						{ data: "company" ,name:"company"},
						{ data: "short_code" ,name:"short_code"},
						{ data: "buy_qty" ,name:"buy_qty"},
						{ data: "buy_pr" ,name:"buy_pr"},
						{ data: "buy_date" ,name:"buy_date"},
						{ data: "total_value_of_invest" ,name:"total_value_of_invest"},
						{ data: "curr_mkt_pr" ,name:"curr_mkt_pr"},
						{ data: "total_mkt_invest" ,name:"total_mkt_invest"},
						{ data: "amount" ,name:"amount"},
					],
                    "fnInitComplete": function (oSettings, json) {

                    }
				});



		});
    </script>
@endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> View Watchlist
            <small>View Watchlist</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li><span>View Watchlist</span></li>
            </ul>
            <!-- <div class="page-toolbar">
                <a class="btn btn-primary" href="{{route('subscriptions.create')}}"><i class="fa fa-plus me-2"></i> Add View Watchlist</a>
            </div> -->
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div>
					<table id="dataTableExample" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>User</th>
                                <th>Company</th>
                                <th>Short Code</th>
                                <th>Buy Qty</th>
                                <th>Buy Pr</th>
                                <th>Buy Date</th>
                                <th>Total Value of Invest</th>
                                <th>Curr. Mkt Pr</th>
                                <th>Total Mkt Invest</th>
                                <th>P & L in Amt/ %</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

