<option value="">--select--</option>
@foreach($states as $row)
    @php $sel=($row->StateCode==$selected) ? 'selected' : '' ;@endphp
    <option value="{{$row->StateCode}}" {{$sel}}>{{$row->StateName}}</option>
@endforeach