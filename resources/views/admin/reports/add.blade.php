@extends('layouts.admin')
@section('title') Add reports @endsection
@section('js')
    <script>
        function makeSlug(_this){
            let slug=(_this.value).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
            $('#slug').val(slug);
        }
    </script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Report
        <small>Add Report</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('reports.index')}}">Reports</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('reports.store')}}">
                @csrf
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <!-- <input class="form-control" name="title" type="text" placeholder="enter title here" value="{{old('title')}}"> -->
                        <input type="text" class="form-control" id="myTitle" name="title" onchange="makeSlug(this)" placeholder="Enter title here" value="{{ old('title') }}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Slug</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug here" value="{{ old('slug') }}">
                            @if ($errors->has('slug'))
                                <span class="help-block">
                                <strong>{{ $errors->first('slug') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Key</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="unique_key" type="text" placeholder="enter key here" value="{{old('unique_key')}}">
                        @if ($errors->has('unique_key'))
                        <span class="help-block">
                            <strong>{{ $errors->first('unique_key') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Upload Image</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="image" type="file">
                        @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Mailchimp_id</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="mailchimp_id" type="text" placeholder="Enter mailchimp_id" value="{{old('mailchimp_id')}}">
                        @if ($errors->has('mailchimp_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mailchimp_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label"> Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="description" id="" class="form-control" placeholder=" Description"></textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="meta_title" type="text" placeholder="Enter meta title" value="{{old('meta_title')}}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Keywords</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_keywords" id="" class="form-control" placeholder="Meta Keywords"></textarea>
                        @if ($errors->has('meta_keywords'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_keywords') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_description" id="" class="form-control" placeholder="Meta Description"></textarea>
                        @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection