@extends('layouts.admin')
@section('title') Add New Company @endsection
@section('content')

<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add New Company
        <small>Add New Company </small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('companies.index')}}">Company</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('companies.store')}}">
                @csrf

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="Enter title here" value="{{old('title')}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">ASX Code</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="asx_code" type="text" placeholder="Enter asx code here" value="{{old('asx_code')}}">
                        @if ($errors->has('asx_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('asx_code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">ASX Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="asx_type_id" id="" class="form-control">
                            <option value="">Select ASX Type</option>
                            @foreach($asxType as $data)
                            <option value="{{$data->title}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('asx_type_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('asx_type_id') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Report Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="report_type[]" id="" class="form-control selectpicker" multiple data-live-search="true">
                            <option value="">Latest Report</option>
                            @foreach($reports as $data)
                            <option value="{{$data->id}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('report_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Company Sector</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="sector_id" id="" class="form-control">
                            <option value="">Select Sector</option>
                            @foreach($sectors as $data)
                            <option value="{{$data->id}}">{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('sector_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sector_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> -->
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Company Bio</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="description" id="description" class="form-control"></textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<script>
    $('select').selectpicker();
    CKEDITOR.replace( 'description' );
</script>
@endsection