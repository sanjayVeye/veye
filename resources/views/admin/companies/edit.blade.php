@extends('layouts.admin')
@section('title') Edit Company @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Client
        <small>Edit Client</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('clients.index')}}">Clients</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('companies.update',$companies->id)}}">
                @csrf
                {{ method_field('PATCH') }}

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="Enter title here" value="{{$companies->title}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">ASX Code</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="asx_code" type="text" placeholder="Enter asx code here" value="{{$companies->asx_code}}">
                        @if ($errors->has('asx_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('asx_code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">ASX Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="asx_type_id" id="" class="form-control">
                            <option value="">Select ASX Type</option>
                            @foreach($asxType as $data)
                            <option value="{{$data->title}}" @if($data->title==$companies->code_type) selected @endif>{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('asx_type_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('asx_type_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Report Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="report_type" id="" class="form-control">
                            <option value="">Latest Report</option>
                            @foreach($reports as $data)
                            <option value="{{$data->id}}" @if($data->id==$companies->report_type) selected @endif>{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('report_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Company Sector</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="sector_id" id="" class="form-control">
                            <option value="">Select Sector</option>
                            @foreach($sectors as $data)
                            <option value="{{$data->id}}" @if($data->id==$companies->sector_id) selected @endif>{{$data->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('sector_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sector_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> -->
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Company Bio</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="description" id="description" class="form-control">{{$companies->description}}</textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection