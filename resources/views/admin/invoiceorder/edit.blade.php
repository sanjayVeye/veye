@extends('layouts.admin')
@section('title') Edit Invoice @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Invoice
        <small>Edit Invoice</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('faq.index')}}">Invoice</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('invoiceorder.update',$data->id)}}">
                @csrf
                {{ method_field('PATCH') }}

                <input type="hidden" name="tag" value="invoiceorder">
                <input type="hidden" name="order_id" value="{{$data->id}}">
                <div class="row">
                    <div class="col-md-6">
                        <h3><strong>From</strong></h3>
                        <p>VEYE PTY LTD <br>
                            ABN: 58 623 120 865 <br>
                            Level 21, 207 Kent Street <br>
                            Sydney NSW <br>
                            Australia</p>
                    </div>
                    <div class="col-md-6">
                        <h3><strong>To,</strong></h3>

                        <div class="form-group formMtb">
                            <input type="text" class="form-control" id="order_receiver_name" name="order_receiver_name" value="{{$data->order_receiver_name}}" placeholder="Customer Name">
                        </div>
                        <div class="form-group formMtb">
                            <input type="text" class="form-control" id="Cust_email" name="Cust_email" value="{{$data->Cust_email}}" placeholder="Email ID">
                        </div>
                        <div class="form-group formMtb">
                            <input type="text" class="form-control" id="post_code" name="post_code" value="{{$data->post_code}}" placeholder="Post Code">
                        </div>
                        <div class="form-group formMtb">
                            <input type="text" class="form-control" id="phone" name="phone" value="{{$data->phone}}" placeholder="Contact Number">
                        </div>

                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="table-responsive">
                            <table class="table  table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">
                                            <input type="checkbox" name="">
                                        </th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Subs_start</th>
                                        <th scope="col">Subs_end</th>
                                        <th scope="col">Month</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody id="invoiceItem">
                                    @foreach($invoceOrder as $v)
                                    <input type="hidden" name="order_item_id[]" value="{{$v->id}}">
                                    <tr>
                                        <th scope="row"><input type="checkbox" name=""></th>
                                        <td>
                                            <select class="form-control" name="productName[]" autocomplete="off">
                                                <option value="">Select a Plan</option>
                                                @foreach($subscriptions as $subs)
                                                <option value="{{$subs->title}}" @if($subs->title=$v->item_name) selected @endif>{{$subs->title}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input class="form-control" type="date" name="subsstartdate[]" value="{{$v->subsstartdate}}" id="subsstartdate_0">
                                        </td>
                                        <td>
                                            <input class="form-control" type="date" name="subsenddate[]" value="{{$v->subsenddate}}" id="subsenddate_0">
                                        </td>
                                        <td> <input class="form-control" type="text" name="month[]" id="month_0" value="{{$v->month}}"> </td>
                                        <td> <input class="form-control" type="text" name="quantity[]" id="quantity_0" value="{{$v->order_item_quantity}}"> </td>
                                        <td> <input class="form-control" type="text" name="price[]" id="price_0" value="{{$v->order_item_price}}"> </td>
                                        <td> <input class="form-control" type="text" name="" id="total_0" value="{{$v->order_item_final_amount}}" disabled> </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>


                        </div>
                        <button type="button" class="btn btn-danger btn-sm" id="removeRows"><i class="fa fa-minus" aria-hidden="true"></i> DELETE</button>
                        <button type="button" class="btn btn-primary btn-sm" id="addRows"><i class="fa fa-plus" aria-hidden="true"></i> ADD MORE</button>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-8">

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="inputEmail4">Payment Type:</label>

                                <select name="mop" class="form-control" id="mop">
                                    <option value="By Card" @if($data->mop='By Card') selected @endif>By Card</option>
                                    <option value="Online Payment" @if($data->mop='Online Payment') selected @endif>Online Payment</option>
                                    <option value="Pay Pal" @if($data->mop='Pay Pal') selected @endif>Pay Pal</option>
                                    <option value="Google Pay" @if($data->mop='Google Pay') selected @endif>Google Pay</option>
                                    <option value="NEFT Bank Transfer" @if($data->mop='NEFT Bank Transfer') selected @endif>NEFT Bank Transfer</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">Payment Mop Details</label>
                                <input type="text" class="form-control" id="card" name="card" value="{{$data->card}}" placeholder="Payment Mop Details">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">Card Last Digit</label>
                                <input type="number" class="form-control" id="digit" name="digit" value="{{$data->digit}}" placeholder="Card Last Digit">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="inputEmail4">Subscriptions</label>
                                <select class="form-control" name="subscription" id="subscription">
                                    <option value="Select subscription">Select subscription</option>
                                    <option value="Normal" @if($data->mop='Normal') selected @endif>Normal</option>
                                    <option value="Premium" @if($data->mop='Premium') selected @endif>Premium</option>
                                    <option value="PS" @if($data->mop='PS') selected @endif>PS</option>
                                    <option value="HS" @if($data->mop='HS') selected @endif>HS</option>
                                    <option value="Mog" @if($data->mop='Mog') selected @endif>Mog</option>
                                    <option value="DIR" @if($data->mop='DIR') selected @endif>DIR</option>
                                    <option value="SOW" @if($data->mop='SOW') selected @endif>SOW</option>
                                    <option value="USR" @if($data->mop='USR') selected @endif>USR</option>
                                    <option value="PS+HSR" @if($data->mop='PS+HSR') selected @endif>PS+HSR</option>
                                    <option value="HP" @if($data->mop='HP') selected @endif>HP</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">Subscription Start Date</label>
                                <input type="date" class="form-control" id="subsstartdate" name="subsstartdate" value="{{$data->subsstartdate}}" placeholder="Subscription Start Date">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">Subscription End Date</label>
                                <input type="date" class="form-control" id="subsenddate" name="subsenddate" value="{{$data->subsenddate}}" placeholder="End Date">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Months</label>
                                <select class="form-control" name="months" id="months">
                                    <option value="Select Month">Select Month</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputPassword4">Notes</label>
                                <textarea class="form-control" name="note" value="{{$data->note}}" placeholder="Your Notes"></textarea>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-lg-8 offset-lg-3">
                                <button type="submit" class="btn btn-primary mr-2">Update</button>
                                <button class="btn btn-light" type="reset">Reset</button>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="formBoxCss">

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>Subtotal:</label>
                                    <div class="input-group input-group-multi">
                                        <div class="input-group-addon">$</div>
                                        <div class="col-md-12"><input type="text" name="order_total_before_tax" value="{{$data->order_total_before_tax}}" id="totalAftertax" disabled class="form-control lessRadius"></div>
                                        <div class="input-group-addon">$</div>
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                    <label>GST Rate:</label>
                                    <div class="input-group input-group-multi">
                                        <div class="col-md-12"><input type="text" name="taxRate" id="taxRate" disabled class="form-control lessRadius" value="11"></div>
                                        <div class="input-group-addon">%</div>
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                    <label>Tax Amount:</label>
                                    <div class="input-group input-group-multi">
                                        <div class="input-group-addon">$</div>
                                        <div class="col-md-12"><input type="text" name="order_total_tax" id="taxAmount" value="{{$data->order_total_tax}}" disabled class="form-control lessRadius"></div>
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                    <label>Grand Total:</label>
                                    <div class="input-group input-group-multi">
                                        <div class="input-group-addon">$</div>
                                        <div class="col-md-12"><input type="text" value="{{$data->order_total_after_tax}}" name="order_total_after_tax" id="subTotal" disabled class="form-control lessRadius"></div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- OWN DESIGN END HERE -->

            </form>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('description');
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
	$(document).on('click', '#checkAll', function() {          	
		$(".itemRow").prop("checked", this.checked);
	});	
	$(document).on('click', '.itemRow', function() {  	
		if ($('.itemRow:checked').length == $('.itemRow').length) {
			$('#checkAll').prop('checked', true);
		} else {
			$('#checkAll').prop('checked', false);
		}
	});  
	var count = $(".itemRow").length;
	$(document).on('click', '#addRows', function() { 
        
		count++;
		var htmlRows = '';
		htmlRows += '<tr>';
		htmlRows += '<td><input class="itemRow" type="checkbox"></td>';          
		htmlRows += '<td><Select name="productName[]" id="productName_'+count+'" class="form-control" autocomplete="off">';

        htmlRows += '<option value="">Select a Plans</option>';
        
         @foreach($subscriptions as $subs)
            htmlRows += '<option value="{{$subs->title}}">{{$subs->title}}</option>';
        @endforeach
       
        htmlRows += '</select></td>';
		htmlRows += '<td><input type="date" name="subsstartdate[]" id="subsstartdate_'+count+'" class="form-control subsstartdate" autocomplete="off"></td>'; 	 
		htmlRows += '<td><input type="date" name="subsenddate[]" id="subsenddate_'+count+'" class="form-control subsenddate" autocomplete="off"></td>';  
		htmlRows += '<td><input type="number" name="month[]" id="month_'+count+'" class="form-control month" autocomplete="off" ></td>';
		htmlRows += '<td><input name="quantity[]" id="quantity_'+count+'" class="form-control quantity" autocomplete="off" ></td>';  
		htmlRows += '<td><input  name="price[]" id="price_'+count+'" class="form-control price" autocomplete="off"></td>';		 
		htmlRows += '<td><input  name="total[]" id="total_'+count+'" class="form-control total" autocomplete="off" disabled></td>';          
		htmlRows += '</tr>';
		$('#invoiceItem').append(htmlRows);
	}); 
	$(document).on('click', '#removeRows', function(){
		$(".itemRow:checked").each(function() {
			$(this).closest('tr').remove();
		});
		$('#checkAll').prop('checked', false);
		calculateTotal();
	});		
	$(document).on('blur', "[id^=quantity_]", function(){
		calculateTotal();
	});	
	$(document).on('blur', "[id^=price_]", function(){
		calculateTotal();
	});	
	$(document).on('blur', "#taxRate", function(){		
		calculateTotal();
	});	
	$(document).on('blur', "#totalAftertax", function(){
	calculateTotal();
	});	
	$(document).on('click', '.deleteInvoice', function(){
		var id = $(this).attr("id");
		if(confirm("Are you sure you want to remove this?")){
			$.ajax({
				url:"action.php",
				method:"POST",
				dataType: "json",
				data:{id:id, action:'delete_invoice'},				
				success:function(response) {
					if(response.status == 1) {
						$('#'+id).closest("tr").remove();
					}
				}
			});
		} else {
			return false;
		}
	});
});	
function calculateTotal(){
	var totalAmount = 0; 
	$("[id^='price_']").each(function() {
		var id = $(this).attr('id');
		id = id.replace("price_",'');
		var price = $('#price_'+id).val();
		var quantity  = $('#quantity_'+id).val();
		if(!quantity) {
			quantity = 1;
		}
		var total = price*quantity;
		$('#total_'+id).val(parseFloat(total));
		totalAmount += total;			
	});
	$('#subTotal').val(parseFloat(totalAmount).toFixed(2));



	
	var taxRate = $("#taxRate").val();
	var subTotal = $('#subTotal').val();	
	if(subTotal) {
		var taxAmount = (subTotal/11).toFixed(2);
		$('#taxAmount').val(taxAmount);
		subTotal = parseFloat(subTotal).toFixed(2)-parseFloat(taxAmount).toFixed(2);
		$('#totalAftertax').val(subTotal);		
		
	}
}
    </script>
@endsection