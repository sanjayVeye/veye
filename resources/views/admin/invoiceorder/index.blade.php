@extends('layouts.admin')
@section('title') Invoice @endsection
@section('head')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-search--dropdown .select2-search__field {
        width: 98%;
    }
</style>
@endsection
@section('js')
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTableExample').dataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            order: [
                [1, 'asc']
            ],
            // "pagingType": "full_numbers",
            "iDisplayLength": 10,
            "language": {
                search: ""
            },
            "serverSide": true,
            "processing": true,
            "paging": true,
            "ajax": {
                url: '{{route("invoiceorder.index")}}?unpaid_order={{@$_GET["unpaid_order"]}}',
                data: function(e) {

                }
            },
            'createdRow': function(row, data, dataIndex) {
                $(row).attr('id', 'tr_' + data.id);
            },
            columns: [{
                    data: "id",
                    name: "chk",
                    searchable: false,
                    sortable: false,
                    visible: true,
                    fnCreatedCell: function(nTd, sData, oData, iRow, iCol) {
                        console.log(oData);
                        $(nTd).html("<input type='checkbox' name='chk[]' class='chk' value=" + oData.id + " />");
                    }
                },
                {
                    data: "sno",
                    name: 'sno',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "username",
                    name: "Invoice No."
                },
                {
                    data: "order_date",
                    name: "Create Date"
                },
                {
                    data: "user",
                    name: "Creator Name"
                },
                {
                    data: "order_receiver_name",
                    name: "Customer Name"
                },
                {
                    data: "subscription",
                    name: "Subscripiton"
                },

                {
                    data: "months",
                    name: "Months"
                },
                {
                    data: "order_total_before_tax",
                    name: "Invoice Total"
                },

                // { data: "months" ,name:"Amount Monthwise"},
                {
                    data: "month",
                    name: 'Amount Monthwise',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                // {
                //     data: "print",
                //     name: 'Print',
                //     searchable: false,
                //     sortable: false,
                //     visible: true
                // },
                {
                    data: "downloadpdf",
                    name: 'Download PDF',
                    searchable: false,
                    sortable: false,
                    visible: true
                },

                {
                    data: "id",
                    name: "Old_Print",

                    fnCreatedCell: function(nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<a href='/oldprintinvoice/" + oData.id + "'><i class='fa fa-print' aria-hidden='true'></i></a>");
                    }

                },
                {
                    data: "unpaidinvoice",
                    name: "Unpaid Invoice",
                    searchable: false,
                    sortable: true,
                    visible: true
                },

                {
                    data: "unpaid",
                    name: "Send to Email",

                    fnCreatedCell: function(nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<a href='/emailprintinvoice/" + oData.id + "'><i class='fa fa-envelope' aria-hidden='true'></i></a>");
                    }
                },
                {
                    data: "note",
                    name: "Remarks"
                },


                {
                    data: "action",
                    name: 'action',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
            ],
            "fnInitComplete": function(oSettings, json) {

            }
        });

        $('#checkall').on('click', function() {
            $('[name="chk[]').not(this).prop('checked', this.checked);
        });

        $('#unpaid').on('click', function() {

            var sThisVal = new Array();

            $('input.chk[type=checkbox]').each(function(i, val) {
                sThisVal[i] = (this.checked ? $(this).val() : "");

                console.log($(this).val());

            });

            $.ajax({
                url: '{{route("invoiceorder.togglepaid")}}?order="paid"',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sThisVal": sThisVal,
                    "unpaid": "unpaid"
                },
                type: 'post',

                success: function(returnedData) {
                    $('[name="chk[]').prop('checked', false);
                    $('#checkall').prop('checked', false);
                    alert("Data updated successfully!");
                }
            });

        });


        $('#paid').on('click', function() {


            var sThisVal = new Array();

            $('input.chk[type=checkbox]').each(function(i, val) {
                sThisVal[i] = (this.checked ? $(this).val() : "");

                console.log($(this).val());

            });

            $.ajax({
                url: '{{route("invoiceorder.togglepaid")}}?order="paid"',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sThisVal": sThisVal,
                    "unpaid": "paid"
                },
                type: 'post',

                success: function(returnedData) {
                    console.log(returnedData);
                    $('[name="chk[]').prop('checked', false);
                    $('#checkall').prop('checked', false);
                    alert("Data updated successfully!");
                }
            });

        });


    });
</script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> FAQ
        <small>Invoice</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li><span>Invoice Order</span></li>
        </ul>
        <div class="page-toolbar">
            <a class="btn btn-primary" href="{{route('invoiceorder.create')}}"><i class="fa fa-plus me-2"></i> Add Invoice</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">

            <span class="btn btn-primary" id="unpaid"><span class="glyphicon glyphicon-sort"></span> Unpaid Invoice</span>

            <span class="btn btn-primary" id="paid"><span class="glyphicon glyphicon-sort"></span> Paid Invoice</span>


            <table id="dataTableExample" class="table table-bordered table-striped table-responsive" width="100%">
                <thead>
                    <tr>

                        <th><input type="checkbox" id="checkall" clsss="chkall" title="Select all" /></th>
                        <th>Sl No</th>
                        <th>Invoice No.</th>
                        <th>Create Date</th>
                        <th>Creator Name</th>
                        <th>Customer Name</th>
                        <th>Subscripiton</th>
                        <th>Months</th>
                        <th>Invoice Total</th>
                        <th>Amount Monthwise</th>
                        <!-- <th>Print</th> -->
                        <th>Download PDF</th>
                        <th>Old_Print</th>
                        <th>Unpaid Invoice</th>
                        <th>Send to Email</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    </tr>
                    </tr>
                </thead>
            </table>

        </div>
    </div>
</div>
@endsection