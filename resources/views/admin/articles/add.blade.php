@extends('layouts.admin')
@section('title') Add Article @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Add Article
            <small>Add Article</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('article.index')}}">Article</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Add</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('article.store')}}">
					@csrf
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Article Title</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="title" type="text" placeholder="Enter title here" value="{{old('title')}}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">ASX Code</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="asx_code" type="text" placeholder="Enter ASX Code" value="{{old('asx_code')}}">
                            @if ($errors->has('asx_code'))
                                <span class="help-block">
                                <strong>{{ $errors->first('asx_code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Upload Article</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="article_img" type="file">
                            @if ($errors->has('article_img'))
                                <span class="help-block">
                                <strong>{{ $errors->first('article_img') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Article Thumbnail</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="thumbnail_img" type="file">
                            @if ($errors->has('thumbnail_img'))
                                <span class="help-block">
                                <strong>{{ $errors->first('thumbnail_img') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                   
						
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

