@extends('layouts.admin')
@section('title') Edit Send Mail @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Send Mail
            <small>Edit Send Mail</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('daytodaymail.index')}}">Send Mail</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('sendmail.update',$sendmail->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}                 

                    <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select User Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="user_type" id="mail-content">
                            <option value="">Choose One</option>
                            <option value="Guest" @if($sendmail->user_type=='Guest') selected @endif>Guest</option>
                            <option value="Old User" @if($sendmail->user_type=='Old User') selected @endif>Old User</option>
                        </select>
                        @if ($errors->has('user_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('user_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Email</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="email" type="text" placeholder="Enter Email Id" value="{{$sendmail->email}}">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Amount</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="amount" type="text" placeholder="Enter Amount" value="{{$sendmail->amount}}">
                        @if ($errors->has('amount'))
                        <span class="help-block">
                            <strong>{{ $errors->first('amount') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Product Name</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="plan" id="plan" style="text-align:right;">
                            <option value="Dividend investor Stock of the Week Report" @if($sendmail->plan=='Dividend investor Stock of the Week Report') selected @endif> Dividend investor &amp; Stock of the Week Report </option>
                            <option value="Premium Service" @if($sendmail->plan=='Premium Service') selected @endif> Premium Service </option>
                            <option value="Masters of Growth" @if($sendmail->plan=='Masters of Growth') selected @endif> Masters of Growth </option>
                            <option value="Penny Stocks Report" @if($sendmail->plan=='Penny Stocks Report') selected @endif> Penny Stocks Report </option>
                            <option value="Hot Sector Report" @if($sendmail->plan=='Hot Sector Report') selected @endif> Hot Sector Report </option>
                            <option value="Dividend Investor Report" @if($sendmail->plan=='Dividend Investor Report') selected @endif> Dividend Investor Report </option>
                        </select>
                        @if ($errors->has('plan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('plan') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                    
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

