<!-- Modal Service -->
<div id="service" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Comment</h4>
            </div>
            <form action="{{route('client-credentials.editProduct')}}" method="post" id="comment_form">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="client_detail_id" name="client_detail_id">
                    <div class="row form-group">
                        <div class="col-md-12">
                            Comment
                            <textarea name="comment" id="comments" class="form-control"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Send MAil -->
<div id="sendmail" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Invoice Send</h4>
            </div>
            <form action="{{route('invoice.sendInvoice')}}" method="post" id="comment_form">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="client_dtl_id" name="client_id">
                    <div class="row form-group">


                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="mail_type" value="normal" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                Normal
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="mail_type" value="premium" id="flexCheckChecked">
                                <label class="form-check-label" for="flexCheckChecked">
                                Premium
                                </label>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="invoice_type" value="paid" id="flexCheckDefaultPaid">
                                <label class="form-check-label" for="flexCheckDefaultPaid">
                                Paid
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="invoice_type" value="unpaid" id="flexCheckCheckedUnpaid">
                                <label class="form-check-label" for="flexCheckCheckedUnpaid">
                                Unpaid
                                </label>
                            </div>
                        </div>



                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Send Mail</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function viewCommentModal(id, comment) {
        $('#client_detail_id').val(id)
        $('#comments').val(comment);
        $('#service').modal('show');
    }

    function viewSendMailModal(id, comment) {
        $('#client_dtl_id').val(id)
        $('#commentss').val(comment);
        $('#sendmail').modal('show');
    }
</script>