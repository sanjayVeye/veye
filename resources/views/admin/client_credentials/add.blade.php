@extends('layouts.admin')
@section('title') Add Client @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Client
        <small>Add Client</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Clients</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('client-credentials.index')}}">Client Credentials</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('client-credentials.store')}}">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        Select Client
                        <select class="form-control" name="client_id" id="client_id" required>
                            <option value="">-select-</option>
                            @foreach($clients as $row)
                            @php
                            $sel=$row->id==old('client_id') ? 'selected' :'';
                            @endphp
                            <option value="{{$row->id}}" {{$sel}}>{{$row->client_name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('client_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('client_id') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        User name
                        <input class="form-control" name="username" type="text" placeholder="User name" value="{{old('username')}}" required>
                        @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        Password
                        <input class="form-control" name="password" type="password" minlength="1" placeholder="Password" value="{{old('password')}}" required>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <h4><b>Subscription Details</b></h4><br />

                <b>Subscription Plans</b><br /><br />

                <div class="row mb-3">
                    @foreach ($subscriptions as $index=>$data)
                    <div class="col-xs-12 col-md-4">
                        <label for="plan_latest_reports{{$index+1}}"><input type="checkbox" name="subscribe_id[]" value="{{$data->id}}" id="plan_latest_reports{{$index+1}}" class="1" readonly="" @if(@$data->title=='Daily Analysis') checked @endif> {{@$data->title}}</label>
                    </div>
                    @endforeach
                </div>

                <br /><br />

                <div class="row form-group">
                    <div class="col-md-4">
                        Product name
                        <input class="form-control" name="product_name" type="text" placeholder="Enter Product Name" value="{{old('product_name')}}">
                        @if ($errors->has('product_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('product_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        Subscription Start Date
                        <input class="form-control" name="start_date" type="date" value="{{old('start_date')}}">
                        @if ($errors->has('start_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('start_date') }}</strong>
                        </span>
                        @endif

                    </div>
                    <div class="col-md-4">
                        Subscription End Date
                        <input class="form-control" name="end_date" type="date" value="{{old('end_date')}}">
                        @if ($errors->has('end_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('end_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        Invoice Number
                        <input class="form-control" name="invoice_no" type="text" placeholder="Enter Invoice Number" value="{{old('invoice_no')}}">
                        @if ($errors->has('invoice_no'))
                        <span class="help-block">
                            <strong>{{ $errors->first('invoice_no') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        Amount Paid
                        <input class="form-control" name="amount_paid" type="text" placeholder="Enter Amount Paid" value="{{old('amount_paid')}}">
                        @if ($errors->has('amount_paid'))
                        <span class="help-block">
                            <strong>{{ $errors->first('amount_paid') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        Product Type
                        <input class="form-control" name="product_type" type="text" placeholder="Enter Product Type" value="{{old('product_type')}}">
                        @if ($errors->has('product_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('product_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        Comment
                        <textarea name="comment" class="form-control" id="" cols="5" rows="5"></textarea>
                        @if ($errors->has('comment'))
                        <span class="help-block">
                            <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection