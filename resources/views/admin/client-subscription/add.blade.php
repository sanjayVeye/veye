@extends('layouts.admin')
@section('title') Add Client Subscriptions @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Client Subscriptions
        <small>Add Client Subscriptions </small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('subscriptions.index')}}">Company</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar"> 

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('subscription.store')}}">
                @csrf

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">First Name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="first_name" type="text" placeholder="Enter First Name" value="{{old('first_name')}}">
                        @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Last Name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="last_name" type="text" placeholder="Enter Last Name" value="{{old('last_name')}}">
                        @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Email Id</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="email" type="text" placeholder="Enter Email Id" value="{{old('email')}}">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Phone</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="phone" type="text" minlength="8" maxlength="12" placeholder="Enter Phone Number" value="{{old('phone')}}">
                        @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Post Code</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="post_code" type="text" placeholder="Enter Post Code" value="{{old('post_code')}}">
                        @if ($errors->has('post_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('post_code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
              

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Note</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="note" type="text" placeholder="Enter Note" value="{{old('note')}}">
                        @if ($errors->has('note'))
                        <span class="help-block">
                            <strong>{{ $errors->first('note') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
             

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection