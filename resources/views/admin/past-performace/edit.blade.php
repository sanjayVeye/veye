@extends('layouts.admin')
@section('title') Edit Past performace @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Past performace
        <small>Edit Past performace</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('past-performace.index')}}">Past performace</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('past-performace.update',$data->id)}}">
                @csrf
                {{ method_field('PATCH') }}

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="template" id="description" class="form-control">{{$data->template}}</textarea>
                        @if ($errors->has('template'))
                        <span class="help-block">
                            <strong>{{ $errors->first('template') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('description');
</script>
@endsection