@extends('layouts.admin')
@section('title') Edit Personal Recommendations @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Personal Recommendations
            <small>Edit Personal Recommendations</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('daytodaymail.index')}}">Personal Recommendations</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('personalrecommendation.update',$data->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}                 

                    <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Company</label>
                    </div>
                    <div class="col-lg-8">
                    <input class="form-control" name="company" type="text" placeholder="Enter Company" value="{{$data->company}}">
                        @if ($errors->has('company'))
                        <span class="help-block">
                            <strong>{{ $errors->first('company') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Buy Price</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="buy_price" type="text" placeholder="Enter Buy Price" value="{{$data->buy_price}}">
                        @if ($errors->has('buy_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('buy_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Buy Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="buy_date" type="date" placeholder="Enter Buy Date" value="{{$data->buy_date}}">
                        @if ($errors->has('buy_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('buy_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report No.1</label>
                    </div>
                    <div class="col-lg-8">
                    <input class="form-control" name="report_no1" type="text" placeholder="Enter Report No." value="{{$data->report_no1}}">
                        @if ($errors->has('report_no1'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_no1') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Sell Price</label>
                    </div>
                    <div class="col-lg-8">
                    <input class="form-control" name="sell_price" type="text" placeholder="Enter Sell Price" value="{{$data->sell_price}}">
                        @if ($errors->has('sell_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sell_price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Sell Date</label>
                    </div>
                    <div class="col-lg-8">
                    <input class="form-control" name="sell_date" type="date" placeholder="Enter Sell Date" value="{{$data->sell_date}}">
                        @if ($errors->has('sell_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sell_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report No.2</label>
                    </div>
                    <div class="col-lg-8">
                    <input class="form-control" name="report_no_2" type="text" placeholder="Enter Report No.2" value="{{$data->report_no_2}}">
                        @if ($errors->has('report_no_2'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_no_2') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Gain/Losses(%)</label>
                    </div>
                    <div class="col-lg-8">
                    <input class="form-control" name="gains_losses" type="text" placeholder="Enter Gain/Losses(%)" value="{{$data->gains_losses}}">
                        @if ($errors->has('gains_losses'))
                        <span class="help-block">
                            <strong>{{ $errors->first('gains_losses') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                    
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

