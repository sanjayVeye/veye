@extends('layouts.admin')
@section('title') Add Day To Day Mail @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Day To Day Mail
        <small>Add Day To Day Mail </small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('subscriptions.index')}}">Company</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('daytodaymail.store')}}">
                @csrf

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Mail Subject</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="Enter Mail Subject" value="{{old('title')}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Want To Add Content Before Report Links</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="content" id="mail-content">
                            <option value="">Choose One</option>
                            <option value="No">NO</option>
                            <option value="Yes">YES</option>
                        </select>
                        @if ($errors->has('content'))
                        <span class="help-block">
                            <strong>{{ $errors->first('content') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Report Type (for displaying content in mail)</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="report_type" id="mail-content">
                            <option value="">Choose One</option>
                            <option value="Latest Report">Latest Report</option>
                            <option value="Dividend Investor Report">Dividend Investor Report</option>
                            <option value="Stock Of The Day Report">Stock Of The Day Report</option>
                        </select>
                        @if ($errors->has('report_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection