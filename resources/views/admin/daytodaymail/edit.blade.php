@extends('layouts.admin')
@section('title') Edit Day To Day Mail @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Day To Day Mail
            <small>Edit Day To Day Mail</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('daytodaymail.index')}}">Day To Day Mail</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('daytodaymail.update',$daytodaymail->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}
                 

                    <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Mail Subject</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="Enter Mail Subject" value="{{$daytodaymail->title}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Want To Add Content Before Report Links</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="content" id="mail-content">
                            <option value="">Choose One</option>
                            <option value="No" @if($daytodaymail->content=='No') selected @endif>NO</option>
                            <option value="Yes" @if($daytodaymail->content=='Yes') selected @endif>YES</option>
                        </select>
                        @if ($errors->has('content'))
                        <span class="help-block">
                            <strong>{{ $errors->first('content') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Select Report Type (for displaying content in mail)</label>
                    </div>
                    <div class="col-lg-8">
                        <select class="form-control" required="" name="report_type" id="mail-content">
                            <option value="">Choose One</option>
                            <option value="Latest Report" @if($daytodaymail->report_type=='Latest Report') selected @endif>Latest Report</option>
                            <option value="Dividend Investor Report" @if($daytodaymail->report_type=='Dividend Investor Report') selected @endif>Dividend Investor Report</option>
                            <option value="Stock Of The Day Report" @if($daytodaymail->report_type=='Stock Of The Day Report') selected @endif>Stock Of The Day Report</option>
                        </select>
                        @if ($errors->has('report_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('report_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                    
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

