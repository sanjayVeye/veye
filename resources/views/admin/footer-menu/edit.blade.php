@extends('layouts.admin')
@section('title') Edit Menues @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Footer Menues
        <small>Edit Footer Menues</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="{{route('footermenu.index')}}">Footer Menues</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{ route('footermenu.update', $menuData->id) }}">
                @csrf
                {{ method_field('PATCH') }}
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="myTitle" name="title" placeholder="Enter title here" value="{{ $menuData->title }}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
  <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Slug</label>
                    </div>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="slug" name="slug" placeholder="Enter slug here" value="{{ $menuData->slug }}">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Order</label>
                    </div>
                    <div class="col-lg-8">
                        <input type="number" class="form-control" name="order" placeholder="Enter order here" value="{{ $menuData->order }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Type</label>
                    </div>
                    <div class="col-lg-8">
                        <select name="type" class="form-control">
                            <option value="0">Type</option>
                            <option value="1">Important Links</option>
                            <option value="2">Quick Links</option>
                            <option value="3">More Pages</option>
                        </select>
                        <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    // Here the value is stored in new variable x 
    function myFunction() {
        var x = document.getElementById("myTitle").value;
        document.getElementById("slug").innerHTML = x.replace(/[^a-zA-Z ]/g, "").replace(/ /g, '-');
        console.log(x.replace(/[^a-zA-Z ]/g, ""));
    }
</script>
@endsection