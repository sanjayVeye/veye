@extends('layouts.admin')
@section('title') Edit banner @endsection
@section('js')
<script>
    function makeSlug(_this) {
        let slug = (_this.value).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
        $('#slug').val(slug);
    }
</script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit banner
        <small>Edit banner</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('banner.index')}}">banner</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('banner.update',$user->id)}}">
                @csrf
                {{ method_field('PATCH') }}
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="enter title here" value="{{$user->title}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Upload Image</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="image" type="file">
                        @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Slug</label>
                    </div>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug here" value="{{$user->slug}}">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection