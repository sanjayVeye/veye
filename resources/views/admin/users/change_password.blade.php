<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Change Password</title>
	<!-- core:css -->
	<link rel="stylesheet" href="{{url('extra/admin/vendors/core/core.css')}}">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<link rel="stylesheet" href="{{url('extra/admin/vendors/select2/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/jquery-tags-input/jquery.tagsinput.min.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/dropzone/dropzone.min.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/dropify/dist/dropify.min.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css')}}">
	<!-- end plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="{{url('extra/admin/fonts/feather-font/css/iconfont.css')}}">
	<link rel="stylesheet" href="{{url('extra/admin/vendors/flag-icon-css/css/flag-icon.min.css')}}">
	<!-- endinject -->
  <!-- Layout styles -->  
	<link rel="stylesheet" href="{{url('extra/admin/css/demo_5/style.css')}}">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{url('extra/admin/images/favicon.png')}}" />
   <script src="{{url('extra/admin/js/sweetalert.min.js')}}"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  
</head>
<body>
	<div class="main-wrapper">

		
		<div class="horizontal-menu">
			@include('admin/topbar')
		</div>
		<div class="page-wrapper">
				
			

			<div class="page-content">

				<nav class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
						<li class="breadcrumb-item active" aria-current="page">Change Password</li>
					</ol>
				</nav>

				<div class="row">

					<div class="col-lg-12 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">

								@if ($errors->any())
								    @foreach ($errors->all() as $error)
								    <div class="alert alert-danger alert-dismissible fade show">
									    <button type="button" class="close" data-dismiss="alert">&times;</button>
									    {{ $error }}
									 </div>
								    @endforeach
								@endif

								<form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('store.change.password')}}">
									@csrf
									<input type="hidden" id="checkPwdStatus" value="0">
									<div class="form-group row">
										<div class="col-lg-3">
											<label class="col-form-label">New Password</label>
										</div>
										<div class="col-lg-8">
											<input class="form-control" name="new_password" type="password" placeholder="*******">
										</div>
									</div>

									<div class="form-group row">
										<div class="col-lg-3">
											<label class="col-form-label">Confirm New Password</label>
										</div>
										<div class="col-lg-8">
											<input class="form-control" name="new_password_confirmation" type="password" placeholder="*******">
										</div>
									</div>


									<div class="form-group row">
										<div class="col-lg-8 offset-lg-3">
											<button type="submit" class="btn btn-primary mr-2" onclick="('#checkPwdStatus').val(1)">Change</button>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>

			</div>

			@include('admin/footer')
	
		</div>
	</div>

	<!-- core:js -->
	<script src="{{url('extra/admin/vendors/core/core.js')}}"></script>
	<!-- endinject -->
	<!-- plugin js for this page -->
	<script src="{{url('extra/admin/vendors/jquery-validation/jquery.validate.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/inputmask/jquery.inputmask.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/select2/select2.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/jquery-tags-input/jquery.tagsinput.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/dropzone/dropzone.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/dropify/dist/dropify.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/moment/moment.min.js')}}"></script>
	<script src="{{url('extra/admin/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js')}}"></script>
	<!-- end plugin js for this page -->
	<!-- inject:js -->
	<script src="{{url('extra/admin/vendors/feather-icons/feather.min.js')}}"></script>
	<script src="{{url('extra/admin/js/template.js')}}"></script>
	<!-- endinject -->
	<!-- custom js for this page -->
	<script src="{{url('extra/admin/js/form-validation.js')}}"></script>
	<script src="{{url('extra/admin/js/bootstrap-maxlength.js')}}"></script>
	<script src="{{url('extra/admin/js/inputmask.js')}}"></script>
	<script src="{{url('extra/admin/js/select2.js')}}"></script>
	<script src="{{url('extra/admin/js/typeahead.js')}}"></script>
	<script src="{{url('extra/admin/js/tags-input.js')}}"></script>
	<script src="{{url('extra/admin/js/dropzone.js')}}"></script>
	<script src="{{url('extra/admin/js/dropify.js')}}"></script>
	<script src="{{url('extra/admin/js/bootstrap-colorpicker.js')}}"></script>
	<script src="{{url('extra/admin/js/datepicker.js')}}"></script>
	<script src="{{url('extra/admin/js/timepicker.js')}}"></script>
	<!-- end custom js for this page -->
	@include('admin/alert')
	<script type="text/javascript">
  		window.onbeforeunload = confirmExit;
  		function confirmExit(){
  			@if(Auth::user()->isChangePasswordFirstTime && !$errors->any())
      			$.get('{{url("/admin/change-password-reset-status")}}',function(data){
    				console.log(data);
    			});
  			@endif
  		}
	</script>
</body>
</html>