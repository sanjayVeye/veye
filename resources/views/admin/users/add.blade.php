@extends('layouts.admin')
@section('title') Add User @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Add User
            <small>Add User</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('users.index')}}">  Users</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Add</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge  ">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('users.store')}}">
					@csrf
                    <input type="hidden" name="post_code" value="0">
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Enter Name</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="name" type="text" placeholder="Enter Name here" value="{{old('name')}}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Enter Email</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="email" type="text" placeholder="Enter Email here" value="{{old('email')}}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Enter Mobile</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="mobile" type="number" placeholder="Enter Mobile here" value="{{old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Enter Password</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="password" type="password" placeholder="*******">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Enter Confirm</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="password_confirmation" type="text" placeholder="*******">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
						
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Select Role</label>
                        </div>
                        <div class="col-lg-8">
                            <select class="form-control" name="role">
                                <option value="">Select Role</option>
                                @foreach($roles as $roleRow)
                                    <option value="{{$roleRow->id}}">{{$roleRow->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

