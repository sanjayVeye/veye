@extends('layouts.admin')
@section('title') Edit User @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit User
            <small>Edit User</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('users.index')}}">Users</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('users.update',$user->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Name</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="name" type="text" placeholder="enter name here" value="{{$user->name}}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Email</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="email" type="text" placeholder="enter email here" value="{{$user->email}}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Mobile</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="mobile" type="number" placeholder="enter mobile here" value="{{$user->mobile}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Password</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="password" type="password" placeholder="*******">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Confirm</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="password_confirmation" type="text" placeholder="*******">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
						
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Role</label>
                        </div>
                        <div class="col-lg-8">
                            @php
                                $aRole=$user->roles->pluck('id')->toArray();
                            @endphp
                            <select class="form-control" name="role">
                                <option value="">Select Role</option>
                                @foreach($role as $roleRow)
                                    @php $sel=in_array($roleRow->id,$aRole) ? 'selected' : '' ;@endphp
                                    <option value="{{$roleRow->id}}" {{$sel}}>{{$roleRow->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i>  Add</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

