@extends('layouts.admin')
@section('title') Edit Term Conditions @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Term Conditions
            <small>Edit Term Conditions</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('termconditions.index')}}">Term Conditions</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('termconditions.update',$data->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="tag" value="term-conditions">
                    <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="meta_title" type="text" placeholder="Enter meta title" value="{{$data->meta_title}}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Keywords</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_keywords" id="" class="form-control" placeholder="Meta Keywords">{{$data->meta_keywords}}</textarea>
                        @if ($errors->has('meta_keywords'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_keywords') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Description</label>
                    </div>
                    <div class="col-lg-8">
                    <textarea name="meta_description" id="" class="form-control" placeholder="Meta Description">{{$data->meta_description}}</textarea>
                        @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                 
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Template</label>
                        </div>
                        <div class="col-lg-8">
                        <textarea name="template" id="description" class="form-control">{{$data->template}}</textarea>
                            @if ($errors->has('template'))
                                <span class="help-block">
                                <strong>{{ $errors->first('template') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
    <script>
      CKEDITOR.replace( 'description' );
    </script> 
@endsection

