@extends('layouts.admin')
@section('title') Add Veye Details @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Add Veye Details
            <small>Add Veye Details</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('veye.index')}}">Veye Details</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Add</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
                @if(@$veyeDetail->id)
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('veye.update',$veyeDetail->id)}}">                
                @csrf
                    {{ method_field('PATCH') }}
                @else
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('veye.store')}}">                
                @csrf
                @endif
                
                    <h4><b> VEYE PTY LTD DETAILS</b></h4><br/>                    
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">ACN Number</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="veye_acn_no" type="number" placeholder="Enter ACN Number" value="{{$veyeDetail->veye_acn_no}}">
                            @if ($errors->has('veye_acn_no'))
                                <span class="help-block">
                                <strong>{{ $errors->first('veye_acn_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">ABN Number</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="veye_abn_no" type="number" placeholder="Enter ABN Number" value="{{$veyeDetail->veye_abn_no}}">
                            @if ($errors->has('veye_abn_no'))
                                <span class="help-block">
                                <strong>{{ $errors->first('veye_abn_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">AFSL Number</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="veye_afsl_no" type="number" placeholder="Enter AFSL Number" value="{{$veyeDetail->veye_afsl_no}}">
                            @if ($errors->has('veye_afsl_no'))
                                <sp an class="help-block">
                                <strong>{{ $errors->first('veye_afsl_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">AR Number</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="veye_ar_no" type="number" placeholder="Enter AR Number" value="{{$veyeDetail->veye_ar_no}}">
                            @if ($errors->has('veye_ar_no'))
                                <span class="help-block">
                                <strong>{{ $errors->first('veye_ar_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <h4><b> GLOBAL MERCES FUNDS MANAGEMENT LTD DETAILS</b></h4><br/>                    
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">ACN Number</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="global_acn_no" type="number" placeholder="Enter ACN Number" value="{{$veyeDetail->global_acn_no}}">
                            @if ($errors->has('global_acn_no'))
                                <span class="help-block">
                                <strong>{{ $errors->first('global_acn_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">ABN Number</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="global_abn_no" type="number" placeholder="Enter ABN Number" value="{{$veyeDetail->global_abn_no}}">
                            @if ($errors->has('global_abn_no'))
                                <span class="help-block">
                                <strong>{{ $errors->first('global_abn_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">AFSL Number</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="global_afsl_no" type="number" placeholder="Enter AFSL Number" value="{{$veyeDetail->global_afsl_no}}">
                            @if ($errors->has('global_afsl_no'))
                                <sp an class="help-block">
                                <strong>{{ $errors->first('global_afsl_no') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <h4><b> OTHER DETAILS</b></h4><br/>                    
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Email</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="email" type="email" placeholder="Enter Email Id" value="{{$veyeDetail->email}}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Phone</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="phone" type="text" placeholder="Enter Phone No." value="{{$veyeDetail->phone}}">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Address</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="address" type="text" placeholder="Enter Address" value="{{$veyeDetail->address}}">
                            @if ($errors->has('address'))
                                <span an class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">State</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="state" type="text" placeholder="Enter State" value="{{$veyeDetail->state}}">
                            @if ($errors->has('state'))
                                <span an class="help-block">
                                <strong>{{ $errors->first('state') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Country</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="country" type="text" placeholder="Enter Country" value="{{$veyeDetail->country}}">
                            @if ($errors->has('country'))
                                <span an class="help-block">
                                <strong>{{ $errors->first('country') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Post Code</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="post_code" type="text" placeholder="Enter Post Code" value="{{$veyeDetail->post_code}}">
                            @if ($errors->has('post_code'))
                                <span an class="help-block">
                                <strong>{{ $errors->first('post_code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                  
                    				
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

