@extends('layouts.admin')
@section('title') Edit Email Template @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Email Template
        <small>Edit Email Template</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('emailtemplates.index')}}">Email Template</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('emailtemplates.update',$emailtemplates->id)}}">
                @csrf
                {{ method_field('PATCH') }}


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template Name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="template_name" type="text" placeholder="enter template_name" value="{{$emailtemplates->template_name}}">
                        @if ($errors->has('template_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('template_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template Id</label>
                    </div>
                    <div class="col-lg-8">
                    <textarea name="template_id" type="text" class="form-control">{{$emailtemplates->template_key}}</textarea>
                        @if ($errors->has('template_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('template_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="template" id="description" class="form-control">{{$emailtemplates->template}}</textarea>
                        @if ($errors->has('template'))
                        <span class="help-block">
                            <strong>{{ $errors->first('template') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('description');
</script>
@endsection