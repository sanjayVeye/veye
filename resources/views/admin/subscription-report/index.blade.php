@extends('layouts.admin')
@section('title') Subscription Report @endsection
@section('head')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-search--dropdown .select2-search__field {
        width: 98%;
    }
    .dataTables_wrapper .dt-buttons {
        float: left !important;
    }
</style>
@endsection
@section('js')
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>


<script type="text/javascript">
    $(document).ready(function() {
        dataTable = $('#dataTableExample').dataTable({
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: [0, 1] // Column index which needs to export
                    }
                },
                {
                    extend: 'csv',
                },
                {
                    extend: 'excel',
                }
            ],
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            // "pagingType": "full_numbers",
            "iDisplayLength": 10,
            "language": {
                search: ""
            },
            "serverSide": true,
            "processing": true,
            "paging": true,
            "ajax": {
                url: '{{route("subscription-report.index")}}',
                data: function(e) {
                e.subscription_start_date = $('#subscription_start_date').val();
				e.subscription_end_date = $('#subscription_end_date').val();
                }
            },
            'createdRow': function(row, data, dataIndex) {
                $(row).attr('id', 'tr_' + data.id);
            },
            columns: [{
                    data: "sno",
                    name: 'sno',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                // { data: "id" ,name:"id"},
                {
                    data: "client_name",
                    name: 'client_name',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "email",
                    name: 'email',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "phone",
                    name: 'phone',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "product_name",
                    name: 'product_name',
                    searchable: false,
                    sortable: false,
                    visible: true
                },

                {
                    data: "subscription_start_date",
                    name: "subscription_start_date"
                },
                {
                    data: "subscription_end_date",
                    name: "subscription_end_date"
                },
               
                {
                    data: "invoice_no",
                    name: "invoice_no"
                },
                {
                    data: "amount",
                    name: "amount"
                },
                {
                    data: "access",
                    name: "access"
                },
                // {
                //     data: "action",
                //     name: "action"
                // },
               
            ],
            "fnInitComplete": function(oSettings, json) {

            }
        });



    });
    // $(document).on('click','#search_btn',function() {
	// 	dataTable.draw();
	// });

</script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Subscription Report
        <small>Subscription Report</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li><span>Subscription Report</span></li>
        </ul>
        <!-- <div class="page-toolbar">
            <a class="btn btn-primary" href="{{route('emailtemplates.create')}}"><i class="fa fa-plus me-2"></i> Add Email Templates</a>
        </div> -->
    </div>
<form action="" method="get">
    <div class="row">
    <div class="col-lg-4 col-xs-4 col-sm-4">
            <h6 class="page-title">Search</h6>
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <label class="col-form-label"></label>
                <input class="form-control" type="text" name="search" id="search" value="{{@$_GET['search']}}" placeholder="Search">
            </div>
        </div>
        <div class="col-lg-4 col-xs-4 col-sm-4">
            <h6 class="page-title">Subscription Start Date</h6>
            <div class="col-lg-6 col-xs-6 col-sm-6">
                <label class="col-form-label">From Date</label>
                <input class="form-control" type="date" name="subscription_start_date" id="subscription_start_date" value="{{@$_GET['subscription_start_date']}}">
            </div>
            <div class="col-lg-6 col-xs-6 col-sm-6">
                <label class="col-form-label">To Date</label>
                <input class="form-control" type="date" name="subscription_start_date_to" id="subscription_start_date_to" value="{{@$_GET['subscription_start_date_to']}}">
            </div>
        </div>
        <div class="col-lg-4 col-xs-4 col-sm-4">
            <h6 class="page-title">Subscription End Date</h6>
            <div class="col-lg-6 col-xs-6 col-sm-6">
                <label class="col-form-label">From Date</label>
                <input class="form-control" type="date" name="subscription_end_date" id="subscription_end_date" value="{{@$_GET['subscription_end_date']}}">
            </div>
            <div class="col-lg-6 col-xs-6 col-sm-6">
                <label class="col-form-label">To Date</label>
                <input class="form-control" type="date" name="subscription_end_date_to" id="subscription_end_date_to" value="{{@$_GET['subscription_end_date_to']}}">
            </div>
        </div>
        <div class="col-lg-4 col-xs-4 col-sm-4">
            <div class="col-lg-6 col-xs-6 col-sm-6" style="margin-top: 69px;">
               <button type="submit" class="btn btn-primary mr-2" id="search_btn">Search</button>
            </div>
        </div>
    </div>
    </form>
    <div class="row">
        <div style="margin-top: 30px !important;" class="col-lg-12 col-xs-12 col-sm-12">
            <div>
                <table id="dataTableExample" class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Client Name </th>
                            <th>Client Email</th>
                            <th>Client Phone</th>
                            <th>Product Name</th>
                            <th>Subscription Start Date</th>
                            <th>Subscription End Date</th>
                            <th>Invoice No</th>
                            <th>Amount</th>
                            <th>Access</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection