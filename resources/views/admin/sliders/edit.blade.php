@extends('layouts.admin')
@section('title') Edit Slider @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Slider
            <small>Edit Slider</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('sliders.index')}}">Slider</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('sliders.update',$user->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Slider Title</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="title" type="text" placeholder="Enter Report Title" value="{{$user->title}}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Slider Description On Bottom</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="description" type="text" placeholder="Enter Description On Bottom" value="{{$user->description}}">
                            @if ($errors->has('description'))
                                <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Upload Slider Image</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="slider_img" type="file">
                            @if ($errors->has('slider_img'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('slider_img') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
					
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

