@extends('layouts.admin')
@section('title') Manage Payment Type @endsection
@section('head')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('js')
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTableExample').dataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            // "pagingType": "full_numbers",
            "iDisplayLength": 10,
            "language": {
                search: ""
            },
            "serverSide": true,
            "processing": true,
            "paging": true,
            "ajax": {
                url: '{{route("payment-transactions.index")}}',
                data: function(e) {

                }
            },
            'createdRow': function(row, data, dataIndex) {
                $(row).attr('id', 'tr_' + data.id);
            },
            columns: [{
                    data: "sno",
                    name: 'sno',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "email",
                    name: "email"
                },
                {
                    data: "amount",
                    name: "amount"
                },
                {
                    data: "unique_session_id",
                    name: "unique_session_id"
                },
                {
                    data: "payment_status",
                    name: "payment_status"
                },
                {
                    data: "created_at",
                    name: "created_at"
                },

            ],
        });
    });
</script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Payment Type
        <small>Manage Payment Type</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li><span>Payment Type</span></li>
        </ul>
        <!-- <div class="page-toolbar">
                <a class="btn btn-primary" href="{{route('payment-type.create')}}"><i class="fa fa-plus me-2"></i>  Add Payment Type</a>
            </div> -->
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">

            <table id="dataTableExample" class="table table-bordered table-striped table-responsive">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Email</th>
                        <th>Amount</th>
                        <th>Unique Session id</th>
                        <th>Payment Status</th>
                        <th>Payment Date</th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>
</div>
@endsection