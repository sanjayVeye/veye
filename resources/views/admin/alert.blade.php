@if(Session::has('success'))
<div class="alert alert-success alert-dismissible show" id="success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    {{Session::get('success')}}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible show" id="error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    {{Session::get('error')}}
</div>
@endif
<script>
    $(document).ready(function(){
        $("#error").fadeOut(5000, function(){ 
            $(this).remove();
        });

        $("#success").fadeOut(5000, function(){ 
            $(this).remove();
        });
    });

</script>