@extends('layouts.admin')
@section('title') Edit Subscribe Page @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Subscribe Page
            <small>Edit Subscribe Page</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('webinar.index')}}">Webinar Page</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('webinar.update',$webinar->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}
                 

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Title</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="title" type="text" placeholder="Enter title here" value="{{$webinar->title}}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                   
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Path</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="path" type="text" placeholder="Enter path" value="{{$webinar->path}}">
                            @if ($errors->has('path'))
                                <span class="help-block">
                                <strong>{{ $errors->first('path') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Reviews</label>
                        </div>
                        <div class="col-lg-8">
                            <textarea name="reviews" id="" class="form-control">{{$webinar->reviews}}</textarea>
                            @if ($errors->has('reviews'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('reviews') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    

                    
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

