@extends('layouts.admin')
@section('title') Add Product @endsection
@section('head')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-search--dropdown .select2-search__field {
        width: 98%;
    }
</style>
@endsection
@section('js')
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Product
        <small>Add Product</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('clients.index')}}">Client</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="table-responsive">
                <table id="dataTableExample" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Product Name</th>
                            <th>Subscription Name</th>
                            <th>Subscription End Date</th>
                            <th>Invoice Number</th>
                            <th>Total Amount Paid</th>
                            <!-- <th>Comment</th> -->
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $index=>$data)
                        <tr id="tr_2" role="row" class="odd">
                            <td class="sorting_1">{{$index+1}}</td>
                            <td>{{$data->product_name}}</td>
                            <td>bablooyadav4120@gmail.com</td>
                            <td>{{$data->start_date}}</td>
                            <td>{{$data->end_date}}</td>
                            <td>{{$data->amount_paid}}</td>
                            <!-- <td><button type="button" class="btn btn-primary modalButton btn-sm" data-toggle="modal" data-id="3048">View More</button></td> -->
                            <td><a href="{{route('products.edit',$data->id)}}"><i class="fa fa-edit" style="font-size: 24px;"></i></a>
                            <a href="{{route('products.destroy',$data->id)}}" onclick="return confitm('Are you sure??????.')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('products.store')}}">
                @csrf
                <input type="hidden" name="user_id" value="{{ $userId }}">
                <h4><b>Subscription Details</b></h4><br />

                <b>Subscription Plans</b><br /><br />
                <div class="row mb-3">
                    @foreach ($subscriptions as $index=>$data)
                    <div class="col-xs-12 col-md-4">
                        <label for="plan_latest_reports{{$index+1}}"><input type="checkbox" name="subscribe_id[]" value="{{$data->id}}" id="plan_latest_reports{{$index+1}}" class="1" readonly=""> {{$data->title}}</label>
                    </div>
                    @endforeach

                </div>

                <br/><br/>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Product Name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="product_name" type="text" placeholder="Enter Product Name" value="{{old('product_name')}}">
                        @if ($errors->has('product_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('product_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Subscription Start Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="start_date" type="date" value="{{old('start_date')}}">
                        @if ($errors->has('start_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('start_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Subscription End Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="end_date" type="date" value="{{old('end_date')}}">
                        @if ($errors->has('end_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('end_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Invoice Number</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="invoice_no" type="text" placeholder="Enter Invoice Number" value="{{old('invoice_no')}}">
                        @if ($errors->has('invoice_no'))
                        <span class="help-block">
                            <strong>{{ $errors->first('invoice_no') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Amount Paid</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="amount_paid" type="text" placeholder="Enter Amount Paid" value="{{old('amount_paid')}}">
                        @if ($errors->has('amount_paid'))
                        <span class="help-block">
                            <strong>{{ $errors->first('amount_paid') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Product Type</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="product_type" type="text" placeholder="Enter Product Type" value="{{old('product_type')}}">
                        @if ($errors->has('product_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('product_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Comment</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="comment" class="form-control" id="" cols="30" rows="10"></textarea>
                        @if ($errors->has('comment'))
                        <span class="help-block">
                            <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection