@extends('layouts.admin')
@section('title') Add Client @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Client
        <small>Add Client</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('clients.index')}}">Client</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('clients.store')}}">
                @csrf

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="name" type="text" placeholder="Enter Name Here" value="{{old('name')}}">
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Email</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="email" type="text" placeholder="Enter Email Here" value="{{old('email')}}">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Mobile</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="mobile" type="number" minlength="10" maxlength="10" placeholder="Enter Mobile Here" value="{{old('mobile')}}">
                        @if ($errors->has('mobile'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mobile') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Post Code</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="post_code" type="number" placeholder="Enter Post Code Here" value="{{old('post_code')}}">
                        @if ($errors->has('post_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('post_code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Password</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="password" type="password" placeholder="*******">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Confirm</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="password_confirmation" type="text" placeholder="*******">
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <h4><b>Subscription Details</b></h4><br />

                <b>Subscription Plans</b><br /><br />
                <div class="row mb-3">
                @foreach ($subscriptions as $index=>$data)
                    <div class="col-xs-12 col-md-4">
                        <label for="plan_latest_reports{{$index+1}}"><input type="checkbox" name="subscribe_id[]" value="{{$data->id}}" id="plan_latest_reports{{$index+1}}" class="1" readonly=""> {{$data->title}}</label>
                    </div>
                    @endforeach
                </div>

                <br /><br />

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Product Name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="product_name" type="text" placeholder="Enter Product Name" value="{{old('product_name')}}">
                        @if ($errors->has('product_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('product_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Subscription Start Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="start_date" type="date" value="{{old('start_date')}}">
                        @if ($errors->has('start_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('start_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Subscription End Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="end_date" type="date" value="{{old('end_date')}}">
                        @if ($errors->has('end_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('end_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Invoice Number</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="invoice_no" type="text" placeholder="Enter Invoice Number" value="{{old('invoice_no')}}">
                        @if ($errors->has('invoice_no'))
                        <span class="help-block">
                            <strong>{{ $errors->first('invoice_no') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Amount Paid</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="amount_paid" type="text" placeholder="Enter Amount Paid" value="{{old('amount_paid')}}">
                        @if ($errors->has('amount_paid'))
                        <span class="help-block">
                            <strong>{{ $errors->first('amount_paid') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Product Type</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="product_type" type="text" placeholder="Enter Product Type" value="{{old('product_type')}}">
                        @if ($errors->has('product_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('product_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Comment</label>
                    </div>
                    <div class="col-lg-8">
                       <textarea name="comment" class="form-control" id="" cols="30" rows="10"></textarea>
                        @if ($errors->has('comment'))
                        <span class="help-block">
                            <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection