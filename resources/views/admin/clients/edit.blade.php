@extends('layouts.admin')
@section('title') Edit Client @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Client
            <small>Edit Client</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('clients.index')}}">Clients</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('clients.update',$client->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}
                    

                    <div class="row form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">First Name</label>
                            <input class="form-control" name="first_name" type="text" placeholder="first name" value="{{$client->first_name}}">
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Last Name</label>
                            <input class="form-control" name="last_name" type="text" placeholder="Last name" value="{{$client->last_name}}">
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">Email</label>
                            <input class="form-control" name="email" type="text" placeholder="Enter Email Here" value="{{$client->email}}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Phone</label>
                            <input class="form-control" name="phone" type="number" minlength="10" maxlength="10" placeholder="Enter phone Here" value="{{$client->phone}}">
                            @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">Post Code</label>
                            <input class="form-control" name="post_code" type="number" placeholder="Enter Post Code Here" value="{{$client->post_code}}">
                            @if ($errors->has('post_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('post_code') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                   				
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
@endsection

