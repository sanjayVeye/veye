@extends('layouts.admin')
@section('title') Edit FAQ @endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit FAQ
            <small>Edit FAQ</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="{{route('faq.index')}}">FAQ</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('faq.update',$data->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="tag" value="faq">

                    <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="Enter title" value="{{$data->title}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                   <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Details</label>
                        </div>
                        <div class="col-lg-8">
                        <textarea name="details" id="details" class="form-control">{{$data->details}}</textarea>
                            @if ($errors->has('details'))
                                <span class="help-block">
                                <strong>{{ $errors->first('details') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="metatitle" type="text" placeholder="Enter meta title" value="{{$data->metatitle}}">
                        @if ($errors->has('metatitle'))
                        <span class="help-block">
                            <strong>{{ $errors->first('metatitle') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Keywords</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="metakeywords" id="" class="form-control" placeholder="Meta Keywords">{{$data->metakeywords}}</textarea>
                        @if ($errors->has('metakeywords'))
                        <span class="help-block">
                            <strong>{{ $errors->first('metakeywords') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Description</label>
                    </div>
                    <div class="col-lg-8">
                    <textarea name="metadescription" id="" class="form-control" placeholder="Meta Description">{{$data->metadescription}}</textarea>
                        @if ($errors->has('metadescription'))
                        <span class="help-block">
                            <strong>{{ $errors->first('metadescription') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                 
           
                    
									
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
    <script>
      CKEDITOR.replace( 'description' );
    </script> 
@endsection

