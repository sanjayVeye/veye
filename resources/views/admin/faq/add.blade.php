@extends('layouts.admin')
@section('title') Add Frequently Asked Questions@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Frequently Asked Questions
        <small>Add Frequently Asked Questions</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('faq.index')}}">Company</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('faq.store')}}">
                @csrf
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label"> Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" placeholder="Enter meta title" value="{{old('title')}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="details" id="" class="form-control" placeholder="Meta Description"></textarea>
                        @if ($errors->has('details'))
                        <span class="help-block">
                            <strong>{{ $errors->first('details') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="meta_title" type="text" placeholder="Enter meta title" value="{{old('meta_title')}}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Keywords</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_keywords" id="" class="form-control" placeholder="Meta Keywords"></textarea>
                        @if ($errors->has('meta_keywords'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_keywords') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_description" id="" class="form-control" placeholder="Meta Description"></textarea>
                        @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection