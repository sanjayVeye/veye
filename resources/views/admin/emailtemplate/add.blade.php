@extends('layouts.admin')
@section('title') Add Email Template@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Email Template
        <small>Add Email Template</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('emailtemplates.index')}}">Company</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('emailtemplates.store')}}">
                @csrf

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template Name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="template_name" type="text" placeholder="enter template_name" value="{{old('template_name')}}">
                        @if ($errors->has('template_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('template_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template Id</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="template_key" type="text" placeholder="enter template key" value="{{old('template_key')}}">
                        @if ($errors->has('template_key'))
                        <span class="help-block">
                            <strong>{{ $errors->first('template_key') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="template" id="description" class="form-control"></textarea>
                        @if ($errors->has('template'))
                        <span class="help-block">
                            <strong>{{ $errors->first('template') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>



                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-plus me-2"></i> Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('description');
</script>
@endsection