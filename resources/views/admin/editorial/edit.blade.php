@extends('layouts.admin')
@section('title') Edit Blog @endsection
@section('js')
<script>
    function makeSlug(_this) {
        let slug = (_this.value).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
        $('#slug').val(slug);
    }
</script>
@endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Editorial
        <small>Edit Editorial</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('editorial.index')}}">Article</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('editorial.update',$data->id)}}">
                @csrf
                {{ method_field('PATCH') }}

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="title" type="text" onchange="makeSlug(this)" placeholder="Enter title here" value="{{$data->title}}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Slug</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="slug" type="text" id="slug" placeholder="Enter slug here" value="{{$data->slug}}">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Date</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="created_at" type="date" value="{{date('Y-m-d',strtotime($data->created_at))}}">
                        @if ($errors->has('date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                 <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Image Alt</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="attachment" type="text" value="{{$data->attachment}}">
                        @if ($errors->has('attachment'))
                        <span class="help-block">
                            <strong>{{ $errors->first('attachment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Upload Image</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="article_img" type="file">
                        @if ($errors->has('article_img'))
                        <span class="help-block">
                            <strong>{{ $errors->first('article_img') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="description" class="form-control" id="description" cols="30" rows="10">{{$data->description}}</textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Details</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="reporthtml" class="form-control" id="reporthtml" cols="30" rows="10">{{$data->reporthtml}}</textarea>
                        @if ($errors->has('reporthtml'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reporthtml') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('reporthtml');
</script>
@endsection