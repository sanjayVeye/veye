@extends('layouts.admin')
@section('title') Manage Blog @endsection
@section('head')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{
            width: 100%!important;
        }
        .select2-search--dropdown .select2-search__field {
            width: 98%;
        }
    </style>
@endsection
@section('js')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
		$(document).ready(function(){
			$('#dataTableExample').dataTable({
					"aLengthMenu": [
						[10, 30, 50, -1],
						[10, 30, 50, "All"]
					],
					// "pagingType": "full_numbers",
					"iDisplayLength": 10,
					"language": {
						search: ""
					},
					"serverSide": true,
					"processing": true,
					"paging": true,
					"ajax":{
						url:'{{route("editorial.index")}}',
						data:function(e){

						}
					},
					'createdRow': function( row, data, dataIndex ) {
						$(row).attr('id', 'tr_'+data.id);
					},
					columns: [
						{ data: "sno" ,name: 'sno',searchable: false, sortable : false, visible:true},
						{ data: "title" ,name:"title"},
                        { data: "slug" ,name:"slug"},
						{ data: "date" ,name:"date"},
						{ data: "article_img" ,name: 'article_img',searchable: false, sortable : false, visible:true},
						{ data: "description" ,name:"description"},
						{ data: "created_at" ,name:"created_at"},
						{ data: "updated_at" ,name:"updated_at"},
						{ data: "action" ,name: 'action',searchable: false, sortable : false, visible:true},
					],
                    "fnInitComplete": function (oSettings, json) {

                    }
				});



		});
    </script>
@endsection
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Manage Editorial
            <small>Manage Editorial</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('home')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li><span>Report</span></li>
            </ul>
            <div class="page-toolbar">
                <a class="btn btn-primary" href="{{route('editorial.create')}}"><i class="fa fa-plus me-2"></i> Add editorial</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div>
					<table id="dataTableExample" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                            <th>Sl No</th>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Date</th>
                                <th>Image</th>
                                <th>Desciption</th>
                                <th>Added At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

