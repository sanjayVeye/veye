@extends('layouts.admin')
@section('title') Edit Homes @endsection
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Homes
        <small>Edit Homes</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('home')}}">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="{{route('homes.index')}}">Homes</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('homes.update',$data->id)}}">
                @csrf
                {{ method_field('PATCH') }}

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label"> Title</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="title" id="" class="form-control" placeholder="Title">{{$data->title}}</textarea>
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Details</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="details" id="" class="form-control" placeholder="details">{{$data->details}}</textarea>
                        @if ($errors->has('details'))
                        <span class="help-block">
                            <strong>{{ $errors->first('details') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Catagories</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="catagories" id="" class="form-control" placeholder="catagories">{{$data->catagories}}</textarea>
                        @if ($errors->has('catagories'))
                        <span class="help-block">
                            <strong>{{ $errors->first('catagories') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="meta_title" type="text" placeholder="Enter meta title" value="{{$data->meta_title}}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Keywords</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_keywords" id="" class="form-control" placeholder="Meta Keywords">{{$data->meta_keywords}}</textarea>
                        @if ($errors->has('meta_keywords'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_keywords') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_description" id="" class="form-control" placeholder="Meta Description">{{$data->meta_description}}</textarea>
                        @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('description');
</script>
@endsection