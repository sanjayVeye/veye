@extends('frontend.layouts.app')
@section('title') Client Login | Veye Pty Ltd @endsection
@section('description') Veye Client login: Gain access to Veye's client portal for comprehensive investment analysis, portfolio management, and real-time market data. Stay informed and make informed decisions. Sign in now! @endsection
@section('canonical') https://veye.com.au/client-login @endsection
@section('nofollow') <META NAME="robots" CONTENT="noindex,nofollow"> @endsection
@section('content')
<?php

use App\Models\LogoEdit;
?>
@php $logodata = LogoEdit::first(); @endphp
<section class="gradient-form">

   <div class="gradient-formBGCLR" style="background-image: url('https://veye.com.au/frontendmain/img/graph_chart.jpg');">
      <div class="container">
         <div class="row">
            <div class="col-lg-4 offset-lg-4">
               <div class="formBox newLogins">
                  <div class="formBoxHead">
                     <img src="{{asset('frontend/assets/images')}}/{{$logodata->header_logo}}" />
                     <h4>Login into Member Account</h4>
                     <p class="useFP">Use your credentials to access your account.</p>
                     <!-- <p>Use your credentials to access your account</p> -->
                  </div>
                  <form method="post" enctype="multipart/form-data" action="{{ route('registration.login.match') }}">
                     @csrf
                     <div class="form-floating">
                        <!-- mb-3 -->
                        <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Your Email">
                        <label for="floatingInput">Email</label>
                     </div>
                     <div class="form-floating">
                        <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
                        <label for="floatingPassword">Password</label>
                     </div>
                     <div class="asscptlogin">
                        <div class="acceptVeye">
                           <label>
                              <input type="checkbox" name="termCondition" value="1" required> <span>I Accept Veye <a href="{{url('pages/terms-conditions')}}">Terms & Conditions</a></span>
                           </label>
                        </div>
                        <div class="acceptVeye">
                           <label>
                              <input type="checkbox" name="accept" value="1" /> <span>Remember Me On This Device</span>
                           </label>
                        </div>
                     </div>
                     <div class="d-flex justify-content-between w-100">
                        @if (session('fail'))
                        <div class="alert alert-danger">
                           {{ session('fail') }}
                        </div>
                        @endif
                        @if (session('success'))
                        <div class="alert alert-success">
                           {{ session('success') }}
                        </div>
                        @endif
                     </div>

                     <div class="text-center">
                        <button class="btn VeyeLogin regLogiHover" type="submit">Login</button>
                     </div>

                     <div class="ForgotPassBox d-flex align-items-start justify-content-center ">
                        <!-- pb-4 -->
                        <div>
                           <p>Not a member?</p>
                           <a href="{{ route('forgot.password') }}" class="ForgotPass srgPost">Forgot password?</a>
                        </div>
                        <a href="{{url('registration')}}" class="btn VeyeRegister text-white" style="    margin-top: -20px !important;"> Register</a>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <style type="text/css">
      .ForgotPassBox p {
         margin: -18px 0 3px -40px !important;
      }

      .srgPost {
         margin-left: -59px;
         line-height: 40px;
      }
      .alert-success {
    --bs-alert-color: crimson;
    /* --bs-alert-bg: #d1e7dd; */
    /* --bs-alert-border-color: #badbcc; */
}
   </style>
</section>
@endsection