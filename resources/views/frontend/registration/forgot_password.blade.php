@extends('frontend.layouts.app')
@section('title') Reset Password | Veye Pty Ltd @endsection
@section('description') Forgot password? Easily reset your password for Veye Clint login. Regain access to your account with just a few simple steps. Retrieve your login credentials now! @endsection
@section('canonical') https://veye.com.au/forgot-password @endsection
@section('nofollow') <META NAME="robots" CONTENT="noindex,nofollow"> @endsection
@section('content')
<?php
use App\Models\LogoEdit;
?>
@php $logodata = LogoEdit::first(); @endphp
      <section class="gradient-form">
         <div class="gradient-formBGCLR">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4 offset-lg-4">
                     <div class="formBox">
                        <div class="formBoxHead">
                        <img src="{{asset('frontend/assets/images')}}/{{$logodata->header_logo}}"/> 
                           <h4>Reset Password</h4>
                           <!-- <p>Use your credentials to access your account</p> -->
                      
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                        @endif
                        @if (\Session::has('error'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{!! \Session::get('error') !!}</li>
                            </ul>
                        </div>
                        @endif
                        </div>
                        @if($keyId)
                        <form method="post" enctype="multipart/form-data" action="{{ route('reset.password') }}">
                           @csrf
                           <input type="hidden" name="email" value="{{$email}}">
                            <input type="hidden" name="token" value="{{$keyId}}">
                          
                           <div class="form-floating">
                              <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password" value="{{old('password')}}">
                              <label for="floatingPassword">New Password</label>
                              @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                           </div>
                           <div class="form-floating">
                              <input type="password" name="confirm" class="form-control" id="floatingPassword" placeholder="Password" value="{{old('password')}}">
                              <label for="floatingPassword">Confirm Password</label>
                              @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                           </div>
                           <div class="acceptVeye">
                              <label>
                                 <input type="checkbox" name="accept" value="1"/> <span>Remember</span>
                              </label>
                           </div>

                           <div class="text-center">
                              <button class="btn VeyeLogin" type="submit">Reset Password</button>
                           </div>

                           <div class="ForgotPassBox d-flex align-items-start justify-content-center pb-4">
                              
                                 <button class="btn VeyeRegister">Login</button>
                           </div>
                        </form>
                        @else
                        <form method="post" enctype="multipart/form-data" action="{{ route('reset.send_pass_reset_link') }}">
                           @csrf
                          
                           <div class="form-floating">
                              <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email ID" value="{{old('email')}}">
                              <label for="floatingPassword">E-Mail Address</label>
                              @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                           </div><br><br>
                           
                           <div class="text-center">
                              <button class="btn VeyeLogin" type="submit">Send Password Reset Link</button>
                           </div>

                        </form>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @endsection