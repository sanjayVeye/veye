<head>
   <meta charset="utf-8" />
   <title>@yield('title')</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="@yield('description')">
   <meta name="keywords" content="@yield('keywords')">
   <meta name="author" content="Veye Pty Ltd">
   <meta name="generator" content="Hugo 0.104.2">
   <!-- BEGIN GLOBAL MANDATORY STYLES -->
  
   @yield("nofollow")
   <link rel="canonical" href="@yield('canonical')" />


   <link href="{{asset('frontend/assets/css/bootstrap.min.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/css/bootstrap.min.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/css/dashboard.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/css/navbar.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/css/style.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/css/slick.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/fontawesome-free-5.15.4-web/css/all.css')}}" rel="stylesheet">
   <script defer src="{{asset('frontend/assets/fontawesome-free-5.15.4-web/js/all.js')}}"></script>
   <!--load all styles -->
   <link href="{{asset('frontend/assets/fontawesome-free-5.15.4-web/css/fontawesome.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/fontawesome-free-5.15.4-web/css/brands.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/assets/fontawesome-free-5.15.4-web/css/solid.css')}}" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
   <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css'>
   <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css'>
   @yield("facebook")
   
   <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.3/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
  
  <!-- CkEditor scripts -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('ckfinder/ckfinder.js')}}"></script>
        <!-- END THEME LAYOUT STYLES -->
   @yield('header')


   <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js'></script>
   <script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js'></script>
   <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<!-- Javascript -->
<script>
   $(function() {
      $(".search-company-name").autocomplete({
         source: "{{route('search.company')}}",
         minLength: 3
      });
   });

   $(function() {
      $(".company-name").autocomplete({
         source: "{{route('searchcom.company_name')}}",
         minLength: 3
      });
   });
</script>
</head>