<?php

use App\Models\LogoEdit;
?>
@php $logodata = LogoEdit::first(); @endphp
<style>
   .bgMiniHeder{
      background: #DBDCDE;
    padding: 6px;
   }
   .minFlex{
      display: flex;
   }
   .disMini{
      margin: 0px 14px;
   }


   .ui-menu-item{
   padding: 5px;
    color: #000;
    background: #f5f3f3;
    max-width: 442px;
    border: 1px solid #ccc;
    text-decoration: none;
 }
</style>
<section class="bgMiniHeder d-none d-sm-block d-sm-none d-md-block">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div style="float: right;margin-right: -19px;">
                  <ul class="minFlex">
                     <li class="disMini">  <a class="text-dark" href="tel:(02) 9052 4957"> <i class="fa fa-phone text-dark" aria-hidden="true"></i> &nbsp; (02) 9052 4957</a></li>
                     <li class="disMini">  <a class="text-dark" href="mailto:info@veye.com.au"> <i class="fa fa-envelope text-dark" aria-hidden="true"></i> &nbsp; info@veye.com.au</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </section>
<nav class="navbar CustomeNavbar navbar-expand-lg navbar-light" aria-label="Offcanvas navbar large">
   
   <!-- d-block -->
   <div class="container d-block">
     
      <div class="row forMobileView ">
         <div class="col-md-2">
            <a class="navbar-brand navbar-brand-logo" href="{{ url('/') }}">
               <img src="{{asset('frontend/assets/images')}}/{{$logodata->header_logo}}" />
            </a>
         </div>
         <div class="col-md-8">
            <div class="firstHead">
               <div class="firstHeadTop"> 
                  <form class="d-none d-sm-block " action="{{url('/search')}}" method="get">
                     <div class="input-group mob__resd mb-3">
                        <input type="text" name="q" class="form-control newTypes search-company-name" placeholder="Search Company Name or ASX Code" aria-label="Search Company Name or ASX Code" aria-describedby="button-addon2" required>
                        <button class="btn btn-Srchg" type="submit" id="button-addon2"> <i class="fa fa-search" aria-hidden="true"></i> Search</button>
                     </div>
                     <!-- <div class="searchBox" role="search">
                     <div class="SearchGrourp" style="display: flex;"> 
                        <input type="text" class="searchTerm" placeholder="Search Company Name or ASX Code" name="q" value=""> 
                     </div>
                     <button class="border-0 bg-transparent" id="SearchByM" type="submit"><i class="fas fa-search"></i></button>
                  </div> -->
                  </form>
                  @if(@Auth::user()->id)
                  <div class="User OnleMedsPuts">
                     <a href="{{url('dashboard')}}">
                        <button class="btn userBtn">
                           <i class="fas fa-user"></i> <span>My Profile</span>
                        </button></a>
                  </div>
                  @else
                  <div class="User d-none d-sm-block">
                     <!-- mobiUesrCss -->
                     <a href="{{url('client-login')}}">
                        <button class="btn userBtn">
                           <i class="fas fa-user"></i> <span>User Login</span>
                        </button></a>
                  </div>
                  @endif

                  <span style="clear:both">

                     <div class="dropdown OnleMedsPuts LangDD d-none d-sm-block">
                        <a class="dropdown-toggle aferscss" href="/" id="Dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                           <img src="{{asset('frontend/assets/images/Australia.svg')}}" />
                           <p class="coutCode">Aus</p>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="Dropdown" style="display: none!important;">
                           <li>
                              <a class="dropdown-item" href="/"><i class="flag-united-kingdom flag"></i>English
                                 <i class="fa fa-check text-success ms-2"></i></a>
                           </li>
                           <li>
                              <hr class="dropdown-divider" />
                           </li>
                          
                        </ul>
                     </div>


                  </span>

                  <!-- <div class="User serchU d-xl-none"> -->
                  <div class="User serchU OnleMedsPuts l d-xxl-none d-xl-none d-xxl-block d-lg-none d-xl-block">
                  <a href="{{url('client-login')}}">
                        <button class="btn userBtn sseUesr">
                           <i class="fa fa-search" aria-hidden="true"></i> <span>User Login</span>
                        </button></a>
                  </div>

                  <button class="navbar-toggler " type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar2" aria-controls="offcanvasNavbar2">
                     <span class="navbar-toggler-icon"></span>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-3"></div>
         <div class="col-md-8">
            @include('frontend.includes.nav')
         </div>
      </div>
   </div>
</nav>

<style type="text/css">
   .btn-Srchg {
      width: 109px;
      border: 1px solid #000000 !important;
      background: #000000 !important;
      color: #fff !important;
      border-radius: 0 3px 3px 0 !important;
      cursor: pointer !important;
      text-align: left !important;
      padding: 0 3px 0 17px !important;
   }

   .newTypes {
      min-width: 336px !important;
    border: 1px solid #23ab66 !important;
   }
</style>