@extends('frontend.layouts.app')
@section('title') Your Detail @endsection
@section('content')
@include('frontend.includes.header_assets')

<section class="ContactPage">
   <div class="container">
      <div class="row">
         <div class="section">
		<div class="container">
			<div class="row">
				<div class="col-xs-0 col-md-4 col-sm-0"></div>
				<div class="col-xs-12 col-md-4 col-sm-0 formBox">
				<div class="formBoxHead">
                  <img style="max-width: 150px;" src="https://veye.com.au/public/frontend/assets/images/16832726091840345373.png"/> 
                     <h4>Enter Details for Account</h4>
                     
                     <!-- <p>Use your credentials to access your account</p> -->
                  </div>
					<div class="card border-success mb-3" style="max-width: 100%;">
						<div class="card-header" style="margin:0;padding: 1px 0 0px 10px;">
							<h3 style="color: #000;">Member Details</h3>
						</div>
						<div class="card-body text-success" style="padding: 0 10px;">
                        <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('paypal.paypalInfo')}}">
                            @csrf
							<input type="hidden" name="amount" value="{{$paymentRecieved}}">								
								<label for="Fname">First Name</label>
								<input class="form-control" type="text" id="Fname" name="fname" placeholder="First Name"><br>
								<label for="Lname">Last Name</label>
								<input class="form-control" type="text" id="Lname" name="lname" placeholder="Last Name"><br>
								<label for="phone">Phone</label>
								<input class="form-control" type="text" id="phone" name="phone" maxlength="10" placeholder="10 Digit Phone No."><br>
								<label for="email">Email</label>
								<input class="form-control" type="email" id="email" name="email" value="" placeholder="Email"><br>
								<label for="post_code">POST CODE</label>
								<input class="form-control" type="text" id="post_code" name="post_code" placeholder="Post Code" maxlength="6"><br>
								
								<input type="text" name="paymentType" value="Paypal" hidden="hidden">
								<button type="submit" class="btn btn-block btn-lg btn-success" style="margin-bottom:10px"> SUBMIT </button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-xs-0 col-md-4 col-sm-0"></div>
			</div>
		</div>
	</div>
      </div>
   </div>
</section>

@endsection