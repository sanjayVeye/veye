@extends('frontend.layouts.app')
@section('title') Checkout Page @endsection
@section('content')
@section('nofollow') <META NAME="robots" CONTENT="noindex,nofollow"> @endsection
<?php

use App\Models\Faq;
use App\Helpers\Helper;
?>
<style>
   .InvestorCNTbox strong {
      font-weight: 700;
      font-size: 20px;
   }

   .esGst {
      font-size: 12px;
      font-weight: 700;
   }


   @media (max-width: 767px) {
      img {
         max-width: 100% !important;
      }
   }

   .stripe-button-el {
      width: 83%;
      height: 42px;
      border-radius: 22px !important;
      border: none;
      color: #fff;
      background: #1275ff;
      background-image: -webkit-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
      background-image: -moz-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
      background-image: -ms-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
      background-image: -o-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
   }

   .InvestorCNTboxLeft h4 {
      font-size: 27px;
      font-weight: 700;
      color: #999;
      padding: 0 0;
      margin: 2px 15px;
      display: inline-block;
      position: relative;
   }

   .InvestorCNTboxLeft h4::after {
      position: absolute;
      border-bottom: 2px solid #e7004b;
      content: "";
      width: 100%;
      top: 17px;
      left: 0;
   }

   .offesEnfsCs {
      font-size: 27px !important;
      margin-top: 20px;
      margin-bottom: 22px !important;
      border-radius: 13px;
      border: 2px solid #ddd;
      text-transform: uppercase;
      padding: 17px 10px;
   }

   .bgcouSty {}

   .h4wsPrice {}

   .PrsGFs {
      font-weight: 100;
   }

   .yoisugS {
      font-size: 14px;
      font-weight: 200;
      margin-bottom: 7px;
      color: gray;
   }
</style>
<style>
   .bblandFt{font-size:28px !important}
   </style>
@php $faqdata = Faq::where('deleted_at',null)->get(); @endphp
<section class="investerReport1" style="margin-top: -42px !important;">
   <div class="container">
      @if($currentDate < @$sucriptionEndDate->offer_end)
         <img src="{{asset('uploads/offer_image')}}/{{$sucriptionEndDate->image}}" alt="">
         @endif
         <div class="investerReportInn">

            <div class="row">

               <!--  <div class="col-md-12">
               <div class="InvestorHeading">
                  <h1 class="text-dark ">{{$title}}</h1>
                  <center><div class="unlsGreb"></div></center>
               </div>
            </div> -->

               <div class="col-md-8">
                  <div class="InvestorCNTbox CheFes carBoxyShaf">
                     {!! @$sucriptionEndDate->description !!}
                     <!-- <h1 class="DivInRes ">Dividend Investor Report</h1>
                     <p>Each week, we carefully analyze and feature a company that presents an ideal blend of growth potential and dividend yield.
We understand the importance of including stable, reliable "cash cow" companies alongside more speculative "rising star" or "question mark" ventures that offer potentially higher risk /return.
Our aim is to assist you in selecting a company that offers a good regular dividend and a lower-risk capital growth opportunity.</p>

                     <div class="whtwedoCsss">
                        <h2>What You Get ? </h2>
                        <ul>
                           <li>
                              <i class="fas fa-check checkBoxst"></i>
                              Daily Analysis to keep up with the latest on what's hot and what's not. Our expert analysts provide insights on market trends and developments, guiding subscribers to make informed decisions.
                           </li>
                           <li>
                              <i class="fas fa-check checkBoxst"></i>
                              Buy, sell and Hold recommendations.
                           </li>
                           <li>
                              <i class="fas fa-check checkBoxst"></i>
                              Access to the past recommendations and past reports.
                           </li>
                        </ul>
                        <h2>Report Frequency: <span>Weekly</span></h2>
                        <h2>Publishing Time: <span>Every Monday</span></h2>
                     </div> -->

                  </div>
               </div>
               <!-- <div class="col-md-1"> </div> -->

               <div class="col-md-4">

                  <div class="InvestorCNTboxLeft respoCheck ">
                     <h3 class="h4wsPrice"> <span class="bblandFt">

                           <!-- <div class="d-xxl-none"> <span>
                      <img src="https://www.linkpicture.com/q/pLinks.png" style="max-width: 100px !important;">
                      </span> </div> -->
                           <div class="yoisugS d-none d-sm-block">Price</div>
                           <span class="d-xxl-none d-xl-none d-xxl-block PrsGFs">Price | </span>
                           ${{$price}} /
                        </span> <span class="bblandFt" >@if(@$sucriptionEndDate->plan_information) {{@$sucriptionEndDate->plan_information}} @else {{$planInformation}} @endif</span> <span class="esGst">(Incl. of GST)</span> </h3>

                     @if($currentDate < @$sucriptionEndDate->offer_end)
                        <h4 class="cardPricingCardBox">{{$sucriptionEndDate->plan_price}} / {{$sucriptionEndDate->plan_information}} </h4>
                        <div class="price_content">
                           <h3 class="offesEnfsCs">
                              <div>Offer ends in:</div> <span class="span-color text-danger bgcouSty" id="timer-count"></span></strong>
                           </h3>
                           <script>
                              // Set the date we're counting down to
                              var countDownDate = new Date("<?php echo $sucriptionEndDate->offer_end; ?>").getTime();

                              // Update the count down every 1 second
                              var x = setInterval(function() {

                                 // Get today's date and time
                                 var now = new Date().getTime();

                                 // Find the distance between now and the count down date
                                 var distance = countDownDate - now;

                                 // Time calculations for days, hours, minutes and seconds
                                 var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                 var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                 var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                 var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                 // Output the result in an element with id="timer-count"
                                 document.getElementById("timer-count").innerHTML = (days > 0 ? (" " + days + " day" + (days > 1 ? "s" : '')) : '') + " " + hours + "h <span class='blink_me'>:</span>" +
                                    minutes + "m <span class='blink_me'>:</span>" + seconds + "s ";

                                 // If the count down is over, write some text
                                 if (distance < 0) {
                                    clearInterval(x);
                                    document.getElementById("timer-count").innerHTML = "EXPIRED";
                                 }
                              }, 1000);
                           </script>
                        </div>
                        @endif

                        <!-- <h3>Duration : <span class="bblandFt">{{$months}}</span></h3> -->
                        <div class="TermAndCondition">
                           <p><img src="{{asset('frontend/assets/images/lock.png')}}" />
                              By providing your details, you agree to Veye’s <a href="/pages/terms-conditions">Terms & Conditions</a> and <a href="/pages/privacy-policy">Privacy Policy</a> and to receive marketing offers by email, text message or phone call from us or our agents until you opt out. </p>
                        </div>
                        <div class="paymentMathod">
                           <!-- <form action="{{route('yourPaymentStatus')}}" method="POST"> -->
                              @php  $prices = str_replace('$', '', $price); @endphp
                           <form role="form" method="POST" id="paymentForm" action="{{url('/payment')}}">
                              @csrf
                              <input type='hidden' name='item_name' value='{{$title}}'>
                              <input type="text" name="amount" value="{{$price}}" hidden="hidden">
                              <input type="hidden" name="plan_id" value="{{$months}}">
                              <input type="hidden" name="report_id" value="{{@$sucriptionEndDate->reports_id}}">

                              <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="{{decrypt(Helper::getGatewayData()->pkey)}}" data-amount="{{$price}}00" data-name="Veye Pty Ltd" data-label="Pay With Card" data-description="{{$months}}.Subscription Plan" data-image="https://veye.com.au/assets/img/veyeImg.svg" data-locale="auto" data-currency="AUD"></script>
                              <!-- @if(@Auth::user()->id)
                              <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="pk_live_g2l16dmM61TVci0Q5NQ0g2Rs" data-amount="{{$price}}00" data-name="Veye Pty Ltd" data-label="Pay With Card" data-description="{{$months}}.Subscription Plan" data-image="https://veye.com.au/assets/img/veyeImg.svg" data-locale="auto" data-currency="aud"></script>
                              @else
                              <button type="button" class="stripe-button-el" data-toggle="modal" data-target="#loginModal">Pay With Card</button>
                              @endif -->

                              <!--  <button class="btn PayCard">Pay With Card</button> -->

                           </form>
                           <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                              <input type='hidden' name='business' value='dheeru.r@veye.com.au'>
                              <input type='hidden' name='item_name' value='{{@$sucriptionEndDate->reports_id}}'>
                              <input type='hidden' name='item_name' value='{{$title}}'>

                              <input type='hidden' name='amount' value="{{$prices}}">
                              <input type='hidden' name='no_shipping' value='1'>
                              <input type='hidden' name='currency_code' value='AUD'>

                              <input type='hidden' name='notify_url' value="{{route('notify')}}">
                              <input type='hidden' name='cancel_return' value="{{route('cancel')}}">
                              <input type='hidden' name='return' value="{{route('return',$transaction_id)}}">
                              <input type="hidden" name="custom" value="{{ $transaction_id }}">
                              <input type="hidden" name="rm" value="2">
                              <input type="hidden" name="cmd" value="_xclick">
                              <button type="submit" class="btn PayPal" name="button" name="pay_now" id="pay_now">Check out with <img src="{{asset('frontend/assets/images/paypal.png')}}" /></button>
                              <!-- @if(@Auth::user()->id)
                              <button type="submit" class="btn PayPal" name="button" name="pay_now" id="pay_now">Check out with <img src="{{asset('frontend/assets/images/paypal.png')}}" /></button>
                              @else
                              <button type="button" class="btn PayPal" data-toggle="modal" data-target="#loginModal">Check out with <img src="{{asset('frontend/assets/images/paypal.png')}}" /></button>
                              @endif -->
                           </form>

                           <!-- Stripe Payment -->
                           <div class="container" style="padding: 0px;">
                              <!-- Button to Open the Modal -->
                              <!-- @if(@Auth::user()->id)
                        <button type="button" class="btn PayPal" data-toggle="modal" data-target="#myModal">
                           Pay with Stripe
                        </button>
                        @else
                        <button type="button" class="btn PayPal" data-toggle="modal" data-target="#loginModal">
                           Pay with Stripe
                        </button>
                        @endif -->

                              <!-- The Modal -->
                              <div class="modal" id="myModal">
                                 <div class="modal-dialog">
                                    <div class="modal-content">

                                       <!-- Modal Header -->
                                       <div class="modal-header">
                                          <h4 class="modal-title">Stripe Payment</h4>
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       </div>

                                       <!-- Modal body -->
                                       <div class="modal-body">
                                          <!-- <form role="form" method="POST" id="paymentForm" action="{{ url('/payment')}}"> -->
                                          <form role="form" method="POST" id="paymentForm" action="{{route('store.storePaymentDetails')}}">
                                             @csrf
                                             <input type='hidden' name='item_name' value='{{$title}}'>
                                             <input type="text" name="amount" value="{{$price}}" hidden="hidden">
                                             <input type="hidden" name="plan_id" value="{{$months}}">
                                             <input type="hidden" name="report_id" value="{{@$sucriptionEndDate->reports_id}}">
                                             <div class="form-group">
                                                <label class="usLabe" for="username">Full name (on the card)</label>
                                                <input type="text" class="form-control msConsyInput" name="fullName" placeholder="Full Name">
                                             </div>
                                             <div class="form-group">
                                                <label class="usLabe" for="cardNumber">Card number</label>
                                                <div class="input-group">
                                                   <input type="text" class="form-control msConsyInput" name="cardNumber" placeholder="Card Number">
                                                   <div class="input-group-append">
                                                      <span class="input-group-text text-muted">
                                                         <i class="fab fa-cc-visa fa-lg pr-1"></i>
                                                         <i class="fab fa-cc-amex fa-lg pr-1"></i>
                                                         <i class="fab fa-cc-mastercard fa-lg"></i>
                                                      </span>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-sm-8">
                                                   <div class="form-group">
                                                      <label class="usLabe"><span class="hidden-xs">Expiration</span> </label>
                                                      <div class="input-group">
                                                         <select class="form-control" name="month">
                                                            <option value="">MM</option>
                                                            @foreach(range(1, 12) as $month)
                                                            <option value="{{$month}}">{{$month}}</option>
                                                            @endforeach
                                                         </select>
                                                         <select class="form-control" name="year">
                                                            <option value="">YYYY</option>
                                                            @foreach(range(date('Y'), date('Y') + 10) as $year)
                                                            <option value="{{$year}}">{{$year}}</option>
                                                            @endforeach
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-4">
                                                   <div class="form-group">
                                                      <label class="usLabe" data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
                                                      <input type="number" class="form-control" placeholder="CVV" name="cvv">
                                                   </div>
                                                </div>
                                             </div>

                                             <!-- Modal footer -->
                                             <div class="modal-footer" style="border-top: inherit !important;">
                                                <button class="subscribe btn btn__smsty btn-primary btn-block" type="submit" style="background-color: #1e6916;"> Pay Now </button>
                                                <button type="button" class="btn btn__smsty btn-danger" data-dismiss="modal">Close</button>
                                             </div>
                                          </form>
                                       </div>

                                    </div>
                                 </div>
                              </div>









                              <!-- The Modal -->
                              <div class="modal" id="loginModal">
                                 <div class="modal-dialog">
                                    <div class="modal-content">

                                       <!-- Modal Header -->
                                       <div class="modal-header">
                                          <h4 class="modal-title">Client Login</h4>
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       </div>

                                       <!-- Modal body -->
                                       <div class="modal-body">
                                          <form method="post" enctype="multipart/form-data" action="{{ route('registration.login.match') }}">
                                             @csrf
                                             <input type="hidden" name="login_type" value="1">
                                             <div class="form-floating mb-3">
                                                <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Your Email">
                                                <label for="floatingInput">Email</label>
                                             </div>
                                             <div class="form-floating">
                                                <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
                                                <label for="floatingPassword">Password</label>
                                             </div>
                                             <div class="acceptVeye">
                                                <label>
                                                   <input type="checkbox" name="accept" value="1" /> <span>Remember</span>
                                                </label>
                                             </div>

                                             <!-- Modal footer -->
                                             <div class="modal-footer">
                                                <button class="subscribe btn btn-primary btn-block" type="submit" style="background-color: #1e6916;"> Login </button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                             </div>
                                             <div class="ForgotPassBox d-flex align-items-start justify-content-center pb-4">
                                                <div>
                                                   <p>Not an remember?</p>
                                                   <a href="{{ route('forgot.password') }}" class="ForgotPass">Forgot password?</a>
                                                </div>
                                                <a href="{{url('registration')}}" class="VeyeRegister text-white"> Register</a>
                                             </div>
                                          </form>
                                       </div>

                                    </div>
                                 </div>
                              </div>

                           </div>
                           <!-- End Stripe Payment -->

                           <ul>
                              <li>
                                 <img src="{{asset('frontend/assets/images/visa.png')}}" />
                              </li>
                              <li>
                                 <img src="{{asset('frontend/assets/images/MasterCard_Logo.svg.png')}}" />
                              </li>
                              <li>
                                 <img src="{{asset('frontend/assets/images/american-express.png')}}" />
                              </li>
                              <li>
                                 <img src="{{asset('frontend/assets/images/google-pay.png')}}" />
                              </li>
                           </ul>
                        </div>
                  </div>
               </div>

            </div>
         </div>
   </div>
</section>



<section class="abbsi__top m-0" style="margin-top: 36px !important;">
   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <div class="VeyeOffers">
               <h3 class="vase__offers">{{$homesdata->title}}
               </h3>
               <p>{{$homesdata->details}}</p>
               <button class="veyOffer__subscriber">Subscribe Now</button>
            </div>
         </div>
         <div class="col-md-6">
            <div class="recomdedTable ">
               <h3 class="text-dark smp__fonts">Past Recommendations</h3>
               <table class="table table-striped shadow past__border">
                  <thead>
                     <tr>
                        <th scope="col">Code</th>
                        <th scope="col">Avg. Buy Price</th>
                        <th scope="col">Sell Price</th>
                        <th scope="col">Gain/Loss</th>
                     </tr>
                  </thead>
                  <tbody class="table-group-divider">
                     @foreach ($resultPast as $datadaily)
                     <tr>
                        <th scope="row"> <span class="bold__tablefonts">{{$datadaily->company}}</span> </th>
                        <td>${{$datadaily->buy_price}}</td>
                        <td>${{$datadaily->sell_price}}</td>
                        @if(0 < $datadaily->gains_losses)
                           <td class="NumGreen">{{$datadaily->gains_losses}}% <i class="fas fa-caret-up"></i></td>
                           @else
                           <td class="NumRed">{{$datadaily->gains_losses}}% <i class="fas fa-caret-down"></i></td>
                           @endif
                     </tr>
                     @endforeach

                  </tbody>
               </table>
               <div class="main__Topso"></div>
               <a class="AllExplore" href="{{url('pages/past-recommendations')}}">Explore More <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="FAQ_Section" style="margin-top: 36px !important;">
   <div class="container">
      <h4 class="FAQ_heading">Frequently Asked Questions</h4>
      <div class="row">
         @foreach ($faqdata as $key => $data)
         <div class="col-md-6">
            <div class="customAccordian">
               <div class="accordion" id="accordionExample">
                  <div class="accordion-item">
                     <h4 class="accordion-header" id="heading-{{ $key }}">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{ $key }}" aria-expanded="false" aria-controls="collapse-{{ $key }}">
                           {{$data->title}}
                        </button>
                     </h4>
                     <div id="collapse-{{ $key }}" class="accordion-collapse collapse" aria-labelledby="heading-{{ $key }}" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                           {{$data->details}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         @endforeach
      </div>
   </div>
</section>
<section class="WhatSay tesy__mest SlideDotsCustome">
   <div class="container  ">
      <h4 style="text-align:center !important"> <span class="whats-css saaAllRepla">What Our Clients Say About Us? </span> <span class="head__tsxt"></span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <div class=" slider-nav3">
         @foreach ($testmanag as $reportdata)
         <div>
            <div class="WhatSaySlide">
               <div class="CustomerData">
                  <h5>{{$reportdata->title}}</h5>
                  <div class="padding__tops"></div>

                  <p class="applYPTgs">{{$reportdata->desciption}}</p>
                  <img src="{{asset('uploads/Test_Management')}}/{{$reportdata->image}}" />
               </div>
            </div>
         </div>
         @endforeach
      </div>
   </div>
</section>
<script>
   $(document).ready(function() {
      $("#btn").click(function() {
         alert("Login First!");
      });
   });
   $(document).ready(function() {
      $("#paynow").click(function() {
         alert("Login First!");
      });
   });
   $(document).ready(function() {
      $("#paycard").click(function() {
         alert("Login First!");
      });
   });
</script>
@endsection