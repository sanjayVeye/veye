<?php
use App\Models\pages;
?>
@php $pagesdata = pages::where('deleted_at',NULL)->where('tag','term-conditions')->first(); @endphp
@extends('frontend.layouts.app')
@section('title') {{$pagesdata->meta_title}} @endsection
@section('description') {{$pagesdata->meta_description}} @endsection
@section('keywords') {{$pagesdata->meta_keywords}} @endsection
@section('canonical')https://veye.com.au/pages/terms-conditions @endsection
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@section('content')
@include('frontend.includes.header_assets')


<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-6">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Terms and Conditions</h1>
               <div class="bd-example">
                  <nav aria-label="breadcrumb ">
                    <!-- <ol class="breadcrumb m-0">
                        <li><a href="">Home / </a></li>
                        <li><a href="">Terms and Conditions</a></li>
                     </ol>-->
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="PrivacyPolicy">
      {!! @$pagesdata->template !!}
      </div>
   </div>
</section>
@endsection