@extends('frontend.layouts.app')
@section('title') Airlines Page @endsection
@section('content')
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@include('frontend.includes.header_assets')
<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>Sector Specific</h1>
         <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="">Home / </a></li>
                  <li><a href="">sector specific</a></li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</section>
<section class="WebinarPG">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="sectorSpecific">
               <div class="sectorSpecificHead">
                  <h2>Airlines</h2>
               </div>
               <p>Lorem ispum dolor sit lorem ispum dolor sit</p>
               <div class="sectorSpecificBody">

                  <div class="sectorSpecificBtn">
                     Galaxy Resources
                  </div>
                  <div class="sectorSpecificBtn">
                     Metals X Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Pilbara Minerals Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Avanco Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Maca Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Iluka Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     PPK Group Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     KIRKLAND LAKE GOLD LTD
                  </div>
                  <div class="sectorSpecificBtn">
                     Empire Energy Group Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Navarre Minerals Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Highfield Resources Limited
                  </div>
               </div>



            </div>


            <div class="sectorSpecific">
               <div class="sectorSpecificHead">
                  <h2>Airlines</h2>
               </div>
               <p>Lorem ispum dolor sit lorem ispum dolor sit</p>
               <div class="sectorSpecificBody">

                  <div class="sectorSpecificBtn">
                     Galaxy Resources
                  </div>
                  <div class="sectorSpecificBtn">
                     Metals X Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Pilbara Minerals Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Avanco Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Maca Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Iluka Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     PPK Group Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     KIRKLAND LAKE GOLD LTD
                  </div>
                  <div class="sectorSpecificBtn">
                     Empire Energy Group Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Navarre Minerals Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Highfield Resources Limited
                  </div>
               </div>



            </div>

            <div class="sectorSpecific">
               <div class="sectorSpecificHead">
                  <h2>Airlines</h2>
               </div>
               <p>Lorem ispum dolor sit lorem ispum dolor sit</p>
               <div class="sectorSpecificBody">

                  <div class="sectorSpecificBtn">
                     Galaxy Resources
                  </div>
                  <div class="sectorSpecificBtn">
                     Metals X Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Pilbara Minerals Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Avanco Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Maca Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Iluka Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     PPK Group Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     KIRKLAND LAKE GOLD LTD
                  </div>
                  <div class="sectorSpecificBtn">
                     Empire Energy Group Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Navarre Minerals Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Highfield Resources Limited
                  </div>
               </div>



            </div>

            <div class="sectorSpecific">
               <div class="sectorSpecificHead">
                  <h2>Airlines</h2>
               </div>
               <p>Lorem ispum dolor sit lorem ispum dolor sit</p>
               <div class="sectorSpecificBody">

                  <div class="sectorSpecificBtn">
                     Galaxy Resources
                  </div>
                  <div class="sectorSpecificBtn">
                     Metals X Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Pilbara Minerals Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Avanco Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Maca Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     Iluka Resources Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     PPK Group Ltd
                  </div>
                  <div class="sectorSpecificBtn">
                     KIRKLAND LAKE GOLD LTD
                  </div>
                  <div class="sectorSpecificBtn">
                     Empire Energy Group Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     De Grey Mining Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Navarre Minerals Limited
                  </div>
                  <div class="sectorSpecificBtn">
                     Highfield Resources Limited
                  </div>
               </div>



            </div>
         </div>
      </div>
   </div>
</section>
@endsection