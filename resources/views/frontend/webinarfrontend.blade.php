@extends('frontend.layouts.app')
@section('title') Free ASX Webinar for Beginners - ASX Investing Webinars @endsection
@section('description') Join our free ASX webinar for beginners and gain essential knowledge to navigate the stock market. Learn investing strategies from experts. @endsection
@section('keywords')  @endsection
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@section('content')
@section('canonical')https://veye.com.au/webinarfrontend @endsection
<style>
   .card__sbody {
      background: #fff;
      padding: 4px;
   }
   .layerbgsVideo{
     background: #02302ce8;
    position: absolute;
    padding: 10px;
    width: 389px;
    height: 289px;
    z-index: 9;
    margin-left: -16px;
    margin-top: -14px;
    border-radius: 14px;
    border: 2px solid #64ad8b;
   }
</style>
<main>
   <section class="AboutMain">
      <div class="container">
         <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
            <h1>Webinar</h1>
              <div class="bd-example">
                        <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
                    </div>
            <!-- <div class="bd-example">
               <nav aria-label="breadcrumb ">
                  <ol class="breadcrumb m-0">
                     <li><a href="">Home / </a></li>
                     <li><a href="">Editorial</a></li>
                  </ol>
               </nav>
            </div> -->
         </div>
      </div>
   </section>
   <section class="WebinarPG">
      <div class="container">
         <div class="row row-cols-2">
            @foreach ($linkwebinar as $reportdata)
            <div class="col-md-4">
               <div class="WebinarPGBox webCardsc">
               @if(@Auth::user()->id)
               @else
                  <!-- <button type="button" class="btn PayPal " data-toggle="modal" data-target="#myModal"> <span class="regWebsna">Register Now </span>  -->
                  <div class="layerbgsVideo"></div>
                  @endif
                   
                  <div class=" card__sbody" style="position:relative;">
                        <div class="WebinarPGBoxImg videoGaTops"> 
                           
                           <iframe width="100%" height="150" src="{{$reportdata->path}}" title="Laravel Service container vs Service provider | laravel service container and providers [Hindi]" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                        </div>
                        <p>{{$reportdata->title}}</p> 
                     </div>
                  </button>
               </div>
            </div>
            @endforeach
            <div class="row">
               <div class="col-md-12">
                  <div class="paginationCdss">
                     <div class="bd-example-snippet bd-code-snippet">
                        <div class="bd-example">
                           <nav aria-label="Another pagination example">
                              <ul class="pagination pagination-lg flex-wrap">
                                 <li class="page-item disabled">
                                    {{ $linkwebinar->links( "pagination::bootstrap-4") }}
                                 </li>

                              </ul>
                           </nav>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>


   <!-- Modal body -->
   <div class="modal" id="myModal">
      <div class="modal-dialog">
         <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
         <h4 class="modal-title">Register Now</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('webinarfrontend-register.store')}}">
                  @csrf
                  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3">
                     <div class="col-md">
                        <div class="form-floating mb-3">
                           <input type="text" name="name" class="form-control" id="floatingInput" placeholder="First Name" value="{{old('name')}}">
                           <label for="floatingInput">First Name</label>
                           @if ($errors->has('name'))
                           <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                           </span>
                           @endif
                        </div>
                     </div>
                     <div class="col-md">
                        <div class="form-floating mb-3">
                           <input type="text" name="last_name" class="form-control" id="floatingInput" placeholder="Last Name" value="{{old('last_name')}}">
                           <label for="floatingInput">Last Name</label>
                           @if ($errors->has('last_name'))
                           <span class="help-block">
                              <strong>{{ $errors->first('last_name') }}</strong>
                           </span>
                           @endif
                        </div>
                     </div>
                     <div class="col-md">
                        <div class="form-floating mb-3">
                           <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Email" value="{{old('email')}}">
                           <label for="floatingInput">Email</label>
                           @if ($errors->has('email'))
                           <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                           </span>
                           @endif
                        </div>
                     </div>

                     <div class="col-md">
                        <div class="form-floating mb-3">
                           <input type="text" name="phone" class="form-control" id="floatingInput" placeholder="Phone" value="{{old('phone')}}">
                           <label for="floatingInput">Phone</label>
                           @if ($errors->has('phone'))
                           <span class="help-block">
                              <strong>{{ $errors->first('phone') }}</strong>
                           </span>
                           @endif
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-floating mb-3">
                           <input type="text" name="post_code" class="form-control" id="floatingInput" placeholder="Post Code" value="{{old('post_code')}}">
                           <label for="floatingInput">Post Code</label>
                           @if ($errors->has('post_code'))
                           <span class="help-block">
                              <strong>{{ $errors->first('post_code') }}</strong>
                           </span>
                           @endif
                        </div>
                     </div>
                     <div class="col-md-12">
                        <p>
                          <img draggable="false" class="emoji" alt=":lock:" src="/frontend/assets/images/lock.svg" style="width: 13px; margin-top: -5px;"> By providing your details, you agree to Veye’s
                        <a style="color: green;" href="/pages/terms-conditions"> Terms &amp; Conditions</a> and <a style="color: green;" href="/pages/privacy-policy"> Privacy Policy</a>. and to receive
                        marketing offers by email, text message or phone call from us or our agents until you opt
                        out.
                        </p>
                     </div>

                  </div>

                  <!-- Modal footer -->
                  <div class="modal-footer">
                     <button class="subscribe btn btn-primary btn-block" type="submit" style="background-color: #1e6916;"> Pay Now </button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
               </form>
            </div>

         </div>
      </div>
   </div>

   <!--End Model -->

   <br>
   <section class="webinarRegister">
      <div class="container">
         <div class="row">
            <div class="col-md-8 offset-md-2">
               <div class="contactForm shadow past__border">
                  <h3>Register Now</h3>
                  <h4>to get informed when we publish new webinar</h4>
                  <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('webinarfrontend-register.store')}}">
                     @csrf
                     <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3">
                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="text" name="name" class="form-control" id="floatingInput" placeholder="First Name" value="{{old('name')}}">
                              <label for="floatingInput">First Name</label>
                              @if ($errors->has('name'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="text" name="last_name" class="form-control" id="floatingInput" placeholder="Last Name" value="{{old('last_name')}}">
                              <label for="floatingInput">Last Name</label>
                              @if ($errors->has('last_name'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('last_name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Email" value="{{old('email')}}">
                              <label for="floatingInput">Email</label>
                              @if ($errors->has('email'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('email') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>

                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="text" name="phone" class="form-control" id="floatingInput" placeholder="Phone" value="{{old('phone')}}">
                              <label for="floatingInput">Phone</label>
                              @if ($errors->has('phone'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-floating mb-3">
                              <input type="text" name="post_code" class="form-control" id="floatingInput" placeholder="Post Code" value="{{old('post_code')}}">
                              <label for="floatingInput">Post Code</label>
                              @if ($errors->has('post_code'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('post_code') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md-12">
                           <p>
                          <img draggable="false" class="emoji" alt=":lock:" src="https://veye.com.au/public/frontend/assets/images/lock.svg" style="width: 13px; margin-top: -5px;"> By providing your details, you agree to Veye’s
                        <a style="color: green;" href="/pages/terms-conditions"> Terms &amp; Conditions</a> and <a style="color: green;" href="/pages/privacy-policy"> Privacy Policy</a> and to receive
                        marketing offers by email, text message or phone call from us or our agents until you opt
                        out.
                        </p>
                        </div>
                        <div class="col-md-12">
                           <button class="btn btn-primary" type="submit">Register Now</button>
                        </div>
                     </div>
                     <!-- </form> -->
               </div>
            </div>
         </div>
      </div>
   </section>
   @endsection