<?php
use App\Models\pages;
?>
@php $pagesdata = pages::where('deleted_at',NULL)->where('tag','privacy-policies')->first(); @endphp

@extends('frontend.layouts.app')
@section('title') Privacy Policy | Veye Pty Ltd @endsection
@section('description') Learn how Veye Pty Ltd protects your privacy. Read our Privacy Policy to understand how we handle your personal information and ensure data security. @endsection
@section('keywords')  @endsection
@section('content')
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@section('canonical')https://veye.com.au/pages/privacy-policy @endsection
@include('frontend.includes.header_assets')

<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-6">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Privacy Policy</h1>
               <div class="bd-example">
                  <nav aria-label="breadcrumb ">
                    <!-- <ol class="breadcrumb m-0">
                        <li><a href="">Home / </a></li>
                        <li><a href="">Privacy Policy</a></li>
                     </ol>-->
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="PrivacyPolicy">
      {!! @$pagesdata->template !!}
      </div>
   </div>
</section>
@endsection