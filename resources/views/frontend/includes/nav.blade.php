<style type="text/css">
   .dropdown-toggle::after { 
    font-size: 14px !important; 
}
</style>

<?php

use App\Models\FooterMenu;
use App\Models\Reports;
use App\Models\LogoEdit;
?>

<div class="offcanvas offcanvas-start MyManu text-bg-light" tabindex="-1" id="offcanvasNavbar2" aria-labelledby="offcanvasNavbar2Label">
   <div class="offcanvas-header">
      <a class="navbar-brand navbar-brand-logo" href="/">
         <img src="https://veye.com.au/assets/img/veye-logo.svg" />
      </a>
      <button type="button" class="btn-close btn-close-black" data-bs-dismiss="offcanvas" aria-label="Close"></button>
   </div>
   <div class="offcanvas-body justify-content-end" style="padding:0px 63px;">
      <ul class="navbar-nav navPading">
         <li class="nav-item">
            <a href="{{url('/')}}" class="nav-link">
               Home
            </a>
         </li> 

         @foreach(Help::frontend_menues() as $menu)
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> {{$menu->title}} </a>
            @if($menu->childs)
            <ul class="dropdown-menu tisMneus" aria-labelledby="navbarDropdownMenuLink">
               @foreach($menu->childs as $child_menu)
               <li><a class="dropdown-item" href="{{route('manage.menues',[$menu->slug,$child_menu->slug])}}">{{$child_menu->title}}</a></li>
               @endforeach
            </ul>
            @endif
         </li>
         @endforeach



         <li class="nav-item">
            <a href="{{url('/subscribe')}}" class="nav-link">
               Subscribe
            </a>
         </li>

         <li class="nav-item">
            <a href="{{url('/contact-us')}}" class="nav-link">
               Contact
            </a>
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More </a>
            <ul class="dropdown-menu tisMneus" aria-labelledby="navbarDropdownMenuLink">

               <li class="nav-item">
                  <a href="{{url('/editorial')}}" class="nav-link">
                     Editorial
                  </a>
               </li>
               <li class="nav-item">
                  <a href="{{url('/webinarfrontend')}}" class="nav-link">
                     Webinar
                  </a>
               </li>
               <li class="nav-item">
                  <a href="{{url('pages/about-us')}}" class="nav-link">
                  About Us
                  </a>
               </li>
                @php $footerMenuData=FooterMenu::where('type',3)->where('deleted_at',NULL)->orderBy('order','ASC')->get(); @endphp
                     @foreach ($footerMenuData as $reportdata)
                     <li class="nav-item"><a href="{{url('pages')}}/{{$reportdata->slug}}" class="nav-link">{{$reportdata->title}}</a></li>
                     @endforeach
              
            </ul>
         </li>
      </ul>
      <div class="RightBoxIN-Menu " >

         <div class="cutomr__reviw">
            <p class="cust__tsxt">Customer Reviews</p>
            <p>
               <img class="star__bos" src="{{asset('frontend/assets/images/star__full.jpg')}}">
            </p>
            <p class="fous__sey">
               <span class="bbo">4.6</span> from <a href="http://www.productreview.com.au/listings/veye"> 72 reviews </a>
            </p>
            <p class="spans__rewviw">
               <a href="/">
                  <img class="sffes__flgs" src="{{asset('frontend/assets/images/pr__image.jpg')}}">
               </a>
            </p>
         </div>

         <div class="loagsMyTs d-xxl-none d-xl-none d-xxl-block d-lg-none d-xl-block">
         <a class="btn GetStarted" href="{{url('client-login')}}">Login Here</a>
         </div>

      </div>
   </div>
</div>