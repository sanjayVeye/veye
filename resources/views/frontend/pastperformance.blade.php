@extends('frontend.layouts.app')
@section('title') Past Performance  @endsection
@section('content')
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@include('frontend.includes.header_assets')
<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>Past Performance</h1>
         <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="">Home / </a></li>
                  <li><a href="">Past Performance</a></li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</section>

<section class="PastRecommend">
   <div class="container">
      <ul>
         <li>Below we present the performance for stocks in our hypothetical income portfolio, which consists of
            recommendations from Daily Analysis, Stock of the week and Dividend stocks. All opened and closed
            recommendations have been included in it.</li>
         <li>Our Income Portfolio is a hypothetical portfolio and consists of stocks that have either paid dividends
            during the aforesaid period or analysed as good income stocks as per their potential to pay dividends in
            the near future</li>
         <li>The recommendations are right since our date of inception i.e. from 8th January 2018 to 28th December
            2018 and from 1st January 2019 to 31st December 2019.
         </li>
         <li>Our recommendations are from Daily Analysis, Stock of the week and Dividend stocks by using latest
            price at the time of publication. Our performance figures are hypothetical, clear and transparent. The
            performance has been verified by an independent accounting firm. For a detailed explanation, Read the
            "Performance Methodology" page.</li>
      </ul>
   </div>
</section>

<section class="chartdivBox">
   <div class="container">
      <div class="chartdivBoxINN">
         <h3>Veye Portfolio Performance </h3>
         <div id="chartdiv"></div>
      </div>
   </div>
</section>

<section class="PastRecommend02">
   <div class="container">
      <div class="recomdedTable ">

         <table class="table table-striped shadow past__border">
            <thead>
               <tr>
                  <th scope="col">Code</th>
                  <th scope="col">Avg. Buy Price</th>
                  <th scope="col">Sell Price</th>
                  <th scope="col">Gain/Loss</th>
               </tr>
            </thead>
            <tbody class="table-group-divider">
               @foreach ($result as $item)
               <tr>
                  <th scope="row"> <span class="bold__tablefonts">{{$item->company}}</span> </th>
                  <td>{{$item->buy_price}}</td>
                  <td>{{$item->sell_price}}</td>
                  <td class="NumGreen">{{$item->gains_losses}} <i class="fas fa-caret-up"></i></td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
      <div class="bd-example-snippet bd-code-snippet float-end">
         <div class="bd-example">
            <nav aria-label="Another pagination example">
               <ul class="pagination pagination-lg flex-wrap">
                  <li class="page-item disabled">
                     <a class="page-link"><svg class="svg-inline--fa fa-long-arrow-alt-left fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                           <path fill="currentColor" d="M134.059 296H436c6.627 0 12-5.373 12-12v-56c0-6.627-5.373-12-12-12H134.059v-46.059c0-21.382-25.851-32.09-40.971-16.971L7.029 239.029c-9.373 9.373-9.373 24.569 0 33.941l86.059 86.059c15.119 15.119 40.971 4.411 40.971-16.971V296z">
                           </path>
                        </svg><!-- <i class="fas fa-long-arrow-alt-left"></i> Font Awesome fontawesome.com --></a>
                  </li>
                  <li class="page-item active"><a class="page-link" href="#">1</a></li>
                  <li class="page-item" aria-current="page">
                     <a class="page-link" href="#">2</a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">4</a></li>
                  <li class="page-item"><a class="page-link" href="#">5</a></li>
                  <li class="page-item"><a class="page-link" href="#">6</a></li>
                  <li class="page-item">
                     <a class="page-link" href="#"><svg class="svg-inline--fa fa-long-arrow-alt-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                           <path fill="currentColor" d="M313.941 216H12c-6.627 0-12 5.373-12 12v56c0 6.627 5.373 12 12 12h301.941v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.569 0-33.941l-86.059-86.059c-15.119-15.119-40.971-4.411-40.971 16.971V216z">
                           </path>
                        </svg><!-- <i class="fas fa-long-arrow-alt-right"></i> Font Awesome fontawesome.com --></a>
                  </li>
               </ul>
            </nav>
         </div>
      </div>
   </div>
</section>
@endsection