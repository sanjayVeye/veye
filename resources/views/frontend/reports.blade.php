@extends('frontend.layouts.app')
@section('title') Reports @endsection
@section('content')
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@include('frontend.includes.header_assets')
<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-6">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Reports</h1>
               <div class="bd-example">
                  <nav aria-label="breadcrumb ">
                     <ol class="breadcrumb m-0">
                        <li><a href="">Home / </a></li>
                        <li><a href="">Report / </a></li>
                        <li><a href="">Healthcare Panorama </a></li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"><i class="fas fa-long-arrow-alt-left"></i> Go back</a>
         </div>
      </div>
      <div class="row">
         <div class="col-md-8">
            <div class="col">
               <div class="card ReportCard shadow">
                  <div class="row g-0">
                     <div class="col-md-4">
                        <svg class="bd-placeholder-img" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image" preserveAspectRatio="xMidYMid slice" focusable="false">
                           <title>Placeholder</title>
                           <rect width="100%" height="100%" fill="#868e96" /><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                        </svg>
                     </div>
                     <div class="col-md-8">
                        <div class="card-body">
                           <h5 class="card-title">TEAM VEYE | 06 DEC 2022</h5>
                           <h5 class="card-Heading">This is a wider card with supporting text below as a natural</h5>
                           <p class="card-text">This is a wider card with supporting text below as a natural
                              lead-in to additional content. This content is a little bit longer.</p>
                           <a class="ReadMoreRprtData">Read More...</a>

                           <div class="TagLabel"><span class="badge rounded-pill bg-danger">Danger</span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="card ReportCard shadow">
                  <div class="row g-0">
                     <div class="col-md-4">
                        <svg class="bd-placeholder-img" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image" preserveAspectRatio="xMidYMid slice" focusable="false">
                           <title>Placeholder</title>
                           <rect width="100%" height="100%" fill="#868e96" /><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                        </svg>
                     </div>
                     <div class="col-md-8">
                        <div class="card-body">
                           <h5 class="card-title">TEAM VEYE | 06 DEC 2022</h5>
                           <h5 class="card-Heading">This is a wider card with supporting text below as a natural</h5>
                           <p class="card-text">This is a wider card with supporting text below as a natural
                              lead-in to additional content. This content is a little bit longer.</p>
                           <a class="ReadMoreRprtData">Read More...</a>

                           <div class="TagLabel"><span class="badge rounded-pill bg-success">Danger</span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="card ReportCard shadow">
                  <div class="row g-0">
                     <div class="col-md-4">
                        <svg class="bd-placeholder-img" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image" preserveAspectRatio="xMidYMid slice" focusable="false">
                           <title>Placeholder</title>
                           <rect width="100%" height="100%" fill="#868e96" /><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                        </svg>
                     </div>
                     <div class="col-md-8">
                        <div class="card-body">
                           <h5 class="card-title">TEAM VEYE | 06 DEC 2022</h5>
                           <h5 class="card-Heading">This is a wider card with supporting text below as a natural</h5>
                           <p class="card-text">This is a wider card with supporting text below as a natural
                              lead-in to additional content. This content is a little bit longer.</p>
                           <a class="ReadMoreRprtData">Read More...</a>

                           <div class="TagLabel"><span class="badge rounded-pill bg-danger">Danger</span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="card ReportCard shadow">
                  <div class="row g-0">
                     <div class="col-md-4">
                        <svg class="bd-placeholder-img" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image" preserveAspectRatio="xMidYMid slice" focusable="false">
                           <title>Placeholder</title>
                           <rect width="100%" height="100%" fill="#868e96" /><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                        </svg>
                     </div>
                     <div class="col-md-8">
                        <div class="card-body">
                           <h5 class="card-title">TEAM VEYE | 06 DEC 2022</h5>
                           <h5 class="card-Heading">This is a wider card with supporting text below as a natural</h5>
                           <p class="card-text">This is a wider card with supporting text below as a natural
                              lead-in to additional content. This content is a little bit longer.</p>
                           <a class="ReadMoreRprtData">Read More...</a>

                           <div class="TagLabel"><span class="badge rounded-pill bg-success">Danger</span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="card ReportCard shadow">
                  <div class="row g-0">
                     <div class="col-md-4">
                        <svg class="bd-placeholder-img" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image" preserveAspectRatio="xMidYMid slice" focusable="false">
                           <title>Placeholder</title>
                           <rect width="100%" height="100%" fill="#868e96" /><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                        </svg>
                     </div>
                     <div class="col-md-8">
                        <div class="card-body">
                           <h5 class="card-title">TEAM VEYE | 06 DEC 2022</h5>
                           <h5 class="card-Heading">This is a wider card with supporting text below as a natural</h5>
                           <p class="card-text">This is a wider card with supporting text below as a natural
                              lead-in to additional content. This content is a little bit longer.</p>
                           <a class="ReadMoreRprtData">Read More...</a>

                           <div class="TagLabel"><span class="badge rounded-pill bg-danger">Danger</span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="card ReportCard shadow">
                  <div class="row g-0">
                     <div class="col-md-4">
                        <svg class="bd-placeholder-img" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image" preserveAspectRatio="xMidYMid slice" focusable="false">
                           <title>Placeholder</title>
                           <rect width="100%" height="100%" fill="#868e96" /><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                        </svg>
                     </div>
                     <div class="col-md-8">
                        <div class="card-body">
                           <h5 class="card-title">TEAM VEYE | 06 DEC 2022</h5>
                           <h5 class="card-Heading">This is a wider card with supporting text below as a natural</h5>
                           <p class="card-text">This is a wider card with supporting text below as a natural
                              lead-in to additional content. This content is a little bit longer.</p>
                           <a class="ReadMoreRprtData">Read More...</a>

                           <div class="TagLabel"><span class="badge rounded-pill bg-success">Danger</span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>


            <div class="bd-example-snippet bd-code-snippet">
               <div class="bd-example">
                  <nav aria-label="Another pagination example">
                     <ul class="pagination pagination-lg flex-wrap">
                        <li class="page-item disabled">
                           <a class="page-link"><i class="fas fa-long-arrow-alt-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item" aria-current="page">
                           <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">6</a></li>
                        <li class="page-item">
                           <a class="page-link" href="#"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </li>
                     </ul>
                  </nav>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <aside class="bd-aside text-muted align-self-start mb-3 mb-xl-4 px-2">
               <nav class="small" id="toc">
                  <ul class="list-unstyled">
                     <li class="my-2">
                        <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#contents-collapse" aria-controls="contents-collapse"><i class="fas fa-chevron-right"></i> More Report More Veye</button>

                        <ul class="list-unstyled list-group respoUlCss collapse" id="contents-collapse">
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Daily Analysis</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Clean Energy Transition Report </li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Emerging Star</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Healthcare Paronama</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Penny stocks Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Stock Of the Week Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Dividend Investor Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Master Of Growth Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Hot Sector Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; US Equity Report</li>

                        </ul>
                     </li>
                     <li class="my-2">
                        <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#forms-collapse" aria-controls="forms-collapse"><i class="fas fa-chevron-right"></i> New Report</button>
                        <ul class="list-unstyled list-group respoUlCss collapse" id="forms-collapse">
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card ccLesdCss ReportCard">
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="img-fluid" src="assets/images/phoCss.jpg">
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body  fsrCdsrd">
                                       <h5 class="ttesCdss">TEAM VEYE | 06 DEC 2022</h5>
                                       <h5 class="card-ttFOnts">Aristocrat Leisure Limited, Expanding the Growth Profile</h5>
                                       <a class=" rrfore">Read More...</a>


                                    </div>
                                 </div>
                              </div>
                           </div>

                        </ul>
                     </li>
                     <li class="my-2">
                        <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#components-collapse" aria-controls="components-collapse"><i class="fas fa-chevron-right"></i> Sector Specific</button>
                        <ul class="list-unstyled list-group respoUlCss collapse" id="components-collapse">
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Daily Analysis</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Clean Energy Transition Report </li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Emerging Star</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Healthcare Paronama</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Penny stocks Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Stock Of the Week Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Dividend Investor Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Master Of Growth Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; Hot Sector Report</li>
                           <li class="list-group-item Code4eaf8a"> <i class="fa fa-chevron-right" aria-hidden="true"></i> &nbsp; US Equity Report</li>
                        </ul>
                     </li>
                  </ul>
               </nav>
            </aside>

            <!-- <div class="AboutUsBg">
                     <div class="AboutUsBgCLR">
                        <div class="SubscribeTo">
                           <h5>Subscribe to our Special Report </h5>
                           <div class="PriceTag">From $200/6 Months</div>
                           <img src="./assets/images/subcribe.png">
                           <button class="sampleReportBtn SubscribeBtn">Subscribe</button>
                        </div>
                     </div>
                  </div> -->
         </div>
      </div>
   </div>
</section>
@endsection