<div class="col">
    <div class="card ReportCard shadow">
        <div class="row g-0">
            <div class="col-md-4">
                <img class="bd-placeholder-img" width="100%" height="160" src="{{asset($item->thumbnail)}}" />
            </div>
                
            <div class="col-md-8">
                <div class="card-body lesRRtbo">
                    <h5 class="card-title">Team Veye  | @if($item->schedule) {{date('d-M-Y',strtotime($item->schedule))}} @else {{date('d-M-Y',strtotime($item->created_at))}} @endif</h5>
                    <a href="{{route('report.read.more',$item->slug)}}"><h5 class="card-Heading itsFcads">{{$item->title}}</h5></a>
                    
                    <a href="{{route('report.read.more',$item->slug)}}" class="ReadMoreRprtData">Read More...</a>

                    @if($item->recommendation==1)
                        <div class="TagLabel"><span class="badge rounded-pill bg-danger">Sell</span></div>
                    @endif
                    @if($item->recommendation==2)
                        <div class="TagLabel"><span class="badge rounded-pill bg-success">Buy</span></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>