<!DOCTYPE html>
<html lang="en">
   <!--<![endif]-->
   <!-- BEGIN HEAD -->
      @include('frontend.includes.header_assets')
   <!-- END HEAD -->

   <body>
      <main>
         @include('frontend.includes.header')
         <!-- BEGIN CONTENT BODY -->
         @yield('content')
         <!-- END CONTENT BODY -->  
         @include('frontend.includes.footer')
      </main>
      @if(@$recommendations=='past_recommendations')
      @else
      @include('frontend.includes.footer_assets')
      @endif
   </body>

</html>