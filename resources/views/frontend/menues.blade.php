@extends('frontend.layouts.app')
@section('title') {{@$slugdata->meta_title}} @endsection
@section('description') {{@$slugdata->meta_description}} @endsection
@section('keywords') {{@$slugdata->meta_keywords}} @endsection
@if(Request::get('page')!=NULL) @section('nofollow') <META NAME="robots" CONTENT="noindex,nofollow"> @endsection
@else @section('nofollow') <meta name="robots" content="index, follow"> @endsection
@endif
@section('canonical')https://veye.com.au/report/{{$parent}}/{{$child}}@endsection

@section('content')
<?php

use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
use App\Models\Menues;
?>

<style type="text/css">
    .menusLinks {
        font-size: 14px !important;
        color: #4EAF8A !important;
    }
</style>
<section class="ReportPG">
    <div class="container">
        <div class="row align-items-end mb-5">
            <div class="col-md-6">
                <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
                    <h1>{{$child_row->title}}</h1>
                    <div class="bd-example">
                        <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row" style="clear:both;margin-top: -14px;">
            <div class="col-md-8">
                @foreach ($results as $item)
                <div class="col">
                    <div class="card ReportCard shadow">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img class="bd-placeholder-img" width="100%" height="155" src="{{asset('/')}}{{$item->thumbnail}}" />
                            </div>
                            @php

                            $companies= db::table('companies')->where('asx_code',$item->asx_code)->first();
                            $desc=@$companies->description;
                          
                            @endphp

                            <div class="col-md-8">
                                <div class="card-body lesBosyCss">
                                    <h5 class="card-title">Team Veye | <i class="fa fa-calendar"></i> @if($item->schedule) {{date('d-M-Y',strtotime($item->schedule))}} @else {{date('d-M-Y',strtotime($item->created_at))}} @endif</h5>
                                    <a href="{{route('report.read.more',[$item->slug])}}"><h5 class="card-Heading">{{$item->title}}</h5></a>
                                    <p class="card-text">{!! substr($desc, 0, 100) !!}...</p>
                                    <a href="{{route('report.read.more',[$item->slug])}}" class="ReadMoreRprtData">Read More...</a>

                                    @if($item->recommendation==1)

                                    <div class="TagLabel"><span class="badge rounded-pill bg-danger">Sell</span></div>
                                    @endif
                                    @if($item->recommendation==2)
                                    <div class="TagLabel"><span class="badge rounded-pill bg-success">Buy</span></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="bd-example-snippet bd-code-snippet">
                    <div class="bd-example">
                        <nav aria-label="Another pagination example">
                            <ul class="pagination pagination-lg flex-wrap">
                                <li class="page-item disabled">
                                    {{ $results->links( "pagination::bootstrap-4") }}
                                    <!-- <a class="page-link"><i class="fas fa-long-arrow-alt-left"></i></a> -->
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <aside class="bd-aside text-muted align-self-start mb-3 mb-xl-4 px-2">
                    <nav class="small" id="toc">
                        <ul class="list-unstyled">
                            <li class="my-2">
                                @php
                                 $dateTime = date('Y-m-d H:i:s');
                                $menuesData = Menues::where('parent',1)->where('deleted_at',null)->get();
                                $daily = DividendReports::where('report_id',2)


                                ->select('dividend_reports.*')
                                             ->where(function($q) use($dateTime){
                                             $q->where('dividend_reports.schedule','<=', $dateTime)
                                             ->orWhere('dividend_reports.schedule',NULL);
                                             })
                                ->take(5)->orderBy('created_at','desc')


                                ->orderBy('schedule','desc')->get();
								
                                @endphp
								@if($child=='latest-reports')
                            <li class="my-2">
                                <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#forms-collapse" aria-controls="forms-collapse"><i class="fas fa-chevron-right"></i> New Report</button>
                                <ul class="btn d-inline-flex align-items-center collapsed border-0" id="forms-collapse">
                                    <div class="card ccLesdCss ReportCard">
                                        @foreach ($daily as $datadaily)
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                                <img class="bd-placeholder-img respoPlacsHold" width="100%" height="85" src="{{asset('/')}}/{{$datadaily->thumbnail}}" />
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body  fsrCdsrd">
                                                    <h5 class="ttesCdss tmthora">TEAM VEYE | <i class="fa fa-calendar tesCader"></i> @if($datadaily->schedule) {{date('d-M-Y',strtotime($datadaily->schedule))}} @else {{date('d-M-Y',strtotime($datadaily->created_at))}} @endif</h5>
                                                    <a href="{{route('report.read.more',$datadaily->slug)}}"><h5 class="card-ttFOnts">{{$datadaily->title}}</h5></a>
                                                    <a style="font-size:12px !important;margin-left: -7px; " class="menusLinks trsEd" href="{{route('report.read.more',$datadaily->slug)}}">Read More...</a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </ul>
                            </li>
							@endif
                            <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#contents-collapse" aria-controls="contents-collapse"><i class="fas fa-chevron-right"></i> More Report More Veye</button>
                            <ul class="list-unstyled list-group respoUlCss collapse show" id="contents-collapse">
                                @foreach ($menuesData as $data)
                                <a class="list-group-item Code4eaf8a" href="{{route('manage.menues',['stock-advisory',$data->slug])}}">{{$data->title}}</a>
                                @endforeach
                            </ul>
                            </li>
                            <li class="my-2">
                                @php
                                $menuesData = Menues::where('parent',3)->where('deleted_at',null)->get();
                                @endphp
                                <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#components-collapse" aria-controls="components-collapse"><i class="fas fa-chevron-right"></i> Sector Specific</button>
                                <ul class="list-unstyled list-group respoUlCss collapse" id="components-collapse">
                                    @foreach ($menuesData as $data)
                                    <a class="list-group-item Code4eaf8a" href="{{route('manage.menues',['sector-specific',$data->slug])}}">{{$data->title}}</a>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </aside>
            </div>
        </div>
    </div>
</section>
@endsection