@extends('frontend.layouts.app')
@section('title') Contact Us | Veye Pty Ltd @endsection
@section('description') If you have any questions or comments about our Website, articles, blog. contact us at info@veye.com.au. We will get back to you as soon as possible! @endsection
@section('content')
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@include('frontend.includes.header_assets')


@section('canonical')https://veye.com.au/contact-us @endsection
<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>Contact Us</h1>
         <div class="bd-example">
            <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
         </div>
        <!--  <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="">Home / </a></li>
                  <li><a href="">contact</a></li>
               </ol>
            </nav>
         </div> -->
      </div>
   </div>
</section>
<section class="About">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="AboutRight text-center">
               <h2>Veye Pty Ltd</h2>
               <p>Stocks Research and Recommendations - Buy, Sell and Hold.
               </p>
                
               <p>Veye is a leading independent equities research firm dedicated to helping its clients improve their investment results
                  through unbiased and precise recommendations around buying, selling or holding stocks.</p>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="ContactPage">
   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <div class="contactForm shadow past__border">
               <h3>Get In Touch</h3>
               <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('contactus.store')}}">
               @csrf
               <input type="hidden" name="form_type" value="contact_us">
               <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3"> 
                  <div class="col-md">
                     <div class="form-floating mb-3">
                        <input type="text" name="name" class="form-control" id="floatingInput" placeholder="Enter Name">
                        <label for="floatingInput">Full Name</label>
                     </div>
                  </div>
                  <div class="col-md">
                     <div class="form-floating mb-3">
                        <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Enter Email">
                        <label for="floatingInput">Email address</label>
                     </div>
                  </div>
                  <div class="col-md">
                     <div class="form-floating mb-3">
                        <input type="text" name="phone" minlength="8" maxlength="12" class="form-control" id="floatingInput" placeholder="Enter Phone">
                        <label for="floatingInput">Phone Number</label>
                     </div>
                  </div>
                  <div class="col-md">
                     <div class="form-floating mb-3">
                        <input type="text" name="post_code" class="form-control" id="floatingInput" placeholder="Enter Post Code">
                        <label for="floatingInput">Post Code</label>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-floating">
                        <textarea class="form-control" name="message" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
                        <label for="floatingTextarea2">Additional Details (Optional)</label>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-check">
                       <!-- <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" required>-->
                        <label class="form-check-label" for="flexCheckChecked">
                          <div class="form-group" style="font-size: 12px;">
                        <img draggable="false" class="emoji" alt=":lock:" src="/assets/img/lock.svg" style="width: 13px; margin-top: -5px;"> By providing your details, you agree to Veye’s
                        <a style="color: green;" href="/pages/terms-conditions"> Terms &amp; Conditions</a> and <a style="color: green;" href="/pages/privacy-policy"> Privacy Policy</a> and to receive
                        marketing offers by email, text message or phone call from us or our agents until you opt
                        out.
                     </div>
                        </label>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <button class="btn btn-primary" type="submit">Submit Request</button>
                  </div>
               </div>
               </form>


            </div>
         </div>
         <div class="col-md-6">
            <div class="card ContactAddress h-100 overflow-hidden rounded-4 shadow-lg">
               <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                  <div class="ContactAddressCard">
                     <h3>Our Location</h3>
                     <p>Veye Pty Ltd Level 21,<br /> 207 Kent Street Sydney,<br /> NSW, 2000</p>
                  </div>
                  <div class="ContactAddressCard">
                     <h3>Quick Contact</h3>
                     <ul class="foot_links foot_linksLast">
                        <li><a href="mailto:info@veye.com.au"><i class="fas fa-envelope"></i> &nbsp;
                              info@veye.com.au</a></li>
                        <li><a href="tel:(02) 9052 4957"><i class="fas fa-phone-volume"></i>
                              &nbsp; (02) 9052 4957</a>
                        </li>
                     </ul>
                  </div>
                  <div class="footer-column h-auto border-0">
                     <a class="btn btn-outline-light btn-floating m-1" role="button" href="https://www.facebook.com/VeyePtyLtd/"><svg class="svg-inline--fa fa-facebook-f fa-w-10" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                           <path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z">
                           </path>
                        </svg><!-- <i class="fab fa-facebook-f"></i> Font Awesome fontawesome.com --></a>
                     <!-- Twitter -->
                     <a href="https://twitter.com/VeyePtyLtd" class="btn btn-outline-light btn-floating m-1" role="button"><svg class="svg-inline--fa fa-twitter fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                           <path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z">
                           </path>
                        </svg><!-- <i class="fab fa-twitter"></i> Font Awesome fontawesome.com --></a>
                     <!-- Google -->
                     <a href="https://www.youtube.com/@veyeaustralia9271" class="btn btn-outline-light btn-floating m-1" role="button"><svg class="svg-inline--fa fa-google fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="google" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488 512" data-fa-i2svg="">
                           <path fill="currentColor" d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z">
                           </path>
                        </svg><!-- <i class="fab fa-google"></i> Font Awesome fontawesome.com --></a>
                     <!-- Instagram -->
                     <a href="https://www.instagram.com/veye.pty.ltd/" class="btn btn-outline-light btn-floating m-1" role="button"><svg class="svg-inline--fa fa-instagram fa-w-14" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                           <path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z">
                           </path>
                        </svg><!-- <i class="fab fa-instagram"></i> Font Awesome fontawesome.com --></a>
                    <!-- <a class="btn btn-outline-light btn-floating m-1" role="button"><svg class="svg-inline--fa fa-youtube fa-w-18" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                           <path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z">
                           </path>
                        </svg><i class="fab fa-youtube"></i> Font Awesome fontawesome.com</a>-->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="ContactMap">
   <div class="container">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6626.015821624344!2d151.203628!3d-33.863687!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12ae46c633c581%3A0x2c839ca02ce3f2ee!2sLvl%2021%2F207%20Kent%20St%2C%20Sydney%20NSW%202000%2C%20Australia!5e0!3m2!1sen!2sin!4v1672561996210!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
   </div>
</section>
@include('sweetalert::alert')
@endsection