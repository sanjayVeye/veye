@extends('frontend.layouts.app')
@section('title') {{$pagesdata->meta_title}} @endsection
@section('description') {{$pagesdata->meta_description}} @endsection
@section('keywords') {{$pagesdata->meta_keywords}} @endsection
@section('content')
@include('frontend.includes.header_assets')
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@section('canonical')https://veye.com.au/pages/about-us @endsection

<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>About Us</h1>
         <div class="bd-example">
            <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
         </div>
        <!--  <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="index.html">Home / </a></li>
                  <li><a href="about.html">about</a></li>
               </ol>
            </nav>
         </div> -->
      </div>
   </div>
</section>

<style type="text/css">
   @media (max-width: 767px) {
    .g-4 {
        margin: 0px !important;
    }
    .AboutRight p, .AboutRight ul li, .PastRecommend ul li {
    line-height: 23px !important;
    margin-bottom: 21px !important;
}
.feature{
   text-align: justify !important;
}
 }
   p{
      font-weight: 300 !important;
   }
   .AboutRight p, .AboutRight ul li, .PastRecommend ul li{
      line-height: 23px !important;
   }
   .g-4{
          margin-top: 20px;
    margin-bottom: 30px;
   }
   .AboutLeft{
         padding-right: inherit !important;
   }
   .AboutRight{
            margin-top: 40px;
    font-size: 24px !important;
    font-weight: 600;
    }
    h3{
      font-weight: 700;
    }
</style>
<br>

{!! @$pagesdata->template !!}

<section class="FAQ_Section" style=" margin-top: 70px;">
   <div class="container">
      <h4 class="FAQ_heading">Frequently Asked Questions </h4>
      <div class="row">
         @foreach ($faqdata as $key => $data)
         <div class="col-md-6">
            <div class="customAccordian">
               <div class="accordion" id="accordionExample">
                  <div class="accordion-item">
                     <h4 class="accordion-header" id="heading-{{ $key }}">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{ $key }}" aria-expanded="false" aria-controls="collapse-{{ $key }}">
                           {{$data->title}}
                        </button>
                     </h4>
                     <div id="collapse-{{ $key }}" class="accordion-collapse collapse" aria-labelledby="heading-{{ $key }}" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                           {{$data->details}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         @endforeach
      </div>
   </div>
</section>



<style type="text/css">
  .TestImaCss{
    position: absolute;
    top: 207px;
  }

  img{
    max-width: 100% !important;
  }
</style>

<section class="WhatSay tesy__mest SlideDotsCustome" style="margin-bottom: 63px;">
   <div class="container  ">
      <h4 style="text-align:center !important"> <span class="whats-css saaAllRepla">What Our Clients Say About Us? </span> <span class="head__tsxt"></span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <div class=" slider-nav3">
         @foreach ($testmanag as $reportdata)
         <div>
            <div class="WhatSaySlide">
               <div class="CustomerData">
                  <h5>{{$reportdata->title}}</h5>
                  <div class="padding__tops"></div>
                  
                  <p class="applYPTgs">{{$reportdata->desciption}}</p>
                  <img class="TestImaCss" src="{{asset('uploads/Test_Management')}}/{{$reportdata->image}}" />
               </div>
            </div>
         </div>
         @endforeach
      </div>
   </div>
</section>
@endsection