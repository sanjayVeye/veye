@extends('frontend.layouts.app')
@section('title') Password @endsection
@section('content')
<?php
use Illuminate\Support\Facades\DB;
?>
@php $clentData =db::table('clients')->where('email',Auth::user()->email)->first();  @endphp 

      <div class="container-fluid">
         <div class="row">
            
            @include('frontend.dashboard.nav')

            <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
               <div class="DashboardRight border-0">
                  <div class="DashboardRightHead">
                     <h2 class="DashboardHeading">Hello {{@$clentData->first_name}} {{@$clentData->last_name}} </h2>
                     <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
                     <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                      </button>
                  </div>
               </div>
               <div class="DashboardRightDown">
                  <section class="PastRecommend02">
                        <div class="recomdedTable ">
                           <div class="UpdateEmail">
                           <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('userpassword.passwordupdate',Auth::user()->id)}}">
                               @csrf
                                 <h3>Update my password</h3>
                                 <!-- <label>
                                    <span>Old Password</span>
                                    <input type="text" name="OldEmail" required />
                                 </label> -->
                                 <label>
                                    <span>New Password</span>
                                    <input type="password" name="password" id="password" required />
                                 </label>
                                 <label>
                                    <span>Confirm New Password</span>
                                    <input type="password" id="confirm_password" required />
                                 </label>
                                 <button class="DasSubscBTN submitsUpdate m-auto">Submit </button>
                              </form>
                           </div>
                        </div>
                  </section>
               </div>
            </main>
         </div>
      </div>

      <script>
         var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
      </script>
      @endsection