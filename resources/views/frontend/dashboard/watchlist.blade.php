@extends('frontend.layouts.app')
@section('title') Watch List @endsection
@section('content')
<?php
use Illuminate\Support\Facades\DB;
?>
@php $clentData =db::table('clients')->where('email',Auth::user()->email)->first();  @endphp 
      <div class="container-fluid">
         <div class="row">

            @include('frontend.dashboard.nav')

            <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
               <div class="DashboardRight">
                  <div class="DashboardRightHead">
                     <h2 class="DashboardHeading">Hello {{$clentData->first_name}} {{$clentData->last_name}} </h2>
                     <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
                     <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                      </button>
                  </div>
                  <h3  class="DashboardSubHeading">My Watchlist</h3>
               </div>
               <div class="DashboardRightDown">                  
                  <div class="MywatchList">
                  <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('addwatchlist.store')}}">
                @csrf
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                     <input type="text" name="company" class="MywatchListBtn company-name" placeholder="Enter company name/code">
                     <!-- <button class="MywatchListBtn">Enter company name/code</button> -->
                     <input type="number" name="buy_qty" class="MywatchListBtn" placeholder="Enter Buy Quantity">
                     <!-- <button class="MywatchListBtn">Buy Quantity</button> -->
                     <input type="number" name="buy_pr" class="MywatchListBtn" placeholder="Enter Buy Price">
                     <!-- <button class="MywatchListBtn">Buy Price</button> -->
                     <input type="text" name="buy_date" class="MywatchListBtn" id = "datepicker" placeholder="Enter Buy Date">
                     <!-- <button class="MywatchListBtn">Buy Date</button> -->
                     <button type="submit" class="MywatchListBtn">Add</button>   
                     </form>
                  </div>
                  
                  <section class="PastRecommend02">
                        <div class="recomdedTable ">
            
                           <table class="table table-striped shadow past__border">
                              <thead>
                                 <tr>
                                    <th>Company</th>
                                    <th>Buy Date</th>
                                    <th>Current Date</th>
                                    <th>Buy Price</th>
                                    <th>Current Price</th>
                                    <th>Profit/Loss %</th>
                                  </tr>
                              </thead>
                              <tbody class="table-group-divider">
                              @php $i=0; @endphp
                        @foreach($watchlist as $data)
                        @php $i++; @endphp
                                 <tr>
                                    <td>{{ $data->company }} </td>
                                    <td>{{ $data->buy_date }} </td>
                                    <td><?php
                                    date_default_timezone_set('Asia/Kolkata');
                                     echo date('d/m/Y'); ?></td>
                                    <td>${{ $data->buy_pr }} </td>
                                    <td>${{ $data->amount }}</td>
                                    <td class="NumGreen">{{ $data->total_mkt_invest }}% <i class="fas fa-caret-up"></i></td>
                                  </tr>
                                  @endforeach
                              </tbody>
                           </table>                            
                        </div>

                        <div class="bd-example-snippet bd-code-snippet float-end">
                           <div class="bd-example">
                              <nav aria-label="Another pagination example">
                                 <ul class="pagination pagination-lg flex-wrap">
                                    <li class="page-item disabled">
                                    {{ $watchlist->links( "pagination::bootstrap-4") }}
                                       <!-- <a class="page-link"><svg class="svg-inline--fa fa-long-arrow-alt-left fa-w-14"
                                             aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-left"
                                             role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                             <path fill="currentColor"
                                                d="M134.059 296H436c6.627 0 12-5.373 12-12v-56c0-6.627-5.373-12-12-12H134.059v-46.059c0-21.382-25.851-32.09-40.971-16.971L7.029 239.029c-9.373 9.373-9.373 24.569 0 33.941l86.059 86.059c15.119 15.119 40.971 4.411 40.971-16.971V296z">
                                             </path>
                                          </svg>
                                       </a> -->
                                    </li>
<!--                                     
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item" aria-current="page">
                                       <a class="page-link" href="#">2</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                                    <li class="page-item">
                                       <a class="page-link" href="#"><svg class="svg-inline--fa fa-long-arrow-alt-right fa-w-14"
                                             aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-right"
                                             role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                             <path fill="currentColor"
                                                d="M313.941 216H12c-6.627 0-12 5.373-12 12v56c0 6.627 5.373 12 12 12h301.941v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.569 0-33.941l-86.059-86.059c-15.119-15.119-40.971-4.411-40.971 16.971V216z">
                                             </path>
                                          </svg>
                                       </a>
                                    </li> -->
                                 </ul>
                              </nav>
                           </div>
                        </div>
                  </section>
               </div>
            </main>
         </div>
      </div>

      @endsection