@extends('frontend.layouts.app')
@section('title') My Invoice @endsection
@section('content')
<?php

use Illuminate\Support\Facades\DB;
use App\Models\InvoiceOrder;
?>
@php $clentData =db::table('clients')->where('email',Auth::user()->email)->first(); @endphp

<div class="container-fluid">
   <div class="row">

      @include('frontend.dashboard.nav')

      <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
         <div class="DashboardRight">
            <div class="DashboardRightHead">
               <h2 class="DashboardHeading">Hello {{@$clentData->first_name}} {{@$clentData->last_name}} </h2>
               <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
               <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i>
               </button>
            </div>
            <h3 class="DashboardSubHeading">My Invoice</h3>
         </div>
         <div class="DashboardRightDown">
            <section class="PastRecommend02">
               <div class="recomdedTable ">
                  <table class="table table-striped past__border">
                     <thead>
                        <tr>
                           <th scope="col">Invoice Number</th>
                           <th scope="col">Invoice Date</th>
                           <th scope="col">Total Amount (*Inclusive GST)</th>
                           <!--<th scope="col"> Type</th>-->
                        </tr>
                     </thead>
                     <tbody class="table-group-divider">
                        @php $i=0; @endphp
                        @foreach($invoiceDetail as $v)
                     
                        <tr>
                           <th scope="row"> <span class="bold__tablefonts">{{ $v->invoice_no }}</span> </th>
                           <td>{{ $v->created_at }}</td>
                           <td>{{ $v->amount }}</td>
                           <!--<td class="NumGreen">{{ $v->product_type }}
                          
                           </td>-->
                        </tr>
                      
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <div class="bd-example-snippet bd-code-snippet float-end">
                  <div class="bd-example">
                     <nav aria-label="Another pagination example">
                        <ul class="pagination pagination-lg flex-wrap">
                           <li class="page-item disabled">
                             
                           </li>

                        </ul>
                     </nav>
                  </div>
               </div>
            </section>
         </div>
      </main>
   </div>
</div>

@endsection