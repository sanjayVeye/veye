@extends('frontend.layouts.app')
@section('title') My Invoice @endsection
@section('content')
<?php
use Illuminate\Support\Facades\DB;
?>
@php $clentData =db::table('clients')->where('email',Auth::user()->email)->first();  @endphp 

<div class="container-fluid">
   <div class="row">

      @include('frontend.dashboard.nav')

      <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
         <div class="DashboardRight">
            <div class="DashboardRightHead">
               <h2 class="DashboardHeading">Hello {{@$clentData->first_name}} {{@$clentData->last_name}} </h2>
               <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
               <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i>
               </button>
            </div>
            <h3 class="DashboardSubHeading">All Reports</h3>
         </div>
        

         @foreach ($results as $item)
                <div class="col">
                    <div class="card ReportCard shadow">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img class="bd-placeholder-img" width="100%" height="155" src="{{asset('/')}}/{{$item->thumbnail}}" />
                            </div>
                            @php

                            $companies= db::table('companies')->where('asx_code',$item->asx_code)->first();
                            $desc=@$companies->description;
                            @endphp

                            <div class="col-md-8">
                                <div class="card-body lesBosyCss">
                                    <h5 class="card-title">Team Veye {{$item->report_no}} | <i class="fa fa-calendar"></i> {{date('d-M-Y',strtotime($item->created_at))}}</h5>
                                    <h5 class="card-Heading">{{$item->title}}</h5>
                                    <p class="card-text">{!! substr($desc, 0, 100) !!}...</p>
                                    <a href="{{route('report.read.more',[$item->slug])}}" class="ReadMoreRprtData">Read More...</a>

                                    @if($item->recommendation==1)

                                    <div class="TagLabel"><span class="badge rounded-pill bg-danger">Sell</span></div>
                                    @endif
                                    @if($item->recommendation==2)
                                    <div class="TagLabel"><span class="badge rounded-pill bg-success">Buy</span></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach


      </main>
   </div>
</div>

@endsection