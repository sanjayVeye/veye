@extends('frontend.layouts.app')
@section('title') Latest Buy @endsection
@section('content')
<?php

use Illuminate\Support\Facades\DB;
?>
@php $clentData =db::table('clients')->where('email',Auth::user()->email)->first(); @endphp
<div class="container-fluid">
   <div class="row">

      @include('frontend.dashboard.nav')

      <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
         <div class="DashboardRight">
            <div class="DashboardRightHead">
               <h2 class="DashboardHeading">Hello {{@$clentData->first_name}} {{@$clentData->last_name}} </h2>
               <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
               <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i>
               </button>
            </div>
            <h3 class="DashboardSubHeading">Latest Buy Recommendations</h3>
         </div>
         <div class="DashboardRightDown">
            <section class="PastRecommend02">
               <div class="recomdedTable ">
                  <table class="table table-striped past__border">
                     <thead>
                        <tr>
                           <th width="64">Sno.</th>
                           <th width="64">Buy Date</th>
                           <th width="144">Company Name - Code</th>
                           <th colspan="2" width="140">Recommendation</th>
                        </tr>
                        <col width="64" span="2">
                        <col width="144">
                        <col width="61">
                        <col width="79">
                     </thead>
                     <tbody class="table-group-divider">
                        @php $i=0; @endphp
                        @foreach($latestbuy as $data)
                        @php $i++; @endphp
                        <tr>
                           <td>{{ $i }}</td>
                           <td>{{ $data->created_at }}</td>
                           <!-- <td>{{ @$data->companyName->title }}</td> -->
                           <td>{{ @$data->companyName->title }} - {{ @$data->asx_code }}</td>
                           <td>Buy</td>
                           <td><a href="{{route('report.read.more',[$data->slug])}}" style="color:red;">Read more..</a></td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <div class="bd-example-snippet bd-code-snippet float-end">
                  <div class="bd-example">
                     <nav aria-label="Another pagination example">
                        <ul class="pagination pagination-lg flex-wrap">
                           <li class="page-item disabled">
                              {{ $latestbuy->links( "pagination::bootstrap-4") }}
                           </li>

                        </ul>
                     </nav>
                  </div>
               </div>
            </section>
         </div>
      </main>
   </div>
</div>

@endsection