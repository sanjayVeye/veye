@extends('frontend.layouts.app')
@section('title') Email @endsection
@section('content')
<?php
use Illuminate\Support\Facades\DB;
?>
@php $clentData =db::table('clients')->where('email',Auth::user()->email)->first();  @endphp 
      <div class="container-fluid">
         <div class="row">

            @include('frontend.dashboard.nav')

            <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
               <div class="DashboardRight border-0">
                  <div class="DashboardRightHead">
                     <h2 class="DashboardHeading">Hello {{@$clentData->first_name}} {{@$clentData->last_name}} </h2>
                     <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
                     <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                      </button>
                  </div>
               </div>
               <div class="DashboardRightDown">
                  <section class="PastRecommend02">
                        <div class="recomdedTable ">
                           <div class="UpdateEmail">
                           <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('useremail.emailupdate',Auth::user()->id)}}">
                               @csrf
                                 <h3>Update my email</h3>
                                 <label>
                                    <span>Old Email</span>
                                    <input type="email" name="oldEmail" value="{{Auth::user()->email}}" required />
                                 </label>
                                 <label>
                                    <span>New Email</span>
                                    <input type="email" name="email" id="newemail" required />
                                 </label>
                                 <label>
                                    <span>Confirm New Email</span>
                                    <input type="email" id="confirm_email" required />
                                 </label>
                                 <button class="DasSubscBTN m-auto">Submit </button>
                              </form>
                           </div>
                        </div>
                  </section>
               </div>
            </main>
         </div>
      </div>

<script>
   var newemail = document.getElementById("newemail")
  , confirm_email = document.getElementById("confirm_email");

function validateEmail(){
  if(newemail.value != confirm_email.value) {
   confirm_email.setCustomValidity("Email Don't Match");
  } else {
   confirm_email.setCustomValidity('');
  }
}

newemail.onchange = validateEmail;
confirm_email.onkeyup = validateEmail;
</script>
@endsection