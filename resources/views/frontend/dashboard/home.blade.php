@extends('frontend.layouts.app')
@section('title') Dashboard @endsection
@section('content')
<?php
use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
?>
<div class="container-fluid">
   <div class="row">

      @include('frontend.dashboard.nav')

      <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
         <div class="DashboardRight">
            <div class="DashboardRightHead">
               <h2 class="DashboardHeading">Hello {{@$clentData->first_name}} {{@$clentData->last_name}} </h2>

             <!--  <button class="DasSubscBTN" style="background-color: #e90c2e;"> Your plan will be expired: &nbsp; <a class="text-dark" href="{{url('subscribe')}}"> Buy Now</a> </button>-->
               
               <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i> 
               </button>
            </div>
            <h3 class="DashboardSubHeading">Reports</h3>

         </div>
         <div class="DashboardRightDown">
            <div class="row row-cols-2">
               @foreach($reportAccess as $v)
              @php 
				$reportCount= db::table('dividend_reports')->where('report_id',$v->report_id)->count(); 
				$reportName= db::table('reports')->where('id',$v->report_id)->first(); 				
				@endphp
            @if($reportName->title == 'Free Reports')
            @else
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="{{asset('frontend/assets/images/Dash1.png')}}" />
                     <p>{{$reportCount}}</p>
                     <!-- <h4>{{@$v->report_name}}</h4> -->
                     <a class="nav-link" href="{{ url('report/stock-advisory/') }}/{{@$reportName->slug}}">
                        <button class="DasSubscBTN">{{@$reportName->title}}</button>
                     </a>
                  </div>
               </div>
               @endif
               @endforeach
               <!-- <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="{{asset('frontend/assets/images/Dash1.png')}}" />
                     <p>{{$dividentReports}}</p>
                     <h4>Dividend Investor Reports</h4>
                     <a class="nav-link" href="{{ url('report/stock-advisory/divident-investor-report') }}">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="{{asset('frontend/assets/images/Dash1.png')}}" />
                     <p>{{$stockWeekReports}}</p>
                     <h4>Stock of the week Reports</h4>
                     <a class="nav-link" href="{{ url('report/stock-advisory/stock-of-the-day-report') }}">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="{{asset('frontend/assets/images/Dash1.png')}}" />
                     <p>{{$masterReports}}</p>
                     <h4>Masters of Growth Reports</h4>
                     <a class="nav-link" href="{{ url('report/stock-advisory/master-of-growth-report') }}">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="{{asset('frontend/assets/images/Dash1.png')}}" />
                     <p>{{$pennyStockReports}}</p>
                     <h4>Penny Stock Reports</h4>
                     <a class="nav-link" href="{{ url('report/stock-advisory/penny-stocks-report') }}">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="{{asset('frontend/assets/images/Dash1.png')}}" />
                     <p>{{$hotReports}}</p>
                     <h4>Hot Sector Reports</h4>
                     <a class="nav-link" href="{{ url('report/stock-advisory/hot-sector-report') }}">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="{{asset('frontend/assets/images/Dash1.png')}}" />
                     <p>{{$articles}}</p>
                     <h4>Articles</h4>
                     <button class="DasSubscBTN">Subscription: Regular </button>
                  </div>
               </div> -->

            </div>
         </div>
      </main>
   </div>
</div>

@endsection