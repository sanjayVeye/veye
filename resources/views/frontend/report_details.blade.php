@extends('frontend.layouts.app')
@section('title') Report Page @endsection
@section('content')
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@include('frontend.includes.header_assets')
<?php

use App\Models\Menues;
use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
?>
@php
$disclaimer= db::table('disclaimer')->first();
@endphp
<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-6">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Report Details</h1>
               <div class="bd-example">
                  <nav aria-label="breadcrumb ">
                     <div class="col-md-6">
                        <a class="GoBack" onclick="location.href='javascript:history.go(-1)'">  Go back</a>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
      </div>

      <style>
         .custm_con h4 {
            font-size: 24px;
            font-weight: 600;
         }

         .custm_con h5 {
            font-size: 18px;
            margin-top: 13px;
            font-weight: 800;
            text-decoration: underline;
            margin-bottom: 20px;
         }

         .custm_con p {
            font-size: 14px;
            /*line-height: justify;*/
            text-align: justify !important;
            /*margin-top: 14px;*/
            /*margin-bottom: 9px;*/
            text-align: initial;
            /*line-height: 28px;*/
            line-height: 1.5;
         }

         .custm_con disc {
            font-size: 14px !important;
            line-height: initial;
            margin-top: 14px;
            margin-bottom: 9px;
            text-align: justify;
            line-height: 28px;
         }

         .custm_con img {
            max-width: 100%;
         }

         .custm_con p strong {
            font-weight: bold !important;
         }

         .custm_con strong {
            font-weight: bold !important;
         }

         .custm_con p b {
            font-weight: bold !important;
         }

         .custm_con b {
            font-weight: bold !important;
         }

         .marg {
            margin: 10px 0px;
         }

         .ressprc {
            color: #18ba96;
         }

         .card-ttFOnts {
            margin-top: 0px !important;
         }

         .iitFfas {
            color: inherit !important;
            background: inherit !important;
            border: inherit !important;
            border-radius: 0px !important;
            padding: 7px 0px 0px !important;
            margin-right: -2px !important;
         }

         .tesvCss {
            font-size: 13px;
         }

         .cardlefts {
            padding-left: 10px;
         }

         .remborder {
            border: none !important;
            box-shadow: inherit !important;
         }

         .moreSpecials {
            margin: 4px 4px !important;
            border: 1px solid #ddd !important;
            border-radius: 16px !important;
            box-shadow: 0px 0px 4px 0px #ddd !important;
            padding: 8px !important;
         }

         .slick-list {
            /*margin: 13px !important;*/
         }

         .wites {
            background: #fff !important;
         }

         .mrrTospssss {
            margin-top: 42px !important;
         }

         .marg14 {
            margin-bottom: 14px !important;
         }
      </style>

      <div class="row">
         <div class="col-md-8">
            <div class="container custm_con">

               <h4>{!! $details->title !!}</h4>
               <div class=" ReportCard mainLine"> </div>
               <span class="team-view-date pedding_green"> Team Veye | <i class="fas fa-calendar-alt"></i> {!! $details->date !!} <span>ASX - SDI</span></span>
               <img class="img-fluid" src="{{asset('uploads/Reports')}}/{{$details->image}}" />
               <p> {!! $details->description !!}</p>
               <div class=" mainLine"> </div>
               <h4 class="marg14" style="font-size: 18px;">Disclaimer</h4>
               {!! html_entity_decode($disclaimer->details) !!}
            </div>
         </div>
         <div class="col-md-4 d-none d-sm-block d-sm-none d-md-block">
            <aside class="bd-aside text-muted align-self-start mb-3 mb-xl-4 px-2">
               <nav class="small" id="toc">
                  <ul class="list-unstyled">
                     <li class="my-2">
                        <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#forms-collapse" aria-controls="forms-collapse"><i class="fas fa-chevron-right"></i> Latest Reports</button>
                        <ul class="btn d-inline-flex align-items-center collapsed border-0" id="forms-collapse">
                           <div class="card ccLesdCss ReportCard">
                              @foreach ($reports as $datadaily)
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <img class="bd-placeholder-img" width="100%" height="70" src="uploads/reports/{{$datadaily->image}}" />
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body cardlefts  fsrCdsrd">
                                       <h6 class="ttesCdss tesvCss">TEAM VEYE | <i class="fa fa-calendar iitFfas" aria-hidden="true"></i> {{date('d-M-Y',strtotime($datadaily->created_at))}}</h6>
                                       <p class="card-ttFOnts">{{$datadaily->title}}</p>
                                       <a class="ressprc" href="{{route('report_details', encrypt($datadaily->id))}}">View Reports..</a>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                           </div>
                        </ul>
                     </li>
                  </ul>
               </nav>
            </aside>
            <div class="AboutUsBg">
               <div class="AboutUsBgCLR">
                  <div class="SubscribeTo">
                     <h5>Subscribe to our Special Report </h5>
                     <div class="PriceTag">From $200/6 Months</div>
                     <img src="{{asset('assets/images/subcribe.png')}}" />
                     <button class="sampleReportBtn SubscribeBtn">Subscribe</button>
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
</section>
@endsection