@extends('frontend.layouts.app')
@section('title') Stock Advisory Services: Best Advisor in Stock Market @endsection
@section('description') Looking for expert guidance in the stock market? Veye provides the best stock advisory services. Get expert guidance from industry-leading advisors. @endsection
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@section('content')
@include('frontend.includes.header_assets')
@section('canonical') https://veye.com.au/stock-advisory @endsection
<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-12">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Stock Advisory</h1>
               <div class="bd-example">
                  <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
               </div>
               <p>Expert guidance and comprehensive stock reports offering valuable insights, analysis, and recommendations to help investors make informed decisions in the market.</p>
            </div>
         </div>
      </div>
      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-3">
         @foreach ($resultdata as $item)
         <div class="col">
            <div class="ReportCard StockAdvisory">
            <a href="{{route('manage.menues',['stock-advisory',$item->slug])}}" class="ReadMoreRprtData"><img class="bd-placeholder-img" width="100%" height="180" src="{{asset('uploads/Reports')}}/{{$item->image}}"/></a>
               <div class="card-body">
                  <h5 class="card-Heading stofda">{{$item->title}}</h4>
                     <!-- <a href="{{route('report_details', encrypt($item->id))}}" class="ReadMoreRprtData">Read More... </a> -->
                     
               </div>
            </div>
         </div>
         @endforeach
      </div>
   </div>
</section>

@endsection