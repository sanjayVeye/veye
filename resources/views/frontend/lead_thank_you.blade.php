@extends('frontend.layouts.app')
@section('title') Thank you Page @endsection
@section('content')
@section('nofollow') <META NAME="robots" CONTENT="noindex,nofollow"> @endsection
@include('frontend.includes.header_assets')
<?php

use App\Models\pages;
?>
@php $pagesdata = pages::where('deleted_at',NULL)->where('tag','thank-you')->first(); @endphp
<section class="ReportPG">
    <div class="container">
        <div class="row align-items-end mb-5">
            <div class="col-md-6">
                <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
                    <h1>Thank You</h1>
                    <div class="bd-example">
                        <!-- <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb m-0">
                                <li><a href="">Home / </a></li>
                                <li><a href="">Thank You</a></li>
                            </ol>
                        </nav> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="PrivacyPolicy">
            <!-- <textarea name="content" id="description" class="form-control"></textarea> -->
            {!! @$pagesdata->template !!}            
        </div>
        <!-- <div class="col-md-12">
                     <button class="btn btn-primary" type="submit">Submit</button>
                  </div> -->
    </div>
</section>
<script>
    CKEDITOR.replace('description');
</script>
@endsection