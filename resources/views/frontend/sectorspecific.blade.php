@extends('frontend.layouts.app')
@section('title') Best Stocks & Sector: Top Sectors to Invest in Stock Market @endsection
@section('description') Discover top stock sectors & best stocks in each sector. Find top 10 stocks per sector, including top 5 sectors in the Indian market. Explore energy sector & maximize returns in Indian stock market. @endsection
@section('canonical') https://veye.com.au/sector-specific @endsection
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@section('content')
@include('frontend.includes.header_assets')
<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>Sector Specific</h1>
         <div class="bd-example">
               <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"><i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i> Go back</a>
           </div>
         <!-- <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="">Home / </a></li>
                  <li><a href="">sector specific</a></li>
               </ol>
            </nav>
         </div> -->
      </div>
   </div>
</section>
<section class="WebinarPG pasiu12Tops">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <p class="setctSi">Explore a comprehensive list of diverse sectors, featuring industries ranging from technology and healthcare to finance and energy, providing insights into the dynamic landscape of investment opportunities.</p>
         @foreach ($masterDlt as $sector)
            <div class="sectorSpecific">
               <div class="sectorSpecificHead">
                  <h2>{{$sector->detail_1}}</h2> 
               </div>
               <p>{{$sector->description}}</p>
               <div class="sectorSpecificBody">
                   @foreach ($company as $comp)
                   @php $arr = explode(',', $comp->report_type); @endphp
                   @if($arr[0]==$sector->ID)
                 <div class="sectorSpecificBtn">
                 <a class="seeALink" href="search/{{str_replace(' ' ,'-',$comp->title)}}"> {{$comp->title}} </a> 
                  </div>
                  @endif
                  @endforeach
               </div>
            </div>

      @endforeach
           
         </div>
      </div>
   </div>
</section>
@endsection