@extends('frontend.layouts.app')

@section('content')

@section('title') Australian Securities Exchange (ASX) - Latest ASX News Today @endsection
@section('description') Stay updated with the latest ASX news today. Get insights, analysis, and market updates on stocks, investments, and financial trends. @endsection
@section('keywords') Australian Securities Exchange (ASX) - Latest ASX News Today @endsection
@if(Request::get('page')!=NULL) @section('nofollow') <META NAME="robots" CONTENT="noindex,nofollow"> @endsection
@else @section('nofollow') <meta name="robots" content="index, follow"> @endsection
@endif
@section('canonical')https://veye.com.au/editorial @endsection
@include('frontend.includes.header_assets')
<?php

use App\Models\Menues;
use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
?>
<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-12">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Editorial</h1>
               <div class="bd-example">
                  <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"><i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i> Go back </a>
               </div>
               <!--  <div class="bd-example">
                  <nav aria-label="breadcrumb ">
                     <ol class="breadcrumb m-0">
                        <li><a href="">Home / </a></li>
                        <li><a href="">Editorial</a></li>
                     </ol>
                  </nav>
               </div> -->
               <!-- <p>Stay ahead of the curve as we bring you expert analysis and commentary on the ever-evolving world of investing. Whether you are an experienced investor or just beginning your investment journey, our editorial section aims to provide you with valuable information and perspectives to enhance your financial decision-making.</p> -->
            </div>
         </div>
      </div>
      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-3">
         @foreach ($editorial as $blogs)
         @php $desc = strlen($blogs->description) > 50 ? substr($blogs->description,0,50).'..' : $blogs->description; @endphp
         <div class="col">
            <div class="ReportCard EditorialCard card card-body shadow">
               <a href="{{route('editorial-details', $blogs->slug)}}"><img src="{{asset('/')}}/{{$blogs->article_img}}" alt="{{$blogs->title}}"/></a>
               <div class="card-body">
                  <h4 class="card-title">TEAM VEYE &nbsp;&nbsp; | <i class="fa fa-calendar" aria-hidden="true"></i><span>{{date('d-M-Y',strtotime($blogs->created_at))}}</span></h4>
                  <a href="{{route('editorial-details', $blogs->slug)}}">
                     <h5 class="card-Heading">{{ strlen($blogs->title) > 50 ? substr($blogs->title,0,50).'..' : $blogs->title }}</h5>
                  </a>
                  <p class="card-text">{!! $desc !!}</p>
                  
               </div>
            </div>
         </div>
         @endforeach
      </div>

      <div class="bd-example-snippet bd-code-snippet">
         <div class="bd-example">
            <nav aria-label="Another pagination example">
               <ul class="pagination pagination-lg flex-wrap">
                  <li class="page-item disabled">
                     {{ $editorial->links( "pagination::bootstrap-4") }}
                  </li>

               </ul>
            </nav>
         </div>
      </div>

   </div>
</section>
@endsection