@extends('frontend.layouts.app')
@section('title') Offer Page @endsection
@section('nofollow') <meta name="robots" content="index, follow"> @endsection
@section('content')
<?php

use App\Models\Faq;
?>
<style>
   .InvestorCNTbox strong {
      font-weight: 700;
      font-size: 20px;
   }

   .esGst {
      font-size: 12px;
      font-weight: 700;
   }


   @media (max-width: 767px) {
      img {
         max-width: 100% !important;
      }
   }

   .stripe-button-el {
      width: 83%;
      height: 42px;
      border-radius: 22px !important;
      border: none;
      color: #fff;
      background: #1275ff;
      background-image: -webkit-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
      background-image: -moz-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
      background-image: -ms-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
      background-image: -o-linear-gradient(#7dc5ee, #008cdd 85%, #30a2e4);
   }

   .InvestorCNTboxLeft h4 {
      font-size: 27px;
      font-weight: 700;
      color: #999;
      padding: 0 0;
      margin: 2px 15px;
      display: inline-block;
      position: relative;
   }

   .InvestorCNTboxLeft h4::after {
      position: absolute;
      border-bottom: 2px solid #e7004b;
      content: "";
      width: 100%;
      top: 17px;
      left: 0;
   }

   .offesEnfsCs {
      font-size: 27px !important;
      margin-top: 20px;
      margin-bottom: 22px !important;
      border-radius: 13px;
      border: 2px solid #ddd;
      text-transform: uppercase;
      padding: 17px 10px;
   }

   .bgcouSty {}

   .h4wsPrice {}

   .PrsGFs {
      font-weight: 100;
   }

   .yoisugS {
      font-size: 14px;
      font-weight: 200;
      margin-bottom: 7px;
      color: gray;
   }
</style>
<style>
   .bblandFt{font-size:28px !important}
   </style>
@php $faqdata = Faq::where('deleted_at',null)->get(); @endphp
<section class="investerReport1" style="margin-top: -42px !important;">
   <div class="container">
      
         <div class="investerReportInn">

            <div class="row">

              

               <div class="col-md-8">
                  <div class="InvestorCNTbox CheFes carBoxyShaf">
                   <h1>We regret to inform you that this offer has expired. However, don't miss out on future opportunities! Keep a close eye on our newsletter for the latest offers and promotions, ensuring you stay informed and never miss a chance to save.</h1>
                  </div>
               </div>
               <!-- <div class="col-md-1"> </div> -->

             

            </div>
         </div>
   </div>
</section>






<script>
   $(document).ready(function() {
      $("#btn").click(function() {
         alert("Login First!");
      });
   });
   $(document).ready(function() {
      $("#paynow").click(function() {
         alert("Login First!");
      });
   });
   $(document).ready(function() {
      $("#paycard").click(function() {
         alert("Login First!");
      });
   });
</script>
@endsection