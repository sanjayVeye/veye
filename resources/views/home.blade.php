@extends('layouts.admin')
@section('content')
<?php

use App\Models\ContactUs;
?>

<div class="page-content hshyBgfs">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Admin Dashboard
        <small>Dashboard</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>

    <style>
        .rounded {
            border-radius: 5px !important;
        }

        .bg-light {
            background-color: #F3F6F9 !important;
        }


        .hshyBgfs {
            background: #F1F3F6;
        }

        .iuyPafing {
            padding-top: 25px;
            padding-bottom: 20px;
            padding-left: 16px;
            padding-right: 13px;
        }

        .usrrCfs {
            max-width: 45px;
        }

        .justify__content {
            justify-content: space-between !important;
            align-items: center !important;
        }

        .d__flex {
            display: flex !important;
            height: 101px;
        }

        .text__primary {
            color: #0FAE87 !important;
            font-weight: 900;
        }

        .ms__3 {
            margin-left: 1rem !important;
        }

        .iifsDes {
            margin-top: 0px !important;
        }

        .mb__2 {
            margin-bottom: 6px !important;
            font-size: 17px;
            font-weight: 600;
            color: #757575;
            margin-top: 1px;
        }

        .mb__0 {
            margin-bottom: 0 !important;
            color: #000;
            font-weight: 700;
            font-size: 17px;
        }

        .bordered {
            margin: 5px;
        }

        .bbhsHeader {
            background: #0FAE87;
            color: #fff;
            font-weight: 700;
        }

        .heightBCs {
            height: 332px !important;
        }
    </style>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-bell fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Subscriber </p>
                        <h6 class="mb__0">{{ $count2 }}</h6>
                    </div>
                </div>



                <!-- <h4 class="widget-thumb-heading">Current Balance</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-green icon-bulb"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">USD</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,644">0</span>
                        </div>
                    </div> -->
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-bell-slash fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Unsubscribe</p>
                        <h6 class="mb__0">{{ $count }}</h6>
                    </div>
                </div>

            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-futbol fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Total Subscriber</p>
                        <h6 class="mb__0">{{ $count2+$count }}</h6>
                    </div>
                </div>

            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <div class=" bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-file-text fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Sale invoice</p>
                        <h6 class="mb__0">0</h6>
                    </div>
                </div>

            </div>
        </div>


        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-money fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Dividend Investor Report </p>
                        <h6 class="mb__0">{{$dividentReports}}</h6>
                    </div>
                </div>



                <!-- <h4 class="widget-thumb-heading">Current Balance</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-green icon-bulb"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">USD</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,644">0</span>
                        </div>
                    </div> -->
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-file fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Stock Of The Day Report</p>
                        <h6 class="mb__0">{{$stockWeekReports}}</h6>
                    </div>
                </div>

            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-bar-chart fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Master of growth Report</p>
                        <h6 class="mb__0">{{$masterReports}}</h6>
                    </div>
                </div>

            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class=" bordered">
                <div class="bg-light rounded d__flex align-items-center justify__content iuyPafing">
                    <i class="fa fa-pie-chart fa-3x text__primary"></i>
                    <div class="ms__3">
                        <p class="mb__2">Penny Stocks Report</p>
                        <h6 class="mb__0">{{$pennyStockReports}}</h6>
                    </div>
                </div>

            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-6 col-xs-6 col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption ">
                        <span class="caption-subject font-dark bold uppercase">Show All Messages</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Today monthly/Yearly
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;"> Option 1</a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;">Option 2</a>
                                </li>
                                <li>
                                    <a href="javascript:;">Option 3</a>
                                </li>
                                <li>
                                    <a href="javascript:;">Option 4</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive heightBCs">
                    <table id="dataTableExample" class="table table-bordered" width="100%">
                        <thead class="bbhsHeader">
                            <tr>
                                <th>Sl No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Post Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6 col-sm-6">
            <div class="portlet light bordered ">
                <div class="portlet-title">
                    <div class="caption ">
                        <span class="caption-subject font-dark bold uppercase">Enquiry date Wise</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Today monthly/Yearly
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;"> Option 1</a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;">Option 2</a>
                                </li>
                                <li>
                                    <a href="javascript:;">Option 3</a>
                                </li>
                                <li>
                                    <a href="javascript:;">Option 4</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @php $contactusdata = ContactUs::limit(5)->where('deleted_at',null)->get(); @endphp
                <div class="portlet-body">
                    <div class="table-responsive heightBCs">
                        <table class="table table-bordered" width="100%">
                            <thead class="bbhsHeader">
                                <tr>
                                    <th>Sl No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Post Code</th>
                                    <th>Message</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contactusdata as $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>{{$data->phone}}</td>
                                    <td>{{$data->post_code}}</td>
                                    <td>{{$data->message}}</td>
                                    <td>
                                        <a href="route(contactus.destroy, $data->id)"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTableExample').dataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],

            "iDisplayLength": 10,
            "language": {
                search: ""
            },
            "searching": false,
            "lengthChange": false,
            "info": false,
            "serverSide": true,
            "processing": true,
            "paging": true,
            "ajax": {
                url: '{{route("dashboard.freeReportEnq")}}',
                data: function(e) {

                }
            },
            'createdRow': function(row, data, dataIndex) {
                $(row).attr('id', 'tr_' + data.id);
            },
            columns: [{
                    data: "sno",
                    name: 'sno',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "name",
                    name: "name"
                },

                {
                    data: "email",
                    name: "email"
                },

                {
                    data: "phone",
                    name: "phone"
                },
                {
                    data: "post_code",
                    name: "post_code"
                },
                {
                    data: "action",
                    name: 'action',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
            ],
            "fnInitComplete": function(oSettings, json) {

            }
        });



    });
</script>
@endsection