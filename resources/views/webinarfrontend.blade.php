@extends('frontend.layouts.app')
@section('title') Home Page @endsection
@section('content')
<main>
   <section class="AboutMain">
      <div class="container">
         <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
            <h1>Webinar</h1>
            <div class="bd-example">
               <nav aria-label="breadcrumb ">
                  <ol class="breadcrumb m-0">
                     <li><a href="">Home / </a></li>
                     <li><a href="">Editorial</a></li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </section>
   <section class="WebinarPG">
      <div class="container">
         <div class="row">
            <div class="col-md-3">
               @foreach ($linkwebinar as $reportdata)
               <div class="WebinarPGBox">
                  <div class="WebinarPGBoxImg">
                     <!-- <img src="./assets/images/blogs__img5.png" /> -->
                     <iframe src="{{$reportdata->path}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                  </div>
                  <p>{{$reportdata->title}}</p>
                  <p><span><i class="fas fa-play"></i> May 7, 2022</span> <span><i class="fas fa-play"></i>{{$reportdata->reviews}} Comments</span> </p>
               </div>
               @endforeach
            </div>

            <div class="row">
               <div class="col-md-12">
                  <div class="paginationCdss">
                     <div class="bd-example-snippet bd-code-snippet">
                        <div class="bd-example">
                           <nav aria-label="Another pagination example">
                              <ul class="pagination pagination-lg flex-wrap">
                                 <li class="page-item disabled">
                                    <a class="page-link"><svg class="svg-inline--fa fa-long-arrow-alt-left fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                          <path fill="currentColor" d="M134.059 296H436c6.627 0 12-5.373 12-12v-56c0-6.627-5.373-12-12-12H134.059v-46.059c0-21.382-25.851-32.09-40.971-16.971L7.029 239.029c-9.373 9.373-9.373 24.569 0 33.941l86.059 86.059c15.119 15.119 40.971 4.411 40.971-16.971V296z"></path>
                                       </svg><!-- <i class="fas fa-long-arrow-alt-left"></i> Font Awesome fontawesome.com --></a>
                                 </li>
                                 <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                 <li class="page-item" aria-current="page">
                                    <a class="page-link" href="#">2</a>
                                 </li>
                                 <li class="page-item"><a class="page-link" href="#">3</a></li>
                                 <li class="page-item"><a class="page-link" href="#">4</a></li>
                                 <li class="page-item"><a class="page-link" href="#">5</a></li>
                                 <li class="page-item"><a class="page-link" href="#">6</a></li>
                                 <li class="page-item">
                                    <a class="page-link" href="#"><svg class="svg-inline--fa fa-long-arrow-alt-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="long-arrow-alt-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                          <path fill="currentColor" d="M313.941 216H12c-6.627 0-12 5.373-12 12v56c0 6.627 5.373 12 12 12h301.941v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.569 0-33.941l-86.059-86.059c-15.119-15.119-40.971-4.411-40.971 16.971V216z"></path>
                                       </svg><!-- <i class="fas fa-long-arrow-alt-right"></i> Font Awesome fontawesome.com --></a>
                                 </li>
                              </ul>
                           </nav>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
   </section>


   <section class="webinarRegister">
      <div class="container">
         <div class="row">
            <div class="col-md-8 offset-md-2">
               <div class="contactForm shadow past__border">
                  <h3>Register No</h3>
                  <h4>to get informed when we publish new webinar</h4>
                  <form class="forms-sample" method="post" enctype="multipart/form-data" action="{{route('webinarfrontend-register.store')}}">
                     @csrf
                     <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3">
                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="text" name="name" class="form-control" id="floatingInput" placeholder="First Name" value="{{old('name')}}">
                              <label for="floatingInput">First Name</label>
                              @if ($errors->has('name'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="text" name="last_name" class="form-control" id="floatingInput" placeholder="Last Name" value="{{old('last_name')}}">
                              <label for="floatingInput">Last Name</label>
                              @if ($errors->has('last_name'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('last_name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Email" value="{{old('email')}}">
                              <label for="floatingInput">Email</label>
                              @if ($errors->has('email'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('email') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>

                        <div class="col-md">
                           <div class="form-floating mb-3">
                              <input type="text" name="phone" class="form-control" id="floatingInput" placeholder="Phone" value="{{old('phone')}}">
                              <label for="floatingInput">Phone</label>
                              @if ($errors->has('phone'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-floating mb-3">
                              <input type="text" name="post_code" class="form-control" id="floatingInput" placeholder="Post Code" value="{{old('post_code')}}">
                              <label for="floatingInput">Post Code</label>
                              @if ($errors->has('post_code'))
                              <span class="help-block">
                                 <strong>{{ $errors->first('post_code') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md-12">
                           <p>
                              By providing your details, you agree to Veye’s Terms & Conditions and Privacy Policy. and
                              to receive
                              marketing offers by email,text message or phone call from us or our agents until you opt
                              out.
                           </p>
                        </div>
                        <div class="col-md-12">
                           <button class="btn btn-primary" type="submit">Register Now</button>
                        </div>
                     </div>
                  <!-- </form> -->
               </div>
            </div>
         </div>
      </div>
   </section>
   @endsection