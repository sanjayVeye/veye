
<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
        <li class="sidebar-toggler-wrapper hide">
            <div class="sidebar-toggler">
                <span></span>
            </div>
        </li>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
        <li class="sidebar-search-wrapper">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
            <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="icon-close"></i>
                </a>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit">
                            <i class="icon-magnifier"></i>
                        </a>
                    </span>
                </div>
            </form>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>


        @foreach(Nav::list() as $key=>$row)
            <li class="nav-item start {{$key==0 ? 'active open' : ''}} ">
                <a href="{{count($row['childs']) > 0 ? 'javascript:;' : url($row['path']) }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">{{$row['name']}}</span>
                    @if($key==0)
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    @foreach($row['childs'] as $cRow)
                        <li class="nav-item start ">
                            <a href="{{url($cRow['path'])}}" class="nav-link ">
                                <i class="icon-bar-chart"></i>
                                <span class="title">{{$cRow['name']}}</span>
                            </a>
                        </li>
					@endforeach
                </ul>
            </li>
        @endforeach
    </ul>
    <!-- END SIDEBAR MENU -->
    <!-- END SIDEBAR MENU -->
