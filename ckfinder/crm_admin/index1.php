<html><head>
        <meta charset="UTF-8">
        <title>Veye :: Admin Panel</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <link href="https://veye.com.au/veye-adminpanel/css/admin-style.css" rel="stylesheet" type="text/css">

</head>
<style>h4 {
    color: #fff;
}</style>
<body>
<div class="main-container">
	<div class="admin-page">
		<div class="page">
        <div class="admin-box">
        	<div class="log-tile">Veye :: CRM Admin</div>
        		<div class="input-box">
            <form method="post" action="https://veye.com.au/veye-adminpanel/">



                    <div class="text"> <br>

                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="">
                    </div>

                    <div class="text"> <br>

                        <input type="password" class="form-control" id="Password" name="password" placeholder="Password">
                    </div>

                    <div class="raw-bar">
                    <label> <input type="checkbox"> Remember Me </label>
	                <button class="log-btn" name="login" type="submit">Login</button>
        			</div>

                    </form>
                </div>

                <div class="raw-box2">
               		<div class="logo"><img style="width: 180px; margin: 0 0 20px 20px;" src="img/veye-new.svg"></div>

                    <div class="logo-ftr"><h4>Veye Admin Panel</h4></div>
						<div class="forgt-pas">
                        	<a href="index.php">I Forgot my password</a>
                        </div>
					<div class="new-ac">
                 		   <a class="text-center" href="index.php">Register a new membership</a>
                    </div>
                </div>
        </div>

        <!--<div class="shadow"></div>-->
        </div>
	</div>
	<div><p style="color: #6a6a6a;margin: 7em 20px 0 0;text-align: right;">Developed By 
    <a href="http://www.tech9logy.com/" style="color: #524f4f;">Tech9logy Creators</a></p></div>
</div>



</body></html>