<?php
session_start();
if(isset($_SESSION['id'])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | General Form Elements</title>

<?php include('head.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- header section starts here -->

<?php include('header.php'); ?>

<!-- header section ends here -->

<!-- sidebar section starts here -->

<?php include('sidebar.php'); ?>

<!-- sidebar section ends here -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content">
		<div class="row">
            <div class="col-xs-12"> 
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">All Product</h3><p id="abcd"></p>
						
						<div class="box-tools pull-right">
							<!-- Buttons, labels, and many other things can be placed here! -->
							<!-- Here is a label for example -->
							<a href="create_invoice.php"><button class="btn btn-primary">Add Invoice</button></a>
						</div><!-- /.box-tools -->
					</div><!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#</th>
								
									<th>ID</th>
									<th>report no.</th>
									
								</tr>
							</thead>
							<tbody>
							    
						     <?php
							    include('conn.php');
							    $sql = "SELECT * FROM latest_reports";
							    $query = $conn->query($sql);
							    
							    $i = 1;
							    while($row = $query->fetch_assoc()){
						    ?>
							    
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $row['ID']; ?></td>
										<td><?php $letest_r= "VEYE-DR" . $row['ID'];  
										
										$sql_l="update latest_reports set report_no='".$letest_r."'";
										$query_l=$conn->query($sql_l);
										
										
										
										
										
										?>
										
										
										
										</td>
									
									</tr>
									<?php $i++; } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>#</th>
								
									<th>ID</th>
									<th>report no.</th>
									
								</tr>
							</tfoot>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</section><!-- /.content --><!-- /.content -->

    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
       $( function() {
    $("#fromdate, #todate").datepicker({
    	dateFormat:'yy-mm-dd'
    });
  } );


 $(function(){
$('#Monthybtn').click(function(){
   
	$('.monthly').show();
	$('.range').hide();
});
$('#Rangebtn').click(function(){
	$('.monthly').hide();
	$('.range').show();
});
 });
  </script>
  <script>
      function delete_dwi(id){
          var country = "product";
          var confm = confirm("Do you really want to delete this product ?");
          var datastring = 'id1='+id+'&table1='+country;
          if(confm){
            
            $.ajax({
               url : "delete_code.php",
               type : "POST",
               data : datastring,
               success : function(data){
                   alert(data);
                   window.location.href="view_all_product.php";
               }
            });
            
          }
          
      }
      
      
    //   function active_or_inactive(id,value){
    //     var country = "city"; 
    //     var datastring = 'id1='+id+'&table1='+country+'&value1='+value; 
         
    //       $.ajax({
    //           url : "active_or_inactive.php",
    //           type : "POST",
    //           data : datastring,
    //           success : function(data){
    //               alert(data);
    //               window.location.href="view_all_city.php";
    //           }
    //         });
          
    //   }
      
  </script>
  
 <?php  include('footer.php');  ?>
<?php
}else{
    header("Location: index.php");
}
?>