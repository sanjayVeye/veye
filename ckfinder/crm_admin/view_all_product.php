<?php
session_start();
if(isset($_SESSION['id'])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | General Form Elements</title>

<?php include('head.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- header section starts here -->

<?php include('header.php'); ?>

<!-- header section ends here -->

<!-- sidebar section starts here -->

<?php include('sidebar.php'); ?>

<!-- sidebar section ends here -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content">
		<div class="row">
            <div class="col-xs-12"> 
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">All Product</h3><p id="abcd"></p>
						<div class="box-tools pull-right">
							<!-- Buttons, labels, and many other things can be placed here! -->
							<!-- Here is a label for example -->
							<a href="add_new_city.php"><button class="btn btn-primary">Add Product</button></a>
						</div><!-- /.box-tools -->
					</div><!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Product Name</th>
									<th>HSN Code</th>									
									<th>GST</th>
									<th width="12%">Action</th>
								</tr>
							</thead>
							<tbody>
							    
						     <?php
							    include('conn.php');
							    $sql = "SELECT * FROM product";
							    $query = $conn->query($sql);
							    
							    $i = 1;
							    while($row = $query->fetch_assoc()){
						    ?>
							    
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $row['pro_name']; ?></td>
										<td><?php echo $row['pro_hsn_code']; ?></td>
										<td><?php echo $row['pro_gst']; ?></td>
										<td>
											<a class="btn btn-success btn-xs" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="edit_product.php?id=<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i></a>
											<span id="10">
												<button class="btn btn-danger btn-xs" id="<?php echo $row['id']; ?>" data-placement="top" data-toggle="tooltip" data-original-title="Delete" onclick="delete_dwi(this.id)"><i class="fa fa-trash-o"></i></button>
											</span>
										</td>
									</tr>
									<?php $i++; } ?>
							</tbody>
							<tfoot>
								<tr>
									<<th>#</th>
									<th>Product Name</th>
									<th>HSN Code</th>									
									<th>GST</th>
									<th width="12%">Action</th>
								</tr>
							</tfoot>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</section><!-- /.content --><!-- /.content -->

    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <script>
      function delete_dwi(id){
          var country = "product";
          var confm = confirm("Do you really want to delete this product ?");
          var datastring = 'id1='+id+'&table1='+country;
          if(confm){
            
            $.ajax({
               url : "delete_code.php",
               type : "POST",
               data : datastring,
               success : function(data){
                   alert(data);
                   window.location.href="view_all_product.php";
               }
            });
            
          }
          
      }
      
      
    //   function active_or_inactive(id,value){
    //     var country = "city"; 
    //     var datastring = 'id1='+id+'&table1='+country+'&value1='+value; 
         
    //       $.ajax({
    //           url : "active_or_inactive.php",
    //           type : "POST",
    //           data : datastring,
    //           success : function(data){
    //               alert(data);
    //               window.location.href="view_all_city.php";
    //           }
    //         });
          
    //   }
      
  </script>
  
 <?php  include('footer.php');  ?>
<?php
}else{
    header("Location: index.php");
}
?>