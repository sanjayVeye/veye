<?php
session_start();
if(isset($_SESSION['id'])){

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>

<?php include('head.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- header section starts here -->

<?php include('header.php'); ?>

<!-- header section ends here -->

<!-- sidebar section starts here -->

<?php include('sidebar.php'); ?>

<!-- sidebar section ends here -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-edit"></i> Add New Emplopyee</h3>
					<div class="box-tools pull-right">
						<a a class="add" href="employee-detail.php"><button class="btn btn-primary">View All Emplopyee</button></a>
					</div>
				</div><!-- /.box-header -->
			<form action="" id="entryform" method="post" enctype="multipart/form-data">
				
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
														<!-- Custom Tabs -->
							<div class="nav-tabs-custom">
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1">
										<div class="row">
											<div class="col-md-8 col-md-offset-2">
												<div class="form-group">
													<label for="name">Employee Name:</label>
													<input type="text" autocomplete="off" class="form-control" id="name" name="name"  value="" placeholder="Enter Employee Name" required>
												</div>
												<div class="form-group">
													<label for="hsn">Employee Phone No.:</label>
													<input type="text" autocomplete="off" class="form-control" id="phone" name="phone" value="" placeholder="Enter Employee Phone No." >
												</div>
												<div class="form-group">
												<label for="hsn">Employee Email</label>
												<input type="email" class="form-control" name="email" placeholder="Enter Employee Email"></textarea>
												</div>
												<div class="form-group">
													<label for="gst">Employee Password</label>
												<input type="text" autocomplete="off" class="form-control" id="slugname" name="password" value="" placeholder="Enter Employee Password" >
												</div>
												<button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button>
											
											</div>
										</div><!-- /.row -->
									</div><!-- /.tab-pane -->
								</div><!-- /.tab-content -->
							</div><!-- nav-tabs-custom -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.box-body -->
				
				
			</form>
		</div><!-- /.box -->
    </section><!-- /.content --><!-- /.content -->

    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!--<b>Version</b>-->
    </div>
    <strong>Copyright &copy; <a href="https://www.veye.com.au/">Veye</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script>
    
    // function setslug(){
    //     var country_name = $('[name=name]').val();
    //     $("#slugname").val(country_name);
    // }
    
</script>

</body>
</html>
<?php
include('conn.php');
if(isset($_POST['submit'])){
    
    $name=$_POST['name'];
    $phone=$_POST['phone'];
    $email=$_POST['email'];
    $password=$_POST['password'];
    
    $sql= "INSERT INTO `employee`(`ename`, `eemail`, `epassword`,`ephone`) VALUES ('".$name."','".$email."','".$password."','".$phone."')";
$query = $conn->query($sql);

if($query==true)
     {
    
     echo '<script>alert("Employee Added Successfully");window.location.href="add_employee.php";</script>';

     }
     else
     {
     echo '<script>alert("Something Went Worng");</script>';   
     }
     
 

}
?>
<?php
}else{
    header("Location: index.php");
}
?>
