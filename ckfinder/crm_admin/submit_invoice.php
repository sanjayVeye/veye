<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>Invoice</title>
		<link rel="stylesheet" type="text/css" href="css/print_style.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

	<style>
.meeta img {
    max-width: 13%;
    float: left;
    background:#676363;
}
.logo1 h2 {
    background: #676363;
    color: #fff;
    text-align: center;
    width: 100%;
    padding: 10px 0;
    margin: 10px 0;
}
p.in-word {
    margin: 0;
}
.name-uper {
    text-transform: uppercase;
}
.bill-to {

    display: inline-block;
}
.bill-too {
    width: 60%;
}
.ship-too {
    width: 39%;
}
</style>

</head>

<body>

	<div id="page-wrap">

		<!--<div class="logo1"><h2 style="text-align:center;">INVOICE</h2></div>-->


		<div style="clear:both"></div>

		<div id="customer">
		<h2 style="text-align:center; width: 100%;float: right;padding: 2% 0%;">INVOICE</h2><br/>
            <div class="meeta" style="width: 50%;display: inline-block;">

                <h2>Carpet of India</h2>
                <p>GST No. 06ALSPM9322Q1Z6</p>
                <p>H NO.-F/1743,</p>
                <p>Sector-49,FARIDABAD-121001</p>
                <p>Phone no. 9876541230</p>
            </div>
           <?php
           include('conn.php');
           $inc_id=$_GET['id'];
        //   echo $inc_id;
        //   die();
           $sql = " select * from create_invoice where id = '".$preview_id."'";
           $query = $conn->query($sql);
           while($fetch_row = $query->fetch_assoc()){

            //   echo "<pre>";
            //   print_r($fetch_row);
               $place_to = $fetch_row['place_to'];
              $invoicedate = $fetch_row['invoice_date'];
               $gst_no = $fetch_row['igst_number'];
               $billing_name = $fetch_row['billing_name'];
               $billing_address = $fetch_row['billing_address'];
               $ship_address = $fetch_row['ship_address'];
               $company_address = $fetch_row['company_address'];
               $invoice_number = $fetch_row['invoice_number'];
               $cust_id = $fetch_row['cust_id'];
               $phone = $fetch_row['phone'];
               $freight = $fetch_row['freight_value'];
            //   $product = $fetch_row['product'];
            //   $qty = $fetch_row['quantity'];
            //   $hsn_code = $fetch_row['hsn_code'];
            //   $tax_value = $fetch_row['taxable_value'];
            //   $gst_rate = $fetch_row['gst_rate'];
            //   $per_pesc = $fetch_row['per_pesc'];
            //   $rate = $fetch_row['rate'];
            //   $array1 = explode(',', $product);
            //   $array2 = explode(',', $qty);
            //   $array3 = explode(',', $hsn_code);
            //   $array4 = explode(',', $tax_value);
            //   $array5 = explode(',', $gst_rate);
            //   $array6 = explode(',', $rate);
            //   $pro_qty = array_combine($array1,$array2);
            //   $hsn_taxvalue = array_combine($array3,$array4);
            //   $gst_rate = array_combine($array5,$array6);
              // $res = array_merge($array1,$array2,$array3,$array4,$array5,$array6);
           }
          //die();
           ?>
            <table id="meta">
                <tr style="background: #eee;">
                    <th class="">Invoice </th>
                    <th><p>Date</p></th>
                </tr>
                <tr>    <td style="text-align: center;"><?php echo $invoice_number;   ?></td>
                    <td style="text-align: center;"><p id="date"><?php echo $invoicedate;   ?></p></td>
                </tr>
                <tr style="background: #eee;">
                    <th>Customer Id </th>
                    <td style="text-align: center;"><?php echo $cust_id;   ?></td>
                </tr>
            </table>

		</div>
<div style="clear:both"></div>
<br>
     <div class="bill-to bill-too
     ">
         <h6>Bill To:</h6>
     	<table>
     		 <tr class="">
     			<td class="bill-width name-uper" style="padding:0px;"><p><?php  echo $billing_name;   ?></p></td>
     		</tr>
     		<tr class="name-uper">
     			<td class="bill-width" style="padding:0px;"><p><?php echo $billing_address;   ?></p></td>
     		</tr class="name-uper">
     		<!--<tr>-->
     		<!--	<td class="bill-width" style="padding:0px;"><p> 547, block L, Karnal.</p></td>-->
     		<!--</tr>-->
     		<!--<tr>-->
     		<!--	<td class="bill-width" style="padding:0px;"><p> Model Town,</p></td>-->
     		<!--</tr>-->
     		<tr>
     			<td class="bill-width" style="padding:0px;"><p><?php echo $gst_no;   ?></p></td>
     		</tr>

     	</table>

     </div>
     <div class="bill-to ship-too">
         <h6>Ship To:</h6>
     	<table>
     		  <tr class="">
     			<td class="bill-width name-uper"><p<?php   echo $ship_name ?>></p></td>
     		</tr>
     		<tr class="name-uper">
     			<td class="bill-width"><p><?php  echo $ship_address;  ?></p></td>
     		</tr class="name-uper">
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     		<tr>
     			<td class="bill-width"><p><?php  echo $ship_gst; ?></p></td>
     		</tr>
     	</table>

     </div>


<div style="clear:both"></div>
<br><br>
     <table id="items">

		  <tr>
		      <th style="text-align: right;">Sr No.</th>
		      <th style="text-align: right;">Description Of Goods</th>
		      <th style="text-align: right;">HSN/SAC</th>
		      <th style="text-align: right;">Gst <br>Rate</th>
		      <th style="text-align: right;">Qty</th>
		      <th style="text-align: right;">Rate</th>
		      <th style="text-align: right;">per</th>
		      <th style="text-align: right;">Amount</th>
		  </tr>
		  <?php
		  	    $sql_1 = "select * from product_fetch where pro_id='".$preview_id."'";
		  	    $query_pr = $conn->query($sql_1);
		  	    $i=1;
		  	    while($fetch_rows=$query_pr->fetch_assoc()){
		  	     $tax_amount = $fetch_rows['tax_value_amount'];
		  	     //$gst_rate = $fetch_rows['gst_rate'];
		  	     $qant =$fetch_rows['qty'];
		  	    ?>
		  	<tr class="no-boder"> 
		  	  <td><?php echo $i;    ?></td>
		      <td> <?php echo $fetch_rows['pro_name'];   ?></td>
		      <td><?php echo $fetch_rows['hsn_code'];    ?></td>
		      <td><?php echo $fetch_rows['gst_rate'];   ?></td>
		      <td><?php if(!empty($qant))
                     {echo money_format('%n',$qant);}
                     else{echo money_format('%n','0');}
                     $pro_quant +=$qant;
                     ?></td>
		      <td><?php echo $fetch_rows['rate'];   ?></td>
		      <td><?php echo $fetch_rows['per_psc'];   ?></td>
		      <td>
		      <?php if(!empty($tax_amount))
                     {echo money_format('%n',$tax_amount);}
                     else{echo money_format('%n','0');}
                     $tax_sum +=$tax_amount;
                     ?>
		      </td>
		       <?php
		       $i++;
		  	    }
		      ?>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td><hr style="border: 1px solid #000;"><?php  echo $tax_sum; ?></td>
		  </tr>
		   <tr class="no-boder">
		      <td></td>
		      <td style="text-align: right;"><span> Freight </span><br></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td><?php echo $freight;   ?></td>

		  </tr>
		   <?php
		  	    $sql_2 = "select * from product_fetch where pro_id='".$preview_id."'";
		  	    $query_2 = $conn->query($sql_2);
		  	    $i=1;
		  	    while($fetch_rows_2=$query_2->fetch_assoc()){
		  	     //$tax_amount = $fetch_rows['tax_value_amount'];
		  	     $gst_rate = $fetch_rows_2['gst_rate'];
		  	     //$qant =$fetch_rows['qty'];
		  	    ?>
		  <tr class="no-boder">
		      <td></td>
		      <td style="text-align: right;"> <span> Output GST@<?php echo $gst_rate;    ?>% </span><br></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td><?php   echo $gst_rate; ?></td>
		      <td>%</td>
		      <td><?php $amt=$tax_sum+$freight; $total_sumc = ($amt*$gst_rate)/100;
		      $total_gst += $total_sumc;

  echo money_format('%n',$total_sumc); ?></td>
		  </tr>
		  <?php }?>
		  <!--<tr class="no-boder">-->
		  <!--    <td></td>-->
		  <!--    <td style="text-align: right;"><span> Output SGST@<?php echo $sgst_cgst_rate;    ?>% </span><br></td>-->
		  <!--    <td></td>-->
		  <!--    <td></td>-->
		  <!--    <td></td>-->
		  <!--    <td><?php echo $cgst;    ?></td>-->
		  <!--    <td>%</td>-->
		  <!--    <td><?php echo $sgst_calculate;    ?></td>-->
		  <!--</tr>-->
		 <td></td>
		      <td style="font-weight:bold; text-align:right">Total</td>
		      <td></td>
		      <td></td>
		      <td><?php echo $pro_quant;  ?>Nos</td>
		      <td></td>
		      <td><p  style=""></p></td>
		      <td><p style=""><?php   $final_total=$tax_sum+$freight+$total_gst; echo money_format('%n',$final_total);   ?></p></td>
		  </tr>
		<tr>
		    <td colspan="8">
		         <p style="text-align:right"><b>E & O.E</b></p><br>
		        <span>Amount Chargeable (in words) : </span><br>

		        <span style="">
		                    		 <?php
            $number = $final_total;
            $no = round($number);
            $point = round($number - $no, 2) * 100;
            $hundred = null;
            $digits_1 = strlen($no);
            $i = 0;
            $str = array();
            $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
            $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
            while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
            }
            $str = array_reverse($str);
            $result = implode('', $str);
            $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
            echo $result . "  ";
		?> Only</span>
		    </td>
		</tr>
			<tr>
		    <td colspan="2" rowspan="2">
		        <p style="text-align: right;    font-size: 13px;"><span >Taxable</span></p><br>
		        <!--<p style="text-align: right;    font-size: 13px;"><span >Taxable</span></p>-->

		    </td>
		     <td colspan="4" style="text-align: center;font-size: 13px;">GST Tax</td>
		      <td colspan="2" rowspan="2" style="text-align: center;font-size: 13px;">Total<br> Tax Amount</td>
		</tr>
		<tr>
		     <td colspan="2" style="text-align: center;font-size: 13px;">Rate </td>
		     <td colspan="2" style="text-align: center;font-size: 13px;">Amount</td>
		 
		</tr>
      <?php 
      $sql_gst = "select * from product_fetch where pro_id='".$preview_id."'";
		  	    $query_gst = $conn->query($sql_gst);
		  	    while($fetch_rows_gst=$query_gst->fetch_assoc()){
		  	   //  $tax_amount = $fetch_rows['tax_value_amount'];
		  	     $gst_rate_gst = $fetch_rows_gst['gst_rate'];
		  	   //  $qant =$fetch_rows['qty'];
		  	    ?>
      
     
			<tr>
		   <td colspan="2" style="text-align:right; font-size:13px;"><?php   echo $final_total;?></td>
		     <td colspan="2" style="text-align:center; font-size:13px;"><?php echo $gst_rate_gst;  ?>% </td>
		     <td  colspan="2" style="text-align:center; font-size:13px;"><?php $amt=$tax_sum+$freight; $total_sumc = ($amt*$gst_rate_gst)/100;
		      $total_gst_sum += $total_sumc;
  echo money_format('%n',$total_sumc); ?></td>
		     <td colspan="2" style="text-align:center; font-size:13px;"><?php echo money_format('%n',$total_sumc);  ?></td>

		</tr>
		<?php  }
		?>
		<tr>
		   <td colspan="2" style="text-align:right; font-size:13px;">Total:<b><?php echo $final_total;?></b></td>
		     <!--<td style="text-align:center; font-size:13px;"></td>-->
		     <td colspan="2" style="text-align:center; font-size:13px;"><b>-</b></td>
		      <!--<td style="text-align:center; font-size:13px;"></td>-->
		     <td colspan="2" style="text-align:center; font-size:13px;"><b><?php echo $total_gst_sum;  ?></b></td>
		     <td colspan="2" style="text-align:center; font-size:13px;"><b><?php echo $total_gst_sum;?></b></td>
		</tr>
		<tr>
		    <td colspan="8">

		        <span style="">Tax Amount (in words) :               		 <?php
            $number = $total_gst_sum;
            $no = round($number);
            $point = round($number - $no, 2) * 100;
            $hundred = null;
            $digits_1 = strlen($no);
            $i = 0;
            $str = array();
            $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
            $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
            while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
            }
            $str = array_reverse($str);
            $result = implode('', $str);
            $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
            echo $result . "  ";
		?> Only</span>
		       <br><br><br>
		    </td>
		</tr>
			<tr>
		    <td colspan="4" class="no-boder"><br><br><br><br>
		        <span style="text-decoration: underline;">Decleration</span><br>
		        <span style="font-size:13px;">We Decleare that this invoice shows the actual price of the goods <br>describes and that all particular are true and correct.</span>
		    </td>


		        <td colspan="4" rowspan="2">
		        <p style="text-align: right;font-size: 13px;"><span >For Carpet Couture</span></p><br><br><br><br>
		        <p style="text-align: right;font-size: 13px;"><span >Authorised Signature</span></p>
		    </td>
		    </td>
		</tr>
		</table>

<p style="text-align: center;padding: 1% 0%;">[ This is a Computer Generated Invoice ]
</p>
</div>
<div style="clear:both"></div>
</div>
</body>
<style>

 @media print{
 .logo::before {
    background: #21211f;
     -webkit-print-color-adjust: exacstyle="background: #eee;"t;
     border-bottom: 6px solid #b27919;
}
td.price2 {
    border-top: 1px solid !important;
     -webkit-print-color-adjust: exact;
    border-bottom: 1px solid !important;
}
.meeta img {
    background:#676363;
    -webkit-print-color-adjust: exact;
}
.logo1 h2 {
    background: #676363;
    -webkit-print-color-adjust: exact;
    color: #fff;
}

.form-below th {
    background: #676363;
     -webkit-print-color-adjust: exact;
 }
#items th {
     -webkit-print-color-adjust: exact;
     background: #676363;
}
.meeta img {
    background:#676363;
    -webkit-print-color-adjust: exact;
}

.logo1 h2 {
    background: #676363;
    -webkit-print-color-adjust: exact;
    color: #fff;
}
.bill-to h6 {
     -webkit-print-color-adjust: exact;
     background: #635b5b;
 }
.logo::after {
    background: #21211f;
     -webkit-print-color-adjust: exact;
    border-bottom: 6px solid #b27919;
 }
 }
.logo {
    height: 118px;
    width: 800px;
    margin: 0 auto;
    max-width: 100%;
}
.logo::before {
    background: #21211f;
    content: "";
    height: 118px;
    width: 31%;
    position: absolute;
    top: 0px;
    left: 19%;
    z-index: -1;
    border-bottom-left-radius: 93%;
    border-bottom: 6px solid #b27919;
}
.logo::after {
    background: #21211f;
    content: "";
    height: 118px;
    width: 31%;
    position: absolute;
    top: 0px;
    left: 50%;
    z-index: -1;
    border-bottom: 6px solid #b27919;
    border-bottom-right-radius: 93%;
}
.logo img {
    width: 14%;
    margin: 0 auto;
    display: block;
    z-index: 999;
}
.print-button button.btn.btn-success {
    font-size: 21px;
    padding: 4px 0;
    width: 16%;
    background: #2e882e;
    color: #fff;
}
.print-button {
    text-align: center;

}
@media print {
  #removebutton {
    display: none;
  }
}
</style>
<div class="print-button">
	<button type="button" id="removebutton" onclick="BillPrint()" class="btn btn-success " >Print</button></div>

</body>
<script>

    function BillPrint(){
        window.print();
    }

</script>
</html>
