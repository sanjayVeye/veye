<?php
session_start();
if(isset($_SESSION['id'])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | General Form Elements</title>

<?php include('head.php'); ?>
<style>
    .container{
        max-width:100%;
    }
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- header section starts here -->

<?php include('header.php'); ?>

<!-- header section ends here -->

<!-- sidebar section starts here -->

<?php include('sidebar.php'); 
    include('conn.php');
    
?>

<!-- sidebar section ends here -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
			Invoice
			<small>Invoice Create1</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Create New Invoice</li>
		</ol>
	</section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
			<form action="" method="post" enctype="multipart/form-data">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-edit"></i> Create New Invoice</h3>
					<div class="box-tools pull-right">
						<a class="btn btn-block btn-primary" href="view_all_invoice.php">View All</a>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body">
				    <?php
				    $edit_id = $_GET['id'];
    $sql= "select * from create_invoice where id = '".$edit_id."'";
    $query = $conn->query($sql);
    while($fetch_row=$query->fetch_assoc()){
        //  echo "<pre>";
        //  print_r($fetch_row);
        $state=$fetch_row['place_to'];
        $invoice_date=$fetch_row['invoice_date'];
        $gst_no=$fetch_row['igst_number'];
        $billing_name=$fetch_row['billing_name'];
        $billing_add=$fetch_row['billing_address'];
        $ship_add=$fetch_row['ship_address'];
        $invoice_no=$fetch_row['invoice_number'];
        $freight_value=$fetch_row['freight_value'];
        $total_amount=$fetch_row['total_amount'];
        $mobile=$fetch_row['phone'];
       
        
    }
	?>				<div class="row">
						<div class="col-md-12">
														<!-- Custom Tabs -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="type">PLACE TO SUPPLY:</label>
										<input list="select-state" name="state" id="state" class="form-control" value="<?php echo $state; ?>" placeholder="Place of Supply";}?>
         <div class="datalist">
            <datalist id="select-state">
               <option value="Andhra Pradesh">Andhra Pradesh</option>
               <option value="Arunachal Pradesh">Arunachal Pradesh</option>
               <option value="Assam">Assam</option>
               <option value="Bihar">Bihar</option>
               <option value="Chhattisgarh">Chhattisgarh</option>
              <option value="Delhi">Delhi</option>
               <option value="Goa">Goa</option>
               <option value="Gujarat">Gujarat</option>
               <option value="Haryana">Haryana</option>
               <option value="Himachal Pradesh">Himachal Pradesh</option>
               <option value="Jammu and Kashmir">Jammu and Kashmir</option>
               <option value="Jharkhand">Jharkhand</option>
               <option value="Karnataka">Karnataka</option>
               <option value="Kerala">Kerala</option>
               <option value="Madya Pradesh">Madya Pradesh</option>
               <option value="Maharashtra">Maharashtra</option>
               <option value="Manipur">Manipur</option>
               <option value="Meghalaya">Meghalaya</option>
               <option value="Mizoram">Mizoram</option>
               <option value="Nagaland">Nagaland</option>
               <option value="Orissa">Orissa</option>
               <option value="Punjab">Punjab</option>
               <option value="Rajasthan">Rajasthan</option>
               <option value="Sikkim">Sikkim</option>
               <option value="Tamil Nadu">Tamil Nadu</option>
               <option value="Tripura">Tripura</option>
               <option value="Uttaranchal">Uttaranchal</option>
               <option value="Uttarakhand">Uttarakhand</option>
               <option value="Uttar Pradesh">Uttar Pradesh</option>
               <option value="West Bengal">West Bengal</option>
            </datalist>
         </div>
									</div>
									<div class="form-group" id="shownumberinput">
										<label for="numbercountry">INVOICE DATE:</label>
										<input type="text" autocomplete="off" required class="form-control" id="invoice_date" name="invoice_date" value="<?php echo $invoice_date;  ?>" placeholder="Enter Invoice Date" >
									</div>
									<div class="form-group">
										<label for="inclusion">IGST NUMBER:</label>
										<input type="text" autocomplete="off" required class="form-control" id="gst_no" name="gst_no" value="<?php echo $gst_no;  ?>" placeholder="Enter GST Number">
									</div>
									<div class="form-group">
										<label for="name">BILLING NAME:</label>
										<input type="text" autocomplete="off" required class="form-control" id="billing_name" name="billing_name" value="<?php echo $billing_name;  ?>" onkeyup="setslug()" placeholder="Enter Billing Name">
									</div>
								</div>

								<div class="col-md-6">
								<div class="form-group">
										<label for="price">YOUR COMPANY ADDRESS:</label>
										<input type="text" autocomplete="off" required class="form-control" id="company_address" name="company_address" placeholder="Enter company address " value="<?php echo "H NO.-F/1743,Sector-49,FARIDABAD-121001";  ?>" readonly>
									</div>
									
								<div class="form-group">
										<label for="exclusion">INVOICE NUMBER:</label>
										<input type="text" autocomplete="off" class="form-control" id="invoice_number" name="invoice_number" required  placeholder="Enter Invoice Number" value="<?php echo "XXXXX/XX-XX/XXX"; ?>" readonly>
									</div>
								<div class="form-group">
										<label for="slugname">BILLING ADDRESS:</label>
										<input type="text" autocomplete="off" class="form-control" id="billing_address" name="billing_address" placeholder="Enter Billing Address"  value="<?php echo $billing_add;  ?>">
									</div>
									<div class="form-group">
										<label for="slugname">SHIP TO ADDRESS:</label>
										<input type="text" autocomplete="off" class="form-control" id="shiping_address" name="shiping_address" placeholder="Enter Shiping  Address" value="<?php echo $ship_add;  ?>">
									</div>
									<div class="form-group">
										<label for="slugname">PHONE NO.:</label>
										<input type="text" autocomplete="off" class="form-control" id="phone" name="phone" placeholder="Enter Phone Number"  value="<?php echo $mobile;   ?>">
									</div>	
								</div>
                                <div class="col-md-12">
                                <hr style="border:1px solid #000000;" />
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 text-center">
                                        <h4>Products</h4>
                                    </div>
                               <?php
                              $sql_pro = "select * from product_fetch where pro_id='".$edit_id."' ";
                              $query_pro = $conn->query($sql_pro);
                              while($fetch_row_pro=$query_pro->fetch_assoc()){
                                    // echo "<pre>";
                                    // print_r($fetch_row_pro);
                                  $pro_name=$fetch_row_pro['pro_name'];
                                  $hsn_code=$fetch_row_pro['hsn_code'];
                                  $gst_rate=$fetch_row_pro['gst_rate'];
                                  $qty=$fetch_row_pro['qty'];
                                  $rate=$fetch_row_pro['rate'];
                                  $perpesc=$fetch_row_pro['per_psc'];
                                  $taxamount=$fetch_row_pro['tax_value_amount'];
                                  
                                
                              
                               ?>
                                    <div class="container" id="dynamic_flight_section">
                                    <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="name">PRODUCT NAME:</label>
                                    <input type="text" autocomplete="off"  class="form-control" id="hsn_code" name="pr_name[]"  placeholder="Enter Product Name" value="<?php echo $pro_name; ?>" >
                                    </div>
                                    </div>
                                                                      
                                        <div class="col-md-3">
                                            	<div class="form-group">
										<label for="type">HSN CODE:</label>
										<input type="text" autocomplete="off"  class="form-control" id="hsn_code" name="hsn_code[]"  placeholder="Enter HSN Code" value="<?php echo $hsn_code ;   ?>">
									</div>	
                                        </div>
                                        <div class="col-md-3">
                                            	<div class="form-group">
										<label for="type">GST RATE:</label>
										<input type="text" autocomplete="off"  class="form-control" id="gst_rate" name="gst_rate[]"  placeholder="Enter GST Code" value="<?php echo $gst_rate;   ?>">
									</div>	
                                        </div>
                                         <div class="col-md-3">
                                           <div class="form-group">
										<label for="name">QUANTITY:</label>
										<input type="text" autocomplete="off" required class="form-control" id="qty-0" name="qunity[]" onkeyup="calculate_ttl()" value="<?php echo $qty; ?>"  placeholder="Enter quantity Value">
									</div>
                                    
                                        </div>
                                        <div class="col-md-3">
                                           <div class="form-group">
										<label for="name">RATE:</label>
										<input type="text" autocomplete="off" required class="form-control" id="rate-0" name="rate[]" onkeyup="calculate_ttl()"  placeholder="Enter rate amount  Value" value="<?php  echo $rate; ?>">
									</div>
                                    
                                        </div>
                                         <div class="col-md-3">
                                           <div class="form-group">
										<label for="name">PER:</label>
										<input type="text" autocomplete="off" required class="form-control" id="per" name="per[]"   placeholder="Enter Per  Value" value="<?php echo $perpesc; ?>">
									</div>
                                    
                                        </div>
                                        <div class="col-md-3">
                                             <div class="form-group">
										<label for="slugname">TAXABLE VALUE:</label>
										<input type="text" autocomplete="off" class="form-control" id="tax_value-0" name="tax_value[]" onkeyup="calculate_ttl()" placeholder="Enter Tax value"  value="<?php echo $taxamount; ?>">
									</div>	
                                            
                                
                                        </div>
                                        <div class="col-md-3">
                                
                                            <div class="form-group">
                                                <label for="arrtime">ADD MORE</label>
                                                <button type="button" id="add" required class="btn btn-success form-control" >ADD</button>
                                            </div>
                                        </div>
                                        <?php }?>
                                    </div>
                                
                                </div>
                                
                                <div class="col-md-12">
                                    <hr style="border:1px solid #000000;" />
                                </div>
                                </div>
                                <div class="col-md-6">
                                           <div class="form-group">
										<label for="name">FREIGHT:</label>
										<input type="text" autocomplete="off" required class="form-control" id="freight" value="<?php echo $freight_value;  ?>" name="freight" onkeyup="calculate_ttl()"  placeholder="Enter freight  Value">
									</div>
                                    
                                        </div>
                                <div class="col-md-6">
                                           <div class="form-group">
										<label for="name">TOTAL AMMOUNT:</label>
										<input type="text" autocomplete="off" required class="form-control" id="toatal_amount" name="total_amount" value="<?php echo $total_amount;   ?>" onkeyup="calculate_ttl()"  placeholder="Enter total amount  Value">
									</div>
                                    
                                        </div>
							</div><!-- /.row -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer">
					<div class="box-tools pull-right">
													<button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button>
												<a href="view_all_package_mgmnt.php" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</form>
		</div><!-- /.box -->
    </section><!-- /.content --><!-- /.content --><!-- /.content -->

    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <script>
        // var counter_id = 5;
            
        // var new_count_id = 1;
    // $(document).ready(function(){
       
       
    //     // $("#add_new").click(function(){
    //     //     alert("hello");
    //     // //   if(new_count_id <= counter_id){
    //     // //     // var new_append_section ='<div class="row" id="dlt_'+new_count_id+'"><div class="col-md-2"><div class="form-group"><label for="airlinename">Airline Name</label><input type="text" autocomplete="off" required class="form-control" id="airlinename"name="airlinename[]" value="" placeholder="Airline Name"></div></div><div class="col-md-2"><div class="form-group"><label for="depdate">Date</label><input type="text" autocomplete="off" required class="form-control departuredate datepicker" id="departuredate" name="depdate[]" value="" placeholder="Date"></div></div><div class="col-md-2"><div class="form-group"><label for="depcity">Daparture City</label><input type="text" autocomplete="off" required class="form-control" id="depcity" name="depcity[]" value="" placeholder="City"></div></div><div class="col-md-2"><div class="form-group"><label for="deptime">Daparture Time</label><input type="text" autocomplete="off" class="form-control" id="deptime6" name="deptime[]" value="" placeholder="Time"></div></div><div class="col-md-2"> <div class="form-group"><label for="arrtime">Arrival Time</label><input type="text" autocomplete="off" class="form-control" id="arrtime6" name="arrtime[]" value="" placeholder="Time"></div></div><button type="button" name="dlt_'+new_count_id+'" id="delete_div" class="btn btn-danger" onclick="RemoveInput(this.name)">Remove</button></div></div>';
    //     // //     var new_section = '<div class="row"><div class="row" id="dynamic_flight_section"><div class="col-md-4"><div class="form-group"><label for="name">PRODUCT NAME:</label><input type="text" autocomplete="off" required class="form-control" id="product" name="product"  placeholder="Enter Product Name"></div></div><div class="col-md-4"><div class="form-group"><label for="slugname">TAXABLE VALUE:</label><input type="text" autocomplete="off" class="form-control" id="tax_value" name="tax_value" placeholder="Enter Tax value" ></div></div><div class="col-md-4"><div class="form-group"><label for="arrtime">Add More</label><button type="button" id="add_new" required class="btn btn-primary form-control" onclick="addmore()">ADD</button></div></div></div></div><div class="col-md-12"><hr style="border:1px solid #000000;" /></div></div>'
    //     // //     $("#dynamic_flight_section").append(new_section);
    //     // //     new_count_id++;
    //     // //     }else{
    //     // //         alert("limit over");
    //     // //     }
           
            
    //     // });
        
    // });
          
//   function RemoveInput(name){
//       $("#"+name).remove();
//       new_count_id--;
     
//   }
            
</script>
<!--<script>-->
<!--var new_count = 1;-->
<!--      var product_count = 100;-->
      
<!--  function addmore(){-->
      
  <!--alert(product_count);-->
<!--     var new_section = '<div class="row" id="dlt_'+new_count+'"><div class="col-md-3"><div class="form-group"><label for="name">PRODUCT NAME:</label><input type="text" autocomplete="off" required class="form-control" id="product" name="pr_name[]"  placeholder="Enter Product Name"></div></div><div class="col-md-3"><div class="form-group"><label for="type">HSN CODE:</label><input type="text" autocomplete="off"  class="form-control" id="hsn_code" name="hsn_code[]"  placeholder="Enter HSN Code"></div></div> <div class="col-md-3"><div class="form-group"><label for="type">GST RATE:</label><input type="text" autocomplete="off"  class="form-control" id="gst_rate" name="gst_rate[]"  placeholder="Enter HSN Code" value="<?php echo $row['hsn_code'] ;   ?>"></div></div><div class="col-md-3"><div class="form-group"><label for="name">QUANTITY:</label><input type="text" autocomplete="off" required class="form-control" id="qty1" name="qunity[]" onkeyup="calculate_ttl_append()" placeholder="Enter quantity Value"></div></div><div class="col-md-3"><div class="form-group"><label for="name">RATE:</label><input type="text" autocomplete="off" required class="form-control" id="rate1" onkeyup="calculate_ttl_append()" name="rate[]"  placeholder="Enter rate Value"></div></div> <div class="col-md-3"><div class="form-group"><label for="type">PER:</label><input type="text" autocomplete="off"  class="form-control" id="per" name=" per[]"   placeholder="Enter per value" value=""></div></div><div class="col-md-3"><div class="form-group"><label for="slugname">TAXABLE VALUE:</label><input type="text" autocomplete="off" class="form-control" id="tax_value_1" onkeyup="calculate_ttl_append()" name="tax_value[]" placeholder="Enter Tax value" ></div></div><div class="col-md-3"><div class="form-group"><label for="slugname">Remove</label><button type="button" name="dlt_'+new_count+'" id="delete_div" class="btn btn-danger" onclick="RemoveInput(this.name)">Remove</button></div></div></div></div>'  -->
   <!--alert(new_section);-->
<!--if(new_count<=product_count){-->
<!--    $('#dynamic_flight_section').append(new_section);-->

<!--    new_count++;-->
<!--}else{-->
<!--    alert("limit over");-->
<!--}-->
<!--  }  -->
<!--  function RemoveInput(name){-->
<!--$("#"+name).remove();-->
<!--      new_count--;-->
     
<!--   }-->
    
<!--</script>-->
<!-- <script>-->
<!--        function get_hsn(value){-->
<!--        var product_name = value;-->
<!--        var datastring = 'name='+product_name;-->
<!--        $.ajax({-->
<!--           url  : "get_hsn.php",-->
<!--           type : "POST",-->
<!--           data : datastring,-->
<!--           success : function(data){-->
<!--               $("#hsn_code").html(data);-->
<!--           }-->
<!--        });-->
<!--        }-->
<!--    </script>-->
    
<!--    <script>-->
<!--        function calculate_ttl(){-->
<!--            var total_qty = $('#qty').val();-->
<!--            var total_qty_number = Number(total_qty);-->
<!--            var per_rate = $('#rate').val();-->
<!--            var total_per_rate = Number(per_rate);-->
<!--            var tax_value_amount = total_qty_number*per_rate;-->
<!--            var final_tax_value_amount = Number(tax_value_amount);-->
            
<!--            $('#tax_value').val(final_tax_value_amount);-->
            
        
             <!--alert(final_tax_value_amount);-->
<!--        }-->
<!--    </script>-->
<!--    <script>-->
<!--        function calculate_ttl_append(){-->
<!--             var total_qty_append = $('#qty1').val();-->
<!--            var total_qty_number_append = Number(total_qty_append);-->
            <!--alert(total_qty_append);-->
<!--            var per_rate_append = $('#rate1').val();-->
<!--            var total_per_rate_append = Number(per_rate_append);-->
<!--            var tax_value_amount_append = total_qty_number_append*per_rate_append;-->
<!--            var final_tax_value_amount_append = Number(tax_value_amount_append);-->
            
<!--            $('#tax_value_1').val(final_tax_value_amount_append);-->
<!--        }-->
<!--    </script>-->
<?php include('footer.php');    ?>

<?php



?>

<script>
$(document).ready(function() {
  var i = 0;

  $("#qty-" + i).change(function() {
    upd_art(i)
  });
  $("#rate-" + i).change(function() {
    upd_art(i)
  });
//   $("#tax_value-" + i).change(function() {
//     upd_art(i)
//   });


  $('#add').click(function() {
    i++;
  // $('#articles').append('<tr id="row' + i + '"><td><input type="number" value=0 id="quantity-' + i + '" name="quantity[]" placeholder="quantity" class="form-control name_list" /></td> <td><input type="number" id="price-' + i + '" name="price[]" value=0  placeholder="price" class="form-control name_list" /></td> <td><input type="number" id="total-' + i + '" name="total[]" placeholder="total" class="form-control name_list" readonly /></td> <td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
var new_section = '<div class="row" id="row' + i + '"><div class="col-md-3"><div class="form-group"><label for="name">PRODUCT NAME:</label><input type="text" autocomplete="off" required class="form-control" id="product" name="pr_name[]"  placeholder="Enter Product Name"></div></div><div class="col-md-3"><div class="form-group"><label for="type">HSN CODE:</label><input type="text" autocomplete="off"  class="form-control" id="hsn_code" name="hsn_code[]"  placeholder="Enter HSN Code"></div></div> <div class="col-md-3"><div class="form-group"><label for="type">GST RATE:</label><input type="text" autocomplete="off"  class="form-control" id="gst_rate" name="gst_rate[]"  placeholder="Enter GST Value" value="<?php echo $row['hsn_code'] ;   ?>"></div></div><div class="col-md-3"><div class="form-group"><label for="name">QUANTITY:</label><input type="text" autocomplete="off" required class="form-control" id="qty-' + i + '" name="qunity[]" onkeyup="calculate_ttl_append()" placeholder="Enter quantity Value"></div></div><div class="col-md-3"><div class="form-group"><label for="name">RATE:</label><input type="text" autocomplete="off" required class="form-control" id="rate-' + i + '" onkeyup="calculate_ttl_append()" name="rate[]"  placeholder="Enter rate Value"></div></div> <div class="col-md-3"><div class="form-group"><label for="type">PER:</label><input type="text" autocomplete="off"  class="form-control" id="per" name=" per[]"   placeholder="Enter per value" value=""></div></div><div class="col-md-3"><div class="form-group"><label for="slugname">TAXABLE VALUE:</label><input type="text" autocomplete="off" class="form-control" id="tax_value-' + i + '" onkeyup="calculate_ttl_append()" name="tax_value[]" placeholder="Enter Tax value" ></div></div><div class="col-md-3"><div class="form-group"><label for="slugname">Remove</label><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></div></div></div></div>';
$('#dynamic_flight_section').append(new_section);
//alert(new_section);
    $("#qty-" + i).change(function() {
      upd_art(i)
    });
    $("#rate-" + i).change(function() {
      upd_art(i)
    });


  });


  $(document).on('click', '.btn_remove', function() {
    var button_id = $(this).attr("id");
    $('#row' + button_id + '').remove();
  });

//   $('#submit').click(function() {
//     alert($('#add_name').serialize()); //alerts all values           
//     $.ajax({
//       url: "wwwdb.php",
//       method: "POST",
//       data: $('#add_name').serialize(),
//       success: function(data) {
//         $('#add_name')[0].reset();
//       }
//     });
//   });

  function upd_art(i) {
    var qty = $('#qty-' + i).val();
    var qty_number = Number(qty);
    var rate = $('#rate-' + i).val();
     var rate_number = Number(rate);
   // var tax = $('#tax_value-' + i).val();
    var totNumber = qty_number * rate_number;
     var final_tax_value_amount = Number(totNumber);
    var tot = final_tax_value_amount.toFixed(2);
    $('#tax_value-' + i).val(tot);
    //alert(tot);

    
  }



  //  setInterval(upd_art, 1000);
});
</script>
<?php
}else{
    header("Location: index.php");
}
?>

<?php
include('conn.php');
if(isset($_POST['submit'])){
$place_to = $_POST['state'];
$invoice_date = $_POST['invoice_date'];
$gst_no = $_POST['gst_no'];
$billing_name = $_POST['billing_name'];
$hsn_code = $_POST['hsn_code'];
$billing_address = $_POST['billing_address'];
$phone = $_POST['phone'];
$quantity = $_POST['qty'];
$freight = $_POST['freight'];
$total_amount = $_POST['total_amount'];
$ship_address = $_POST['shiping_address'];
$product               =       array_filter($_POST['pr_name']);
$hsn               =       array_filter($_POST['hsn_code']);
$qty               =       array_filter($_POST['qunity']);
$rate               =       array_filter($_POST['rate']);
$tax_value               =       array_filter($_POST['tax_value']);
$per_psc   =   array_filter($_POST['per']);
$gst_rate               =       array_filter($_POST['gst_rate']); 
$dt2=date("Y-m-d H:i:s");   
$sql="UPDATE `create_invoice` SET `place_to`='".$place_to."',`invoice_date`='".$invoice_date."',`igst_number`='".$gst_no."',`billing_name`='".$billing_name."',`billing_address`='".$billing_address."',`ship_address`='".$ship_address."',`phone`='".$phone."',`freight_value`='".$freight."',`total_amount`='".$total_amount."',`current_datetime`='".$dt2."' WHERE id='".$edit_id."'";
  $query= $conn->query($sql);
  //$last_id = mysqli_insert_id($conn);
//   echo $last_id;
//   die();

 $product_id = array();
foreach($product as $proname){
    $sql_1 = "INSERT INTO product_fetch (pro_id, pro_name) VALUE('".$edit_id."', '".$proname."')";
    $conn->query($sql_1);
    $product_id[] = $conn->insert_id;
}
$hsn_code_data = array_combine($product_id, $hsn);
   foreach($hsn_code_data as $key => $hsn_data){
      $update_hsn_code = "UPDATE product_fetch SET hsn_code='".$hsn_data."' WHERE id='".$key."' ";
     $row= $conn->query($update_hsn_code);
   
  }

  $qty_data = array_combine($product_id, $qty);
   foreach($qty_data as $key => $qunatity){
      $upadet_qty_data = "UPDATE product_fetch SET qty='".$qunatity."' WHERE id='".$key."' ";
      $row1=$conn->query($upadet_qty_data);
      
  }
  
  $gst_rate_data = array_combine($product_id, $gst_rate);
  foreach($gst_rate_data as $key => $gstrate){
      $upadet_gst_rate_data = "UPDATE product_fetch SET gst_rate ='".$gstrate."' WHERE id='".$key."' ";
      $row_6=$conn->query($upadet_gst_rate_data);
    //   echo "<pre>";
    //   print_r($row_6);
    //   die();
      
  }
   $rate_data = array_combine($product_id, $rate);
   foreach($rate_data as $key => $rate_ammnt){
      $upadet_rate_data = "UPDATE product_fetch SET rate='".$rate_ammnt."' WHERE id='".$key."' ";
      $row3=$conn->query($upadet_rate_data);
    //   echo "<pre>";
    //   print_r($row3);
    //   die();
  }
  
  $per_psc_data = array_combine($product_id, $per_psc);
   foreach($per_psc_data as $key => $perpsc){
      $upadet_per_psc_data = "UPDATE product_fetch SET per_psc='".$perpsc."' WHERE id='".$key."' ";
      $row_7=$conn->query($upadet_per_psc_data);
    //   echo "<pre>";
    //   print_r($row_7);
    //   die();
  }
  
  $tax_value_data = array_combine($product_id,$tax_value);
  foreach($tax_value_data as $key => $tax){
      $upadet_tax_value_data = "UPDATE product_fetch SET tax_value_amount='".$tax."' WHERE id='".$key."' ";
      $row2=$conn->query($upadet_tax_value_data);
   
  }
if($query==TRUE){
    echo '<script>alert("Invoice Update Successfully.php");window.location.href="view_all_invoice.php";</script>';
}
else{
    
    echo '<script>alert("Something Went wrong");window.location.href="view_all_invoice.php";</script>';
}

}
?>







