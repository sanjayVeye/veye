<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>Invoice</title>
		<link rel="stylesheet" type="text/css" href="css/print_style.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

	<style>
.meeta img {
    max-width: 13%;
    float: left;
    background:#676363;
}
.logo1 h2 {
    background: #676363;
    color: #fff;
    text-align: center;
    width: 100%;
    padding: 10px 0;
    margin: 10px 0;
}
p.in-word {
    margin: 0;
}
.name-uper {
    text-transform: uppercase;
}
.bill-to {

    display: inline-block;
}
.bill-too {
    width: 60%;
}
.ship-too {
    width: 39%;
}
</style>

</head>

<body>
	
	<div id="page-wrap">

		<!--<div class="logo1"><h2 style="text-align:center;">INVOICE</h2></div>-->
		
		
		<div style="clear:both"></div>
		
		<div id="customer">
		<h2 style="text-align:center; width: 100%;float: right;padding: 2% 0%;">INVOICE</h2><br/>
            <div class="meeta" style="width: 50%;display: inline-block;">
            
                <h2>Carpet of India</h2>
                <p>GST No. 06ALSPM9322Q1Z6</p>
                <p>H NO.-F/1743,</p>
                <p>Sector-49,FARIDABAD-121001</p>
                <p>Phone no. 9876541230</p>
            </div>
           
            <table id="meta">
                <tr style="background: #eee;">
                    <th class="">Invoice </th>
                    <th><p>Date</p></th>
                </tr>
                <tr>

                    <td style="text-align: center;">COI/001</td>
                    <td style="text-align: center;"><p id="date">27-08-2019</p></td>
                </tr>
                <tr style="background: #eee;">
                    <th>Customer Id </th>
                    <td style="text-align: center;">COI010</td>
                </tr>
            </table>
		
		</div>
<div style="clear:both"></div>
<br>
     <div class="bill-to bill-too
     ">
         <h6>Bill To:</h6>
     	<table>
     		 <tr class="">
     			<td class="bill-width name-uper" style="padding:0px;"><p>Mr. Tushar Chawla</p></td>
     		</tr>
     		<tr class="name-uper">
     			<td class="bill-width" style="padding:0px;"><p>Bhagwati interiors and Exteriors (P)Ltd.</p></td>
     		</tr class="name-uper">
     		<tr>
     			<td class="bill-width" style="padding:0px;"><p> 547, block L, Karnal.</p></td>
     		</tr>
     		<tr>
     			<td class="bill-width" style="padding:0px;"><p> Model Town,</p></td>
     		</tr>
     		<tr>
     			<td class="bill-width" style="padding:0px;"><p>Gst no. 06AAHCB4011A1ZJ</p></td>
     		</tr>
     		
     	</table>

     </div>
     <div class="bill-to ship-too">
         <h6>Ship To:</h6>
     	<table>
     		  <tr class="">
     			<td class="bill-width name-uper"><p></p></td>
     		</tr>
     		<tr class="name-uper">
     			<td class="bill-width"><p></p></td>
     		</tr class="name-uper">
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     	</table>

     </div>
     
     
<div style="clear:both"></div>
<br><br>
     <table id="items">
		
		  <tr>
		      <th style="text-align: right;">Sr No.</th>
		      <th style="text-align: right;">Description Of Goods</th>
		      <th style="text-align: right;">HSN/SAC</th>
		      <th style="text-align: right;">Gst <br>Rate</th>
		      <th style="text-align: right;">Qty</th>
		      <th style="text-align: right;">Rate</th>
		      <th style="text-align: right;">per</th>
		      <th style="text-align: right;">Amount</th>
		  </tr>
		  	<tr class="no-boder">
		  	  <td>1</td>
		      <td>Engineered Wood Laminate(EL-135)</td>
		      <td>57021000</td>
		      <td>5%</td>
		      <td>1.00</td>
		      <td>88,000</td>
		      <td>Nos</td>
		      <td>88,000</td>
		  </tr>
		   <tr class="no-boder">
		  	  <td>2</td>
		      <td>
		          Carpetile (BS - 109) Blue with Stripes
		          <ul style="padding-left: 10%;">
		              <li>abc</li>
		              <li>abc</li>
		          </ul>
		          </td>
		      <td>57021000</td>
		      <td>5%</td>
		      <td>1.00</td>
		      <td>1,36,000</td>
		      <td>Nos</td>
		      <td>1,36,000</td>
		  </tr>
		   <tr class="no-boder">
		      <td>3</td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		       <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td><hr style="border: 1px solid #000;">2,24,000.00</td>
		  </tr>
		   <tr class="no-boder">
		      <td></td>
		      <td style="text-align: right;"><span> Freight </span><br></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td>2200.00</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td style="text-align: right;"> <span> Output CGST@2.5% </span><br></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td>2.50</td>
		      <td>%</td>
		      <td>5600.00</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td style="text-align: right;"><span> Output SGST@2.5% </span><br></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td>2.50</td>
		      <td>%</td>
		      <td>5600.00</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
	
		  <tr class="no-boder">
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		     <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		   <tr class="no-boder">
		     <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		   <tr class="no-boder">
		     <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		   <tr class="no-boder">
		     <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		   <tr class="no-boder">
		     <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		   <tr class="no-boder">
		     <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
	
		   <td></td>
		      <td style="font-weight:bold; text-align:right">Total</td>
		      <td></td>
		      <td></td>
		      <td>2.00 Nos</td>
		      <td></td>
		      <td><p  style=""></p></td>
		      <td><p style="">2.35,537.00</p></td>
		      
		  </tr>
		  
		
		<tr>
		    <td colspan="8">
		         <p style="text-align:right"><b>E & O.E</b></p><br>
		        <span>Amount Chargeable (in words) : </span><br>
		        <span style="">Indian Rupees Two Lakh Thirty Seven thousand five hundred Ten Only</span>
		       
		        
		    </td>
		</tr>
			<tr>
		    <td colspan="2" rowspan="2">
		        <p style="text-align: right;    font-size: 13px;"><span >Taxable</span></p><br>
		        <p style="text-align: right;    font-size: 13px;"><span >Taxable</span></p>
		        
		    </td>
		     <td colspan="2" style="text-align: center;font-size: 13px;">Central Tax</td>
		     <td colspan="2" style="text-align: center;font-size: 13px;">State Tax</td>
		      <td colspan="2" rowspan="2" style="text-align: center;font-size: 13px;">Total<br> Tax Amount</td>
		</tr>
		<tr>
		     <td style="text-align: center;font-size: 13px;">Rate </td>
		     <td style="text-align: center;font-size: 13px;">Amount</td>
		      <td style="text-align: center;font-size: 13px;">Rate </td>
		     <td style="text-align: center;font-size: 13px;">Amount</td>
		</tr>
		
			<tr>
		   <td colspan="2" style="text-align:right; font-size:13px;">2,26,200.00</td>
		     <td style="text-align:center; font-size:13px;">2.50% </td>
		     <td style="text-align:center; font-size:13px;">5655.00</td>
		      <td style="text-align:center; font-size:13px;">2.50% </td>
		     <td style="text-align:center; font-size:13px;">5655.00</td>
		     <td colspan="2" style="text-align:center; font-size:13px;">11310.00</td>
		      
		</tr>
		<tr>
		   <td colspan="2" style="text-align:right; font-size:13px;">Total: 2,26,200.00</td>
		     <td style="text-align:center; font-size:13px;"></td>
		     <td style="text-align:center; font-size:13px;">5655.00</td>
		      <td style="text-align:center; font-size:13px;"></td>
		     <td style="text-align:center; font-size:13px;">5655.00</td>
		     <td colspan="2" style="text-align:center; font-size:13px;">11310.00</td>
		      
		</tr>
		
			<tr>
		    <td colspan="8">
		         
		        <span style="">Tax Amount (in words) : Indian Rupees Two Lakh Thirty Seven thousand five hundred Ten Only</span>
		       
		       <br><br><br>
		        
		    </td>
		</tr>
			<tr>
		    <td colspan="4" class="no-boder"><br><br><br><br>
		        <span style="text-decoration: underline;">Decleration</span><br>
		        <span style="font-size:13px;">We Decleare that this invoice shows the actual price of the goods <br>describes and that all particular are true and correct.</span>
		    </td>
		     
		       
		        <td colspan="4" rowspan="2">
		        <p style="text-align: right;font-size: 13px;"><span >For Carpet of India</span></p><br><br><br><br>
		        <p style="text-align: right;font-size: 13px;"><span >Authorised Signature</span></p>
		        
		    </td>
		    </td>
		</tr>
		</table>
	
<p style="text-align: center;padding: 1% 0%;">[ This is a Computer Generated Invoice ]
</p>
</div>
<div style="clear:both"></div>
</div>
</body>
<style>

 @media print{
 .logo::before {
    background: #21211f;
     -webkit-print-color-adjust: exacstyle="background: #eee;"t;
     border-bottom: 6px solid #b27919;
}
td.price2 {
    border-top: 1px solid !important;
     -webkit-print-color-adjust: exact;
    border-bottom: 1px solid !important;
}
.meeta img {
    background:#676363;
    -webkit-print-color-adjust: exact;
}
.logo1 h2 {
    background: #676363;
    -webkit-print-color-adjust: exact;
    color: #fff;
}

.form-below th {
    background: #676363;
     -webkit-print-color-adjust: exact;
 }
#items th {
     -webkit-print-color-adjust: exact;
     background: #676363;
}
.meeta img {
    background:#676363;
    -webkit-print-color-adjust: exact;
}

.logo1 h2 {
    background: #676363;
    -webkit-print-color-adjust: exact;
    color: #fff;
}
.bill-to h6 {
     -webkit-print-color-adjust: exact;
     background: #635b5b;
 }
.logo::after {
    background: #21211f;
     -webkit-print-color-adjust: exact;
    border-bottom: 6px solid #b27919;
 }
 }
.logo {
    height: 118px;
    width: 800px;
    margin: 0 auto;
    max-width: 100%;
}
.logo::before {
    background: #21211f;
    content: "";
    height: 118px;
    width: 31%;
    position: absolute;
    top: 0px;
    left: 19%;
    z-index: -1;
    border-bottom-left-radius: 93%;
    border-bottom: 6px solid #b27919;
}
.logo::after {
    background: #21211f;
    content: "";
    height: 118px;
    width: 31%;
    position: absolute;
    top: 0px;
    left: 50%;
    z-index: -1;
    border-bottom: 6px solid #b27919;
    border-bottom-right-radius: 93%;
}
.logo img {
    width: 14%;
    margin: 0 auto;
    display: block;
    z-index: 999;
}
.print-button button.btn.btn-success {
    font-size: 21px;
    padding: 4px 0;
    width: 16%;
    background: #2e882e;
    color: #fff;
}
.print-button {
    text-align: center;
  
}
@media print {
  #removebutton {
    display: none;
  }
}
</style>
<div class="print-button">
	<button type="button" id="removebutton" onclick="BillPrint()" class="btn btn-success " >Print</button></div>

</body>
<script>
    
    function BillPrint(){
        window.print();
    }
    
</script>
</html>