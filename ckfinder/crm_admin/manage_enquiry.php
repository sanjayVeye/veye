<?php
session_start();
if(isset($_SESSION['id'])){

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Veye Admin Panel</title>

<?php include('head.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- header section starts here -->

<?php include('header.php'); ?>

<!-- header section ends here -->

<!-- sidebar section starts here -->

<?php include('sidebar.php'); ?>

<!-- sidebar section ends here -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content">
        <div class="box box-primary">
			
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-edit"></i> View All Enquiry</h3>
					<div class="box-tools pull-right">
						<a a class="add" href="enquiry_assign.php"><button class="btn btn-primary">Enquiry Assign To Employee</button></a>
					</div>
				</div><!-- /.box-header -->
				<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Post Code</th>
									<th>Message</th>
									
									<th width="12%">Action</th>
								</tr>
							</thead>
							<tbody>
							    
							    <?php
							    include('conn.php');
							    $sql = "SELECT * FROM contact order by name";
							    $query = $conn->query($sql);
							    
							    $i = 1;
							    while($row = $query->fetch_assoc()){
						        ?>
							    
									<tr>
										<td><?php echo $i; ?></td>
										
										<td><?php echo $row['name']; ?></td>
										<td><?php echo $row['email']; ?></td>
										<td><?php echo $row['phone']; ?></td>
										<td><?php echo $row['post_code']; ?></td>
										<td><?php echo $row['message']; ?></td>
										
										<td>
										    <a href="edit_employee_detail.php?id=<?php echo $row['id']; ?>" class="edit_class"><button class="btn btn-info btn-xs"><i class="fa fa-edit"></i></button></a>
										    <button <?php if($row['status']==1){ echo "disabled"; } ?> class="btn btn-danger btn-xs" id="<?php echo $row['id']; ?>" data-placement="top" data-toggle="tooltip" data-original-title="Delete" onclick="delete_dwi(this.id)"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<?php $i++; } ?>
							</tbody>
							<tfoot>
									<th>#</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Post Code</th>
									<th>Message</th>
									
									<th width="12%">Action</th>
								</tr>
							</tfoot>
						</table>
				
				
				
			
			
		</div><!-- /.box -->
    </section><!-- /.content --><!-- /.content -->

    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!--<b>Version</b>-->
    </div>
    <strong>Copyright &copy; <a href="https://www.veye.com.au/">Veye</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script>
    
    // function setslug(){
    //     var country_name = $('[name=name]').val();
    //     $("#slugname").val(country_name);
    // }
    
</script>

</body>
</html>

<?php
}else{
    header("Location: index.php");
}
?>
 <script>
         function delete_dwi(id){
             var category = "employee";
             var cnfrm =  confirm(" Do You Really Want To Delete This Employee");
             var datastring = 'id1='+id+'&categogy1='+category;
            //  alert(datastring)
            
              if(cnfrm){
        
        $.ajax({
             url : "delete_code.php",
            type : "POST",
            data : datastring,
            
            success : function(data){
                // alert(data);
                window.location.href="employee-detail.php";
                 }
              });
          }
         }
</script> 
