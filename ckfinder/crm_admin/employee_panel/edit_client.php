<?php
session_start();
if(isset($_SESSION['id'])){
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | General Form Elements</title>

<?php include('head.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- header section starts here -->

<?php include('header.php'); ?>

<!-- header section ends here -->

<!-- sidebar section starts here -->

<?php include('sidebar.php'); ?>

<!-- sidebar section ends here -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
     <?php
    include('conn.php');
    $id = $_GET['id'];
    $sql_fetch = "SELECT * FROM client WHERE id='".$id."' ";
    $fetch_query = $conn->query($sql_fetch);
    $fetch_row = $fetch_query->fetch_assoc();
    ?>
    
  <section class="content">
        <div class="box box-primary">
			<form action="" id="entryform" method="post" enctype="multipart/form-data">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-edit"></i> Add New Client</h3>
					<div class="box-tools pull-right">
						<a class="view_all_product.php"><button class="btn btn-primary">View All</button></a>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
														<!-- Custom Tabs -->
							<div class="nav-tabs-custom">
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1">
										<div class="row">
											<div class="col-md-8 col-md-offset-2">
												<div class="form-group">
													<label for="name">Client Name:</label>
													<input type="text" autocomplete="off" class="form-control" id="name" name="client_name"  value="<?php echo $fetch_row['client_name']; ?>" placeholder="Enter product name" required>
												</div>
												<div class="form-group">
													<label for="hsn">Phone No.:</label>
													<input type="text" autocomplete="off" class="form-control" id="phone" name="phone" value="<?php echo $fetch_row['phone']; ?>" placeholder="Enter HSN code" >
												</div>
												<div class="form-group">
												<label for="hsn">Address:</label>
												<textarea rows="4" cols="50" autocomplete="off" class="form-control" name="address" placeholder="Enter Address"><?php echo $fetch_row['address']; ?></textarea>
												</div>
												<div class="form-group">
													<label for="gst">GST:</label>
												<input type="text" autocomplete="off" class="form-control" id="slugname" name="gst" value="<?php echo $fetch_row['gst']; ?>" placeholder="Enter GST" >
												</div>
												<div class="form-group">
													<label for="gst">TIN No.:</label>
												<input type="text" autocomplete="off" class="form-control" id="slugname" name="tin_no" value="<?php echo $fetch_row['tin_number']; ?>" placeholder="Enter GST" >
												</div>
											</div>
										</div><!-- /.row -->
									</div><!-- /.tab-pane -->
								</div><!-- /.tab-content -->
							</div><!-- nav-tabs-custom -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer">
					<div class="box-tools pull-right">
													<button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button>
												<a href="add_product.php" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</form>
		</div><!-- /.box -->
    </section><!-- /.content --><!-- /.content -->

    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->


<script src="dist/js/demo.js"></script>
</body>
</html>

<?php
if(isset($_POST['submit'])){
    include('conn.php');
     $client_name = $_POST['client_name'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $gst = $_POST['gst'];
    $tin_number = $_POST['tin_no'];
 $sql="update client set client_name='".$client_name."', phone='".$phone."', address='".$address."', gst='".$gst."', tin_number='".$tin_number."' where id='".$id."'";
 $query = $conn->query($sql);
 if($query==TRUE){
     echo '<script>alert("client Update successfully!"); window.location.href="view_all_client.php";</script>';
     
 }else{
     echo '<script>alert("Something Went Wrong!");window.location.href="view_all_client.php";</script>';
 }
    
}

?>
<?php
}else{
    header("Location: index.php");
}
?>
