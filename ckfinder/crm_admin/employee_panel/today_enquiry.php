<?php
session_start();
if(isset($_SESSION['id'])){
 date_default_timezone_set("Australia/Sydney")
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vaye Employee</title>

<?php include('head.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- header section starts here -->

<?php include('header.php'); ?>

<!-- header section ends here -->

<!-- sidebar section starts here -->

<?php include('sidebar.php'); ?>

<!-- sidebar section ends here -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content">
        <div class="box box-primary">
			
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-edit"></i> View Today Enquiry</h3>
					<!--<div class="box-tools pull-right">-->
					<!--	<a a class="add" href="enquiry_assign.php"><button class="btn btn-primary">Enquiry Assign To Employee</button></a>-->
					<!--</div>-->
				</div><!-- /.box-header -->
				<table id="example1" class="table table-bordered table-striped" style="overflow-x:auto !important;">
							<thead>
								<tr>
									<th>#</th>
									<th>Date and Time</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Post Code</th>
									<th>Source of leads</th>
									<th>Ip Address</th>
									<th>Message</th>
									<!--<th>Status</th>-->
									<!--<th width="12%">Action</th>-->
								</tr>
							</thead>
							<tbody>
							    
							    <?php
							    
							    include('conn.php');
							  
                                 $date= date('d-m-y');
							    
							    $sql = "SELECT * FROM contact where assigned_emp ='".$_SESSION['id']."' AND enquiry_date='".$date."'";
							    $query = $conn->query($sql);
							 //   echo $sql;
							 //   die();
							    $i = 1;
							    while($row = $query->fetch_assoc()){
						        ?>
							    
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $row['enquiry_date']; ?></td>
										<td><?php echo $row['name']; ?></td>
										<td><?php echo $row['email']; ?></td>
										<td><?php echo $row['phone']; ?></td>
										<td><?php echo $row['post_code']; ?></td>
										<td></td>
										<td></td>
										<td><?php echo $row['message']; ?></td>
										
										<!--<td><select id="<?php echo $row['ID']; ?>"  onchange="enquiry_status(this.id,this.value)" <?php if($row['status']=="Finalise"){echo "disabled";} ?>>-->
										<!--    <option value="">select</option>-->
										<!--    <option value="Not Interested" <?php if($row['status']=="Not Interested"){ echo "selected";} else{ echo "Not Interested";}  ?>>Not Interested</option>-->
										<!--    <option value="Meeting"<?php if($row['status']=="Meeting"){echo "selected";} else{echo "Meeting";}  ?>>Meeting</option>-->
										<!--    <option value="Finalise" <?php if($row['status']=="Finalise"){echo "selected";}else{echo "Finalise";}  ?>>Finalise</option>-->
										<!--    <option value="Remark"<?php if($row['status']=="Remark"){echo "selected";}else{echo "Remark";} ?>>Remark</option>-->
										   
										    
										    
										<!--</select>-->
										<!--</td>-->
									</tr>
									<?php $i++; } ?>
							</tbody>
						<tfoot>
								<tr>
									<th>#</th>
									<th>Date and Time</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Post Code</th>
										<th>Source of leads</th>
									<th>Ip Address</th>
									<th>Message</th>
									<!--<th>Status</th>-->
									<!--<th width="12%">Action</th>-->
								</tr>
							</tfoot>
						</table>
				
				
				
			
			
		</div><!-- /.box -->
    </section><!-- /.content --><!-- /.content -->

    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!--<b>Version</b>-->
    </div>
    <strong>Copyright &copy; <a href="https://www.veye.com.au/">Veye</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script>
    
    // function setslug(){
    //     var country_name = $('[name=name]').val();
    //     $("#slugname").val(country_name);
    // }
    
</script>

</body>
</html>

<?php
}else{
    header("Location: index.php");
}
?>
 <script>
         function enquiry_status(id,value)
         {
             var datastring='id1='+id+'&value1='+value;
             
            var popup=confirm("Do You want to change the status of enquiry.");
           
            if(popup){
            
            $.ajax({
                
              url: "change_enquiry_status.php",
              type:  "POST",
              data: datastring,
              success: function(data){
                  alert(data);
                  window.location.href="today_enquiry_status.php";
              }
              
            });
            
        } 
         }
</script> 
