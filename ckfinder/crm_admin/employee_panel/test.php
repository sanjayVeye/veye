<?php
session_start();
if(isset($_SESSION['id'])){

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vaye Employee</title>

<?php include('head.php'); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper">
<style>
    textarea {
    width: 100%;
    border-radius: 5px;
    border: 1px solid #ccc;
}
select {
    width: 100%;
    height: 20px;
    margin-bottom: 10px;
    border: 1px solid #ccc;
    background: #fff;
    color: #666;
    padding-left: 8px;
    line-height: 30px !important;
}
input.form-control {
    margin-bottom: 10px;
}
.modal-body {
    /* position: relative; */
    padding: 0;
}
.modal-content {
    padding: 10px;
}
.modal-dialog {
    width: 320px;
}
</style>
<?php include('header.php'); ?>
<?php include('sidebar.php'); ?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content">
        <div class="box box-primary">
			
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-edit"></i> View All Enquiry</h3>
					<!--<div class="box-tools pull-right">-->
					<!--	<a a class="add" href="enquiry_assign.php"><button class="btn btn-primary">Enquiry Assign To Employee</button></a>-->
					<!--</div>-->
				</div><!-- /.box-header -->
				<div style="overflow-x:auto;">
				 	<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr><th>#</th>
									<th>Date and Time</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Post Code</th>
									
									<th>Message</th>
									<th>Status</th>
									<!--<th width="12%">Action</th>-->
								</tr>
							</thead>
							<tbody>
							    
							    <div class="input1_box"></div>
							    <?php
							    include('conn.php');
							    $sql = "SELECT * FROM contact where assigned_emp ='".$_SESSION['id']."' order by name";
							    $query = $conn->query($sql);
							    
							    $i = 1;
							    while($row = $query->fetch_assoc()){
						        ?>
							    
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $row['enquiry_date']; ?></td>
										<td><?php echo $row['name']; ?></td>
										<td><?php echo $row['email']; ?></td>
										<td><?php echo $row['phone']; ?></td>
										<td><?php echo $row['post_code']; ?></td>
									
										<td><?php echo $row['message']; ?></td>
									<td><input type="button" name="view" value="Add Remark" id="<?php echo $row["ID"]; ?>" class="btn btn-info btn-xs view_data" /></td>  
									</tr>
									<?php $i++; } ?>
							</tbody>
							<tfoot>
									<tr>
									    <th>#</th>
									<th>Date and Time</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Post Code</th>
									
									<th>Message</th>
									<th>Status</th>
									<!--<th width="12%">Action</th>-->
								</tr>
							</tfoot>
						</table>   
				</div>
				
			
				
				
				
			
			
		</div><!-- /.box -->
    </section><!-- /.content -->

   
  </div>
  <!-- /.content-wrapper -->
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!--<b>Version</b>-->
    </div>
    <strong>Copyright &copy; <a href="https://www.veye.com.au/">Veye</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- The Modal -->
  <div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Add Remarks</h4>  
                </div>  
                <div class="modal-body" id="employee_detail">
                  
                </div>
                  <form>
                        <select>
                            <option>Select Enquiry Status</option>
                            <option>Not Interested</option>
                            <option>Meeting</option>
                            <option>Finalize</option>
                            
                        </select>
                        <input type="date" class="form-control" name="date" placeholder="Enter Meeting Date">
                        <input type="time" class="form-control" name="time" placeholder="Enter Meeting Time">
                        <textarea placeholder="Add Remark"></textarea>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button> 
                    </form>
                    
                <div class="modal-footer">  
                      
                </div>  
           </div>  
      </div>  
 </div> 
  


<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>


<script>  
 $(document).ready(function(){  
      $('.view_data').click(function(){ 
         
           var employee_id = $(this).attr("id");  
           $.ajax({  
                url:"test1.php",  
                method:"post",  
                data:{employee_id:employee_id},  
                success:function(data){  
                     $('#employee_detail').html(data);  
                     $('#dataModal').modal("show");  
                }  
           });  
      });  
 });  
 </script>
</body>
</html>

<?php
}else{
    header("Location: index.php");
}
?>
 