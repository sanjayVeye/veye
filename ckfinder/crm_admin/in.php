<?php

$inc='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>Invoice</title>
		<link rel="stylesheet" type="text/css" href="css/print_style.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

	<style>
.meeta img {
    max-width: 13%;
    float: left;
    background:#676363;
}
.logo1 h2 {
    background: #676363;
    color: #fff;
    text-align: center;
    width: 100%;
    padding: 10px 0;
    margin: 10px 0;
}
p.in-word {
    margin: 0;
}
.name-uper {
    text-transform: uppercase;
}
.bill-to {

    display: inline-block;
}
.bill-too {
    width: 60%;
}
.ship-too {
    width: 39%;
}
</style>

</head>

<body>
	
	<div id="page-wrap">

		<!--<div class="logo1"><h2 style="text-align:center;">INVOICE</h2></div>-->
		
		
		<div style="clear:both"></div>
		
		<div id="customer">
		<h2 style="text-align:center; width: 100%;float: right;padding: 4% 0%;">INVOICE</h2><br/>
            <div class="meeta" style="width: 50%;display: inline-block;">
            
                <h2>Carpet of India</h2>
                <p>GST No. 06ALSPM9322Q1Z6</p>
                <p>H NO.-F/1743,</p>
                <p>Sector-49,FARIDABAD-121001</p>
                <p>Phone no. 9876541230</p>
            </div>
           
            <table id="meta">
                <tr style="background: #eee;">
                    <th class="">Invoice </th>
                    <th><p>Date</p></th>
                </tr>
                <tr>

                    <td style="text-align: center;">COI/001</td>
                    <td style="text-align: center;"><p id="date">27-08-2019</p></td>
                </tr>
                <tr style="background: #eee;">
                    <th>Customer Id </th>
                    <td style="text-align: center;">COI010</td>
                </tr>
            </table>
		
		</div>
<div style="clear:both"></div>
<br><br><br>
     <div class="bill-to bill-too
     ">
         <h6>Bill To:</h6>
     	<table>
     		 <tr class="">
     			<td class="bill-width name-uper" style="padding:0px;"><p>Mr. Tushar Chawla</p></td>
     		</tr>
     		<tr class="name-uper">
     			<td class="bill-width" style="padding:0px;"><p>Bhagwati interiors and Exteriors (P)Ltd.</p></td>
     		</tr class="name-uper">
     		<tr>
     			<td class="bill-width" style="padding:0px;"><p> 
547, block L, Karnal.</p></td>
     		</tr>
     		<tr>
     			<td class="bill-width" style="padding:0px;"><p> Model Town,</p></td>
     		</tr>
     		<tr>
     			<td class="bill-width" style="padding:0px;"><p>Gst no. 06AAHCB4011A1ZJ</p></td>
     		</tr>
     		
     	</table>

     </div>
     <div class="bill-to ship-too">
         <h6>Ship To:</h6>
     	<table>
     		  <tr class="">
     			<td class="bill-width name-uper"><p></p></td>
     		</tr>
     		<tr class="name-uper">
     			<td class="bill-width"><p></p></td>
     		</tr class="name-uper">
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     		<tr>
     			<td class="bill-width"><p></p></td>
     		</tr>
     	</table>

     </div>
     
     
<div style="clear:both"></div>
<br><br><br>
     <table id="items">
		
		  <tr>
		      <th>Sr No.</th>
		      <th>Description</th>
		      <th>Qty</th>
		      <th>Rate</th>
		      <th>Amount</th>
		  </tr>
		  	<tr class="no-boder">
		  	  <td>1</td>
		      <td>Engineered Wood Laminate(EL-135)</td>
		      <td>5 box</td>
		      <td></td>
		      <td>8700</td>
		  </tr>
		   <tr class="no-boder">
		      <td>2</td>
		      <td>Carpetile (BS - 109) Blue with Stripes</td>
		      <td>1 box</td>
		      <td></td>
		      <td>4505</td>
		  </tr>
		   <tr class="no-boder">
		      <td>3</td>
		      <td>Installation Charges</td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;Engineered Wood Laminate(EL-135)</td>
		      <td></td>
		      <td></td>
		      <td>2500</td>
		  </tr>
		   <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;Carpetile (BS - 109) Blue with Stripes</td>
		      <td></td>
		      <td></td>
		      <td>2000</td>
		  </tr>
		  <tr class="no-boder">
		      <td>4</td>
		      <td>Cartage</td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;Engineered Wood Laminate(EL-135)</td>
		      <td></td>
		      <td></td>
		      <td>500</td>
		  </tr>
		   <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;Carpetile (BS - 109) Blue with Stripes</td>
		      <td></td>
		      <td></td>
		      <td>500</td>
		  </tr>
		  <tr class="no-boder">
		      <td>5</td>
		      <td>GST On Engineered Wood Laminate(EL-135)(18% GST rate applicable.)</td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;SGST:</td>
		      <td></td> 
		      <td>9%</td>
		      <td>783</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;CGST:</td>
		      <td></td> 
		      <td>9%</td>
		      <td>783</td>
		  </tr>
		  <tr class="no-boder">
		      <td>6</td>
		      <td>GST On Installation Charges(18% GST rate applicable.)</td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		   <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;SGST:</td>
		      <td></td>
		      <td>9%</td>
		      <td>225</td>
		  </tr>
		   <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;CGST:</td>
		      <td></td> 
		      <td>9%</td>
		      <td>225</td>
		  </tr>
		  <tr class="no-boder">
		      <td>7</td>
		      <td>GST On Cartage(18% GST rate applicable.)</td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;SGST:</td>
		      <td></td> 
		      <td>9%</td>
		      <td>45</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;CGST:</td>
		      <td></td> 
		      <td>9%</td>
		      <td>45</td>
		  </tr>
		  <tr class="no-boder">
		      <td>8</td>
		      <td>GST On Carpetile (BS - 109) Blue with Stripes(12% GST rate applicable.)</td>
		      <td></td>
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;SGST:</td>
		      <td></td>
		      <td>6%</td>
		      <td>270.3</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;CGST:</td>
		      <td></td>
		      <td>6%</td>
		      <td>270.3</td>
		  </tr>
		  <tr class="no-boder">
		      <td>9</td>
		      <td>GST On Installation Charges(18% GST rate applicable.)</td>
		      <td></td>
		     <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;SGST:</td>
		      <td></td> 
		      <td>9%</td>
		      <td>180</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;CGST:</td>
		      <td></td>
		      <td>9%</td>
		      <td>180</td>
		  </tr>
		  <tr class="no-boder">
		      <td>10</td>
		      <td>GST On Cartage(12% GST rate applicable.)</td>
		      <td></td> 
		      <td></td>
		      <td></td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;SGST:</td>
		      <td></td> 
		      <td>6%</td>
		      <td>30</td>
		  </tr>
		  <tr class="no-boder">
		      <td></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;CGST:</td>
		      <td></td> 
		      <td>6%</td>
		      <td>30</td>
		  </tr>
		   <tr class="no-boder">
		      <td></td>
		      <td></td>
		      <td></td> 
		      <td></td>
		      <td></td>
		  </tr>
<tr class="no-boder">
		      <td></td>
		      <td></td>
		      <td></td> 
		      <td></td>
		      <td></td>
		  </tr>

		  
		   <tr>
		      <td colspan="2">Thank you for your business</td>
		      
		      <td colspan="2" ><p  style="    width: 48%;display: inline-block;">Total</p></td>
		      <td><p style="width: 100%;float: right;">21771.6</p></td>
		      
		  </tr>
		  
		</table>
	<br>
<p style="text-align: center;padding: 7%;">If you have any questions about this invoice, please contact
<br>
[Carpet of India, 9876541230, navdeep@carpetofindia.com]
</p>




</div>
<div style="clear:both"></div>
</div>
</body>
<style>

 @media print{
 .logo::before {
    background: #21211f;
     -webkit-print-color-adjust: exacstyle="background: #eee;"t;
     border-bottom: 6px solid #b27919;
}
td.price2 {
    border-top: 1px solid !important;
     -webkit-print-color-adjust: exact;
    border-bottom: 1px solid !important;
}
.meeta img {
    background:#676363;
    -webkit-print-color-adjust: exact;
}
.logo1 h2 {
    background: #676363;
    -webkit-print-color-adjust: exact;
    color: #fff;
}

.form-below th {
    background: #676363;
     -webkit-print-color-adjust: exact;
 }
#items th {
     -webkit-print-color-adjust: exact;
     background: #676363;
}
.meeta img {
    background:#676363;
    -webkit-print-color-adjust: exact;
}

.logo1 h2 {
    background: #676363;
    -webkit-print-color-adjust: exact;
    color: #fff;
}
.bill-to h6 {
     -webkit-print-color-adjust: exact;
     background: #635b5b;
 }
.logo::after {
    background: #21211f;
     -webkit-print-color-adjust: exact;
    border-bottom: 6px solid #b27919;
 }
 }
.logo {
    height: 118px;
    width: 800px;
    margin: 0 auto;
    max-width: 100%;
}
.logo::before {
    background: #21211f;
    content: "";
    height: 118px;
    width: 31%;
    position: absolute;
    top: 0px;
    left: 19%;
    z-index: -1;
    border-bottom-left-radius: 93%;
    border-bottom: 6px solid #b27919;
}
.logo::after {
    background: #21211f;
    content: "";
    height: 118px;
    width: 31%;
    position: absolute;
    top: 0px;
    left: 50%;
    z-index: -1;
    border-bottom: 6px solid #b27919;
    border-bottom-right-radius: 93%;
}
.logo img {
    width: 14%;
    margin: 0 auto;
    display: block;
    z-index: 999;
}
.print-button button.btn.btn-success {
    font-size: 21px;
    padding: 4px 0;
    width: 16%;
    background: #2e882e;
    color: #fff;
}
.print-button {
    text-align: center;
    margin: 2% 0;
}
@media print {
  #removebutton {
    display: none;
  }
}
</style>
<div class="print-button">
	<button type="button" id="removebutton" onclick="BillPrint()" class="btn btn-success " >Print</button></div>

</body>
<script>
    
    function BillPrint(){
        window.print();
    }
    
</script>
</html>';
echo $inc;
die();
