<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="dashboard.php"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li><a href="dashboard.php"><i class="fa fa-dashboard active text-red"></i> <span>Dashboard</span></a></li>
     
        <li class=" treeview">
          <a href="#">
            <i class="fa fa-university text-red"></i> <span>Employee Manegement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li ><a href="add_employee.php"><i class="fa fa-circle-o"></i>Add Employee</a></li>
            <li><a href="employee-detail.php"><i class="fa fa-circle-o"></i>View All Employee</a></li>
          </ul>
        </li>
        <li class=" treeview">
          <a href="#">
            <i class="fa fa-university text-red"></i> <span>Enquiry Manegement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li ><a href="manage_enquiry.php"><i class="fa fa-circle-o"></i>View All Enquiry</a></li>
            <li><a href="enquiry_assign.php"><i class="fa fa-circle-o"></i>Enquiry Assign To Employee</a></li>
            <li><a href="today_enquiry.php"><i class="fa fa-circle-o"></i>Today Enquiry Status</a></li>
            <li><a href="finalize_qurery.php"><i class="fa fa-circle-o"></i>Finalize Query</a></li>
             <li><a href="today_enquiry_status.php"><i class="fa fa-circle-o"></i>Today Enquiry Status</a></li>
          </ul>
        </li>
         
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>