 $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    autoplay:false,
    focusOnSelect: true,
    responsive: [
        {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

   $('.slider-navdailymain').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    autoplay:false,
    focusOnSelect: true,
    responsive: [
        {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


  $('.slider-navdaily').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    autoplay:false,
    focusOnSelect: true,
    responsive: [
        {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


 $('.slider-nav-recomm').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    autoplay:false,
    focusOnSelect: true,
    responsive: [
        {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.slider-nav3').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true,
    autoplay:false,
    focusOnSelect: true,
    responsive: [
        {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
    ]
  });
  $('.slider-nav4').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    autoplay:false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 380,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

 
  $('a[data-slide]').click(function(e) {
    e.preventDefault();
    var slideno = $(this).data('slide');
    $('.slider-nav, .slider-nav3').slick('slickGoTo', slideno - 1);
  });

  $("#SearchByM").click(function(){
    $('.SearchGrourp').css("display", "flex");
    $('[aria-label="Close"].btn-close').show();
  });
  
  $('[aria-label="Close"].btn-close').click(function(){
    $('.SearchGrourp').css("display", "none");
    $('[aria-label="Close"].btn-close').hide();
  });
 