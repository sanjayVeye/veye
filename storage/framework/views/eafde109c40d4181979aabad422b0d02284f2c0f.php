<?php $__env->startSection('title'); ?> Reset Password | Veye Pty Ltd <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Forgot password? Easily reset your password for Veye Clint login. Regain access to your account with just a few simple steps. Retrieve your login credentials now! <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au/forgot-password <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <META NAME="robots" CONTENT="noindex,nofollow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php
use App\Models\LogoEdit;
?>
<?php $logodata = LogoEdit::first(); ?>
      <section class="gradient-form">
         <div class="gradient-formBGCLR">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4 offset-lg-4">
                     <div class="formBox">
                        <div class="formBoxHead">
                        <img src="<?php echo e(asset('frontend/assets/images')); ?>/<?php echo e($logodata->header_logo); ?>"/> 
                           <h4>Reset Password</h4>
                           <!-- <p>Use your credentials to access your account</p> -->
                      
                        <?php if(\Session::has('success')): ?>
                        <div class="alert alert-success">
                            <ul>
                                <li><?php echo \Session::get('success'); ?></li>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <?php if(\Session::has('error')): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <li><?php echo \Session::get('error'); ?></li>
                            </ul>
                        </div>
                        <?php endif; ?>
                        </div>
                        <?php if($keyId): ?>
                        <form method="post" enctype="multipart/form-data" action="<?php echo e(route('reset.password')); ?>">
                           <?php echo csrf_field(); ?>
                           <input type="hidden" name="email" value="<?php echo e($email); ?>">
                            <input type="hidden" name="token" value="<?php echo e($keyId); ?>">
                          
                           <div class="form-floating">
                              <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password" value="<?php echo e(old('password')); ?>">
                              <label for="floatingPassword">New Password</label>
                              <?php if($errors->has('password')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('password')); ?></strong>
                            </span>
                            <?php endif; ?>
                           </div>
                           <div class="form-floating">
                              <input type="password" name="confirm" class="form-control" id="floatingPassword" placeholder="Password" value="<?php echo e(old('password')); ?>">
                              <label for="floatingPassword">Confirm Password</label>
                              <?php if($errors->has('password')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('password')); ?></strong>
                            </span>
                            <?php endif; ?>
                           </div>
                           <div class="acceptVeye">
                              <label>
                                 <input type="checkbox" name="accept" value="1"/> <span>Remember</span>
                              </label>
                           </div>

                           <div class="text-center">
                              <button class="btn VeyeLogin" type="submit">Reset Password</button>
                           </div>

                           <div class="ForgotPassBox d-flex align-items-start justify-content-center pb-4">
                              
                                 <button class="btn VeyeRegister">Login</button>
                           </div>
                        </form>
                        <?php else: ?>
                        <form method="post" enctype="multipart/form-data" action="<?php echo e(route('reset.send_pass_reset_link')); ?>">
                           <?php echo csrf_field(); ?>
                          
                           <div class="form-floating">
                              <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email ID" value="<?php echo e(old('email')); ?>">
                              <label for="floatingPassword">E-Mail Address</label>
                              <?php if($errors->has('email')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('email')); ?></strong>
                            </span>
                            <?php endif; ?>
                           </div><br><br>
                           
                           <div class="text-center">
                              <button class="btn VeyeLogin" type="submit">Send Password Reset Link</button>
                           </div>

                        </form>
                        <?php endif; ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/registration/forgot_password.blade.php ENDPATH**/ ?>