<?php $__env->startSection('title'); ?> Best ASX Stock Market Recommendations - Stocks to Buy <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Veye Pty Ltd- Stock Market Research Firm - Unlock the best ASX stock market recommendations. Expertly chosen stocks to buy <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?>  <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au/ <?php $__env->stopSection(); ?>
<?php $__env->startSection('facebook'); ?> 
 

<!-- Facebook Meta Tags -->
  <meta property="og:url" content="https://veye.com.au/">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Best ASX Stock Market Recommendations - Veye">
  <meta property="og:description" content="Veye Pty Ltd- Stock Market Research Firm - Unlock the best ASX stock market recommendations. Expertly chosen stocks to buy, ensuring optimal returns.">
  <meta property="og:image" content="https://veye.com.au/public/uploads/reports/16859600771681847180.png">

  <!-- Twitter Meta Tags -->
  <meta name="twitter:card" content="summary_large_image">
  <meta property="twitter:domain" content="veye.com.au">
  <meta property="twitter:url" content="https://veye.com.au/">
  <meta name="twitter:title" content="Best ASX Stock Market Recommendations - Veye">
  <meta name="twitter:description" content="Veye Pty Ltd- Stock Market Research Firm - Unlock the best ASX stock market recommendations. Expertly chosen stocks to buy, ensuring optimal returns.">
  <meta name="twitter:image" content="https://veye.com.au/public/uploads/reports/16859600771681847180.png">

 <!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-M895KWH');</script>

<!-- End Google Tag Manager -->


<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php

use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
use App\Models\Menues;
?>


<section class="FirstFace_Sec">
   <div class="FirstFace_Sec02">
      <h1>ASX Stocks Research & Recommendations - <strong>Buy, Sell and Hold</strong></h1>
      <a href="<?php echo e(route('subscriptionMenu')); ?>" class="GetStarted">Get Started</a>
      <a href="pages/about-us" class="ReadMore text-dark knw__gs">Know More </a>
   </div>
</section>
<div class="container  ">
   <section class="FirstFace_SecSub SlideDotsCustome">

      <div class="row align-items-center">
         <div class="col-md-12">
            <div class="Daily_A Dais__tops">
               <h2 class="dailyanofnts">Daily Analysis</h2>
               <!-- <button class="rdmore__css">Read More</button> -->
            </div>
         </div>
         <!-- <div class="col-md-3">
                     <div class="Daily_A Dais__tops text-center">
                        <h2>Daily Analysis</h2>
                        <button class="rdmore__css">Read More</button>
                     </div>
                     </div> -->
         <div class="col-md-12">
            <div class="slider slider-navdailymain">
               <?php $__currentLoopData = $latest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datadaily): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <div>
                  <div class="SlideBox rris">
                     <img src="<?php echo e(asset('/'.$datadaily->thumbnail)); ?>" alt="<?php echo e($datadaily->title); ?>"  />
                     <div class="SlideBoxCnt">
                        <small class="indxTexm">TEAM VEYE | <i class="fa fa-calendar indtseVs"></i> <?php echo e(date('d-M-Y',strtotime($datadaily->schedule))); ?></small>
                        <?php if(strlen($datadaily->title) > '39'): ?>
                        <p class="carsoTxt"><a href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>"><?php echo e($datadaily->asx_code); ?>-<?php echo e(strlen($datadaily->title) > 70 ? substr($datadaily->title,0,70).'..' : $datadaily->title); ?>...</a></p>
                        <?php else: ?>
                        <p class="carsoTxt"><a href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>"><?php echo e($datadaily->asx_code); ?>-<?php echo e($datadaily->title); ?>...</a></p>
                        <br />
                        <?php endif; ?>

                        <a class="indResd" href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>">Read More..</a>
                     </div>
                  </div>
               </div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-md-12">
            <a class="ExploreBlue exdCXcss" href="<?php echo e(url('report/stock-advisory/latest-reports')); ?>" style="margin: 43px -22px;">Explore More
               <i class="fa fa-arrow-circle-right arre_ost" aria-hidden="true"></i></a>
         </div>
      </div>


   </section>
</div>
<style>
   .bolsSbfr {
      font-weight: 700 !important;
   }
</style>
<section class="rowcfLes topeQls">
   <div class="container  ">
      <div class="row ">
         <div class="col-md-6 remove__columns">
            <div class="VeyeOffers">
               <h3 class="vase__offers saaAllRepla"><strong class="bolsSbfr"><?php echo e($homesdata->title); ?></strong></h3>
               <p class="applYPTgs"><?php echo e($homesdata->details); ?></p>
               <a href="<?php echo e(route('subscriptionMenu')); ?>" class="veyOffer__subscriber aSubsd">Subscribe Now</a>
            </div>
         </div>
         <div class="col-md-6 remove__columns">
            <div class="recomdedTable ">
               <h3 class="fontPastRec saaAllRepla">Past Recommendations</h3>
               <div class="radiusTbaleBox table-responsive">
                  <table class="table table-striped errShFromInd shadow past__border">
                     <thead>
                        <tr>
                           <th scope="col">Code</th>
                           <th scope="col">Avg. Buy Price</th>
                           <th scope="col">Sell Price</th>
                           <th scope="col">Gain/Loss</th>
                        </tr>
                     </thead>
                     <tbody class="table-group-divider">
                        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datadaily): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                           <th scope="row"> <span class="bold__tablefonts"><?php echo e($datadaily->company); ?></span> </th>
                           <td>$<?php echo e($datadaily->buy_price); ?></td>
                           <td>$<?php echo e($datadaily->sell_price); ?></td>
                           <?php if(0 < $datadaily->gains_losses): ?>
                              <td class="NumGreen"><?php echo e($datadaily->gains_losses); ?>% <i class="fas fa-caret-up"></i></td>
                              <?php else: ?>
                              <td class="NumRed"><?php echo e($datadaily->gains_losses); ?>% <i class="fas fa-caret-down"></i></td>
                              <?php endif; ?>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
               </div>
               <div class="main__Topso"></div>
               <a class="ExploreBlue" style="margin: 1px 30px 0px 0px;" href="pages/past-recommendations">Explore More <i class="fa fa-arrow-circle-right arre_ost" aria-hidden="true"></i></a>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="StockAdvi ReportTypeCss SlideDotsCustome">
   <div class="container position-relative">
      <h4> <span class="headeDlight saaAllRepla">Special Reports</span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <div class="slider slider-navdaily">

         <?php $__currentLoopData = $divReports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <?php

         $companies= db::table('companies')->where('asx_code',$reportdata->asx_code)->first();
         $desc=@$companies->description;


         ?>
         <div>
            <div class="SlideBox sliderPasd cardstcokAvs">
               <img class="stockAdvImg" src="<?php echo e(asset('/')); ?>/<?php echo e($reportdata->thumbnail); ?>" alt="<?php echo e($reportdata->title); ?>"/>
               <div class="stockadvers padssT__css">
                  <small class="small__titla">TEAM VEYE | <i class="fa fa-calendar indtseVs"></i> <?php echo e(date('d-M-Y',strtotime($reportdata->created_at))); ?></small>
                  <a class="specialRedmo" href="<?php echo e(route('report.read.more',$reportdata->slug)); ?>">
                     <h5 class="restyefonts"><?php echo e($reportdata->title); ?></h5>
                  </a>
                  <!-- <p class="spcifotrepo6t"><?php echo e(strlen($desc) > 130 ? substr($desc,0,130).'..' : $desc); ?></p> -->

                  <!-- Remove all time -->
                  <div class="readMoreBoxHeighEqual"></div>
                  <!--  -->

                  <a class="specialRedmo" href="<?php echo e(route('report.read.more',$reportdata->slug)); ?>">Read More...</a>
               </div>
            </div>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      </div>
      <!-- <a class="ExploreBlue respon" style="margin: 17px 30px 0px 0px;" href="<?php echo e(url('report/stock-advisory/latest-reports')); ?>">Explore More <i class="fa fa-arrow-circle-right arre_ost" aria-hidden="true"></i></a> -->
   </div>
</section>
<section class="StockAdvi  resportTyTop">
   <div class="container  ">
      <h4 class="d-none d-sm-block"> <span class="headeDlight  saaAllRepla">Report Type</span> <span class="head__tsxt"></span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <div class="ReportType SlideDotsCustome ">
         <div class="row myRow lessMyCss">
            <div class="col-md-9 d-none d-sm-block myRowPosi">
               <div class="slider slider-nav slider-nav2">
                  <?php $__currentLoopData = $reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($reportdata->title == 'Free Reports'): ?>
                  <?php else: ?>
                  <div>
                     <div class="ReportTypeSlide topbrder Shds__gss">
                        <img src="<?php echo e(asset('frontend/assets/images/dividend investor report.png')); ?>" alt="<?php echo e($reportdata->title); ?>"/>
                        <h5 class="text-center"><?php echo e($reportdata->title); ?></h5>
                        <p class="applYPTgs"><?php echo e(strlen($reportdata->description) > 130 ? substr($reportdata->description,0,130).'..' : $reportdata->description); ?>

                        </p>
                        <a href="<?php echo e(route('manage.menues',['stock-advisory',$reportdata->slug])); ?>"><button class="respoRead">Know More <i class="fas fa-arrow-right"></i></button></a>
                     </div>
                  </div>
                  <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               </div>
               <a class="ExploreBlue respon" style="margin: 15px 36px 0px 0px;" href="stock-advisory">Explore All Reports <i class="fa fa-arrow-circle-right arre_ost" aria-hidden="true"></i></a>
            </div>
            <div class="col-md-3 iPadCss">
               <div class=TopASX>
                  <img src="https://veye.com.au/assets/img/veye-logo.svg" />
                  <p>Top 5 ASX Dividend Stocks To Look At</p>
                  <button class=" get__reports" data-toggle="modal" data-target="#myHomeModal">Get Your Free Report</button>
               </div>
            </div>
         </div>

      </div>
   </div>
</section>




<!-- Modal body -->
<div class="modal" id="myHomeModal">
   <div class="modal-dialog">
      <div class="modal-content mdConst">

         <!-- Modal Header -->
         <div class="modal-header">
            <h4 class="modal-title text-dark"> <strong>Register below to get your report- It's for FREE!</strong> </h4>
            <button type="button" class="close cuClose" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('contactus.store')); ?>">
               <?php echo csrf_field(); ?>
               <input type="hidden" name="form_type" value="free_report">
               <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3">

                  <div class="col-md-12">
                     <div class="form-floating ">
                        <input type="text" name="name" class="form-control modalInsu" id="floatingInput" placeholder="Enter Name">
                        <label for="floatingInput">Full Name</label>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-floating ">
                        <input type="email" name="email" class="form-control modalInsu" id="floatingInput" placeholder="Enter Email">
                        <label for="floatingInput">Email address</label>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-floating ">
                        <input type="text" name="phone" minlength="8" maxlength="12" class="form-control modalInsu" id="floatingInput" placeholder="Enter Phone">
                        <label for="floatingInput">Phone Number</label>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-floating">
                        <input type="text" name="post_code" class="form-control modalInsu" id="floatingInput" placeholder="Enter Post Code">
                        <label for="floatingInput">Post Code</label>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-floating">
                        <textarea class="form-control" name="message" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
                        <label for="floatingTextarea2">Additional Details (Optional)</label>
                     </div>
                     <div class="form-group" style="font-size: 12px;padding: 12px;">
                        <img draggable="false" class="emoji" alt=":lock:" src="/assets/img/lock.svg" style="width: 13px; margin-top: -5px;"> By providing your details, you agree to Veye’s
                        <a style="color: #54af8a;" href="/pages/terms-conditions"> Terms &amp; Conditions</a> and <a style="color: #54af8a;" href="/pages/privacy-policy"> Privacy Policy</a>. and to receive
                        marketing offers by email, text message or phone call from us or our agents until you opt
                        out.
                     </div>
                  </div>

               </div>

               <!-- Modal footer -->
               <div class="modal-footer">
                  <button class="subscribe btn btn-primary btn-block" type="submit" style="background-color: #56b18b;
                            border: 1px solid #56b18b;"> Get Your Report Instantly </button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
               </div>
            </form>
         </div>

      </div>
   </div>
</div>

<!--End Model -->






<section class="StockAdvi marginvoothn d-none d-sm-block">
   <div class="container  ">
      <h4> <span class="headeDlight saaAllRepla">Categories</span> <span class="head__tsxt"> </span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <!-- <p class="SubText"><?php echo e($homesdata->catagories); ?>

      </p> -->
      <div class="Catagories">
         <div class="row row-cols-2 g-2">
            <?php $__currentLoopData = $reportscat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4 lefstColun">
               <div class="ReportTypeSlide maxSpecr CatagoriesBox ">
                  <img class="lesftNone spciForcImg" src="/public/uploads/editorial/<?php echo e($reportdata->image); ?>" alt="<?php echo e($reportdata->title); ?>" />
                  <h5 class="blueChips"><?php echo e($reportdata->title); ?></h5>
                  <p class="applYPTgs onehuS catr__psty"><?php echo e($reportdata->desciption); ?></p>
                  <a href="<?php echo e(route('manage.menues',['opportunity-stock',$reportdata->slug])); ?>" class="cate__explore">Explore <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </div>
         <!-- <a class="ExploreBlue" style="margin: 18px 30px 0px 0px;" href="#!">Explore More <i class="fa fa-arrow-circle-right arre_ost" aria-hidden="true"></i></a> -->
      </div>
   </div>
</section>

<section class="sampleReport d-none d-sm-block" style="margin-top: -33px;">
   <div class="container">
      <div class="sampleReportLeft">
         <h4> <span class="headeDlight saaAllRepla">Sector Specific</span> </h4>
         <!-- <div class="bar_seprator"></div> -->
      </div>
      <?php $code=''; $i=1; ?>
      <?php $__currentLoopData = $sector; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php
      $path=route('manage.menues',['sector-specific',$reportdata->slug]);

      $code.='<a href="'.$path.'">
         <div class="sampleReportInBox">'; $code.='<img src="/public/uploads/Sectors/'.$reportdata->image.'"  alt="'.$reportdata->title.'"/>'.$reportdata->title.'</div></a>';
          ?>
      <?php if($i==3 || $i==7 || $i==10): ?> <?php {{echo '<div class="sampleReportRight">'.$code.'</div>';$code='';}} ?> <?php endif; ?>
      <?php $i++; ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

   </div>

   <!-- <center>
      <a class="ExploresecEso" style="margin: 18px 30px 0px 0px;" href="<?php echo e(route('sectorSpecific')); ?>">Explore More <i class="fa fa-arrow-circle-right arre_ost" aria-hidden="true"></i></a>
   </center> -->


   <!-- <a style="margin-top: 43px;" href="<?php echo e(route('sectorSpecific')); ?>" class="sector__exlore">Explore More <i class="fas fa-arrow-right"></i></a> -->
   </div>
</section>
<section class="AboutUs d-none d-sm-block" style="    margin-top: 62px;margin-bottom: 33px;">
   <div class="container  ">
      <div class="row">
         <div class="col-md-7 remove__columns">
            <div class="AboutUstLeft">
               <h4> <span class="headeDlight saaAllRepla">About us</span> <span class="head__tsxt"> </span> </h4>
               <!-- <div class="bar_seprator"></div> -->
               <p class="applYPTgs">Veye is a leading independent equities research firm dedicated to helping its
                  clients improve
                  their investment results through unbiased and precise recommendations around buying, selling or
                  holding stocks. Our sole purpose is to empower our clients in achieving their financial
                  ambitions through our expertise innovation, excellence & service. <br> <br>
                  We believe in building
                  effective, long-term relationships through trust and mutual benefit and strive hard to help you
                  accumulate or multiply your wealth and attain a state of financial freedom in your life.
               </p>
               <!-- <p class="wepAdding"></p> -->
               <a href="pages/about-us" class="aboutFonts"> Read More...</a>
            </div>
         </div>
         <div class="col-md-5 remove__columns ">
            <div class="AboutUsBg">
               <div class="AboutUsBgCLR">
                  <div class="SubscribeTo">
                     <h5>Subscribe to our Special Report </h5>
                     <div class="PriceTag">From $200/6 Months</div>
                     <img src="<?php echo e(asset('frontend/assets/images/subcribe.png')); ?>" />
                     <a href="<?php echo e(route('subscriptionMenu')); ?>" class="subscrrespot">Subscribe</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="WhatSay tesy__mest SlideDotsCustome" style="margin-bottom: 63px;">
   <div class="container  ">
      <h4 style="text-align:center !important"> <span class="whats-css saaAllRepla">What Our Clients Say About Us? </span> <span class="head__tsxt"></span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <div class=" slider-nav3">
         <?php $__currentLoopData = $testmanag; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div>
            <div class="WhatSaySlide">
               <div class="CustomerData">
                  <h5><?php echo e($reportdata->title); ?></h5>
                  <div class="padding__tops"></div>

                  <p class="applYPTgs"><?php echo e($reportdata->desciption); ?></p>
                  <img class="TestImaCss" src="<?php echo e(asset('uploads/Test_Management')); ?>/<?php echo e($reportdata->image); ?>" alt="<?php echo e($reportdata->title); ?>"/>
               </div>
            </div>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
   </div>
</section>

<style type="text/css">
   
   .SlideBox .boxedus {
      border: 1px solid #ddd !important;
      border-radius: 16px !important;
      box-shadow: 0px 0px 4px 0px #ddd !important;
      padding: 9px !important;
      height: 225px;
   }

   .readMoreBoxHeighEqual {}

   .mdConst {
      background-color: rgb(255 255 255);
      border-radius: 11px;
      box-shadow: 0px 0px 0px 2px #fff;
   }

   .modalInsu {}

   .form-floating>label {
      padding: 11px 10px !important;
      height: 37px !important;
   }

   .form-floating>.form-control,
   .form-floating>.form-control-plaintext,
   .form-floating>.form-select {
      height: 44px !important;
   }

   .spcifotrepo6t {
      font-size: 14px;
      /*font-family: system-ui;*/
      line-height: 19px;
   }

   .cuClose {
      color: #000;
      border-radius: 100px;
      border: none;
      font-size: 19px;
      width: 30px;
      font-weight: bolder;
      height: 30px;
   }

   .detsilsHeight {
      position: absolute;
      right: 0;
      left: 0;
      bottom: 0px;
      display: flex;
      background-color: #ffffff;
      padding: 9px;
      flex-flow: column;
      margin: 4px 7px;
   }

   .specilaSidebcx {
      margin: 5px 5px !important;
   }

   .creadmore {
      color: black !important;
      font-size: 12px;
      margin-top: 4px;
      font-weight: 600 !important;
   }

   .teamVye {
      font-size: 11px !important;
      /*font-family: myFontHeading;*/
      margin-bottom: 10px;
      font-weight: 800;
   }

   /*.TestImaCss{
    position: absolute;
   top: 209px;*/
</style>

<section class="RecentPosts SlideDotsCustome" style="    margin-top: 32px;
    margin-bottom: 25px;">
   <div class="container  ">
      <h4 class="senters"> <span class="headeDlight saaAllRepla">Our Recent Editorial</span> <span class="head__tsxt"> </span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <div class="RecentPostsMbox">
         <div class="slider slider-nav4">
            <?php $__currentLoopData = $blog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="SlideBox specilaSidebcx">
               <img class="boxedus" src="<?php echo e(asset('/')); ?>/<?php echo e($reportdata->article_img); ?>" />
               <div class=" detsilsHeight eduSpHBs ">
                  <!-- heigBiTest -->
                  <small class="smaTest teamVye">TEAM VEYE | <i class="fa fa-calendar indtseVs"></i> <?php echo e(date('d-M-Y',strtotime($reportdata->created_at))); ?></small>
                  <!-- <p><?php echo e($reportdata->desciption,(0)); ?>....</p> -->
                  <a style="text-decoration: none; color: #000;" href="<?php echo e(route('editorial-details', $reportdata->slug)); ?>">
                     <?php echo strlen($reportdata->title) > 50 ? substr($reportdata->title,0,50).'..' : $reportdata->title; ?>

                  </a>
                  <a class="creadmore" href="<?php echo e(route('editorial-details', $reportdata->slug)); ?>">Read More..</a>
               </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </div>
      </div>
      <a class="ExploreBlue respon" style="margin: 18px 30px 0px 0px;" href="<?php echo e(route('editorial')); ?>">Explore More <i class="fa fa-arrow-circle-right arre_ost" aria-hidden="true"></i></a>
   </div>
</section>
<style type="text/css">
   .spnhsGrab {
      font-size: 30px;
      font-weight: 800;
      background: #ddd;
   }

   .ss5tx {
      color: #4EAF8A;
   }

   .udHeig {
      height: 51px;
      border-radius: 4px;
   }

   .sGets {
      background: #4EAF8A;
      border: 0px;
      border-radius: 4px;
      height: 53px;
      font-size: 23px;
      font-weight: 600;
   }

   .mbSh3 {}

   .lbgGs {
      font-weight: 700;
   }
</style>
<?php if($homepopup==false): ?>
<?php if($popupStatus->status==1): ?>
<div  class="modal fade" id="enFormModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content" style="    padding: 0px 18px;background: #ddd;">
         <div class="modal-header" style="padding: 8px; border-bottom: 0px;">
           
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="    margin-top: 5px;"></button>
         </div>
         <div>
            <img src="https://www.linkpicture.com/q/yey.png" style="max-width: 142px; margin-bottom: 17px;">
            <h2 class="spnhsGrab">Grab Our FREE Report on <br> Top <span class="ss5tx">5 ASX Dividend Stocks </span> <br> to Buy
               in 2023 </h2>
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('contactus.store')); ?>">
               <?php echo csrf_field(); ?>
               <input type="hidden" name="free_report" value="free_report">
               <div class="mbSh3">
                  <label for="recipient-name" class="col-form-label lbgGs">Name</label>
                  <input type="text" class="form-control udHeig" name="name" id="recipient-name" placeholder="Full Name">
               </div>
               <div class="mbSh3">
                  <label for="recipient-name" class="col-form-label lbgGs">Email Id</label>
                  <input type="email" class="form-control udHeig" name="email" id="recipient-name" placeholder="Your Email Id">
               </div>
               <div class="mbSh3">
                  <label for="recipient-name" class="col-form-label lbgGs">Phone Number</label>
                  <input type="text" class="form-control udHeig" name="phone" id="recipient-name" placeholder="Phone Number">
                   <input type="hidden" class="form-control udHeig" name="post_code" id="recipient-name" placeholder="Phone Number">
               </div>
               <div class="form-group" style="font-size: 12px; padding-bottom: 0px; padding-top: 15px;">
                  <img draggable="false" class="emoji" alt=":lock:" src="/assets/img/lock.svg" style="width: 13px; margin-top: -5px;"> By providing your details, you agree to Veye’s
                  <a style="color: #54af8a;" href="/pages/terms-conditions"> Terms &amp; Conditions</a> and <a style="color: #54af8a;" href="/pages/privacy-policy"> Privacy Policy</a>. and to receive
                  marketing offers by email, text message or phone call from us or our agents until you opt
                  out.
               </div>
         </div>
         <div class="modal-footer">
            <button type="submit" class="btn btn-primary sGets w-100">GET YOUR FREE REPORT NOW</button>
         </div>
         </form>
      </div>
   </div>
</div>
<?php endif; ?>
<?php endif; ?>



</div>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
   $(window).on('load', function() {
      $('#enFormModal').modal('show');
   });
</script>

<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M895KWH"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\development82\htdocs\veyework\resources\views/frontend/index.blade.php ENDPATH**/ ?>