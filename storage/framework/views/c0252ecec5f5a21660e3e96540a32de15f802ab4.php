<?php

use App\Models\FooterMenu;
use App\Models\Reports;
use App\Models\LogoEdit;
?>
<?php $logodata = LogoEdit::first(); ?>
<footer class="text-lg-start text-white">
   <!-- Grid container -->
   <!-- Section: Links -->
   <section class="mainFooter">
      <div class="container">
         <!--Grid row-->
         <div class="row">
            <div class="col-md-3">
               <a class="navbar-brand navbar-brand-logo" href="/">
                  <img class="footerKaPading" src="<?php echo e(asset('frontend/assets/images')); ?>/<?php echo e($logodata->footer_logo); ?>" />
               </a>
            </div>
            <div class="col-md-9">
               <div class="footerFCNT border-0">
                  <p>Veye Pty Ltd only provides general, and not personalised financial advice, and has not taken
                     your personal circumstances into account. Veye Pty Ltd operates under AFSL 523157. For more
                     information please see our <a class="ppHosg" target="_blank" href="/pages/financial-services-guide">Financial Services Guide.</a> Please remember that
                     investments can go up and down. Any past performance shown is not an indication of Future
                     Returns. Commission and other costs charged by executing broker are not considered when
                     calculating past performance. We request our readers not to interpret our reports as direct
                     recommendations. Veye Pty Ltd does not guarantee the performance of, or returns on any
                     investment.
                  </p>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-3">
               <div class="footer-column">
                  <h4>About Veye</h4>
                  <p>Veye is a leading independent equities research firm dedicated to helping its clients improve
                     their
                     investment results through unbiased and precise recommendations around buying, selling or
                     holding
                     stocks.
                  </p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="footer-column">
                  <h4>Quick Links</h4>
                  <ul class="foot_links">
                     <?php $footerMenuData=FooterMenu::where('type',2)->where('deleted_at',NULL)->orderBy('order','ASC')->get(); ?>
                     <?php $__currentLoopData = $footerMenuData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <li><a href="<?php echo e(route('manage.menues',['stock-advisory',$data->slug])); ?>"><?php echo e($data->title); ?></a></li>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
               </div>
            </div>
            <div class="col-md-3">
               <div class="footer-column">
                  <h4>Important Links</h4>
                  <ul class="foot_links">
                     <?php $footerMenuData=FooterMenu::where('type',1)->where('deleted_at',NULL)->orderBy('order','ASC')->get(); ?>
                     <?php $__currentLoopData = $footerMenuData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <li><a href="<?php echo e(url('pages')); ?>/<?php echo e($reportdata->slug); ?>"><?php echo e($reportdata->title); ?></a></li>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  </ul>
               </div>
            </div>
            <div class="col-md-3">
               <div class="footer-column border-0">
                  <h4>Reach Us</h4>
                  <ul class="foot_links foot_linksLast">
                     <li class="liBusBo"><i class="fas fa-map-marker-alt"></i>   
                        <span class="veAdsCss">  Veye Pty Ltd <br> Level 21, 207 Kent Street,
                        Sydney, NSW, 2000</span>
                     </li>
                     <li><a href="mailto:info@veye.com.au"><i class="fas fa-envelope"></i> 
                           info@veye.com.au</a>
                     </li>
                     <li><a href="tel:(02) 9052 4957"><i class="fas fa-phone-volume"></i>
                           (02) 9052 4957</a>
                     </li>
                  </ul>
                  <div class="sunvscCss">
                     <label class="subsLabev">Subscribe to our newsletter</label>
                     <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter Your Email" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <button class="btn hsyCds btn-outline-secondary" type="button" id="button-addon2">Subscribe</button>
                     </div>
                     <div class="form-group" style="font-size: 12px;margin-bottom: 21px;">
                        <img draggable="false" class="emoji" alt=":lock:" src="<?php echo e(asset('frontend/assets/images/lock.svg')); ?>" style="width: 13px; margin-top: -5px;"> By providing your details, you agree to Veye’s
                        <a class="tserHosve" style="color: green;" href="/pages/terms-conditions"> Terms &amp; Conditions</a> and <a class="tserHosve" style="color: green;" href="/pages/privacy-policy"> Privacy Policy</a> and to receive
                        marketing offers by email, text message or phone call from us or our agents until you opt
                        out.
                     </div>
                  </div>
                  <div class="socialTocs">
                     <a class="btn btn-outline-light btn-floating m-1 fbcolor" href="http://www.facebook.com/VeyePtyLtd/" target="_blank" role="button"><i class="fab fa-facebook-f "></i></a>
                     <!-- Twitter -->
                     <a class="btn btn-outline-light btn-floating m-1 twiftFb" href="http://twitter.com/VeyePtyLtd" target="_blank" class="text-white" role="button"><i class="fab fa-twitter"></i></a>
                     <!-- Google -->
                    <a class="btn btn-outline-light btn-floating m-1 fbcolor" target="_blank" class="text-white" role="button" href="http://www.linkedin.com/company/13629374/admin/"><i class="fab fa-linkedin"></i></a>
                     <!-- Instagram -->
                     <a class="btn btn-outline-light btn-floating m-1 instagColor" href="http://www.instagram.com/veye.pty.ltd/" target="_blank" class="text-white" role="button"><i class="fab fa-instagram"></i></a>
                     <a class="btn btn-outline-light btn-floating m-1 YouTubCsss" href="http://www.youtube.com/channel/UC9HnXavh2BIj5VQctuhArjQ" target="_blank" class="text-white" role="button"><i class="fab fa-youtube"></i></a>
                  </div>
               </div>
            </div>
         </div>
         <!--Grid row-->
      </div>
   </section>
   <style type="text/css">
      .veAdsCss{
        position: absolute;
    margin-left: 7px;
      }
      .liBusBo{
        margin-bottom: 53px;
      }
      .tserHosve:hover{
         color: #fff !important;
      }
   </style>


   <section class="subFooter">
      <div class="container">
         <p>Copyrights © 2023 Veye | ACN 623 120 865 | ABN 58 623 120 865 | AFSL 523157</p>
      </div>
   </section>
   <!-- Section: Copyright -->
   <!-- Grid container -->
</footer><?php /**PATH /var/www/mainsite/resources/views/frontend/includes/footer.blade.php ENDPATH**/ ?>