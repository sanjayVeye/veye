<?php $__env->startSection('title'); ?> Edit Page <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Edit Page
        <small>Edit Page</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="<?php echo e(route('aboutus.index')); ?>">Page</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Edit</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 smabeStyge">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('add-page.update',$data->id)); ?>">
                <?php echo csrf_field(); ?>
                <?php echo e(method_field('PATCH')); ?>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Page name</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="page_name" type="text" placeholder="Enter meta title" value="<?php echo e($data->page_name); ?>">
                        <?php if($errors->has('page_name')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('page_name')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Slug</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="tag" type="text" placeholder="Enter meta title" value="<?php echo e($data->tag); ?>">
                        <?php if($errors->has('tag')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('tag')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Title</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="meta_title" type="text" placeholder="Enter meta title" value="<?php echo e($data->meta_title); ?>">
                        <?php if($errors->has('meta_title')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('meta_title')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Keywords</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_keywords" id="" class="form-control" placeholder="Meta Keywords"><?php echo e($data->meta_keywords); ?></textarea>
                        <?php if($errors->has('meta_keywords')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('meta_keywords')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Meta Description</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="meta_description" id="" class="form-control" placeholder="Meta Description"><?php echo e($data->meta_description); ?></textarea>
                        <?php if($errors->has('meta_description')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('meta_description')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Template</label>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="template" id="description" class="form-control"><?php echo e($data->template); ?></textarea>
                        <?php if($errors->has('template')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('template')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('description');
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/add-page/edit.blade.php ENDPATH**/ ?>