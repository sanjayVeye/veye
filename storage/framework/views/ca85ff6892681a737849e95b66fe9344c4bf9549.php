<?php $__env->startSection('title'); ?> ASX Reports - Subscribe and Improve Your Investment Results <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?>Subscribe for unbiased and precise recommendations on buying, selling, or holding ASX-listed stocks. Improve your investment results today. <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?>  <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/subscribe <?php $__env->stopSection(); ?>
<?php

use App\Models\Reports;
?>
<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>Subscribe</h1>
         <div class="bd-example">
            <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"><i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i> Go back</a>
         </div>
         <!-- <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="">Home / </a></li>
                  <li><a href="">Contact</a></li>
               </ol>
            </nav>
         </div> -->

      </div>
   </div>
</section>
<style type="text/css">
   .RePostNews {
      width: 75px;
      position: absolute;
      left: -7px;
      top: -7px;
   }
</style>
<section class="About">
   <div class="container">
      <div class="row row-cols-1 row-cols-md-4 divfIpas mb-3 text-center">

         <?php $__currentLoopData = $subscriptionmenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subscription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <?php $reportIds = explode(",", $subscription->reports_id); ?>
         <?php if($subscription->report_type == '1'): ?>
         <div class="col">
            <div class="  anothersPricing mb-4 shadow">

               <div class="PricingCardBoxheader">
               <?php if($subscription->red_strap_text): ?>
                  <img class="RePostNews" src="https://i.imgur.com/YQeZTDm.png">
                  <?php endif; ?>
                  <h2><?php echo e($subscription->title); ?></h2>
               </div>
               <center>
                  <img class="imgDummys" src="<?php echo e(asset('uploads/offer_image')); ?>/<?php echo e($subscription->icon_image); ?>">
                  <?php if($currentDate < @$subscription->offer_end): ?>
                     <h5 class="onoffers">On Offer</h5>
                     <?php endif; ?>
               </center>
               <div class="PricingCardBoxbody">
                  
                  <?php if($currentDate < @$subscription->offer_end): ?>
                  <h3 class="cardPricingCardBox"><?php echo e($subscription->offer_price); ?> / <?php echo e($subscription->plan_information); ?></h3>
                     <h4 class="cardPricingCardBox"> <?php echo e($subscription->plan_price); ?> / <?php echo e($subscription->plan_information); ?> </h4>
                     <?php else: ?>
                     <h3 class="cardPricingCardBox"><?php echo e($subscription->sale_price); ?> / <?php echo e($subscription->plan_information); ?></h3>
                     <?php if($subscription->plan_cut_price): ?>
                     <h4 class="cardPricingCardBox"> <?php echo e($subscription->plan_cut_price); ?> / <?php echo e($subscription->sale_information); ?> </h4>
                     <?php endif; ?>
                     <?php endif; ?>


                     <p>Inclusive of GST</p>
                     <center>
                        <div class="hrstyles"></div>
                     </center>
                     <ul class="list-unstyled  reslistys">
                        <!-- <li><i class="far fa-check-square"></i> Dividend + Growth company report </li>
                     <li><i class="far fa-check-square"></i> Buy, Sell, Hold Recommendations </li> -->
                        <?php foreach ($reportIds as $v) { ?>
                           <?php
                           $subscriptionName = Reports::find($v);
                           ?>
                           <li><i class="far fa-check-square"></i> <?php echo e(@$subscriptionName ->title); ?></li>

                        <?php } ?>
                     </ul>
                     <form action="<?php echo e(route('checkout')); ?>" method="get" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" value="<?php echo e(Crypt::encrypt($subscription->id)); ?>" name="id">
                        
                        <input type="hidden" value="<?php echo e($subscription->title); ?>" name="title">
                        <?php if($currentDate < @$subscription->offer_end): ?>
                        <input type="hidden" value="<?php echo e($subscription->offer_price); ?>" name="price">
                        <input type="hidden" value="<?php echo e($subscription->plan_information); ?>" name="months">
                        <?php else: ?>
                        <input type="hidden" value="<?php echo e($subscription->sale_price); ?>" name="price">
                        <input type="hidden" value="<?php echo e($subscription->sale_information); ?>" name="months">
                        <?php endif; ?>
                        <input type="hidden" value="1" name="quantity">
                        <button class=" ownBtncs btn btn-lg">Buy Now</button>

                     </form>
               </div>

            </div>
         </div>
         <?php endif; ?>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

         <div class="col" style="display: none;">
            <div class="PricingCardBox anothePrisvByc PricingCardBoxCustome shadow customHightSpecila mb-4">
               <div class="PricingCardBoxheader">
                  <h2>Customise your own plan</h2>
                  <img class="imgDummys" src="https://static.thenounproject.com/png/581496-200.png">
               </div>
               <div class="PricingCardBoxbody">
                  <h5>Select reports of your own
                     choice
                  </h5>


                  <h6 class="buildwonp">Build your own plan by adding plan of your choice.</h6>
                  <ul class="list-unstyled" style="margin-bottom: 0px;">
                     <?php $__currentLoopData = $subscriptionmenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subscription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <li><?php echo e($subscription->title); ?>

                     </li>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  </ul>
                  <button type="button" class="ownBtncs btn btn-lg custoButList" data-bs-toggle="modal" data-bs-target="#exampleModalCenteredScrollable">CUSTOMISE NOW</button>
                  <a href="tel:61290524957" type="button" class="btn talktoslse btn-lg">TALK TO SALES</a>

                  <div class="estRtsCss"></div>

               </div>
            </div>
         </div>

      </div>
   </div>
</section>
<div class="modal fade" id="exampleModalCenteredScrollable" tabindex="-1" aria-labelledby="exampleModalCenteredScrollableTitle" aria-hidden="true">
   <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
         <div class="modal-body DevidenReport">
            <div class="row">
               <div class="col-md-6">
                  <div class="DevidenReportBoxFirst">
                     <h4>Choose Plan</h4>
                     <div class="DevidenReportBox">
                        <div class="DevidenReportTable">
                           <table class="table table-striped">
                              <tbody>
                                 <?php $__currentLoopData = $subscriptionmenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subscription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <tr>
                                    <td><?php echo e($subscription->title); ?></td>
                                    <td> <?php echo e($subscription->sale_price); ?> / <?php echo e($subscription->plan_information); ?></td>
                                    <td class="NumGreen add-to-cart"><a href="#0" class="cmn--btn cart-number-btn add-to-cart" data-product_id="<?php echo e($subscription->id); ?>"> Add</a> </td>
                                 </tr>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="InvestorCNTboxLeft p-0">
                     <div class="DevidenReportBoxSecond " id="cart">
                        <h5>Plan details</h5>

                        <div class="planDetaScr">
                           <?php $total = '0'; ?>
                           <?php $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <div class="DevidenReportBoxSecondBox">
                              <h5><?php echo e($cart->productName->title); ?> <span><b> <?php echo e($cart->price); ?></b> <a href="#0" class="remove-cart" data-cart_id="<?php echo e($cart->id); ?>">
                                       <i class="fas fa-trash-alt remove-cart"></i></a></span></h5>
                              <p>Membership duration: <?php echo e($cart->productName->plan_information); ?></p>
                           </div>
                           <?php
                           $price = str_replace('$', '', $cart->price);
                            $total += $price; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>

                        <div class="DevidenReportBoxSecondBox">
                           <h5>Total amount <span><b> $<?php echo e($total); ?></b></span></h5>
                        </div>
                     </div>
                     <div class="TermAndCondition">
                        <p><img src="<?php echo e(asset('frontend/assets/images/lock.png')); ?>" />
                           By providing your details, you agree to Veye’s <a href="/pages/terms-conditions">Terms & Conditions</a> and
                           <a href="/pages/privacy-policy">Privacy Policy.</a> and to receive marketing offers by email, text message
                           or phone call from us or our agents until you opt out.
                        </p>
                     </div>
                     <div class="paymentMathod">
                        <form action="<?php echo e(route('checkout')); ?>" method="get" enctype="multipart/form-data">
                           <?php echo csrf_field(); ?>
                           <input type="hidden" value="<?php echo e(Crypt::encrypt('custom')); ?>" name="id">
                           <button class="btn PayCard">Pay With Card</button>
                           <button class="btn PayPal">Check out with <img src="<?php echo e(asset('frontend/assets/images/paypal.png')); ?>" /></button>
                        </form>
                        <ul>
                           <li>
                              <img src="<?php echo e(asset('frontend/assets/images/visa.png')); ?>" />
                           </li>
                           <li>
                              <img src="<?php echo e(asset('frontend/assets/images/MasterCard_Logo.svg.png')); ?>" />
                           </li>
                           <li>
                              <img src="<?php echo e(asset('frontend/assets/images/american-express.png')); ?>" />
                           </li>
                           <li>
                              <img src="<?php echo e(asset('frontend/assets/images/google-pay.png')); ?>" />
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<section class="FAQ_Section">
   <div class="container">
      <h4 class="FAQ_heading">Frequently Asked Questions</h4>
      <div class="row">
         <?php $__currentLoopData = $faqdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div class="col-md-6">
            <div class="customAccordian">
               <div class="accordion" id="accordionExample">
                  <div class="accordion-item">
                     <h4 class="accordion-header" id="heading-<?php echo e($key); ?>">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-<?php echo e($key); ?>" aria-expanded="false" aria-controls="collapse-<?php echo e($key); ?>">
                           <?php echo e($data->title); ?>

                        </button>
                     </h4>
                     <div id="collapse-<?php echo e($key); ?>" class="accordion-collapse collapse" aria-labelledby="heading-<?php echo e($key); ?>" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                           <?php echo e($data->details); ?>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\development82\htdocs\veyework\resources\views/frontend/subscriptionmenu.blade.php ENDPATH**/ ?>