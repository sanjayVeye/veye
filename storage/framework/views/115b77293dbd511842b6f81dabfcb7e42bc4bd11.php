<!DOCTYPE html>
<html lang="en">
   <!--<![endif]-->
   <!-- BEGIN HEAD -->
      <?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
   <!-- END HEAD -->

   <body>
      <main>
         <?php echo $__env->make('frontend.includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
         <!-- BEGIN CONTENT BODY -->
         <?php echo $__env->yieldContent('content'); ?>
         <!-- END CONTENT BODY -->  
         <?php echo $__env->make('frontend.includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      </main>
      <?php if(@$recommendations=='past_recommendations'): ?>
      <?php else: ?>
      <?php echo $__env->make('frontend.includes.footer_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <?php endif; ?>
   </body>

</html><?php /**PATH /var/www/mainsite/resources/views/frontend/layouts/app.blade.php ENDPATH**/ ?>