<div class="col">
    <div class="card ReportCard shadow">
        <div class="row g-0">
            <div class="col-md-4">
                <img class="bd-placeholder-img" width="100%" height="160" src="<?php echo e(asset($item->thumbnail)); ?>" />
            </div>
                
            <div class="col-md-8">
                <div class="card-body lesRRtbo">
                    <h5 class="card-title">Team Veye  | <?php if($item->schedule): ?> <?php echo e(date('d-M-Y',strtotime($item->schedule))); ?> <?php else: ?> <?php echo e(date('d-M-Y',strtotime($item->created_at))); ?> <?php endif; ?></h5>
                    <a href="<?php echo e(route('report.read.more',$item->slug)); ?>"><h5 class="card-Heading itsFcads"><?php echo e($item->title); ?></h5></a>
                    
                    <a href="<?php echo e(route('report.read.more',$item->slug)); ?>" class="ReadMoreRprtData">Read More...</a>

                    <?php if($item->recommendation==1): ?>
                        <div class="TagLabel"><span class="badge rounded-pill bg-danger">Sell</span></div>
                    <?php endif; ?>
                    <?php if($item->recommendation==2): ?>
                        <div class="TagLabel"><span class="badge rounded-pill bg-success">Buy</span></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH /var/www/mainsite/resources/views/frontend/search_reports_details.blade.php ENDPATH**/ ?>