<?php $__env->startSection('title'); ?> Manage Clients <?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{
            width: 100%!important;
        }
        .select2-search--dropdown .select2-search__field {
            width: 98%;
        }
        /* .table-scrollable{
            overflow-x: inherit !important;
            overflow-y: inherit !important;
        } */
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>

    <script type="text/javascript">
		$(document).ready(function(){
			$('#dataTableExample').dataTable({
                order: [[7, 'desc']],
					"aLengthMenu": [
						[10, 30, 50, -1],
						[10, 30, 50, "All"]
					],
					// "pagingType": "full_numbers",
					"iDisplayLength": 10,
					"language": {
						search: ""
					},
					"serverSide": true,
					"processing": true,
					"paging": true,
					"ajax":{
						url:'<?php echo e(route("clients.index")); ?>',
						data:function(e){

						}
					},
					'createdRow': function( row, data, dataIndex ) {
						$(row).attr('id', 'tr_'+data.id);
					},
					columns: [
						{ data: "sno" ,name: 'sno',searchable: false, sortable : false, visible:true},
						{ data: "client_name" ,name:"client_name",searchable: false, sortable : true, visible:true},
                        { data: "first_name" ,name:"first_name",visible:false},	
                        { data: "last_name" ,name:"last_name",visible:false},						
						{ data: "email" ,name:"email"},
                        { data: "phone" ,name:"phone"},
                        { data: "post_code" ,name:"post_code"},
						{ data: "created_at" ,name:"created_at"},
						{ data: "updated_at" ,name:"updated_at"},
                        { data: "type" ,name:"type",searchable: false, sortable : false, visible:true},
						{ data: "action" ,name: 'action',searchable: false, sortable : false, visible:true},
					],
                    "fnInitComplete": function (oSettings, json) {

                    }
				});

                $('#info_table').dataTable({
					"aLengthMenu": [
						[10, 30, 50, -1],
						[10, 30, 50, "All"]
					],
					"pagingType": "full_numbers",
					"iDisplayLength": 10,
					"language": {
						search: ""
					},
					"serverSide": true,
					"processing": true,
					"paging": true,
					"ajax":{
						url:'<?php echo e(route("client-credentials.addProduct",0)); ?>',
						data:function(e){
                            e.client_pk=$('#client_id').val();
						}
					},
					'createdRow': function( row, data, dataIndex ) {
						$(row).attr('id', 'tr_'+data.id);
					},
					columns: [
						{ data: "sno" ,name: 'sno',searchable: false, sortable : false, visible:true},
						{ data: "product_name" ,name:"product_name"},					
						{ data: "subscription_start_date" ,name:"subscription_start_date"},
						{ data: "subscription_end_date" ,name:"subscription_end_date"},
						{ data: "invoice_no" ,name:"invoice_no"},
                        { data: "amount" ,name:"amount"},
                        { data: "product_type_text" ,name:"product_type_text"},
						{ data: "action" ,name: 'action',searchable: false, sortable : false, visible:true},
					],
                    "fnInitComplete": function (oSettings, json) {

                    }
				});

            
		});

        $(function(){
            $('#service_form').submit(function(e){
                e.preventDefault(0);
                var id=$(this).find('#client_id').val();
                var type=$(this).find('#type').val();
                var url='<?php echo e(route("clients.type_update")); ?>';
                $.post(url,{
                    _type:type,
                    _token:'<?php echo e(csrf_token()); ?>',
                    _is_type_change:1,
                    _client_id:id
                },function(res){
                    alert('Updated successsfully.');
                });
                
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Manage Clients
            <small>Manage Clients</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li><span>Clients</span></li>
            </ul>
            <div class="page-toolbar">
                <a class="btn btn-primary" href="<?php echo e(route('clients.create')); ?>"><i class="fa fa-plus me-2"></i> Add Clients</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div>
					<table id="dataTableExample" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Client Name</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>  
                                <th>Phone</th>   
                                <th>PostCode</th>                                
                                <th>Created On</th>
                                <th>Updated On</th>
                                <th>Client Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>

                    <?php echo $__env->make('admin.clients.modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/clients/index.blade.php ENDPATH**/ ?>