<?php $__env->startSection('title'); ?> Registration | Veye Pty Ltd <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?>  Register now for Veye Clint! Join our investment community and gain exclusive access to powerful tools, expert analysis, and real-time market data. <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au/registration <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <META NAME="robots" CONTENT="noindex,nofollow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php
use App\Models\LogoEdit;
?>
<?php $logodata = LogoEdit::first(); ?>
      <section class="gradient-form">
         <style type="text/css">
            .formBoxHead h4 {
             font-size: 22px !important; 
             margin: 11px 0px 7px !important; 
         }
         </style>
         <div class="gradient-formBGCLR">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4 offset-lg-4">
                     <div class="formBox newLogins">
                        <div class="formBoxHead">
                        <img src="<?php echo e(asset('frontend/assets/images')); ?>/<?php echo e($logodata->header_logo); ?>"/> 
                           <h4>Client Registration</h4>
                           <!-- <p>Use your credentials to access your account</p> -->
                        </div>
                        <form method="post" enctype="multipart/form-data" action="<?php echo e(route('registration.store')); ?>">
                           <?php echo csrf_field(); ?>
                            <div class="form-floating ">
                              <input type="text" name="first_name"  class="form-control" id="floatingInput"
                                 placeholder="First Name">
                              <label for="floatingInput">First Name</label>
                           </div>
                           <div class="form-floating ">
                              <input type="text" name="last_name"  class="form-control" id="floatingInput"
                                 placeholder="Last Name">
                              <label for="floatingInput">Last Name</label>
                           </div>
                           <div class="form-floating ">
                              <input type="email" name="email"  class="form-control" id="floatingInput"
                                 placeholder="Your Email">
                              <label for="floatingInput">Email</label>
                           </div>
                           <div class="form-floating">
                              <input type="text" name="mobile" minlength="10" maxlength="10" class="form-control" id="floatingInput"
                                 placeholder="Phone Number">
                              <label for="floatingInput">Phone</label>
                           </div>
                           <div class="form-floating">
                              <!-- mb-3 -->
                              <input type="text" name="post_code" class="form-control" id="floatingInput"
                                 placeholder="Post Code">
                              <label for="floatingInput">Post Code</label>
                           </div>
                           <!-- <div class="form-floating">
                              <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                              <label for="floatingPassword">Password</label>
                           </div>
                           <div class="form-floating">
                              <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                              <label for="floatingPassword">Confirm Password</label>
                           </div> -->
                           <div class="acceptVeye">
                              <label>
                                 <input type="checkbox" name="accept" value="1"/> <span>I accept Veye <a href="/pages/terms-conditions">Terms & Conditions</a></span>
                              </label>
                              <!-- <label>
                                 <input type="checkbox" /> <span>Remember me on this device</span>
                              </label> -->
                           </div>

                           <div class="text-center">
                              <button class="btn VeyeLogin " type="submit">Register</button>
                           </div>

                           <div class="ForgotPassBox d-flex " style="    margin-top: -28px;">
                            <!-- align-items-start justify-content-center -->
                              <div>
                                 <p>Not a member?</p>
                                 <a class="ForgotPass">Forgot password?</a>
                              </div>
                           </div>
                        </form>
                        <a href="<?php echo e(url('client-login')); ?>" ><button class="btn regLogiHover VeyeRegister text-white" style="margin-top: -83px;margin-left: 157px;">Login</button></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script>
         var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
      </script>
      <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/registration/register.blade.php ENDPATH**/ ?>