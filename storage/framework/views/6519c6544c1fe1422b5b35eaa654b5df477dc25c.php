<?php
    $selected_plans=[];
    
    if($clientDetails){
        $selected_plans=$clientDetails->plans()->pluck('report_id')->toArray();

        $product_name   =$clientDetails->product_name;
        $start_date     =date('Y-m-d',strtotime($clientDetails->subscription_start_date));
        $end_date       =date('Y-m-d',strtotime($clientDetails->subscription_end_date));
        $invoice_no     =$clientDetails->invoice_no;
        $amount_paid    =$clientDetails->amount;
        $product_type   =$clientDetails->product_type_text;
        $comment        =$clientDetails->comment;
        $pk             =$clientDetails->id;
    }

?>

<div class="row">
    <div class="col-md-12">
        <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('client-credentials.manageAddMore')); ?>">
            <?php echo csrf_field(); ?>
            <?php if(isset($pk )): ?>
                <input type="hidden" name="client_details_pk" value="<?php echo e($pk); ?>">
            <?php endif; ?>
            <input type="hidden" name="client_id" value="<?php echo e($user->id); ?>">
            <h4><b>Subscription Details</b></h4><br />
            <b>Subscription Plans</b><br /><br />
            <div class="row mb-3">
            <?php $__currentLoopData = $subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-xs-12 col-md-4">
                    <?php
                        $sel=in_array($data->id,$selected_plans) ? 'checked' : '';
                    ?>
                    <label for="plan_latest_reports<?php echo e($index+1); ?>"><input type="checkbox" name="subscribe_id[]" value="<?php echo e($data->id); ?>" id="plan_latest_reports<?php echo e($index+1); ?>" class="1" readonly="" <?php echo e($sel); ?>> <?php echo e(@$data->title); ?></label>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <br /><br />

            <div class="row form-group">
                <div class="col-md-4">
                    Product name
                    <input class="form-control" name="product_name" type="text" placeholder="Enter Product Name" value="<?php echo e((isset($product_name)) ? $product_name : ''); ?>">
                    <?php if($errors->has('product_name')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('product_name')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="col-md-4">
                    Subscription Start Date
                    <input class="form-control" name="start_date" type="date" value="<?php echo e((isset($start_date)) ? $start_date : ''); ?>">
                    <?php if($errors->has('start_date')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('start_date')); ?></strong>
                        </span>
                    <?php endif; ?>

                </div>
                <div class="col-md-4">
                    Subscription End Date
                    <input class="form-control" name="end_date" type="date" value="<?php echo e((isset($end_date)) ? $end_date : ''); ?>">
                    <?php if($errors->has('end_date')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('end_date')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    Invoice Number
                    <input class="form-control" name="invoice_no" type="text" placeholder="Enter Invoice Number" value="<?php echo e((isset($invoice_no)) ? $invoice_no : ''); ?>">
                    <?php if($errors->has('invoice_no')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('invoice_no')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="col-md-4">
                    Amount Paid
                    <input class="form-control" name="amount_paid" type="text" placeholder="Enter Amount Paid" value="<?php echo e((isset($amount_paid)) ? $amount_paid : ''); ?>">
                    <?php if($errors->has('amount_paid')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('amount_paid')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="col-md-4">
                    Product Type
                    <input class="form-control" name="product_type" type="text" placeholder="Enter Product Type" value="<?php echo e((isset($product_type)) ? $product_type : ''); ?>">
                    <?php if($errors->has('product_type')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('product_type')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    Comment
                    <textarea name="comment" class="form-control" id="comment" cols="5" rows="5"><?php echo e((isset($comment)) ? $comment : ''); ?></textarea>
                    <?php if($errors->has('comment')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('comment')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-8 offset-lg-3">
                    <button type="submit" class="btn btn-primary mr-2">Add</button>
                    <button class="btn btn-light" type="reset" onclick="callOthers()">Reset</button>
                </div>
            </div>
        </form>
    </div>
</div><?php /**PATH /var/www/mainsite/resources/views/admin/client_credentials/subscription.blade.php ENDPATH**/ ?>