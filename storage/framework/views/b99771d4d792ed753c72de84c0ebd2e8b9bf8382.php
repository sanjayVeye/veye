<?php

use App\Models\Admin\ClientDetailsPlan;
use Illuminate\Support\Facades\DB;
?>
<nav id="sidebarMenu" class="col-md-3 col-lg-3 d-md-block bg-light sidebar collapse">

   <div class="position-sticky sidebar-sticky">

      <a href="<?php echo e(route('dashboard.home')); ?>">
         <h1 class="DashboardHeading"><i class="far fa-user"></i> My Dashboard</h1>
      </a>

      <ul class="nav flex-column">
         <!-- <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">
                           Reports
                        </a>
                     </li> -->

         <li class="nav-item start">
            <a href="#" class="nav-link nav-toggle">
               <i class="icon-home"></i>
               <span class="title"> My Reports</span>

            </a>
            <ul class="sub-menu">
               <?php
               $dateTime = date('Y-m-d H:i:s');
               $clentData =db::table('clients')->where('email',Auth::user()->email)->first();

               $reportAccess =ClientDetailsPlan::with('report')->where('client_id',@$clentData->id)->where('access',1)
               ->where('subscription_start_date','<=',$dateTime) ->where('subscription_end_date','>=',$dateTime)
               ->groupBy('report_id')
                  ->get();
                  ?>
                  <?php $__currentLoopData = $reportAccess; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php
                  $reportCount= db::table('dividend_reports')->where('report_id',$v->report_id)->count();
                  $reportName= db::table('reports')->where('id',$v->report_id)->first();
                  ?>
                  <?php if($reportName->title == 'Free Reports'): ?>
                  <?php else: ?>
                  <li class="nav-item start ">
                     <a href="<?php echo e(url('user-report/stock-advisory/')); ?>/<?php echo e(@$reportName->slug); ?>" class="nav-link " style="margin-left: 30px!important;">
                        <i class=""></i>
                        <span class="title"><?php echo e(@$reportName->title); ?></span>
                     </a>
                  </li>
                  <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
         </li>
         <!-- <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('my.watchlist')); ?>">
               My Watchlist
            </a>
         </li> -->
         <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('latest.buy')); ?>">
               Latest Buy Recommendations
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('latest.sell')); ?>">
               Latest Sell Recommendations
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('my.email')); ?>">
               My Email
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('my.password')); ?>">
               My Password
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('my.subscription')); ?>">
               My Subscription
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('my.invoice')); ?>">
               My Invoice
            </a>
         </li>


         <li class="nav-item">
            <a class="nav-link" href="javascript:void" onclick="$('#logout-form').submit();">
               Log Out
            </a>
         </li>
      </ul>
      <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
         <?php echo csrf_field(); ?>
      </form>
   </div>
</nav><?php /**PATH /var/www/mainsite/resources/views/frontend/dashboard/nav.blade.php ENDPATH**/ ?>