<?php $__env->startSection('title'); ?> Edit Client <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Client
            <small>Edit Client</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="<?php echo e(route('clients.index')); ?>">Clients</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('clients.update',$client->id)); ?>">
                    <?php echo csrf_field(); ?>
                    <?php echo e(method_field('PATCH')); ?>

                    

                    <div class="row form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">First Name</label>
                            <input class="form-control" name="first_name" type="text" placeholder="first name" value="<?php echo e($client->first_name); ?>">
                            <?php if($errors->has('first_name')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('first_name')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Last Name</label>
                            <input class="form-control" name="last_name" type="text" placeholder="Last name" value="<?php echo e($client->last_name); ?>">
                            <?php if($errors->has('last_name')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('last_name')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">Email</label>
                            <input class="form-control" name="email" type="text" placeholder="Enter Email Here" value="<?php echo e($client->email); ?>">
                            <?php if($errors->has('email')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Phone</label>
                            <input class="form-control" name="phone" type="number" minlength="10" maxlength="10" placeholder="Enter phone Here" value="<?php echo e($client->phone); ?>">
                            <?php if($errors->has('phone')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('phone')); ?></strong>
                            </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">Post Code</label>
                            <input class="form-control" name="post_code" type="number" placeholder="Enter Post Code Here" value="<?php echo e($client->post_code); ?>">
                            <?php if($errors->has('post_code')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('post_code')); ?></strong>
                            </span>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                   				
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/clients/edit.blade.php ENDPATH**/ ?>