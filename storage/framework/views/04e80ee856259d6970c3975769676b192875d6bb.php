<?php $__env->startSection('title'); ?> My Subscription <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php

use Illuminate\Support\Facades\DB;
?>
<?php $clentData =db::table('clients')->where('email',Auth::user()->email)->first(); ?>
<div class="container-fluid">
   <div class="row">

      <?php echo $__env->make('frontend.dashboard.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
         <div class="DashboardRight">
            <div class="DashboardRightHead">
               <h2 class="DashboardHeading">Hello <?php echo e(@$clentData->first_name); ?> <?php echo e(@$clentData->last_name); ?> </h2>
               <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
               <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i>
               </button>
            </div>
            <h3 class="DashboardSubHeading">My Subscription</h3>
         </div>
         <div class="DashboardRightDown">
            <div class="MySubscription">
               <button class="MySubscriptionBtn BTN_Green">Active Reports</button>
            </div>
            <section class="PastRecommend02">
               <div class="recomdedTable ">
                  <table class="table table-striped  past__border">
                     <thead>
                        <tr>
                           <th>Sno.</th>
                           <th>Plan Name</th>
                           <!-- <th>Subscription Start Date</th>-->
                           <!-- <th>Subscription End Date</th> -->
                           <th>Invoice Number</th>
                           <th>Total Amount Paid</th>
                           <!--<th>Status</th>-->
                           <th></th>
                        </tr>
                     </thead>
                     <tbody class="table-group-divider">
                        <?php $i=0;
                        date_default_timezone_set('Asia/Kolkata');
                        $currentDate = date('Y-m-d h:i:s');
                        ?>
                        <?php $__currentLoopData = $subscription; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>  
                           <td><?php echo e($i); ?></td>
                           <td><?php echo e($data->report->title); ?></td>
                           <!--<td><?php echo e(date("d-M-Y", strtotime($data->subscription_start_date))); ?></td>-->
                           <!-- <td><?php echo e(date("d-M-Y", strtotime($data->subscription_end_date))); ?></td> -->
                           <td><?php echo e($data->invoice_no->invoice_no); ?></td>
                           <td><?php echo e($data->invoice_no->amount); ?></td>
                           <?php if(date("d-M-Y", strtotime($currentDate)) < date("d-M-Y", strtotime($data->subscription_start_date))): ?>
                            <!--  <td style="color:green;">Extension</td>-->
                              <?php else: ?>
                             <!--  <td></td>-->
                              <!-- <td style="color:red;">Buy Now</td> -->
                              <?php endif; ?>
                              <td style="color:red;"><a href="<?php echo e(route('my.subscriptionGeneratePDFS',$data->invoice_no->id)); ?>"> Download Invoice</a></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
               </div>
               <div class="bd-example-snippet bd-code-snippet float-end">
                  <div class="bd-example">
                     <nav aria-label="Another pagination example">
                        <ul class="pagination pagination-lg flex-wrap">
                           <li class="page-item disabled">
                              <?php echo e($subscription->links( "pagination::bootstrap-4")); ?>

                           </li>

                        </ul>
                     </nav>
                  </div>
               </div>
            </section>
         </div>
         <div class="DashboardRightDown">
            <div class="MySubscription">
               <button class="MySubscriptionBtn BTN_Red">Inactive Reports</button>
            </div>
            <section class="PastRecommend02">
               <div class="recomdedTable ">
                  <table class="table table-striped  past__border">
                     <thead>
                        <tr>
                           <th>Sno.</th>
                           <th>Plan Name</th>
                           <th>Subscription Start Date</th>
                           <!-- <th>Subscription End Date</th> -->
                           <th>Invoice Number</th>
                           <th>Total Amount Paid</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody class="table-group-divider">
                        <?php $i=0; ?>
                        <?php $__currentLoopData = $subscriptionDeactive; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>
                           <td><?php echo e($i); ?></td>
                           <td><?php echo e(@$data->report->title); ?></td>
                           <td><?php echo e(date("d-M-Y", strtotime($data->subscription_start_date))); ?></td>
                           <!-- <td><?php echo e(date("d-M-Y", strtotime($data->subscription_end_date))); ?></td> -->
                           <td><?php echo e($data->invoice_no->invoice_no); ?></td>
                           <td><?php echo e($data->invoice_no->amount); ?></td>
                           <td><a href="" style="color:red;">Buy Now</a></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                     </tbody>
                  </table>
               </div>
               <div class="bd-example-snippet bd-code-snippet float-end">
                  <div class="bd-example">
                     <nav aria-label="Another pagination example">
                        <ul class="pagination pagination-lg flex-wrap">
                           <li class="page-item disabled">
                              <?php echo e($subscriptionDeactive->links( "pagination::bootstrap-4")); ?>

                           </li>

                        </ul>
                     </nav>
                  </div>
               </div>
            </section>
         </div>
      </main>
   </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/dashboard/mySubscription.blade.php ENDPATH**/ ?>