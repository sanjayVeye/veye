<?php $__env->startSection('title'); ?> <?php echo e(@$slugdata->meta_title); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> <?php echo e(@$slugdata->meta_description); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?> <?php echo e(@$slugdata->meta_keywords); ?> <?php $__env->stopSection(); ?>
<?php if(Request::get('page')!=NULL): ?> <?php $__env->startSection('nofollow'); ?> <META NAME="robots" CONTENT="noindex,nofollow"> <?php $__env->stopSection(); ?>
<?php else: ?> <?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php endif; ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/report/<?php echo e($parent); ?>/<?php echo e($child); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php

use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
use App\Models\Menues;
?>

<style type="text/css">
    .menusLinks {
        font-size: 14px !important;
        color: #4EAF8A !important;
    }
</style>
<section class="ReportPG">
    <div class="container">
        <div class="row align-items-end mb-5">
            <div class="col-md-6">
                <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
                    <h1><?php echo e($child_row->title); ?></h1>
                    <div class="bd-example">
                        <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row" style="clear:both;margin-top: -14px;">
            <div class="col-md-8">
                <?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col">
                    <div class="card ReportCard shadow">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img class="bd-placeholder-img" width="100%" height="155" src="<?php echo e(asset('/')); ?><?php echo e($item->thumbnail); ?>" />
                            </div>
                            <?php

                            $companies= db::table('companies')->where('asx_code',$item->asx_code)->first();
                            $desc=@$companies->description;
                          
                            ?>

                            <div class="col-md-8">
                                <div class="card-body lesBosyCss">
                                    <h5 class="card-title">Team Veye | <i class="fa fa-calendar"></i> <?php if($item->schedule): ?> <?php echo e(date('d-M-Y',strtotime($item->schedule))); ?> <?php else: ?> <?php echo e(date('d-M-Y',strtotime($item->created_at))); ?> <?php endif; ?></h5>
                                    <a href="<?php echo e(route('report.read.more',[$item->slug])); ?>"><h5 class="card-Heading"><?php echo e($item->title); ?></h5></a>
                                    <p class="card-text"><?php echo substr($desc, 0, 100); ?>...</p>
                                    <a href="<?php echo e(route('report.read.more',[$item->slug])); ?>" class="ReadMoreRprtData">Read More...</a>

                                    <?php if($item->recommendation==1): ?>

                                    <div class="TagLabel"><span class="badge rounded-pill bg-danger">Sell</span></div>
                                    <?php endif; ?>
                                    <?php if($item->recommendation==2): ?>
                                    <div class="TagLabel"><span class="badge rounded-pill bg-success">Buy</span></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <div class="bd-example-snippet bd-code-snippet">
                    <div class="bd-example">
                        <nav aria-label="Another pagination example">
                            <ul class="pagination pagination-lg flex-wrap">
                                <li class="page-item disabled">
                                    <?php echo e($results->links( "pagination::bootstrap-4")); ?>

                                    <!-- <a class="page-link"><i class="fas fa-long-arrow-alt-left"></i></a> -->
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <aside class="bd-aside text-muted align-self-start mb-3 mb-xl-4 px-2">
                    <nav class="small" id="toc">
                        <ul class="list-unstyled">
                            <li class="my-2">
                                <?php
                                 $dateTime = date('Y-m-d H:i:s');
                                $menuesData = Menues::where('parent',1)->where('deleted_at',null)->get();
                                $daily = DividendReports::where('report_id',2)


                                ->select('dividend_reports.*')
                                             ->where(function($q) use($dateTime){
                                             $q->where('dividend_reports.schedule','<=', $dateTime)
                                             ->orWhere('dividend_reports.schedule',NULL);
                                             })
                                ->take(5)->orderBy('created_at','desc')


                                ->orderBy('schedule','desc')->get();
								
                                ?>
								<?php if($child=='latest-reports'): ?>
                            <li class="my-2">
                                <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#forms-collapse" aria-controls="forms-collapse"><i class="fas fa-chevron-right"></i> New Report</button>
                                <ul class="btn d-inline-flex align-items-center collapsed border-0" id="forms-collapse">
                                    <div class="card ccLesdCss ReportCard">
                                        <?php $__currentLoopData = $daily; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datadaily): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                                <img class="bd-placeholder-img respoPlacsHold" width="100%" height="85" src="<?php echo e(asset('/')); ?>/<?php echo e($datadaily->thumbnail); ?>" />
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body  fsrCdsrd">
                                                    <h5 class="ttesCdss tmthora">TEAM VEYE | <i class="fa fa-calendar tesCader"></i> <?php if($datadaily->schedule): ?> <?php echo e(date('d-M-Y',strtotime($datadaily->schedule))); ?> <?php else: ?> <?php echo e(date('d-M-Y',strtotime($datadaily->created_at))); ?> <?php endif; ?></h5>
                                                    <a href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>"><h5 class="card-ttFOnts"><?php echo e($datadaily->title); ?></h5></a>
                                                    <a style="font-size:12px !important;margin-left: -7px; " class="menusLinks trsEd" href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>">Read More...</a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </ul>
                            </li>
							<?php endif; ?>
                            <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#contents-collapse" aria-controls="contents-collapse"><i class="fas fa-chevron-right"></i> More Report More Veye</button>
                            <ul class="list-unstyled list-group respoUlCss collapse show" id="contents-collapse">
                                <?php $__currentLoopData = $menuesData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a class="list-group-item Code4eaf8a" href="<?php echo e(route('manage.menues',['stock-advisory',$data->slug])); ?>"><?php echo e($data->title); ?></a>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            </li>
                            <li class="my-2">
                                <?php
                                $menuesData = Menues::where('parent',3)->where('deleted_at',null)->get();
                                ?>
                                <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#components-collapse" aria-controls="components-collapse"><i class="fas fa-chevron-right"></i> Sector Specific</button>
                                <ul class="list-unstyled list-group respoUlCss collapse" id="components-collapse">
                                    <?php $__currentLoopData = $menuesData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a class="list-group-item Code4eaf8a" href="<?php echo e(route('manage.menues',['sector-specific',$data->slug])); ?>"><?php echo e($data->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </aside>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/menues.blade.php ENDPATH**/ ?>