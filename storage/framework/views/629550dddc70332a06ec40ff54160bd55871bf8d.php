<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->startSection('title'); ?> <?php echo e(@$details->title); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> <?php echo e(@$details->description); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?> <?php echo e(@$details->meta_keywords); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/blog/<?php echo e($details->slug); ?><?php $__env->stopSection(); ?>
<?php

use App\Models\Menues;
use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
?>

<?php
$disclaimer= db::table('disclaimer')->first();
?>
<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-6">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Editorial</h1>
              <div class="bd-example">
               <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"><i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i> Go back </a>
           </div>
            </div>
         </div>
      </div>

      <style>
         .custm_con h1 {
            font-size: 24px;
            font-weight: 600;
         }

          .custm_con h2 {
            font-size: 22px;
            font-weight: 600;
         }
          .custm_con h3 {
            font-size: 20px;
            font-weight: 600;
         }
          .custm_con h4 {
            font-size: 18px;
            font-weight: 600;
         }

         .custm_con h5 {
            font-size: 18px;
            margin-top: 16px;
            font-weight: 800;
            text-decoration: underline;
            margin-bottom: 20px;
         }
          .custm_con h6 {
            font-size: 14px;
            font-weight: 600;
         }

         .custm_con p {
            font-size: 16px;
            /*line-height: justify;*/
            text-align: justify !important;
            /*margin-top: 14px;*/
            /*margin-bottom: 9px;*/
            text-align: initial;
            /*line-height: 28px;*/
            line-height: 1.5;
         }

         .custm_con disc {
            font-size: 14px !important;
            line-height: initial;
            margin-top: 14px;
            margin-bottom: 9px;
            text-align: justify;
            line-height: 28px;
         }

         .custm_con img {
            max-width: 100%;
         }

         .custm_con p strong {
            font-weight: bold !important;
         }

         .custm_con strong {
            font-weight: bold !important;
         }

         .custm_con p b {
            font-weight: bold !important;
         }

         .custm_con b {
            font-weight: bold !important;
         }

         .marg {
            margin: 10px 0px;
         }

         .ressprc {
           color: #18ba96;
    font-size: 12px;
         }

         .card-ttFOnts {
            margin-top: 0px !important;
         }

         .iitFfas {
            color: inherit !important;
            background: inherit !important;
            border: inherit !important;
            border-radius: 0px !important;
            padding: 7px 0px 0px !important;
            margin-right: -2px !important;
         }

         .tesvCss {
            font-size: 12px;
         }
         .card-ttFOnts{
                margin-left: 2px !important;
         }

         .cardlefts {
            padding-left: 10px;
         }

         .remborder {
            border: none !important;
            box-shadow: inherit !important;
         }

         .moreSpecials {
            margin: 4px 4px !important;
            border: 1px solid #ddd !important;
            border-radius: 16px !important;
            box-shadow: 0px 0px 4px 0px #ddd !important;
            padding: 8px !important;
         }

         .slick-list {
            /*margin: 13px !important;*/
         }

         .wites {
            background: #fff !important;
         }

         .mrrTospssss {
            margin-top: 42px !important;
         }

         .marg14 {
            margin-bottom: 14px !important;
         }
         .ssssGtxt{
                font-size: 24px !important;
    line-height: 28px !important;
         }
         
      </style>

      <div class="row">
         <div class="col-md-8">
            <div class="container custm_con carY_shadow">

               <h1 class="cosTcpos"><?php echo $details->title; ?></h1>
               <div class=" ReportCard mainLine"> </div>
               <span class="team-view-date pedding_green tesrFss"> Team Veye | <i class="fas fa-calendar-alt"></i><?php echo e(date('d-M-Y',strtotime($details->created_at))); ?>


                  <!-- <span>ASX - SDI</span> -->

               </span>


               <img class="img-fluid" src="<?php echo e(asset('/')); ?>/<?php echo e($details->article_img); ?>" alt="<?php echo e($details->attachment); ?>" />
               <p> <?php echo html_entity_decode($details->reporthtml); ?></p>
               <div class=" mainLine"> </div>
               <h4 class="marg14" style="font-size: 18px;">Disclaimer</h4>
               <?php echo html_entity_decode($disclaimer->details); ?>

            </div>
         </div>
         <div class="col-md-4 d-none d-sm-block d-sm-none d-md-block">
            <aside class="bd-aside text-muted align-self-start mb-3 mb-xl-4 px-2">
               <nav class="small" id="toc">
                  <ul class="list-unstyled">
                   
                     <li class="my-2">
                    
                        <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#forms-collapse" aria-controls="forms-collapse"><i class="fas fa-chevron-right"></i> Latest Editorial</button>
                        <ul class="btn d-inline-flex align-items-center collapsed border-0" id="forms-collapse">
                           <div class="card ccLesdCss ReportCard">
                              <?php $__currentLoopData = $editorial; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datadaily): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <div class="row g-0">
                                 <div class="col-md-4">
                                    <a class="ressprc" href="<?php echo e(route('editorial-details', $datadaily->slug)); ?>"><img class="bd-placeholder-img" width="100%" height="70" alt="<?php echo e($datadaily->title); ?>" src="<?php echo e(asset('/')); ?>/<?php echo e($datadaily->article_img); ?>" /></a>
                                 </div>
                                 <div class="col-md-8">
                                    <div class="card-body cardlefts  fsrCdsrd">
                                       <h6 class="ttesCdss tesvCss">TEAM VEYE | <i class="fa fa-calendar iitFfas" aria-hidden="true"></i> <?php echo e(date('d-M-Y',strtotime($datadaily->created_at))); ?></h6>
                                       <a class="ressprc" href="<?php echo e(route('editorial-details', $datadaily->slug)); ?>"><p class="card-ttFOnts"><?php echo e(strlen($datadaily->title) > 50 ? substr($datadaily->title,0,50).'..' : $datadaily->title); ?></p></a>
                                       
                                    </div>
                                 </div>
                              </div>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </div>
                        </ul>
                     </li>
                  
                  </ul>
               </nav>
            </aside>
            <div class="AboutUsBg">
               <div class="AboutUsBgCLR">
                  <div class="SubscribeTo">
                     <h5 class="subssTc ssssGtxt">Subscribe to our Special Report </h5>
                     <div class="PriceTag">From $200/6 Months</div>
                     <img src="<?php echo e(asset('assets/images/subcribe.png')); ?>" />
                     <a href="<?php echo e(route('subscriptionMenu')); ?>"><button class="sampleReportBtn SubscribeBtn">Subscribe</button></a>
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/editorial-details.blade.php ENDPATH**/ ?>