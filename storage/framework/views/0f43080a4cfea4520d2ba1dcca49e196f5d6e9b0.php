<?php $__env->startSection('title'); ?> Edit Report <?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script>
    function makeSlug(_this) {
        let slug = (_this.value).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
        $('#slug').val(slug);
    }
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-content">
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Edit Report
            <small>Edit Report</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="<?php echo e(route('reports.index')); ?>">Reports</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><span>Edit</span></li>
            </ul>
            <div class="page-toolbar">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('allreports.update',$datas->id)); ?>">
                    <input type="hidden" name="rid" value="<?php echo @$datas->report_id; ?>">
                    <?php echo csrf_field(); ?>
                    <?php echo e(method_field('PATCH')); ?>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Report Title</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="title" type="text" onchange="makeSlug(this)" placeholder="enter title here" value="<?php echo e($datas->title); ?>">
                            <?php if($errors->has('title')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('title')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Slug</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="slug" type="text" id="slug" placeholder="enter slug here" value="<?php echo e($datas->slug); ?>">
                        <?php if($errors->has('slug')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('slug')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Report No.</label>
                    </div>
                    <div class="col-lg-8">
                        <input class="form-control" name="report_no" type="text" id="report_no" placeholder="enter report no here" value="<?php echo e($datas->report_no); ?>">
                        <?php if($errors->has('report_no')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('report_no')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">ASX Code</label>
                        </div>
                        <div class="col-lg-8">
                        <?php $reportsId = explode(",", $datas->asx_code); ?>
                        <select  name="asx_code[]" id="asx_code" multiple class="form-control selectpicker" data-live-search="true">
                            <option value="">Choose One</option>
                            <?php $__currentLoopData = $asxCodes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           
                                    <option value="<?php echo e($data->asx_code); ?>" <?php foreach ($reportsId as $v) { ?> <?php if ($data->asx_code == $v) { ?> selected <?php } ?><?php } ?>><?php echo e($data->asx_code); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                            <?php if($errors->has('asx_code')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('asx_code')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Report Type</label>
                        </div>
                        <div class="col-lg-8">
                        <select class="form-control" required="" name="report_id" id="report_id" style="text-align:right;">
                            <option value="">Choose One</option>
                            <?php $__currentLoopData = $reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($data->id); ?>" <?php if($data->id==$datas->report_id): ?> selected <?php endif; ?>><?php echo e($data->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                            <?php if($errors->has('report_id')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('report_id')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Report Category</label>
                        </div>
                        <div class="col-lg-8">
                        <select class="form-control" required="" name="reportcategory" id="reportcategory" style="text-align:right;">
                            <option value="">Choose One</option>
                            <?php $__currentLoopData = $reportsCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($data->id); ?>" <?php if($data->id==$datas->reportcategory): ?> selected <?php endif; ?>><?php echo e($data->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                            <?php if($errors->has('reportcategory')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('reportcategory')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Sector</label>
                        </div>
                        <div class="col-lg-8">
                        <select class="form-control" name="sector" id="reportcategory">
                            <option value="">Choose One</option>
                            <?php $__currentLoopData = $sector; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($data->id); ?>" <?php if($data->id==$datas->sector): ?> selected <?php endif; ?>><?php echo e($data->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                            <?php if($errors->has('sector')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('sector')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Upload Report </label>
                        </div>
                        <div class="col-lg-2">
                            <img src="<?php echo e(asset($datas->attachment)); ?>" alt="" style="width: 90px;">
                        </div>
                        <div class="col-lg-6">
                            <input class="form-control" name="attachment" type="file" style="width: 100px;">
                            <?php if($errors->has('attachment')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('attachment')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Report Thumbnail</label>
                        </div>
                        <div class="col-lg-2">
                            <img src="<?php echo e(asset($datas->thumbnail)); ?>" alt="" style="width: 90px;">
                        </div>
                        <div class="col-lg-6">
                            <input class="form-control" name="thumbnail" type="file">
                            <?php if($errors->has('thumbnail')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('thumbnail')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Schedule Date</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="schedule" type="date" placeholder="enter date here" value="<?php echo e(date('Y-m-d', strtotime($datas->schedule))); ?>">
                            <?php if($errors->has('schedule')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('schedule')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Time</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="report_time" type="time" placeholder="enter time here" value="<?php echo e($datas->report_time); ?>">
                            <?php if($errors->has('report_time')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('report_time')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Recommendation</label>
                        </div>
                        <div class="col-lg-8">
                        <select class="form-control" name="recommendation" id="recommendation" style="text-align:right;">
                            <option value="">Choose One</option>
                            <?php $__currentLoopData = $recommendationData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($data->id); ?>" <?php if($data->id==$datas->recommendation): ?> selected <?php endif; ?>><?php echo e($data->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                            <?php if($errors->has('recommendation')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('recommendation')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Current Price</label>
                        </div>
                        <div class="col-lg-8">
                            <input class="form-control" name="current_price" type="text" placeholder="Enter Current Price" value="<?php echo e($datas->current_price); ?>">
                            <?php if($errors->has('current_price')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('current_price')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label class="col-form-label">Report Content</label>
                        </div>
                        <div class="col-lg-8">
                            <textarea name="reporthtml" id="description" class="form-control"><?php echo $datas->reporthtml; ?></textarea>
                            <?php if($errors->has('reporthtml')): ?>
                                <span class="help-block">
                                <strong><?php echo e($errors->first('reporthtml')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                 		
                    <div class="form-group row">
                        <div class="col-lg-8 offset-lg-3">
                            <button type="submit" class="btn btn-primary mr-2">Update</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<script>
    $('select').selectpicker();
    CKEDITOR.replace( 'description' );
</script>
    <script>
      CKEDITOR.replace( 'description' );
    </script> 
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/all-reports/edit.blade.php ENDPATH**/ ?>