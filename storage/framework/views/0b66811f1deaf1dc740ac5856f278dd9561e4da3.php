<?php $__env->startSection('title'); ?>  Technical Analysis <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?>  <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?>  <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('nofollow'); ?> <META NAME="robots" CONTENT="noindex,nofollow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au/<?php echo e($_SERVER['REQUEST_URI']); ?> <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php

use Illuminate\Support\Facades\DB;
?>
<section class="ReportPG specitops">
    <div class="container">


        <style>
            .custm_con h4 {
                font-size: 32px;
                font-weight: 600;
            }

            .GoBack {
                color: #fff;
                display: block;
                text-align: left;
                background: #18ba96;
                width: 103px;
                padding: 7px;
                font-weight: 800;
                box-shadow: 1px 1px #999;
            }

            .custm_con h5 {
                font-size: 23px;
                margin-top: 13px;
                font-weight: 800;
                text-decoration: underline;
                margin-bottom: 20px;
            }

            .custm_con p {
                font-size: 15px;
                line-height: initial;
                margin-top: 14px;
                margin-bottom: 9px;
                text-align: justify;
                line-height: 28px;
            }

            .specitops {
                margin-top: 110px;
            }

            .padbox {
                box-shadow: 0 1px 6px rgb(61 65 84 / 15%) !important;
                padding: 20px !important;
                border-radius: 5px !important;
            }

            .h4csdsf {
                text-decoration: none !important;
                padding-bottom: 4px;
                max-width: 287px;
                border-bottom: 4px solid #18ba96;

            }

            .custm-h2 {
                font-weight: 500 !important;
                color: #000 !important;
                font-size: 18px !important;
                text-decoration: underline !important;
            }

            .SubscribeTo h5 {
                line-height: 35px;
            }
        </style>
        <?php
        $resistance= db::table('resistance')->first();
        ?>
        <div class="bd-example">
            <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <div class="container custm_con padbox">
                    <h5 class="h4csdsf">Technical Analysis Defined:</h5>
                    <!-- <div class=" ReportCard mainLine"> </div> -->
                    <p class="resTCss"> <?php echo $resistance->details; ?></p>
                </div>
            </div>
            <br />
            <div class="col-md-4 d-none d-sm-block d-sm-none d-md-block">
                <!--  -->
                <div class="AboutUsBg">
                    <div class="AboutUsBgCLR">
                        <div class="SubscribeTo">
                            <h5>Subscribe to our Special Report </h5>
                            <div class="PriceTag">From $200/6 Months</div>
                            <img src="<?php echo e(asset('assets/images/subcribe.png')); ?>" />
                            <button class="sampleReportBtn SubscribeBtn">Subscribe</button>
                        </div>
                    </div>
                </div>

                <!--  -->
                <br>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/more_report_details.blade.php ENDPATH**/ ?>