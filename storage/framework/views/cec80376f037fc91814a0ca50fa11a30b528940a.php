<style type="text/css">
   .dropdown-toggle::after { 
    font-size: 14px !important; 
}
</style>

<?php

use App\Models\FooterMenu;
use App\Models\Reports;
use App\Models\LogoEdit;
?>

<div class="offcanvas offcanvas-start MyManu text-bg-light" tabindex="-1" id="offcanvasNavbar2" aria-labelledby="offcanvasNavbar2Label">
   <div class="offcanvas-header">
      <a class="navbar-brand navbar-brand-logo" href="/">
         <img src="https://veye.com.au/assets/img/veye-logo.svg" />
      </a>
      <button type="button" class="btn-close btn-close-black" data-bs-dismiss="offcanvas" aria-label="Close"></button>
   </div>
   <div class="offcanvas-body justify-content-end" style="padding:0px 63px;">
      <ul class="navbar-nav navPading">
         <li class="nav-item">
            <a href="<?php echo e(url('/')); ?>" class="nav-link">
               Home
            </a>
         </li> 

         <?php $__currentLoopData = Help::frontend_menues(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo e($menu->title); ?> </a>
            <?php if($menu->childs): ?>
            <ul class="dropdown-menu tisMneus" aria-labelledby="navbarDropdownMenuLink">
               <?php $__currentLoopData = $menu->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child_menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <li><a class="dropdown-item" href="<?php echo e(route('manage.menues',[$menu->slug,$child_menu->slug])); ?>"><?php echo e($child_menu->title); ?></a></li>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <?php endif; ?>
         </li>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



         <li class="nav-item">
            <a href="<?php echo e(url('/subscribe')); ?>" class="nav-link">
               Subscribe
            </a>
         </li>

         <li class="nav-item">
            <a href="<?php echo e(url('/contact-us')); ?>" class="nav-link">
               Contact
            </a>
         </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More </a>
            <ul class="dropdown-menu tisMneus" aria-labelledby="navbarDropdownMenuLink">

               <li class="nav-item">
                  <a href="<?php echo e(url('/editorial')); ?>" class="nav-link">
                     Editorial
                  </a>
               </li>
               <li class="nav-item">
                  <a href="<?php echo e(url('/webinarfrontend')); ?>" class="nav-link">
                     Webinar
                  </a>
               </li>
               <li class="nav-item">
                  <a href="<?php echo e(url('pages/about-us')); ?>" class="nav-link">
                  About Us
                  </a>
               </li>
                <?php $footerMenuData=FooterMenu::where('type',3)->where('deleted_at',NULL)->orderBy('order','ASC')->get(); ?>
                     <?php $__currentLoopData = $footerMenuData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <li class="nav-item"><a href="<?php echo e(url('pages')); ?>/<?php echo e($reportdata->slug); ?>" class="nav-link"><?php echo e($reportdata->title); ?></a></li>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              
            </ul>
         </li>
      </ul>
      <div class="RightBoxIN-Menu " >

         <div class="cutomr__reviw">
            <p class="cust__tsxt">Customer Reviews</p>
            <p>
               <img class="star__bos" src="<?php echo e(asset('frontend/assets/images/star__full.jpg')); ?>">
            </p>
            <p class="fous__sey">
               <span class="bbo">4.6</span> from <a href="http://www.productreview.com.au/listings/veye"> 72 reviews </a>
            </p>
            <p class="spans__rewviw">
               <a href="/">
                  <img class="sffes__flgs" src="<?php echo e(asset('frontend/assets/images/pr__image.jpg')); ?>">
               </a>
            </p>
         </div>

         <div class="loagsMyTs d-xxl-none d-xl-none d-xxl-block d-lg-none d-xl-block">
         <a class="btn GetStarted" href="<?php echo e(url('client-login')); ?>">Login Here</a>
         </div>

      </div>
   </div>
</div><?php /**PATH D:\development82\htdocs\veyework\resources\views/frontend/includes/nav.blade.php ENDPATH**/ ?>