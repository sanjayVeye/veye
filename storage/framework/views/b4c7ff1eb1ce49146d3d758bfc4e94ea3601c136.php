<?php $__env->startSection('title'); ?> My Invoice <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php

use Illuminate\Support\Facades\DB;
use App\Models\InvoiceOrder;
?>
<?php $clentData =db::table('clients')->where('email',Auth::user()->email)->first(); ?>

<div class="container-fluid">
   <div class="row">

      <?php echo $__env->make('frontend.dashboard.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
         <div class="DashboardRight">
            <div class="DashboardRightHead">
               <h2 class="DashboardHeading">Hello <?php echo e(@$clentData->first_name); ?> <?php echo e(@$clentData->last_name); ?> </h2>
               <!-- <button class="DasSubscBTN">Subscription: Regular </button> -->
               <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i>
               </button>
            </div>
            <h3 class="DashboardSubHeading">My Invoice</h3>
         </div>
         <div class="DashboardRightDown">
            <section class="PastRecommend02">
               <div class="recomdedTable ">
                  <table class="table table-striped past__border">
                     <thead>
                        <tr>
                           <th scope="col">Invoice Number</th>
                           <th scope="col">Invoice Date</th>
                           <th scope="col">Total Amount (*Inclusive GST)</th>
                           <!--<th scope="col"> Type</th>-->
                        </tr>
                     </thead>
                     <tbody class="table-group-divider">
                        <?php $i=0; ?>
                        <?php $__currentLoopData = $invoiceDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     
                        <tr>
                           <th scope="row"> <span class="bold__tablefonts"><?php echo e($v->invoice_no); ?></span> </th>
                           <td><?php echo e($v->created_at); ?></td>
                           <td><?php echo e($v->amount); ?></td>
                           <!--<td class="NumGreen"><?php echo e($v->product_type); ?>

                          
                           </td>-->
                        </tr>
                      
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
               </div>
               <div class="bd-example-snippet bd-code-snippet float-end">
                  <div class="bd-example">
                     <nav aria-label="Another pagination example">
                        <ul class="pagination pagination-lg flex-wrap">
                           <li class="page-item disabled">
                             
                           </li>

                        </ul>
                     </nav>
                  </div>
               </div>
            </section>
         </div>
      </main>
   </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/dashboard/myInvoice.blade.php ENDPATH**/ ?>