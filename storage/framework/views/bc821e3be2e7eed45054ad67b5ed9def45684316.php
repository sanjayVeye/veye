<?php $__env->startSection('title'); ?> Manage Clients <?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
<link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-search--dropdown .select2-search__field {
        width: 98%;
    }
    .dataTables_wrapper .dt-buttons {
    float: left !important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTableExample').dataTable({
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: [0, 1] // Column index which needs to export
                    }
                },
                {
                    extend: 'csv',
                },
                {
                    extend: 'excel',
                }
            ],
            order: [[3, 'desc']],
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            // "pagingType": "full_numbers",
            "iDisplayLength": 10,
            "language": {
                search: ""
            },
            "serverSide": true,
            "processing": true,
            "paging": true,
            "ajax": {
                url: '<?php echo e(route("client-credentials.index")); ?>',
                data: function(e) {

                }
            },
            'createdRow': function(row, data, dataIndex) {
                $(row).attr('id', 'tr_' + data.id);
            },
            columns: [{
                    data: "sno",
                    name: 'sno',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "client_name",
                    name: "client_name",
                    searchable: false,
                    sortable: true,
                    visible: true
                },
                {
                    data: "first_name",
                    name: "first_name",
                    visible: false
                },
                {
                    data: "last_name",
                    name: "last_name",
                    visible: false
                },
                {
                    data: "email",
                    name: "email"
                },
                {
                    data: "created_at",
                    name: "created_at"
                },
                {
                    data: "updated_at",
                    name: "updated_at"
                },
                {
                    data: "access",
                    name: "access",
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "action",
                    name: 'action',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "add_more",
                    name: 'add_more',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
            ],
            "fnInitComplete": function(oSettings, json) {

            }
        });

    });
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Clients
        <small>Manage Clients</small>
    </h1> 
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li><span>Clients</span></li>
        </ul>
        <div class="page-toolbar">
            <a class="btn btn-primary" href="<?php echo e(route('client-credentials.create')); ?>">Add Clients</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="table-responsive">
                <table id="dataTableExample" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Client Name</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>User Name</th>
                            <th>Created On</th>
                            <th>Updated On</th>
                            <th>Access</th>
                            <th>Action</th>
                            <th>Add More</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal Service -->
<div id="service" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:100%!important">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Client Information</h4>
            </div>
            <!-- <form action="javascript:void(0)" method="post" id="service_form">
                <?php echo csrf_field(); ?>
                <?php echo e(method_field('PATCH')); ?> -->
                <div class="modal-body">
                    <input type="hidden" id="client_id">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12 col-sm-12">
                            <div class="table-responsive">
                                <table id="client_infos" class="table table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Subscription Start</th>
                                            <th>Subscritpion End</th>
                                            <th>Access</th>
                                            <th>Comment</th>
                                            <th>Update</th>
                                        </tr>
                                    </thead>
                                    <tbody id="client_info">
                                                                          
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <!-- <button type="submit" class="btn btn-default">Submit</button> -->
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
        $(document).on('click', '.update_comment', function(){ 
            // alert('sd');
            var rowid = $(this).data("id");
            var comment = $("#comment"+rowid).val();
            $.ajax({
                type:'post',
                url: '<?php echo e(route("clientsCredentials.updateComment")); ?>',
                data:{
                    "_token": "<?php echo e(csrf_token()); ?>",
                    'rowid':rowid,'comment':comment},
                success:function(res){
                    if(res == 1){
                    alert('Comment Updated');
                    }else{
                    alert('Sorry !! Something went wrong');
                    }
                }
            });
        });

</script>
<script type="text/javascript">
    $(document).on('click', '.report_access', function(){ 
    var id = $(this).data("id");
    var value = $(this).data("value");
    $.ajax({
          type:'post',
          url: '<?php echo e(route("clientsCredentials.reportAccess")); ?>',
          data:{
            "_token": "<?php echo e(csrf_token()); ?>",
            'id':id,'value':value},
          success:function(res){
            console.log(res);
                $('#client_info').html(res);
  
          }
      });
  });
</script> 
<script type="text/javascript">
    function infoModal(id) {
        
        $('#client_id').val(id)
        $('#info').modal('show');
        $('#info_table').DataTable().ajax.reload();
    }

    function clientInfoModal(id) {
        // $('#client_id').val(id);
        // alert(id);
        // var id = $('#service_form').find('#client_id').val();
        // var url = '<?php echo e(route("clients.type_update")); ?>';
       
        $('#service').modal('show');
        $.ajax({
            url: '<?php echo e(route("clientsCredentials.info")); ?>',
            method: "post",
            data: {
                "_token": "<?php echo e(csrf_token()); ?>",
                'id': id
            },
            success: function(data) {
                console.log(data);
                $('#client_info').html(data);

// $.each(data, function(index) {
//     var row = "<tr><td>" + data[index].product_name + "</td><td>" + data[index].subscription_start_date + "</td><td>" + data[index].subscription_end_date + "</td><td><button class='btn btn-xs btn-success report_access' id="+ data[index].subscription_start_date +" data-id="+ data[index].id +" data-value='0' style='padding:0px auto;font-size: 15px;margin:0px;'><i class='fa fa-toggle-on text-white' style='padding-top: 2.5px;'></i> <small>Grant</small></button></td><td><textarea class='form-control' name='comment' id='comment"+ data[index].id +"'>"+ data[index].comment +"</textarea></td><td><input type='submit' name='update_comment' class='btn btn-info update_comment' data-id='"+ data[index].id +"'></td></tr>";
//     $("table tbody").append(row);
//     // $("#product_name").append(data[index].product_name);
//             // alert(id);
//         });

                
            }
        });
        // $.get(url, {
        //     _client_id: id
        // }, function(res) {
        //     console.log(res);
        //     $('#type').val(res.client_type);
        // });

    }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/client_credentials/index.blade.php ENDPATH**/ ?>