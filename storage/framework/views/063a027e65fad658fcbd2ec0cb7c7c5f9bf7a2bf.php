<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
    <li class="sidebar-search-wrapper">
        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
        <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
        <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <!-- <div class="input-group">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <a href="javascript:;" class="btn submit">
                        <i class="icon-magnifier"></i>
                    </a>
                </span>
            </div> -->


    <style>
        .mstys__4{
            margin-left: 5px !important;
        }
        .mbss__4{
            margin-bottom: 1.5rem !important;
        }
        .d__flexss{
    display: flex !important; 
}
.position-relative {
    position: relative !important;
}
.rounded-circle {
    border-radius: 50% !important;
}
.bg__ssuccess {
    background-color: #198754 !important;
}
.pYys___1{
    padding: 7px !important;
}
.border__2{
    border-width: 5px !important;
}
.end__s0{
    /* margin-right: 15px !important; */
    margin-right: 0px !important;
    position: absolute;
    margin-top: -11px;
    margin-left: 19px;
}
.ms99tsC_cs3{
    margin-left: 1rem !important;
}
.stygVis{
    font-weight: 700;
    color: #191C24;
    font-size: 18px;
    /* margin: 0px; */
}
    </style>




            <div class="d__flexss align-items-center mstys__4 mbss__4">
         <div class="position-relative">
            <img class="rounded-circle" src="<?php echo e(asset('assets/layouts/layout/img/user.png')); ?>" alt="" 
            style="width: 40px; height: 40px;">
            <div class="bg__ssuccess rounded-circle border border__2 
            border-white position-absolute end__s0 bottom-0 pYys___1"></div>
         </div>
         <div class="ms99tsC_cs3">
            <h6 class="mb-0 stygVis">Veye Admin</h6>
            <!-- <span>User Admin</span> -->
         </div>
      </div>





        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>
    <?php $__currentLoopData = Nav::list(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <li class="nav-item start <?php echo e($key==0 ? 'active open' : ''); ?> ">
            <a href="<?php echo e(count($row['childs']) > 0 ? 'javascript:;' : url($row['path'])); ?>" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title"><?php echo e($row['name']); ?></span>
                <?php if($key==0): ?>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    <?php else: ?>
                    <span class="selected"></span>
                    <span class="arrow"></span>

                <?php endif; ?>
            </a>
            <ul class="sub-menu">
                <?php $__currentLoopData = $row['childs']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cRow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="nav-item start ">
                        <a href="<?php echo e(url($cRow['path'])); ?>" class="nav-link ">
                        <!-- <i class="fa fa-circle" aria-hidden="true"></i> -->
                        ⦿
                            <span class="title"><?php echo e($cRow['name']); ?></span>
                        </a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

 
        <li class="nav-item start <?php echo e($key==0 ? 'active open' : ''); ?> ">
            <a href="<?php echo e(count($row['childs']) > 0 ? 'javascript:;' : url($row['path'])); ?>" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Reports</span>
                <?php if($key==0): ?>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                <?php endif; ?>
            </a>
            <ul class="sub-menu">
                <?php
                use App\Models\Reports;
                 $reports = Reports::where('deleted_at',NULL)->get(); ?>
                <?php $__currentLoopData = $reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($data->id=='All Reports'): ?>
                    <li class="nav-item start ">
                        <a href="<?php echo e(url('admin/allreports')); ?>?rid=all" class="nav-link ">
                            <i class="<?php echo e($data->title); ?>"></i>
                            <span class="title"><?php echo e($data->title); ?></span>
                        </a>
                    </li>
                    <?php else: ?>
                    <li class="nav-item start ">
                        <a href="<?php echo e(url('admin/allreports')); ?>?rid=<?php echo e($data->id); ?>" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title"><?php echo e($data->title); ?></span>
                        </a>
                    </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </li>

        <li class="nav-item start <?php echo e($key==0 ? 'active open' : ''); ?> ">
            <a href="<?php echo e(count($row['childs']) > 0 ? 'javascript:;' : url($row['path'])); ?>" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Search Reports</span>
                <?php if($key==0): ?>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                <?php endif; ?>
            </a>
            <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="<?php echo e(url('admin/searchReports')); ?>" class="nav-link ">
                            <i class=""></i>
                            <span class="title">Search All Reports</span>
                        </a>
                    </li>
            </ul>
        </li>
</ul><?php /**PATH /var/www/mainsite/resources/views/layouts/nav.blade.php ENDPATH**/ ?>