<?php
use App\Models\pages;
?>
<?php $pagesdata = pages::where('deleted_at',NULL)->where('tag','privacy-policies')->first(); ?>


<?php $__env->startSection('title'); ?> Privacy Policy | Veye Pty Ltd <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Learn how Veye Pty Ltd protects your privacy. Read our Privacy Policy to understand how we handle your personal information and ensure data security. <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?>  <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/pages/privacy-policy <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-6">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Privacy Policy</h1>
               <div class="bd-example">
                  <nav aria-label="breadcrumb ">
                    <!-- <ol class="breadcrumb m-0">
                        <li><a href="">Home / </a></li>
                        <li><a href="">Privacy Policy</a></li>
                     </ol>-->
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="PrivacyPolicy">
      <?php echo @$pagesdata->template; ?>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/privacypolicies.blade.php ENDPATH**/ ?>