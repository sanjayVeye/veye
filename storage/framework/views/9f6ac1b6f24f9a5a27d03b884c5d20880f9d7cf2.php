
<!-- Modal Service -->
<div id="info" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <input type="hidden" id="client_id">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
            <table id="info_table" class="table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Product Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Invoice Number</th>
                        <th>Total Amount Paid</th>
                        <th>Type</th> 
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<!-- Modal Service -->
<div id="service" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Service</h4>
        </div>
        <form action="javascript:void(0)" method="post" id="service_form">
            <?php echo csrf_field(); ?>
            <?php echo e(method_field('PATCH')); ?>

            <div class="modal-body">
                <input type="hidden" id="client_id">
                <div class="row form-group">
                    <div class="col-md-12">
                        Client Type
                        <select name="type" class="form-control" id="type">
                            <option value="">-Select-</option>
                            <option value="regular">Regular</option>
                            <option value="premium">Premium</option>
                        </select>
                    </div>
                </div>
            
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
</div>
<script type="text/javascript">
    function infoModal(id){
        $('#client_id').val(id)
        $('#info').modal('show');
        $('#info_table').DataTable().ajax.reload();
    }
    function serviceModal(id){
        $('#client_id').val(id);
        
        var id=$('#service_form').find('#client_id').val();
        var url='<?php echo e(route("clients.type_update")); ?>';
        $('#service').modal('show');
        $.get(url,{
            _client_id:id
        },function(res){
            console.log(res);
            $('#type').val(res.client_type);
        });

        
    }
</script><?php /**PATH /var/www/mainsite/resources/views/admin/clients/modal.blade.php ENDPATH**/ ?>