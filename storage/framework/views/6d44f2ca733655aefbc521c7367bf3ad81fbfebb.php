

<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title'); ?> Australian Securities Exchange (ASX) - Latest ASX News Today <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Stay updated with the latest ASX news today. Get insights, analysis, and market updates on stocks, investments, and financial trends. <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?> Australian Securities Exchange (ASX) - Latest ASX News Today <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/editorial <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



<style type="text/css">
        h1, a{
  margin: 0;
  padding: 0;
  text-decoration: none;
}

.section{
  padding: 14rem 2rem;
}

.section .error{
  font-size: 162px;
    font-weight: 800;
    color: #4EAF8A;
}

 

.back-home{
  display: inline-block;
    color: #fff;
    background: #4EAF8A;
    text-transform: uppercase;
    font-weight: 400;
    padding: 7px 10px;
    transition: all 0.2s linear;
    border-radius: 100px;
}
.back-home:hover{
  background: #222;
  color: #ddd;
}
.pageh3s{
    font-size: 30px !important;
    font-weight: 400;
    margin-bottom: 12px;
}
.opps4gs{
        margin-bottom: 28px;
    font-size: 22px;
}
    </style>



    <div class="section">
  <center>
    <h1 class="error">404</h1>
  <div class="page">
    <h3 class="pageh3s">OPPS! PAGE NOT FOUNT</h3>
    <p class="opps4gs">Sorry, the page you are looking for doesn't exist.</p>
  </div>
  <a class="back-home" href="">RETURN TO HOME PAGE</a>
  </center>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/404.blade.php ENDPATH**/ ?>