<script defer src="<?php echo e(asset('frontend/assets/fontawesome-free-5.15.4-web/js/brands.js')); ?>"></script>
<script defer src="<?php echo e(asset('frontend/assets/fontawesome-free-5.15.4-web/js/solid.js')); ?>"></script>
<script defer src="<?php echo e(asset('frontend/assets/fontawesome-free-5.15.4-web/js/fontawesome.js')); ?>"></script>
<script src="<?php echo e(asset('frontend/assets/js/bootstrap.bundle.min.js')); ?>"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js'></script>
<script src="<?php echo e(asset('frontend/assets/js/custom.js')); ?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js"></script>



<script>
    $(function() {
        $("#datepicker").datepicker();
        var a = $("#datepicker").datepicker("getDate");
      
    });
</script>
<script type="text/javascript">
    $(document).on('click', '.remove-cart', function(e) {
        // alert('test');
        e.preventDefault();
        var cart_id = $(this).data('cart_id');
        var quantity = $('.productQuantity').val();
        if (quantity == undefined) {
            quantity = 1;
        }
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": "<?php echo e(csrf_token()); ?>",
            },
            method: "GET",
            url: "<?php echo e(route('removeCart')); ?>",
            data: {
                cart_id: cart_id,
                quantity: quantity
            },
            success: function(data) {
                $('#cart').html(data);
                //alert(data);
                // if(data == null) {
                // //      notify('cartdata', response.cartdata);
                // //      $('#cart').html(response.cartdata);
                //     alert('success');
                // //     getCartCount();
                // }else{
                //     notify('error', response.error);
                // }
            }
        });
    })
</script>

<script type="text/javascript">
    $(document).on('click', '.add-to-cart', function(e) {
        // alert('test');
        e.preventDefault();
        var product_id = $(this).data('product_id');
        var quantity = $('.productQuantity').val();
        if (quantity == undefined) {
            quantity = 1;
        }
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": "<?php echo e(csrf_token()); ?>",
            },
            method: "POST",
            url: "<?php echo e(route('addtocart')); ?>",
            data: {
                product_id: product_id,
                quantity: quantity
            },
            success: function(data) {
                $('#cart').html(data);
                //alert(data);
                // if(data == null) {
                // //      notify('cartdata', response.cartdata);
                // //      $('#cart').html(response.cartdata);
                //     alert('success');
                // //     getCartCount();
                // }else{
                //     notify('error', response.error);
                // }
            }
        });
    })
</script>
<?php if(@Auth::user()->user_type=='0'): ?>
<?php else: ?>
<script>
    $('body').bind('copy paste',function(e) {
    e.preventDefault(); return false; 
});

$(document).bind("contextmenu", function (e) {
        e.preventDefault();
    });
</script>
<?php endif; ?>


<?php echo $__env->yieldContent('footer_js'); ?><?php /**PATH D:\development82\htdocs\veyework\resources\views/frontend/includes/footer_assets.blade.php ENDPATH**/ ?>