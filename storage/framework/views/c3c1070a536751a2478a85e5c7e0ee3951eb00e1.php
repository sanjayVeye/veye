<?php $__env->startSection('title'); ?> Stock Advisory Services: Best Advisor in Stock Market <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Looking for expert guidance in the stock market? Veye provides the best stock advisory services. Get expert guidance from industry-leading advisors. <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au/stock-advisory <?php $__env->stopSection(); ?>
<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-12">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Stock Advisory</h1>
               <div class="bd-example">
                  <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
               </div>
               <p>Expert guidance and comprehensive stock reports offering valuable insights, analysis, and recommendations to help investors make informed decisions in the market.</p>
            </div>
         </div>
      </div>
      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-3">
         <?php $__currentLoopData = $resultdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div class="col">
            <div class="ReportCard StockAdvisory">
            <a href="<?php echo e(route('manage.menues',['stock-advisory',$item->slug])); ?>" class="ReadMoreRprtData"><img class="bd-placeholder-img" width="100%" height="180" src="<?php echo e(asset('uploads/Reports')); ?>/<?php echo e($item->image); ?>"/></a>
               <div class="card-body">
                  <h5 class="card-Heading stofda"><?php echo e($item->title); ?></h4>
                     <!-- <a href="<?php echo e(route('report_details', encrypt($item->id))); ?>" class="ReadMoreRprtData">Read More... </a> -->
                     
               </div>
            </div>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
   </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/stockadvisory.blade.php ENDPATH**/ ?>