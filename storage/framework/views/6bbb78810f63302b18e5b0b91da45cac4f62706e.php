<?php $__env->startSection('title'); ?> Add Product <?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
<link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-search--dropdown .select2-search__field {
        width: 98%;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>

    <script type="text/javascript">
		$(document).ready(function(){
			$('#dataTableExample').dataTable({
					"aLengthMenu": [
						[10, 30, 50, -1],
						[10, 30, 50, "All"]
					],
					"pagingType": "full_numbers",
					"iDisplayLength": 10,
					"language": {
						search: ""
					},
					"serverSide": true,
					"processing": true,
					"paging": true,
					"ajax":{
						url:'<?php echo e(route("client-credentials.addProduct",$user->id)); ?>',
						data:function(e){
                            
						} 
					},
					'createdRow': function( row, data, dataIndex ) {
						$(row).attr('id', 'tr_'+data.id);
					},
					columns: [
						{ data: "sno" ,name: 'sno',searchable: false, sortable : false, visible:true},
						{ data: "downloadpdf" ,name: 'downloadpdf',searchable: false, sortable : false, visible:true},
						{ data: "sendmail" ,name: 'sendmail',searchable: false, sortable : false, visible:true},
						{ data: "product_name" ,name:"product_name"},					
						{ data: "subscription_start_date" ,name:"subscription_start_date"},
						{ data: "subscription_end_date" ,name:"subscription_end_date"},
						{ data: "invoice_no" ,name:"invoice_no"},
                        { data: "amount" ,name:"amount"},
                        { data: "product_type_text" ,name:"product_type_text"},
                        { data: "comment" ,name:"comment"},
                        { data: "reports" ,name:"reports",searchable: false, sortable : false, visible:true},
                        { data: "invoice" ,name:"invoice",searchable: false, sortable : false, visible:true},
						{ data: "action" ,name: 'action',searchable: false, sortable : false, visible:true},
					],
                    "fnInitComplete": function (oSettings, json) {

                    }
				});
                callOthers();
		});
    </script>
    <script>
        function callOthers(details_id=0){
            $('.panel').html('Loading....');
            $.get('<?php echo e(route("client-credentials.addMoreOther")); ?>',{
                client_details_id:details_id,
                client_id:'<?php echo e($user->id); ?>'
            },function(response){
                $('.panel').html(response);
            });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Product
        <small>Add Product</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo e(route('clients.index')); ?>">Client</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="table-responsive">
                <table id="dataTableExample" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Download PDF</th>
                            <th>Send PDF</th>
                            <th>Product Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Invoice Number</th>
                            <th>Total Amount Paid</th>
                            <th>Type</th> 
                            <th>Comment</th> 
                            <th>Reports</th> 
                            <th>Add Invoice</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="panel"></div>
        </div>
    </div>
    <?php echo $__env->make('admin.client_credentials.others', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<script>
function confirmation(){
    var result = confirm("Are you sure to delete?");
    if(result){
        // Delete logic goes here
    }
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/client_credentials/addMoreProducts.blade.php ENDPATH**/ ?>