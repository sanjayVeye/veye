
<?php $__env->startSection('title'); ?> Thank you <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('nofollow'); ?> <META NAME="robots" CONTENT="noindex,nofollow"> <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<style type="text/css">
  		h1, a{
  margin: 0;
  padding: 0;
  text-decoration: none;
}

.section{
  padding: 14rem 2rem;
}

.section .error{
  font-size: 162px;
    font-weight: 800;
    color: #4EAF8A;
}



.back-home{
  display: inline-block;
    color: #fff;
    background: #505050;
    text-transform: uppercase;
    font-weight: 400;
    padding: 7px 10px;
    transition: all 0.2s linear;
    border-radius: 100px;
    margin-top: 9px;
}
.back-home:hover{
  background: #222;
  color: #ddd;
}
.pageh3s{
	    font-size: 25px !important;
    font-weight: 400;
    margin-bottom: 24px;
}
.opps4gs{
	    margin-bottom: 28px;
    font-size: 22px;
}
.socialTocs {
    margin-top: 10px;
}
.Scial8Icns{
    max-width: 45px;
    margin: 0px 4px;
}
.DonecSs{
  max-width: 55px;
    margin-bottom: 20px;
}
.floowus{
  font-size: 22px;
    margin-top: 18px;
    font-weight: 600;
    margin-bottom: 19px;
}
 @media (max-width: 767px) {
    .borderone{
           position: absolute !important;
    width: 100% !important;
    margin-top: 19px !important;
    left: 0 !important;
    right: 0 !important;
}
}
.borderone{
border: 2px solid #bbbbbb;
    padding-top: 11px;
    position: absolute;
    left: 571px;
    right: 0;
    margin-top: 19px;
    width: 430px;
    border-left: none;
    border-right: none;
    overflow: hidden;
    z-index: -2;
}
  	</style>



  	<div class="section">
  <center> 
  <div class="page">
  	<h3 class="pageh3s">Thank You. Your submission has been received!</h3>
  	<p class="opps4gs">We will be in touch and contact you soon!</p>
    <div class="borderone"></div>
    <img class="DonecSs" src="https://cdn-icons-png.flaticon.com/512/2143/2143150.png">


  </div>
  <a class="back-home" href="/">RETURN TO HOME PAGE</a>
  <p class="floowus">Follow Us</p>
  <div class="socialTocs">
          <a href="https://www.facebook.com/VeyePtyLtd/"> <img class="Scial8Icns" src="https://cdn-icons-png.flaticon.com/512/3665/3665167.png"></a>
          <a href="https://www.instagram.com/veye.pty.ltd/"> <img class="Scial8Icns" src="   https://cdn-icons-png.flaticon.com/512/4103/4103007.png"></a>
          <a href="https://twitter.com/VeyePtyLtd"> <img class="Scial8Icns" src="https://cdn-icons-png.flaticon.com/512/1312/1312142.png"></a>
          <a href="https://www.linkedin.com/company/13629374/admin/"> <img class="Scial8Icns" src="https://cdn-icons-png.flaticon.com/512/3665/3665214.png"></a>    
  </div>

  </center>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/thankyou.blade.php ENDPATH**/ ?>