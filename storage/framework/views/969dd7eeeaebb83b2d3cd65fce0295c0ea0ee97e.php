<?php $__env->startSection('title'); ?> Past Recommendations | Veye Pty Ltd <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Access our past recommendations for insightful investment decisions. Explore Veye Pty Ltd's track record through our comprehensive Past Recommendations section. <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?>  <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/pages/past-recommendations <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php
use App\Models\DividendReports;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js">
</script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js">
</script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js">
</script>
<script>
   $(document).ready(function () {
    $('#myTables').DataTable({
        order: [[5, 'desc']],
    });
});
</script>
<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>Past Recommendations</h1>
         <div class="bd-example">
                  <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
               </div>
         <!-- <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="">Home / </a></li>
                  <li><a href="">Past Recommendations</a></li>
               </ol>
            </nav>
         </div> -->
      </div>
   </div>
</section>

<section class="PastRecommend">
   <div class="container">
      <ul>
         <li>Presented below is our research report for BUY and Sell recommendations. The report is for all closed
            recommendations. A closed recommendation is where a BUY recommendation was given on a particular
            stock followed by a subsequent SELL recommendation. The stock, hence, is no longer being held in the
            portfolio.
         </li>
         <li>Separate returns have been given for instances where a stock has been given multiple buying
            recommendations. Averaging has not been done so as to avoid discrepancy arising due to holding time
            factor.
         </li>
         <li>Companies which have undergone a merger / takeover are duly noted and returns are not shown if the
            newly formed entity remained within the portfolio at that point.
         </li>
         <li>The transaction fees and charges are excluded in the calculation.Gains and losses are hypothetical and are
            calculated using the initial buy price and include dividends. Calculations are based on prices and dividends
            for one security in each company for each recommendation.
         </li>
         <li>All Investors are advised to conduct their own independent research into individual stocks before making a
            purchase decision. In addition, investors are advised that past stock performance is no guarantee of future
            price appreciation.</li>
      </ul>
   </div>
</section>
<style type="text/css">
   .recomdedTable table tr th, .recomdedTable table tr td { 
    font-weight: 700 !important;
}
 
</style>

<section class="PastRecommend02">
   <div class="container">
      <div class="recomdedTable table-responsive " style="padding: 20px 0px;">
         <!-- <div class="input-group float-end w-50 mb-3">
            <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i> Search</span>
         </div> -->

         <table id="myTables" class="card-body shadow table table-bordered" style="width: 100%;margin-top: 21px !important;">
            <thead>
               <tr>
                  <th>Company</th>
                  <th>Buy Price</th>
                  <th>Buy Date</th>
                  <th>Report No.</th>
                  <th>Sell Price</th>
                  <th>Sell Date</th>
                  <th>Sell Report No.</th>
                  <th>Gain/Loss</th>
               </tr>
            </thead>
            <tbody>
               <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
               <tr>
                  <th scope="row"> <span class="bold__tablefonts"><?php echo e($item->company); ?></span></th>
                  <td>$<?php echo e($item->buy_price); ?></td>
                  <td><?php echo e($item->buy_date); ?></td>
                
                 <td><a href="<?php echo e(route('report.read.more',@$item->slug1)); ?>"><?php echo e($item->report_no1); ?></a></td>
               
                  
                  <td>$<?php echo e($item->sell_price); ?></td>
                  <td><?php echo e($item->sell_date); ?></td>
                  
                  <td><a href="<?php echo e(route('report.read.more',@$item->slug2)); ?>"><?php echo e($item->report_no_2); ?></a></td>
                 
                  <?php if(0 < $item->gains_losses): ?>
                     <td class="NumGreen"><?php echo e($item->gains_losses); ?>% <i class="fas fa-caret-up"></i></td>
                     <?php else: ?>
                     <td class="NumRed"><?php echo e($item->gains_losses); ?>% <i class="fas fa-caret-down"></i></td>
                     <?php endif; ?>
               </tr>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </table>
      </div>
     

   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/pastrecommendations.blade.php ENDPATH**/ ?>