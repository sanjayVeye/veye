<?php $__env->startSection('title'); ?> <?php echo e($report->title); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> <?php echo e($report->meta_description); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?> <?php echo e($report->meta_keywords); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au<?php echo e($_SERVER['REQUEST_URI']); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php

use App\Models\Menues;
use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
?>
<style>
   .custm_con h4 {
      font-size: 24px;
      font-weight: 600;
   }

   .custm-h2 {
      font-weight: 600;
   }

   .PastRecommend ul li {
      margin-bottom: 10px !important;
      line-height: 22px !important;
   }

   .PastRecommend ul {
      border-bottom: none !important;
   }

   .custm_con h5 {
      font-size: 18px;
      margin-top: 13px;
      font-weight: 800;
      text-decoration: underline;
      margin-bottom: 20px;
   }

   /* .custm_con p {
   font-size: 14px; 
   text-align: justify !important;  
   text-align: initial; 
   line-height: 1.5;
   }*/
   .custm_con p {
      font-size: 14px;
     
      margin-top: 14px;
      margin-bottom: 9px;
      line-height: 1.5;
   }

   .custm_con disc {
      font-size: 14px !important;
      line-height: initial;
      margin-top: 14px;
      margin-bottom: 9px;
      text-align: justify;
      line-height: 28px;
   }

   .custm_con img {
      max-width: 100%;
   }

   .custm_con p strong {
      font-weight: bold !important;
   }

   .custm_con strong {
      font-weight: bold !important;
   }

   .custm_con p b {
      font-weight: bold !important;
   }

   .custm_con b {
      font-weight: bold !important;
   }

   .marg {
      margin: 10px 0px;
   }

   .ressprc {
      color: #18ba96;
   }

   .card-ttFOnts {
      margin-top: 0px !important;
   }

   .iitFfas {
      color: inherit !important;
      background: inherit !important;
      border: inherit !important;
      border-radius: 0px !important;
      padding: 7px 0px 0px !important;
      margin-right: -2px !important;
   }

   .tesvCss {
      font-size: 13px;
   }

   .cardlefts {
      padding-left: 10px;
   }

   .remborder {
      border: none !important;
      box-shadow: inherit !important;
   }

   .moreSpecials {
      margin: 4px 4px !important;
      border: 1px solid #ddd !important;
      border-radius: 10px !important;
      box-shadow: 0px 0px 4px 0px #ddd !important;
      padding: 8px !important;
   }

   .slick-list {
      /*margin: 13px !important;*/
   }

   .wites {
      background: #fff !important;
   }

   .mrrTospssss {
      margin-top: 42px !important;
   }

   .marg14 {
      margin-bottom: 14px !important;
   }

   

   .menuLirh {
      border-bottom: 1px solid #302929;
      padding: 3px 0px 6px !important;
      margin-bottom: 4px !important;
   }

   .smak__ys {
      font-size: 12px !important;
      font-weight: 800 !important;
   }

   .yyVfCCs {
      padding: 8px !important;
   }

   .imgobjecftt {
      max-width: 100% !important;
      max-height: 205px !important;
   }

   .uuiDfC {
      font-size: 11px !important;
      margin-top: 3px !important;
      margin-bottom: 2px !important;
      line-height: inherit !important;
   }

   .resxe {
      font-size: 12px !important;
   }

   .divStyl {
      padding: 0px !important;
      margin-bottom: 0px !important;
   }

   .margt5mis {
      padding: 0px !important;
	   margin-top: 10px;
   }

   .firser {
      margin-left: -4px;
      font-size: 14px !important;
   }

   .ppfisr {
      margin-left: -2px;
      font-size: 13px !important;
      margin-top: 6px !important;
      margin-bottom: 0px !important;
   }
    .floTysl {
     
      margin-top: -45px;
   }
</style>
<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-12">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
            </div>
         </div>
      </div>
      <?php
      $disclaimer= db::table('disclaimer')->first();
      $resistance= db::table('resistance')->first();
    
	  $daily = DividendReports::where('report_id',@$report->report_id)->whereDate('created_at','<',$report->created_at)->take(4)->orderBy('created_at','desc')->get();
	  
      $company= db::table('companies')->where('asx_code',$report->asx_code )->first();
      if($company)
      {$asxType= db::table('asx_type')->where('id',$company->asx_type_id )->first();}
      
      ?>
      <div class="row">
         <div class="col-md-12">
            <div class="container custm_con">
               <div class="row" style="clear:both">
                  <div class="col-md-2"></div>
                  <div class="col-md-12">
                     <div class="bd-example">
                        <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
                     </div>
                     <div class="bd-example marg">
                        <!--  <a class="GoBack" onclick="location.href='javascript:history.go(-1)'" style="margin-left: 98px;
                        position: absolute;"> 
                        Go back
                     </a> -->
                        <?php if(@Auth::user()->user_type=='0'): ?>
		   <!-- <span style="margin-right:10px">Report No:- <?php echo e($report->report_no); ?></span> -->
                        <button type="button" class="ownBtncs btn btn-lg" data-bs-toggle="modal" data-bs-target="#exampleModalCenteredScrollable" style="float: right;    margin-top: -38px;"><i class="fa fa-edit" style="font-size: 24px;"></i>Edit</button>
                        <!-- Start Popup -->
                        <div class="modal fade" id="exampleModalCenteredScrollable" tabindex="-1" aria-labelledby="exampleModalCenteredScrollableTitle" aria-hidden="true">
                           <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
                              <div class="modal-content">
                                 <div class="modal-body DevidenReport">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('update-report.update',$report->id)); ?>">
                                             <?php echo csrf_field(); ?>
                                             <?php echo e(method_field('PATCH')); ?>

                                             <div class="DevidenReportBoxFirst">
                                                <h4>Edit Description</h4>
                                                <div class="DevidenReportBox">
                                                   <textarea name="reporthtml" id="description" class="form-control"><?php echo $report->reporthtml; ?></textarea>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <div class="col-md-12">
                                                   <button type="submit" class="btn btn-primary mr-2">Update</button>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- End Popup -->
                        <?php endif; ?>
                     </div>
                     <div class="row" style="clear:both">
                        <?php if(@$child_row->title=='Opportunity Stock'): ?>
                        <div class="col-md-8">
                           <?php else: ?>
                           <div class="col-md-10 offset-1">
                              <?php endif; ?>
                              <section class="PastRecommend remborder margt5mis ">
                                 <div class="container custm_con menulsitShad ">
                                    <!-- style="float:right !important" -->
                                    <h4 style="font-size: 24px;color: #000;"><?php echo e($report->title); ?>    <div class="menuLirh"></div>   <span class="team-view-date pedding_green  floTysl" > Team Veye | <i class="fa fa-calendar"></i> <?php if($report->schedule): ?> <?php echo e(date('d-M-Y',strtotime($report->schedule))); ?> <?php else: ?> <?php echo e(date('d-M-Y',strtotime($report->created_at))); ?> <?php endif; ?>
                                          <span><?php echo e(@$company->code_type); ?> - <?php echo e(@$report->asx_code); ?></span></span><br>
                                     

                                   
                                    </h4>
                                    <div class=" ReportCard divStyl"> </div>
                                    <div class="row">
                                       <?php if(@$child_row->title=='Opportunity Stock'): ?>
                                       <?php else: ?>
                                       <h4 style="font-size: 24px;">
                                          <span class="team-view-date pedding_green bghatnahai" style="float:right;  margin-top: -41px;  font-size: 12px;">
                                             recommendation : <span class="buygrens">buy</span> &nbsp;
                                             |
                                             <!-- <i class="fas fa-calendar-alt"></i> -->
                                             <!--  <?php echo e(date('d-M-Y',strtotime($report->created_at))); ?> -->
                                             <span class="bgSeNone">Sector : <?php echo e(@$report->sectorName->title); ?></span>
                                          </span>
                                       </h4>
                                       <?php endif; ?>
                                       <?php if(@$child_row->title=='Opportunity Stock'): ?>
                                       <img class="img-fluid" src="<?php echo e(asset('/')); ?>/<?php echo e($report->thumbnail); ?>">
                                       <?php endif; ?>

                                       <!-- <img class="img-fluid" style='max-width: 10%;' src="<?php echo e(asset('uploads/reports')); ?>/<?php echo e($report->thumbnail); ?>" /> -->
                                       <!--  <img class="img-fluid" src="<?php echo e(asset('uploads/reports')); ?>/<?php echo e($report->thumbnail); ?>" /> -->
                                    </div>
                                    <?php echo html_entity_decode($report->reporthtml); ?>

                                    <!-- <div class=" mainLine"> </div> -->
                                    <h4 class="mrrTospssss" style="font-size: 18px;">Technical Analysis Defined : </h4>
                                    <div class=" mainLine"> </div>
                                    <?php echo html_entity_decode(strlen($resistance->details) > 50 ? substr($resistance->details,0,500).'..' : $resistance->details ); ?>

                                    <a href="<?php echo e(route('more_report_details')); ?>">Read more</a>
                                    <div class=" mainLine"> </div>
                                    <h4 class="marg14" style="font-size: 18px;">Disclaimer</h4>
                                    <?php echo html_entity_decode($disclaimer->details); ?>

                                    <section>
                                       <div class="col-md-12 " style="    margin: 36px 0px 0px 0px;">
                                          <h4 style="font-size: 22px;margin-top: 15px;">More Reports from Veye</h4>
                                          <div class=" hrLinesf"> </div>
                                          <div class="slider slider-navdaily">
                                             <?php $__currentLoopData = $daily; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datadaily): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <div>
                                                <div class="SlideBox moreSpecials rris">
                                            <img class="imgobjecftt" src="<?php echo e(asset('/')); ?>/<?php echo e($datadaily->thumbnail); ?>" />
                                                   <div class="SlideBoxCnt yyVfCCs wites">
                                                      <small class="smak__ys">TEAM VEYE | <?php if($datadaily->schedule): ?> <?php echo e(date('d-M-Y',strtotime($datadaily->schedule))); ?> <?php else: ?> <?php echo e(date('d-M-Y',strtotime($datadaily->created_at))); ?> <?php endif; ?></small>
                                                     <a href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>"><p class="carsoTxt uuiDfC"><?php echo e($datadaily->asx_code); ?>-<?php echo e($datadaily->title); ?>...</p></a>
                                                      <a class="resxe" href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>">Read More..</a>
                                                   </div>
                                                </div>
                                             </div>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </div>
                                       </div>
                                    </section>
                                 </div>
                              </section>
                           </div>
                           <?php if(@$child_row->title=='Opportunity Stock'): ?>
                           <div class="col-md-4" style="padding: 0px;">
                              <aside class="bd-aside text-muted align-self-start mb-3 mb-xl-4 px-2" style="    margin-top: 12px;">
                                 <nav class="small" id="toc">
                                    <ul class="list-unstyled">
                                       <li class="my-2">
                                          <?php
                                            $dateTime = date('Y-m-d H:i:s');
                                          $daily = DividendReports::where('report_id',$report->report_id)

                                           ->select('dividend_reports.*')
                                             ->where(function($q) use($dateTime){
                                             $q->where('dividend_reports.schedule','<=', $dateTime)
                                             ->orWhere('dividend_reports.schedule',NULL);
                                             })
                                          ->take(5)->orderBy('id','desc')->get();
                                          ?>
                                          <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#forms-collapse" aria-controls="forms-collapse"><i class="fas fa-chevron-right"></i> New Report</button>
                                          <ul class="btn d-inline-flex align-items-center collapsed border-0" id="forms-collapse">
                                             <div class="card ccLesdCss ReportCard">
                                                <?php $__currentLoopData = $daily; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datadaily): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="row g-0">
                                                   <div class="col-md-4">
                                                      <img class="bd-placeholder-img " width="100%" height="85" src="<?php echo e(asset('/')); ?>/<?php echo e($datadaily->thumbnail); ?>" />
                                                   </div>
                                                   <div class="col-md-8">
                                                      <div class="card-body cardlefts  fsrCdsrd">
                                                         <h6 class="ttesCdss tesvCss firser">TEAM VEYE | <i class="fa fa-calendar iitFfas" aria-hidden="true"></i> <?php if($datadaily->schedule): ?> <?php echo e(date('d-M-Y',strtotime($datadaily->schedule))); ?> <?php else: ?> <?php echo e(date('d-M-Y',strtotime($datadaily->created_at))); ?> <?php endif; ?></h6>
                                                         <a href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>"><p class="card-ttFOnts ppfisr"><?php echo e($datadaily->title); ?></p></a>
                                                         <a style="font-size: 12px !important;" class="ressprc" href="<?php echo e(route('report.read.more',$datadaily->slug)); ?>">Read More..</a>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                             </div>
                                          </ul>
                                       </li>
                                       <li class="my-2">
                                          <?php
                                          $menuesData = Menues::where('parent',1)->where('deleted_at',null)->get();
                                          ?>
                                          <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#contents-collapse" aria-controls="contents-collapse"><i class="fas fa-chevron-right"></i> More Report More Veye</button>
                                          <ul class="list-unstyled list-group respoUlCss collapse show" id="contents-collapse">
                                             <?php $__currentLoopData = $menuesData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <a class="list-group-item Code4eaf8a" href="<?php echo e(route('manage.menues',['stock-advisory',$data->slug])); ?>"><?php echo e($data->title); ?> </a>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </ul>
                                       </li>
                                       <li class="my-2">
                                          <?php
                                          $menuesData = Menues::where('parent',3)->where('deleted_at',null)->get();
                                          ?>
                                          <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#components-collapse" aria-controls="components-collapse"><i class="fas fa-chevron-right"></i> Sector Specific</button>
                                          <ul class="list-unstyled list-group respoUlCss collapse" id="components-collapse">
                                             <?php $__currentLoopData = $menuesData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <a class="list-group-item Code4eaf8a" href="<?php echo e(route('manage.menues',[$data->slug,$data->slug])); ?>"><?php echo e($data->title); ?></a>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </ul>
                                       </li>
                                    </ul>
                                 </nav>
                              </aside>
                           </div>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</section>
<script>
   CKEDITOR.replace('description');
</script>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/menues_details_daily.blade.php ENDPATH**/ ?>