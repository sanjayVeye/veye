<?php $__env->startSection('title'); ?> Dashboard <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php
use App\Models\DividendReports;
use Illuminate\Support\Facades\DB;
?>
<div class="container-fluid">
   <div class="row">

      <?php echo $__env->make('frontend.dashboard.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <main class="col-md-9 ms-sm-auto col-lg-9 px-md-4">
         <div class="DashboardRight">
            <div class="DashboardRightHead">
               <h2 class="DashboardHeading">Hello <?php echo e(@$clentData->first_name); ?> <?php echo e(@$clentData->last_name); ?> </h2>

             <!--  <button class="DasSubscBTN" style="background-color: #e90c2e;"> Your plan will be expired: &nbsp; <a class="text-dark" href="<?php echo e(url('subscribe')); ?>"> Buy Now</a> </button>-->
               
               <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i> 
               </button>
            </div>
            <h3 class="DashboardSubHeading">Reports</h3>

         </div>
         <div class="DashboardRightDown">
            <div class="row row-cols-2">
               <?php $__currentLoopData = $reportAccess; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php 
				$reportCount= db::table('dividend_reports')->where('report_id',$v->report_id)->count(); 
				$reportName= db::table('reports')->where('id',$v->report_id)->first(); 				
				?>
            <?php if($reportName->title == 'Free Reports'): ?>
            <?php else: ?>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="<?php echo e(asset('frontend/assets/images/Dash1.png')); ?>" />
                     <p><?php echo e($reportCount); ?></p>
                     <!-- <h4><?php echo e(@$v->report_name); ?></h4> -->
                     <a class="nav-link" href="<?php echo e(url('report/stock-advisory/')); ?>/<?php echo e(@$reportName->slug); ?>">
                        <button class="DasSubscBTN"><?php echo e(@$reportName->title); ?></button>
                     </a>
                  </div>
               </div>
               <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <!-- <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="<?php echo e(asset('frontend/assets/images/Dash1.png')); ?>" />
                     <p><?php echo e($dividentReports); ?></p>
                     <h4>Dividend Investor Reports</h4>
                     <a class="nav-link" href="<?php echo e(url('report/stock-advisory/divident-investor-report')); ?>">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="<?php echo e(asset('frontend/assets/images/Dash1.png')); ?>" />
                     <p><?php echo e($stockWeekReports); ?></p>
                     <h4>Stock of the week Reports</h4>
                     <a class="nav-link" href="<?php echo e(url('report/stock-advisory/stock-of-the-day-report')); ?>">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="<?php echo e(asset('frontend/assets/images/Dash1.png')); ?>" />
                     <p><?php echo e($masterReports); ?></p>
                     <h4>Masters of Growth Reports</h4>
                     <a class="nav-link" href="<?php echo e(url('report/stock-advisory/master-of-growth-report')); ?>">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="<?php echo e(asset('frontend/assets/images/Dash1.png')); ?>" />
                     <p><?php echo e($pennyStockReports); ?></p>
                     <h4>Penny Stock Reports</h4>
                     <a class="nav-link" href="<?php echo e(url('report/stock-advisory/penny-stocks-report')); ?>">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="<?php echo e(asset('frontend/assets/images/Dash1.png')); ?>" />
                     <p><?php echo e($hotReports); ?></p>
                     <h4>Hot Sector Reports</h4>
                     <a class="nav-link" href="<?php echo e(url('report/stock-advisory/hot-sector-report')); ?>">
                        <button class="DasSubscBTN">Subscription: Regular </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3">
                  <div class="DashReprtBox">
                     <img src="<?php echo e(asset('frontend/assets/images/Dash1.png')); ?>" />
                     <p><?php echo e($articles); ?></p>
                     <h4>Articles</h4>
                     <button class="DasSubscBTN">Subscription: Regular </button>
                  </div>
               </div> -->

            </div>
         </div>
      </main>
   </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/dashboard/home.blade.php ENDPATH**/ ?>