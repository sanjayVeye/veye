<?php $__env->startSection('title'); ?> Add Page <?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
<link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-search--dropdown .select2-search__field {
        width: 98%;
    }

    .table-scrollable {
        overflow-x: inherit !important;
        overflow-y: inherit !important;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTableExample').dataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            // "pagingType": "full_numbers",
            "iDisplayLength": 10,
            "language": {
                search: ""
            },
            "serverSide": true,
            "processing": true,
            "paging": true,
            "ajax": {
                url: '<?php echo e(route("add-page.index")); ?>',
                data: function(e) {

                }
            },
            'createdRow': function(row, data, dataIndex) {
                $(row).attr('id', 'tr_' + data.id);
            },
            columns: [{
                    data: "sno",
                    name: 'sno',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
                {
                    data: "page_name",
                    name: "page_name"
                },
                {
                    data: "meta_title",
                    name: "meta_title"
                },
                {
                    data: "meta_keywords",
                    name: "meta_keywords"
                },
                {
                    data: "meta_description",
                    name: "meta_description"
                },
                {
                    data: "template",
                    name: "template"
                },
                {
                    data: "action",
                    name: 'action',
                    searchable: false,
                    sortable: false,
                    visible: true
                },
            ],
            "fnInitComplete": function(oSettings, json) {

            }
        });



    });
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Manage Add Page
        <small>Manage Add Page</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Administrator</span><i class="fa fa-angle-right"></i></li>
            <li><span>Add Page</span></li>
        </ul>
        <div class="page-toolbar">
            <a class="btn btn-primary" href="<?php echo e(route('add-page.create')); ?>"><i class="fa fa-plus me-2"></i> Add Page</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">

            <table id="dataTableExample" class="table table-bordered table-striped table-responsive">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Page name</th>
                        <th>Meta Title</th>
                        <th>Meta Keywords</th>
                        <th>Meta Description</th>
                        <th>Template</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/add-page/index.blade.php ENDPATH**/ ?>