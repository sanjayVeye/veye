<?php $__env->startSection('title'); ?> Add Client <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content">
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Add Client
        <small>Add Client</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(route('home')); ?>">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Clients</span><i class="fa fa-angle-right"></i></li>
            <li>
                <a href="<?php echo e(route('client-credentials.index')); ?>">Client Credentials</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><span>Add</span></li>
        </ul>
        <div class="page-toolbar">

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <form class="forms-sample" method="post" enctype="multipart/form-data" action="<?php echo e(route('client-credentials.store')); ?>">
                <?php echo csrf_field(); ?>
                <div class="row">
                    <div class="col-md-4">
                        Select Client
                        <select class="form-control" name="client_id" id="client_id" required>
                            <option value="">-select-</option>
                            <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php
                            $sel=$row->id==old('client_id') ? 'selected' :'';
                            ?>
                            <option value="<?php echo e($row->id); ?>" <?php echo e($sel); ?>><?php echo e($row->client_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if($errors->has('client_id')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('client_id')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        User name
                        <input class="form-control" name="username" type="text" placeholder="User name" value="<?php echo e(old('username')); ?>" required>
                        <?php if($errors->has('username')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('username')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        Password
                        <input class="form-control" name="password" type="password" minlength="1" placeholder="Password" value="<?php echo e(old('password')); ?>" required>
                        <?php if($errors->has('password')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>

                <h4><b>Subscription Details</b></h4><br />

                <b>Subscription Plans</b><br /><br />

                <div class="row mb-3">
                    <?php $__currentLoopData = $subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-xs-12 col-md-4">
                        <label for="plan_latest_reports<?php echo e($index+1); ?>"><input type="checkbox" name="subscribe_id[]" value="<?php echo e($data->id); ?>" id="plan_latest_reports<?php echo e($index+1); ?>" class="1" readonly="" <?php if(@$data->title=='Daily Analysis'): ?> checked <?php endif; ?>> <?php echo e(@$data->title); ?></label>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

                <br /><br />

                <div class="row form-group">
                    <div class="col-md-4">
                        Product name
                        <input class="form-control" name="product_name" type="text" placeholder="Enter Product Name" value="<?php echo e(old('product_name')); ?>">
                        <?php if($errors->has('product_name')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('product_name')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        Subscription Start Date
                        <input class="form-control" name="start_date" type="date" value="<?php echo e(old('start_date')); ?>">
                        <?php if($errors->has('start_date')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('start_date')); ?></strong>
                        </span>
                        <?php endif; ?>

                    </div>
                    <div class="col-md-4">
                        Subscription End Date
                        <input class="form-control" name="end_date" type="date" value="<?php echo e(old('end_date')); ?>">
                        <?php if($errors->has('end_date')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('end_date')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        Invoice Number
                        <input class="form-control" name="invoice_no" type="text" placeholder="Enter Invoice Number" value="<?php echo e(old('invoice_no')); ?>">
                        <?php if($errors->has('invoice_no')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('invoice_no')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        Amount Paid
                        <input class="form-control" name="amount_paid" type="text" placeholder="Enter Amount Paid" value="<?php echo e(old('amount_paid')); ?>">
                        <?php if($errors->has('amount_paid')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('amount_paid')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4">
                        Product Type
                        <input class="form-control" name="product_type" type="text" placeholder="Enter Product Type" value="<?php echo e(old('product_type')); ?>">
                        <?php if($errors->has('product_type')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('product_type')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        Comment
                        <textarea name="comment" class="form-control" id="" cols="5" rows="5"></textarea>
                        <?php if($errors->has('comment')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('comment')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8 offset-lg-3">
                        <button type="submit" class="btn btn-primary mr-2">Add</button>
                        <button class="btn btn-light" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/admin/client_credentials/add.blade.php ENDPATH**/ ?>