<?php $__env->startSection('title'); ?> <?php echo e($pagesdata->meta_title); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> <?php echo e($pagesdata->meta_description); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?> <?php echo e($pagesdata->meta_keywords); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/pages/about-us <?php $__env->stopSection(); ?>

<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>About Us</h1>
         <div class="bd-example">
            <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"> <i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i>Go Back</a>
         </div>
        <!--  <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="index.html">Home / </a></li>
                  <li><a href="about.html">about</a></li>
               </ol>
            </nav>
         </div> -->
      </div>
   </div>
</section>

<style type="text/css">
   @media (max-width: 767px) {
    .g-4 {
        margin: 0px !important;
    }
    .AboutRight p, .AboutRight ul li, .PastRecommend ul li {
    line-height: 23px !important;
    margin-bottom: 21px !important;
}
.feature{
   text-align: justify !important;
}
 }
   p{
      font-weight: 300 !important;
   }
   .AboutRight p, .AboutRight ul li, .PastRecommend ul li{
      line-height: 23px !important;
   }
   .g-4{
          margin-top: 20px;
    margin-bottom: 30px;
   }
   .AboutLeft{
         padding-right: inherit !important;
   }
   .AboutRight{
            margin-top: 40px;
    font-size: 24px !important;
    font-weight: 600;
    }
    h3{
      font-weight: 700;
    }
</style>
<br>

<?php echo @$pagesdata->template; ?>


<section class="FAQ_Section" style=" margin-top: 70px;">
   <div class="container">
      <h4 class="FAQ_heading">Frequently Asked Questions </h4>
      <div class="row">
         <?php $__currentLoopData = $faqdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div class="col-md-6">
            <div class="customAccordian">
               <div class="accordion" id="accordionExample">
                  <div class="accordion-item">
                     <h4 class="accordion-header" id="heading-<?php echo e($key); ?>">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-<?php echo e($key); ?>" aria-expanded="false" aria-controls="collapse-<?php echo e($key); ?>">
                           <?php echo e($data->title); ?>

                        </button>
                     </h4>
                     <div id="collapse-<?php echo e($key); ?>" class="accordion-collapse collapse" aria-labelledby="heading-<?php echo e($key); ?>" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                           <?php echo e($data->details); ?>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
   </div>
</section>



<style type="text/css">
  .TestImaCss{
    position: absolute;
    top: 207px;
  }

  img{
    max-width: 100% !important;
  }
</style>

<section class="WhatSay tesy__mest SlideDotsCustome" style="margin-bottom: 63px;">
   <div class="container  ">
      <h4 style="text-align:center !important"> <span class="whats-css saaAllRepla">What Our Clients Say About Us? </span> <span class="head__tsxt"></span> </h4>
      <!-- <div class="bar_seprator"></div> -->
      <div class=" slider-nav3">
         <?php $__currentLoopData = $testmanag; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div>
            <div class="WhatSaySlide">
               <div class="CustomerData">
                  <h5><?php echo e($reportdata->title); ?></h5>
                  <div class="padding__tops"></div>
                  
                  <p class="applYPTgs"><?php echo e($reportdata->desciption); ?></p>
                  <img class="TestImaCss" src="<?php echo e(asset('uploads/Test_Management')); ?>/<?php echo e($reportdata->image); ?>" />
               </div>
            </div>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/about-us.blade.php ENDPATH**/ ?>