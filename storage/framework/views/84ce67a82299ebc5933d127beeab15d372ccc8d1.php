<?php
use App\Models\pages;
?>
<?php $pagesdata = pages::where('deleted_at',NULL)->where('tag','fsg')->first(); ?>


<?php $__env->startSection('title'); ?> <?php echo e($pagesdata->meta_title); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> <?php echo e($pagesdata->meta_description); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('keywords'); ?> <?php echo e($pagesdata->meta_keywords); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?>https://veye.com.au/pages/financial-services-guide <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<section class="ReportPG">
   <div class="container">
      <div class="row align-items-end mb-5">
         <div class="col-md-6">
            <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
               <h1>Financial Services Guide</h1>
               <div class="bd-example">
                  <nav aria-label="breadcrumb ">
                    <!-- <ol class="breadcrumb m-0">
                        <li><a href="">Home / </a></li>
                        <li><a href="">Financial Services Guide</a></li>
                     </ol>-->
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="PrivacyPolicy">
      <?php echo @$pagesdata->template; ?>

      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/financial_services_guide.blade.php ENDPATH**/ ?>