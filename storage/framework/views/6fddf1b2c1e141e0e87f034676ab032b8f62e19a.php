<?php $__env->startSection('title'); ?> Search Report <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('header'); ?>
<style type="text/css">
    .menusLinks {
        font-size: 12px !important;
        color: #4EAF8A !important;
    }

    .b65Ts {
        margin-bottom: 11px;
        border-radius: 0px;
    }

    .comPrfi {
        font-size: 25px;
        margin-bottom: 14px;
        font-weight: 700;
    }

    .pprfil {
        color: #000;
        text-align: justify;
        font-family: sans-serif;
        font-size: 16px;
        line-height: 26px;
    }

    .itsFcads {
        font-size: 13px !important;
        margin-bottom: 3px !important;
        line-height: 20px !important;
    }

    .lesRRtbo {
        padding: 2px 10px;
    }

    .related-alert {
        padding: 5px 10px;
        width: 100%;
        margin-bottom: 30px;
        background-color: rgba(45, 171, 95, 0.2);
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<section class="ReportPG">
    <div class="container">
        <div class="row align-items-end " style="margin-bottom: 22px;">
            <div class="col-md-10 offset-1">
                <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
                    <h1>Search Result For : <?php echo e(@$asxCode); ?></h1>
                    <?php if($divident_reports->total() > '8000'): ?>
                    <div class="related-alert">* (0) Report(s) Found...</div>
                    <?php else: ?>
                    <div class="related-alert">* (<?php echo e($divident_reports->total()); ?>) Report(s) Found...</div>
                    <?php endif; ?>
                    <div class="bd-example">
                    </div>
                </div>
            </div>

        </div>

        <div class="container" style="padding: 0px;">
            <div class="row">
                <div class="col-md-10 offset-1">
                    <div class="b65Ts">

                        <h3 class="comPrfi">Company's Profile</h3>
                        <p class="pprfil"><?php echo e(@$company->description); ?></p>

                    </div>
                </div>
            </div>
        </div>


        <div class="row" style="clear:both;margin-top: 15px;">

            <div class="col-md-6 offset-1">

                <?php $__empty_1 = true; $__currentLoopData = $divident_reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                <?php echo $__env->make('frontend.search_reports_details',['item'=>$item], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                <?php $__currentLoopData = Help::getDividendReports(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php echo $__env->make('frontend.search_reports_details',['item'=>$item], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php endif; ?>
                <div class="bd-example-snippet bd-code-snippet">
                    <div class="bd-example">
                        <nav aria-label="Another pagination example">
                            <ul class="pagination pagination-lg flex-wrap">
                                <li class="page-item disabled">
                                    <?php echo e($divident_reports->links( "pagination::bootstrap-4")); ?>

                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <aside class="bd-aside text-muted align-self-start mb-3 mb-xl-4 px-2">
                    <nav class="small" id="toc">
                        <ul class="list-unstyled">
                            <li class="my-2">
                                <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#contents-collapse" aria-controls="contents-collapse"><i class="fas fa-chevron-right"></i> More Report More Veye</button>
                                <ul class="list-unstyled list-group respoUlCss collapsed" id="contents-collapse">
                                    <?php $__currentLoopData = Help::menuesByParent(1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a class="list-group-item Code4eaf8a" href="<?php echo e(route('manage.menues',[$data->slug,$data->slug])); ?>"><?php echo e($data->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>

                            <li class="my-2">
                                <button class="btn d-inline-flex align-items-center collapsed border-0" data-bs-toggle="collapse" aria-expanded="false" data-bs-target="#components-collapse" aria-controls="components-collapse"><i class="fas fa-chevron-right"></i> Sector Specific</button>
                                <ul class="list-unstyled list-group respoUlCss collapse" id="components-collapse">
                                    <?php $__currentLoopData = Help::menuesByParent(3); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a class="list-group-item Code4eaf8a" href="<?php echo e(route('manage.menues',[$data->slug,$data->slug])); ?>"><?php echo e($data->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </aside>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/search_reports.blade.php ENDPATH**/ ?>