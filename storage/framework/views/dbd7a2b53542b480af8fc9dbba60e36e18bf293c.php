<?php $__env->startSection('content'); ?>
<?php

use App\Models\LogoEdit;
?>
<?php $logodata = LogoEdit::first(); ?>
<div class="container-xxl  position-relative bg-white d-flex p-0">
    <div class="container-fluid smalBgs" style=" background-image: url('<?php echo e(asset('assets/img/bullsBgs.jpg')); ?>');">
        <div class="row row h-100 align-items-center justify-content-center">
            <!-- <div class="col-12 col-sm-8 d-none d-sm-block col-md-6 col-lg-5 col-xl-6">
                  <div class="rounded p-4 p-sm-5 my-4 mx-3">
                     <img class="img-fluid" src="img/adminImage.gif">
                      
                  </div>
               </div> -->
            <style>
                img.imgLlgst {
                    width: 100%;
                }
            </style>
            <div class="marginCsTop col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                <div class="bg-light  rounded p-4 p-sm-5 my-4 mx-3">
                    <div class=" align-items-center justify-content-between mb-3">
                        <center>
                            <img class="imgLlgst" src="<?php echo e(asset('frontend/assets/images')); ?>/<?php echo e($logodata->header_logo); ?>">
                        </center>
                        <h3 class="text-primary text-center"> VEYE :: ADMIN PANEL </h3>
                    </div>
                    <form method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="form-floating mb-3 ">
                            <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>

                            <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <label for="floatingInput">Email address</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password">

                            <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <label for="floatingPassword">Password</label>
                        </div>
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>

                                <label class="form-check-label" for="remember">
                                    <?php echo e(__('Remember Me')); ?>

                                </label>

                            </div>
                            <?php if(Route::has('password.request')): ?>
                            <a href="<?php echo e(route('password.request')); ?>">
                                <?php echo e(__('Forgot Your Password?')); ?>

                            </a>
                            <?php endif; ?>
                        </div>
                        <button type="submit" class="btn btn-primary py-3 w-100 mb-4">
                            <?php echo e(__('Login')); ?>

                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/auth/login.blade.php ENDPATH**/ ?>