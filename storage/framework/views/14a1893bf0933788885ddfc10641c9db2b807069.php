<?php $__env->startSection('title'); ?> Client Login | Veye Pty Ltd <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Veye Client login: Gain access to Veye's client portal for comprehensive investment analysis, portfolio management, and real-time market data. Stay informed and make informed decisions. Sign in now! <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au/client-login <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <META NAME="robots" CONTENT="noindex,nofollow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php

use App\Models\LogoEdit;
?>
<?php $logodata = LogoEdit::first(); ?>
<section class="gradient-form">

   <div class="gradient-formBGCLR" style="background-image: url('https://veye.com.au/frontendmain/img/graph_chart.jpg');">
      <div class="container">
         <div class="row">
            <div class="col-lg-4 offset-lg-4">
               <div class="formBox newLogins">
                  <div class="formBoxHead">
                     <img src="<?php echo e(asset('frontend/assets/images')); ?>/<?php echo e($logodata->header_logo); ?>" />
                     <h4>Login into Member Account</h4>
                     <p class="useFP">Use your credentials to access your account.</p>
                     <!-- <p>Use your credentials to access your account</p> -->
                  </div>
                  <form method="post" enctype="multipart/form-data" action="<?php echo e(route('registration.login.match')); ?>">
                     <?php echo csrf_field(); ?>
                     <div class="form-floating">
                        <!-- mb-3 -->
                        <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Your Email">
                        <label for="floatingInput">Email</label>
                     </div>
                     <div class="form-floating">
                        <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
                        <label for="floatingPassword">Password</label>
                     </div>
                     <div class="asscptlogin">
                        <div class="acceptVeye">
                           <label>
                              <input type="checkbox" name="termCondition" value="1" required> <span>I Accept Veye <a href="<?php echo e(url('pages/terms-conditions')); ?>">Terms & Conditions</a></span>
                           </label>
                        </div>
                        <div class="acceptVeye">
                           <label>
                              <input type="checkbox" name="accept" value="1" /> <span>Remember Me On This Device</span>
                           </label>
                        </div>
                     </div>
                     <div class="d-flex justify-content-between w-100">
                        <?php if(session('fail')): ?>
                        <div class="alert alert-danger">
                           <?php echo e(session('fail')); ?>

                        </div>
                        <?php endif; ?>
                        <?php if(session('success')): ?>
                        <div class="alert alert-success">
                           <?php echo e(session('success')); ?>

                        </div>
                        <?php endif; ?>
                     </div>

                     <div class="text-center">
                        <button class="btn VeyeLogin regLogiHover" type="submit">Login</button>
                     </div>

                     <div class="ForgotPassBox d-flex align-items-start justify-content-center ">
                        <!-- pb-4 -->
                        <div>
                           <p>Not a member?</p>
                           <a href="<?php echo e(route('forgot.password')); ?>" class="ForgotPass srgPost">Forgot password?</a>
                        </div>
                        <a href="<?php echo e(url('registration')); ?>" class="btn VeyeRegister text-white" style="    margin-top: -20px !important;"> Register</a>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <style type="text/css">
      .ForgotPassBox p {
         margin: -18px 0 3px -40px !important;
      }

      .srgPost {
         margin-left: -59px;
         line-height: 40px;
      }
      .alert-success {
    --bs-alert-color: crimson;
    /* --bs-alert-bg: #d1e7dd; */
    /* --bs-alert-border-color: #badbcc; */
}
   </style>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/registration/login.blade.php ENDPATH**/ ?>