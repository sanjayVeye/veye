<?php $__env->startSection('title'); ?> Best Stocks & Sector: Top Sectors to Invest in Stock Market <?php $__env->stopSection(); ?>
<?php $__env->startSection('description'); ?> Discover top stock sectors & best stocks in each sector. Find top 10 stocks per sector, including top 5 sectors in the Indian market. Explore energy sector & maximize returns in Indian stock market. <?php $__env->stopSection(); ?>
<?php $__env->startSection('canonical'); ?> https://veye.com.au/sector-specific <?php $__env->stopSection(); ?>
<?php $__env->startSection('nofollow'); ?> <meta name="robots" content="index, follow"> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<section class="AboutMain">
   <div class="container">
      <div class="ReportPGHeadLft bd-example-snippet bd-code-snippet">
         <h1>Sector Specific</h1>
         <div class="bd-example">
               <a class="GoBack" onclick="location.href='javascript:history.go(-1)'"><i class="fa fa-arrow-circle-left text-gres" aria-hidden="true"></i> Go back</a>
           </div>
         <!-- <div class="bd-example">
            <nav aria-label="breadcrumb ">
               <ol class="breadcrumb m-0">
                  <li><a href="">Home / </a></li>
                  <li><a href="">sector specific</a></li>
               </ol>
            </nav>
         </div> -->
      </div>
   </div>
</section>
<section class="WebinarPG pasiu12Tops">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <p class="setctSi">Explore a comprehensive list of diverse sectors, featuring industries ranging from technology and healthcare to finance and energy, providing insights into the dynamic landscape of investment opportunities.</p>
         <?php $__currentLoopData = $masterDlt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="sectorSpecific">
               <div class="sectorSpecificHead">
                  <h2><?php echo e($sector->detail_1); ?></h2> 
               </div>
               <p><?php echo e($sector->description); ?></p>
               <div class="sectorSpecificBody">
                   <?php $__currentLoopData = $company; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <?php $arr = explode(',', $comp->report_type); ?>
                   <?php if($arr[0]==$sector->ID): ?>
                 <div class="sectorSpecificBtn">
                 <a class="seeALink" href="search/<?php echo e(str_replace(' ' ,'-',$comp->title)); ?>"> <?php echo e($comp->title); ?> </a> 
                  </div>
                  <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               </div>
            </div>

      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           
         </div>
      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/mainsite/resources/views/frontend/sectorspecific.blade.php ENDPATH**/ ?>