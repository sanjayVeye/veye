<?php



use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:sanctum'], function(){

    //
});
Route::get('client-lead', [ApiController::class, 'clientLead'])->name('clientlead.api');
Route::get('get-lead', [ApiController::class, 'getLead'])->name('getlead.api');
Route::get('add-lead', [ApiController::class, 'addlead'])->name('addlead.api');
