<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\frontend\ClientsRegistrationController;
use App\Http\Controllers\frontend\HomeController;
use App\Http\Controllers\frontend\FrontendController;
use App\Http\Controllers\frontend\RegisterNoController;
use App\Http\Controllers\frontend\ContactUsController;
use App\Http\Controllers\frontend\WatchListController;
use App\Http\Controllers\Payment\PaymentController;
use Illuminate\Support\Facades\Auth;

/* 
    Clients Registration
    Dated 06-02-2023
    */

Route::get('search-company', [HomeController::class, 'search_company'])->name('search.company');
Route::get('search-company-name', [ClientsRegistrationController::class, 'company_name'])->name('searchcom.company_name');
Route::resource('registration', ClientsRegistrationController::class)->except(['destroy']);

Route::get('client-login', [ClientsRegistrationController::class, 'login'])->name('registration.login');

Route::post('client-login-match', [ClientsRegistrationController::class, 'loginMatch'])->name('registration.login.match');

Route::get('dashboard', [ClientsRegistrationController::class, 'dashboard'])->name('dashboard.home');

Route::get('forgot-password', [ClientsRegistrationController::class, 'forgotPassword'])->name('forgot.password');

Route::post('send-password-reset-link', [ClientsRegistrationController::class, 'send_pass_reset_link'])->name('reset.send_pass_reset_link');

Route::post('reset-password', [ClientsRegistrationController::class, 'resetPassword'])->name('reset.password');

Route::get('password/reset/{id}', [ClientsRegistrationController::class, 'forgotPasswordSet'])->name('password.forgotPasswordSet');

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('404', [HomeController::class, 'pageMotFound'])->name('pagenotfound');

Route::get('search', [HomeController::class, 'search'])->name('search.report');
Route::get('search/{id}', [HomeController::class, 'sectorSearch'])->name('searchSector.report');
Route::get('sector-specific', [HomeController::class, 'sectorSpecific'])->name('sectorSpecific');
Route::get('subscribe', [HomeController::class, 'subscriptionMenu'])->name('subscriptionMenu');
//changed subscriptionMenu to subscribe
Route::get('editorial', [HomeController::class, 'editorial'])->name('editorial');
//changed subscriptionMenu to subscribe
Route::get('article/{slug}', [HomeController::class, 'article'])->name('article');
Route::get('articles', [HomeController::class, 'articles'])->name('articles');

Route::get('stock-advisory', [HomeController::class, 'stockadvisory'])->name('stockadvisory');

/// Register No ///
Route::resource('webinarfrontend-register', RegisterNoController::class);

Route::resource('contactus', ContactUsController::class);
Route::post('email_subscribe', [ContactUsController::class, 'emailSubscribe'])->name('email.subscribe');

Route::get('more-report-details', [HomeController::class, 'more_report_details'])->name('more_report_details');

Route::get('report_details/{id}', [HomeController::class, 'report_details'])->name('report_details');

Route::get('blog/{slug}', [HomeController::class, 'editorial_details'])->name('editorial-details');
Route::get('editorial-details/{slug}', [HomeController::class, 'editorial_details_blog'])->name('editorial-details_blog');

Route::get('contact-us', [HomeController::class, 'contact_us'])->name('contact_us');

Route::get('checkout', [HomeController::class, 'checkout'])->name('checkout');
Route::get('addtocart', [HomeController::class, 'addToCart'])->name('addtocart');
Route::get('removeCart', [HomeController::class, 'removeCart'])->name('removeCart');
Route::get('offer/{slug}', [HomeController::class, 'offer'])->name('offer');
Route::get('thankyou', [HomeController::class, 'thankyou'])->name('thankyou');

Route::get('editorialdetails', [HomeController::class, 'editorialdetails'])->name('editorialdetails');

//Webinar / frontend
Route::get('webinarfrontend', [HomeController::class, 'webinarfrontend'])->name('webinarfrontend');

Route::resource('update-report', FrontendController::class)->except(['destroy']);
Route::get('report/{parent}/{child?}', [FrontendController::class, 'manage_menues'])->name('manage.menues');
Route::get('pages/{parent}', [FrontendController::class, 'manage_footer'])->name('manage.footer');
Route::get('report-read-more', [FrontendController::class, 'report_read_more_nf'])->name('report.read.more.nf');
Route::get('report-read-more/{parent?}', [FrontendController::class, 'report_read_more'])->name('report.read.more');
// Auth::routes();
// Auth::routes();

// Route::post('/payment', [PaymentController::class, 'payment']);
Route::post('/payment', [PaymentController::class, 'payment'])->name('stripe.payment');
Route::post('/payment-details', [PaymentController::class, 'storeYourPaymentDetails'])->name('store.storeYourPaymentDetails');
Route::post('/store-payment-details', [PaymentController::class, 'store_payment_details'])->name('store.storePaymentDetails');

Route::post('notify', [PaymentController::class, 'notifyPayment'])->name('notify');
Route::get('cancel', [PaymentController::class, 'cancelPayment'])->name('cancel');
Route::post('return/{id}', [PaymentController::class, 'returnPayment'])->name('return');
Route::post('paypal-info', [PaymentController::class, 'paypalInfo'])->name('paypal.paypalInfo');

Route::post('your-payment-status', [PaymentController::class, 'yourPaymentStatus'])->name('yourPaymentStatus');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('addwatchlist', WatchListController::class)->except(['destroy']);
    Route::get('my-watchlist', [ClientsRegistrationController::class, 'watchlist'])->name('my.watchlist');
    Route::post('create-watch-list', [WatchListController::class, 'passwordupdate'])->name('createWatchlist.store');

    Route::get('latest-buy', [ClientsRegistrationController::class, 'latestbuy'])->name('latest.buy');
    Route::get('latest-sell', [ClientsRegistrationController::class, 'latestsell'])->name('latest.sell');
    Route::get('my-email', [ClientsRegistrationController::class, 'updateEmail'])->name('my.email');
    Route::get('my-password', [ClientsRegistrationController::class, 'updatePass'])->name('my.password');
    Route::get('my-subscription', [ClientsRegistrationController::class, 'mySubscription'])->name('my.subscription');
    Route::get('my-invoice', [ClientsRegistrationController::class, 'myInvoice'])->name('my.invoice');
    Route::get('subscription-pdfs/{id}', [ClientsRegistrationController::class, 'subscriptionGeneratePDFS'])->name('my.subscriptionGeneratePDFS');

    Route::post('email-update/{id}', [ClientsRegistrationController::class, 'emailupdate'])->name('useremail.emailupdate');
    Route::post('password-update/{id}', [ClientsRegistrationController::class, 'passwordupdate'])->name('userpassword.passwordupdate');

    Route::get('user-report/{parent}/{child?}', [ClientsRegistrationController::class, 'userReports'])->name('user.reports');

    // Route::get('registration/destroy/{id}',[ClientsRegistrationController::class,'destroy'])->name('registration.destroy');

    // Route::get('privacypolicies', [FrontendController::class, 'Privacypolicies'])->name('Privacypolicies');
    // Route::get('about-us',[FrontendController::class,'about_us'])->name('about-us');


    // Route::get('terms-condition',[FrontendController::class,'terms-condition'])->name('terms-condition');
    // Route::get('about-us',[FrontendController::class,'about-us'])->name('about-us');

});
