$(document).ready(function(){
    $('ul.nav li').click(function(){
        //alert('clicked');
        $(".nav"). find("li.active").removeClass('active');
        $(this).addClass('active');
    });
    
    $('.cl-white .phone .phone').on('keyup', function(){
        console.log('phone triggered.');
        var phone = $('.cl-white .phone .phone').val();
        if ($.isNumeric(phone)) {
            if (phone.length < 10) {
                $('.cl-white .alert-error-phone').show(1000);
                $('.cl-white .alert-error-phone').html('<p style="margin-left: 20px; color: #b00000;">* '+ (10-phone.length) +' More Numbers Required.</p>');
            } 
            if(phone.length == 10) {
                $('.cl-white .alert-error-phone').html('<p style="margin-left: 20px; color: #2AEA2D;">*  Valid Phone Number.</p>');
                $('.cl-white .alert-error-phone').hide(1500);
            }

            if (phone.length > 10) {
                $('.cl-white .alert-error-phone').show(1000);
               $('.alert-error-phone').html('<p style="margin-left: 20px; color: #b00000;">* Phone number is not valid.</p>'); 
            }
        } else {
            $('.cl-white .alert-error-phone').show(1000);
            $('.alert-error-phone').html('<p style="margin-left: 20px; color: #b00000;">* Please check your entered phone number..<br>* Phone number must be a 10 digit number.</p>');
        }
    });
    

    $(".cl-white .email .email").keyup(function(){
        var email = $(".cl-white .email .email").val();
        if(isValidEmailAddress(email)){
            $('.alert-error-email').html('<p style="margin-left: 20px; color: #2AEA2D;">* Valid Email.</p>');
            $('.alert-error-email').hide(1500);
        } else {
            $('.alert-error-email').show(1000);
            $('.alert-error-email').html('<p style="margin-left: 20px; color: #b00000;">* Please enter a valid email.</p>');
        }
    });

        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }
    

    $('.dashboardPanels').find('div.panel').hide();
    // $('.dashboardNav li').on('click',function(){
    // 	alert('id');
    // 	var id = $(this).attr('id');
    // 	$('div.dashboardPanels').find('div#'.id.'').show();
    // });
    
    $('a.embed').gdocsViewer({ width: 800, height: 1000 });
    


    var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });



    $(function(){

        $('.carousel-control').click(function(e){
            e.preventDefault();
            $('#myCarousel').carousel( $(this).data() );
        });

    });//END document.ready

    $('.nav .dropdown').hover(function(){
        $(this).addClass("open");
    },function(){
        $(this).removeClass("open");
    });

    $('.form-control-feedback').click(function(){
        $('.search-form .form-group').css({'width': '100%', 'border-radius': '4px 4px 4px 4px'});
        $('div.form-control-feedback').css({'margin': '0px'});
        $('.search-form .form-group input.form-control').css({'padding': '10px 25px 0 25px', 'margin-right': '200px'});
        $('.loginRemoveDiv span').addClass('glyphicon glyphicon-remove glyphicon-lg');
        $('.loginRemoveDiv').addClass('form-control-remove');
        $(this).find('span').removeClass('glyphicon glyphicon-search glyphicon-lg');
    });

    $('.loginRemoveDiv').click(function(){
        $('div.form-control-feedback').css({'margin': ''});
        $('div.form-control-feedback span').addClass('glyphicon glyphicon-search glyphicon-lg');
        $('.search-form .form-group input.form-control').css({'padding': '', 'margin-right': ''});
        $(this).removeClass('form-control-remove');
        $(this).find('span').removeClass('glyphicon glyphicon-remove glyphicon-lg');
        $('.search-form .form-group').css({'width': '', 'border-radius': ''});
        $('.search-form .form-group').css({'float': 'right !important', 'transition': 'all 0.5s, border-radius 0s', 'width': '45px', 'height': '45px', 'background-color': '#fff', 'box-shadow': '0 1px 1px rgba(0, 0, 0, 0.075) inset', 'border-radius': '25px', 'border': '1px solid #ccc', 'margin-bottom': '-12px', 'margin-top': '-10px'});
        $('.search-form .form-group input.form-control').val('');
    });
    


    $(function () {
        $('#myList a:first').tab('show');
        $('#myList a').on('click', function (e) {
          e.preventDefault()
          $('#myList a').removeClass('active');
          $(this).addClass('active');
          $(this).tab('show');
        });
  });

$("button.client_dtl_submit").click(function(){
    var request = 'clientDetailsUpdation';
    var fname = $('#client_dtl_fname').val();
    var lname = $('#client_dtl_lname').val();
    var phone = $('#client_dtl_phone').val();
    var post_code = $('#client_dtl_post_code').val();
    var Cid = $('#client_dtl_id').val();
        $.ajax({
        url : 'ajax.php',
        type:'post',
        data:{'r': request , 'i':Cid, 'fname':fname, 'lname':lname, 'phone':phone, 'post_code':post_code},
        dataType:"json",
        success: function(data){
            
            if(data.toString().indexOf('empty')!=-1) {
                $('.msg').removeClass('alert alert-success');
                $('.msg').addClass('alert alert-danger');
                $('.msg').html('<strong>ERROR!!</strong> Please fill required fields!!..');
            
			}else if(data.toString().indexOf('false')!=-1) {
                $('.msg').removeClass('alert alert-success');
                $('.msg').addClass('alert alert-danger');
                $('.msg').html('<strong>ERROR!!</strong> Error in updating your records.<br>Please check your details carefully and try again!!..');
            }else {
                $('.msg').removeClass('alert alert-danger');
                $('.msg').addClass('alert alert-success');
                $('.msg').html('Your Record Saved Successfully!!');
            }
        },
        error: function(data){
            $('#client_dtl_msg').html("<span class='alert alert-danger' style='margin-left: -20px;'>* Ajax Error.</span>.");
        }
    });
    return false;
});

$("#client_email_submit").click(function(){
    var request = "clientEmailUpdation";
    var oldEmail = $("#client_old_email").val();
    var newEmail = $("#client_new_email").val();
    var cnfNewEmail = $("#client_cnf_new_email").val();
    var id = $('#client_email_id').val();
        $.ajax({
        url : 'ajax.php',
        type:'post',
        data:{'r': request , 'i':id, 'oldEmail':oldEmail, 'newEmail':newEmail, 'cnfNewEmail':cnfNewEmail},
        dataType:"json",
        success: function(data){
           if (data.toString().indexOf('true')!=-1) {
                $('div#client_email_msg').html("<span class='alert alert-success' style='margin-left: -20px;'>* Record Saved Successfully.</span>.");
            }
            if(data.toString().indexOf('empty')!=-1) {
                $('div#client_email_msg').html("<span class='alert alert-danger' style='margin-left: -20px;'>* Please fill required fields..</span>.");
            } 
            if(data.toString().indexOf('false')!=-1) {
                $('div#client_email_msg').html("<span class='alert alert-danger' style='margin-left: -20px;'>* Error in updating records..</span>.");
            }
            if(data.toString().indexOf('oldEmailError')!=-1 ) {
                $('div#client_email_msg').html("<span class='alert alert-danger' style='margin-left: -20px;'>* Error! Old email did not matched in our records..</span>.");
            } 
            if(data.toString().indexOf('cnfNewEmailError')!=-1) {
                $('div#client_email_msg').html("<span class='alert alert-danger' style='margin-left: -20px;'>* New email and Confirm New Password did not matched..</span>.");
            }
            if (data.toString().indexOf('oldNewMatchedError')!=-1) {
                $('div#client_email_msg').html("<span class='alert alert-danger' style='margin-left: -20px;'>* Please try some new email..</span>.");
            }
        },
        error: function(data){
            $('#client_email_msg').html("<span class='alert alert-danger' style='margin-left: -20px;'>* Ajax Error.</span>.");
        }
    });
    return false;
});
});


// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});