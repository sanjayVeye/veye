<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    protected $fillable = ['title', 'menu_id', 'status', 'created_at', 'updated_at','deleted_at'];

    public function menuName(){
		return $this->hasOne(Menus::class,'id','menu_id');
	}

}
