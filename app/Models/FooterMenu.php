<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FooterMenu extends Model
{
    protected $table = 'cms_footer';

    protected $fillable = [
        'title', 
        'parent', 
        'order', 
        'slug', 
        'created_at', 
        'updated_at',
        'deleted_at'];
}
