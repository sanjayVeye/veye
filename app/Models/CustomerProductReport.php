<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerProductReport extends Model
{
     protected $table = 'customer_product_report';
      protected $fillable = [
        'lead_id', 
        'customer_id', 
        'productreport_id', 
        'start_date',
        'end_date',
        'created_at', 
        'updated_at', 
        ];
}
