<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportCategories extends Model
{
    protected $fillable = ['title','slug','image','desciption','meta_title','meta_keywords','meta_description', 'created_at', 'updated_at','deleted_at'];
}
