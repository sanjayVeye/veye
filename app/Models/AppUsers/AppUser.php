<?php

namespace App\Models\AppUsers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppUser extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id','address','kyc_details','is_verified','status','image'
    ];
}
