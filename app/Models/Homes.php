<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Homes extends Model
{
    protected $table = 'homes';

    protected $fillable = [
        
        'title', 
        'catagories', 
        'metatitle', 
        'metadescription',
        'metakeywords',
        'deleted_at',
        'created_at', 
        'updated_at',];
    }
    