<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    protected $table = 'payment-type';

    protected $fillable = [
        'title', 
        'created_at', 
        'updated_at',
        'deleted_at'];
}
