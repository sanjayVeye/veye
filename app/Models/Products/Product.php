<?php

namespace App\Models\Products;

use App\Models\Master\Unit;
use App\Models\Products\ProductQr;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $appends = ['attr','image_path'];
    public function qrcodes(){
        return $this->hasMany(ProductQr::class,'product_id','id');
    }

    public function getAttrAttribute(){
        $list=$this->attribute;
        if($list){
            $arr=[];
            $json = json_decode($list, true);
            foreach($json as $val){
                $unit=Unit::find($val['unit']);
                array_push($arr,array('title'=>$val['title'],'value'=>$val['value'],'unit'=>$unit->title,'id'=>$unit->id));
            }
            return json_encode($arr);
        }

    }
    public function getImagePathAttribute(){
        return url('uploads/products/images/'.$this->image);
    }
}
