<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductQr extends Model
{
    use HasFactory;
    protected $table='product_qrs';
    protected $fillable=[
        'product_id','unique_code','points','remarks','applied_by'
    ];

}
