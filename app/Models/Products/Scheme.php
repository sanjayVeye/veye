<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{
    use HasFactory;
    protected $table='schemes';
    protected $appends = ['scheme_image'];
    public function qrcodes(){
        return $this->hasMany(SchemeQr::class,'scheme_id','id');
    }
    public function getSchemeImageAttribute(){
        if(!$this->id)
            return '';
        return route('scheme-image',$this->id);
    }

    public function p1(){
        return $this->hasOne(Product::class,'id','buying_product');
    }
    public function p2(){
        return $this->hasOne(Product::class,'id','offer_product');
    }
}
