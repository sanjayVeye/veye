<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchemeQr extends Model
{
    use HasFactory;
    protected $fillable=[
        'scheme_id','unique_code','created_by'
    ];
}
