<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WatchLists extends Model
{
    protected $fillable = ['user_id', 'company', 'short_code', 'buy_qty', 'buy_pr', 'buy_date', 'total_value_of_invest', 'curr_mkt_pr', 'total_mkt_invest', 'amount', 'created_at', 'updated_at','deleted_at'];
}
