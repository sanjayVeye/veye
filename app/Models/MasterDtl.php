<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterDtl extends Model
{
    protected $table = 'master_dtl';

    protected $fillable = [
        'ID', 
        'formid',
        'detail_1', 
        'deleted_at',
        'created_at',
        'updated_at'];
}
