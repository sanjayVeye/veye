<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';

    protected $fillable = [
        
        'title', 
        'details', 
        'metatitle', 
        'metadescription',
        'metakeywords',
        'deleted_at',
        'created_at', 
        'updated_at',];
    }
    
  