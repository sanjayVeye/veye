<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DividendReports extends Model
{
    protected $fillable = ['report_id','report_no','slug', 'title', 'description', 'thumbnail', 'attachment', 'reporthtml', 'asx_code', 'reportcategory', 'sector', 'investment_type', 'bulletpoint', 'recommendation', 'current_price', 'pdf', 'schedule', 'report_time', 'created_at', 'updated_at','deleted_at'];
    
    public function companyName(){
        return $this->hasOne(Companies::class,'asx_code','asx_code');
    }

    public function sectorName(){
        return $this->hasOne(Sectors::class,'id','sector');
    }

    public function scopeAsxCodeFilter($query,$asx_codes){
        $query->where(function($q) use($asx_codes){
            foreach($asx_codes as $asx){
                $q->orWhere('asx_code','like',"%$asx%");
            }
        });
        return $query;
    }
    
}

