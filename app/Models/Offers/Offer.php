<?php

namespace App\Models\Offers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Offer extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $appends = ['image_path'];
    public function details(){
        return $this->hasMany(OfferDetail::class,'offer_id','id');
    }

    public function qrcodes(){
        return $this->hasMany(OfferQr::class,'offer_id','id');
    }
    public function getImagePathAttribute(){
        return route('offers.api.show',$this->id).'?rand='.time();
    }
}
