<?php

namespace App\Models\Offers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferQr extends Model
{
    use HasFactory;
    protected $fillable=[
        'offer_id','unique_code','points','remarks','created_by'
    ];
}
