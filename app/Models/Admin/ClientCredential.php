<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientCredential extends Model
{
    use HasFactory;
    protected $fillable=[
        'username','password','client_id','subscription_start_date','subscription_end_date',
    ];
}
