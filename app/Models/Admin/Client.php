<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory,SoftDeletes;
    protected $appends=['all_plan','enable_plan','disable_plan'];
    public function getAllPlanAttribute(){
        return $this->detailsPlan()->count();
    }
    public function getEnablePlanAttribute(){
        return $this->detailsEnablePlan()->count();
    }
    public function getDisablePlanAttribute(){
        return $this->detailsDisablePlan()->count();
    }
    public function details(){
        return $this->hasMany(ClientDetails::class,'client_id','id');
    }
    public function detailsPlan(){
        return $this->hasMany(ClientDetailsPlan::class,'client_id','id');
    }
    public function detailsEnablePlan(){
        return $this->hasMany(ClientDetailsPlan::class,'client_id','id')->where('access','=', 1);
    }
    public function detailsDisablePlan(){
        return $this->hasMany(ClientDetailsPlan::class,'client_id','id')->where('access','=', 0);
    }
}
