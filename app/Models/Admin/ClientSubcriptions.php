<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientSubcriptions extends Model
{
	protected $table = 'client_subcriptions';
    protected $fillable = ['id','first_name','last_name','email','phone','page_refrance','ip_address','lat_lng','day_time_phone','post_code','color_code','user_agent','http_connection','comment','note','colorstatus','readed','subscribe','status','deleted_at','created_at','updated_at'];

}
 