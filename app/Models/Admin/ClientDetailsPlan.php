<?php

namespace App\Models\Admin;

use App\Models\Reports;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientDetailsPlan extends Model
{
    use HasFactory;
    protected $fillable=[
        'client_id','report_id','client_details_id','subscription_start_date','subscription_end_date','access','comment','report_name','report_key','report_id'
    ];
    public function report(){
        return $this->hasOne(Reports::class,'id','report_id');
    }
    public function client(){
        return $this->hasOne(ClientSubcriptions::class,'id','client_id');
    }
    public function invoice_no(){
        return $this->hasOne(ClientDetails::class,'id','client_details_id');
    }
}
