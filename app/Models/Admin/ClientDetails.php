<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\InvoiceOrder;

class ClientDetails extends Model
{
    use HasFactory;
    protected $fillable=[
        'client_id','product_name','subscription_start_date','subscription_end_date','invoice_no','amount','comment','product_type_text','report_access'
    ];

    public function plans(){
        return $this->hasMany(ClientDetailsPlan::class,'client_details_id','id');
    }
 
    public function client(){
        return $this->hasOne(Client::class,'id','client_id');
    }
    public function invoice(){
        return $this->hasOne(InvoiceOrder::class,'client_details_id','id');
    }
}