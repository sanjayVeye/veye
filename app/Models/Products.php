<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ['user_id', 'subscribe_id','product_name','start_date','end_date','invoice_no','amount_paid','product_type','comment', 'status', 'created_at', 'updated_at','deleted_at'];
}
