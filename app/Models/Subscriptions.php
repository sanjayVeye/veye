<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
	protected $table = 'subscriptions';
	
    protected $fillable = ['id','title', 'reports_id','slug', 'subscribe_title', 'yearly_description', 'red_strap_text', 'plan_information', 'plan_price', 'plan_cut_price', 'sale_information', 'offer_price', 'sale_price', 'report_type', 'display_order','offer_start','offer_end', 'image', 'icon_image', 'description', 'created_at', 'updated_at','deleted_at'];

     public function reportMenu(){
    	
		return $this->hasOne(Reports::class,'id','reports_id');
	}
}


