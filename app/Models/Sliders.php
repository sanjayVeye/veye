<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    protected $fillable = ['title', 'description', 'slider_img', 'created_at', 'updated_at','deleted_at'];
}
