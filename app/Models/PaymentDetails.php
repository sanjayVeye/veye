<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
	protected $table = 'payment_details';
    protected $fillable = ['ID','client_id','payment_amount','payment_type','payment_token','created_at','updated_at'];

}
 