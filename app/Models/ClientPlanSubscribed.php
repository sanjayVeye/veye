<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientPlanSubscribed extends Model
{
    protected $table = 'client_plan_subscribed';

    protected $fillable = [
        'title', 
        'created_at', 
        'updated_at',
        'deleted_at'];
}