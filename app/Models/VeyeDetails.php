<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VeyeDetails extends Model
{
    protected $fillable = ['veye_acn_no', 'veye_abn_no', 'veye_afsl_no', 'veye_ar_no', 'global_acn_no', 'global_abn_no', 'global_afsl_no', 'email', 'phone', 'address', 'state', 'country', 'post_code', 'created_at', 'updated_at','deleted_at'];
}
