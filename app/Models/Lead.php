<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $table = 'lead';
     protected $fillable = [
        
        'fname', 
        'lname', 
        'email', 
        'phone',
        'page_reference',
        'ip_address', 
        'lat_lng', 
        'day_time_phone', 
        'post_code',
        'user_agent',
        'http_connection', 
        'comment', 
        'note',
        'colorstatus',
        'readed',
        'subscribe',
        'lead_comment_id', 
        'status', 
        'rawID', 
        'lead_attend_status',
        'upload_method',
        'is_crm_updated', 
        'lead_created_at', 
        'lead_updated_at',
        'created_at',
        'updated_at',
        'type',
        'term_condition', 
        'consent', 
        'page_url',        
        'external_lead_id',
        'called', 
        'not_interested_on',
        'deal_of_day',
        'report',
        'last_updated_by',
        'sub_status',
        'amount',
        'months',
        'latest_comment',
        'lead_status',
        'lead_type',
        'lead_source',
        'source',
        'membership_type',
        'condition',
        'condition_color',
        'customer_service',
        'check_date',
    'access'];
    
}
