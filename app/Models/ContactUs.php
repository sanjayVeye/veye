<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = 'contact';

    protected $fillable = ['name', 'email', 'phone', 'post_code', 'message','form_type','assigned_emp','status','enquiry_date','remark', 'created_at', 'updated_at','deleted_at'];
}
