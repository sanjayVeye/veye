<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddPopup extends Model
{
    protected $table = 'add-popup';

    protected $fillable = [
        'image', 
        'status',
        'created_at', 
        'updated_at',
        'deleted_at'];
}
