<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $fillable = ['name', 'last_name', 'email', 'phone', 'post_code', 'message','form_type', 'created_at', 'updated_at','deleted_at'];
}
