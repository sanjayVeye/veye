<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\DividendReports;

class PersonalRecommendations extends Model
{
    protected $table = 'past_recommendations';
    protected $fillable = ['company', 'buy_price', 'buy_date', 'report_no1', 'sell_price', 'sell_date', 'report_no_2', 'gains_losses', 'status', 'created_at', 'updated_at','deleted_at'];

    public function dividentReport($reportno){
        //$reportno='VEYE-DR7108';
       // return DividendReports::where('report_no',$reportno)->first();
        return $this->hasOne(DividendReports::class,'report_no','report_no1')->where('dividend_reports.report_no',$reportno)->first();
    }

    public function dividentReport2($reportno){
        //$reportno='VEYE-DR7108';
        //return DividendReports::where('report_no',$reportno)->first();
       return $this->hasOne(DividendReports::class,'report_no','report_no_2')->where('dividend_reports.report_no',$reportno)->first();
    }
}

