<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	protected $table = 'cart';
    protected $fillable = ['id','session_id','user_id','product_id','name','price','quantity','image','created_at','delated_at'];

     public function productName(){    	
		return $this->hasOne(Subscriptions::class,'id','product_id');
	}
}
 