<?php

namespace App\Models;

use App\Models\Offers\OfferDetail;
use App\Models\Offers\OfferQr;
use App\Models\Products\SchemeQr;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScanQrRedemHistory extends Model
{
    use HasFactory;
    protected $appends=['others'];
    public function getOthersAttribute(){
        return ScanQrHistory::whereIn('qr_code',explode(',',$this->qr))
                                ->sum('points');
    }
}
