<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SendMail extends Model
{
    protected $fillable = ['user_type', 'email', 'amount', 'plan', 'status', 'created_at', 'updated_at','deleted_at'];
}
