<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PastPerformace extends Model
{
    protected $table = 'past_performace';

    protected $fillable = [
        'template', 
        'created_at', 
        'updated_at',
        'deleted_at'];
}