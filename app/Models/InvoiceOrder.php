<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\ClientDetails;

class InvoiceOrder extends Model
{
    protected $table = 'invoice_order';
     protected $fillable = [
        
        'username',
        'user_id', 
        'client_details_id', 
        'user', 
        'order_date',
        'order_receiver_name',
        'order_receiver_address', 
        'order_total_before_tax', 
        'order_total_tax', 
        'order_tax_per',
        'order_total_after_tax',
        'order_amount_paid', 
        'order_total_amount_due', 
        'note',
        'Cust_email',

        'post_code',
        'phone',
        'subscription', 
        'months', 
        'digit', 
        'card',
        'mop',
        'del', 
        'cancel', 
        'status',
        'commentcancel',

        'delreason',
        'year',
        'subsstartdate', 
        'subsenddate', 
        'unpaid', 
       
        'deleted_at',
        'created_at', 
        'updated_at',];

        public function clientDetail(){
            return $this->hasOne(ClientDetails::class,'id','client_details_id');
        }
    
}
