<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    protected $table = 'testimonials';
    protected $fillable = ['title', 'company_name','desciption','image', 'status', 'created_at', 'updated_at','deleted_at'];
}
