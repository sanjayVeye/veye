<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $fillable = ['title', 'slug', 'unique_key','image','mailchimp_id','description','meta_title','meta_keywords','meta_description', 'created_at', 'updated_at','deleted_at'];
}
