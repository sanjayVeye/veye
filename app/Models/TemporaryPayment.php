<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemporaryPayment extends Model
{
    protected $table = 'temporary_payment';

    protected $fillable = [
        'txn_id', 
        'unique_session_id', 
        'amount', 
        'payment_status',
        'email',
        'plan_id',
        'created_at', 
        'updated_at',
        'deleted_at'];
}
