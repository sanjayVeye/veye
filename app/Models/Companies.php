<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $fillable = ['title', 'asx_code', 'asx_type_id', 'report_type', 'sector_id', 'description', 'created_at', 'updated_at','deleted_at'];
  
    public function AsxType(){
        return $this->hasOne(AsxType::class,'id','asx_type_id');
    }
    public function scopeSectorSpecficSearch($query,$report_type){
        $companies=$this->where('report_category','>',1)
                        ->where('report_type','like',"%$report_type%")
                        ->where('report_type','!=',null)
                        ->select('report_type','id')
                        ->get();
        
        $arr=[];
        foreach($companies as $cRow){
            $report_type_arr=explode(',',$cRow->report_type);
            if($report_type_arr[0] == $report_type){
                array_push($arr,$cRow->id);
            }
        }

        if(count( $arr) > 0)
            $query->whereIn('id',$arr);

        return $query;
    }
}