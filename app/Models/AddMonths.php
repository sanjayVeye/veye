<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddMonths extends Model
{
    protected $table = 'add-months';

    protected $fillable = [
        'title', 
        'created_at', 
        'updated_at',
        'deleted_at'];
}