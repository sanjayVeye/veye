<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sectors extends Model
{
    protected $fillable = ['title','slug','image','description','meta_title','meta_keywords','meta_description', 'created_at', 'updated_at','deleted_at'];
}
