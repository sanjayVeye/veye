<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
   protected $table = 'customer';
    protected $fillable = [
        'lead_id', 
        'membership_id', 
        'custom_plan_month', 
        'productreport_id',
        'price',
        'user_id', 
        'status', 
        'created_at', 
        'updated_at', 
        ];
}
