<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceOrderItem extends Model
{
     protected $table = 'invoice_order_item';
     protected $fillable = [
           
        'order_item_id', 
        'order_id', 
        'username', 
        'item_code',
        'item_name',
        'order_item_quantity', 
        'order_item_price', 
        'order_item_final_amount', 
        'subsstartdate',
        'subsenddate',
        'month', 
        'deleted_at',
        'created_at', 
        'updated_at',];
}
