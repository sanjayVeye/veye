<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        
        'parent_id',
        'slug', 
        'title',
        'description', 
        'image', 
        'imagealt',
        'meta_title',
        'meta_keywords', 
        'meta_description',  
        'created_at', 
        'updated_at',
        'deleted_at'];
}
