<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailSubcription extends Model
{
	protected $table = 'email_subscribe';
    protected $fillable = ['id','email','created_at','updated_at'];

}
 