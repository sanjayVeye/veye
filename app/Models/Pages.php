<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $fillable = ['meta_title','meta_keywords','meta_description','template', 'tag', 'status', 'created_at', 'updated_at','deleted_at'];
}
