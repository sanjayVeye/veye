<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class daytoday_mails extends Model
{
    protected $fillable = ['title', 'content', 'report_type', 'status', 'created_at', 'updated_at','deleted_at'];
}
