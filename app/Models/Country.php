<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    protected $fillable = [
        'title', 
        'image', 
        'status',
        'created_at', 
        'updated_at',
        'deleted_at'];
}