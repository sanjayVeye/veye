<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recommendations extends Model
{
    protected $fillable = ['title', 'status', 'created_at', 'updated_at','deleted_at'];
}
