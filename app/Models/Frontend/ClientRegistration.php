<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientRegistration extends Model
{
    protected $table = 'clients';
    protected $fillable = [
        'user_id', 
        'first_name', 
        'last_name', 
        'email', 
        'phone', 
        'post_code', 
        'password', 
        'payment', 
        'client_type', 
        'created_at', 
        'updated_at',
        'deleted_at'];
}
