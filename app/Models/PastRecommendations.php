<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PastRecommendations extends Model
{
    protected $fillable = ['company', 'buy_price', 'buy_date', 'report_no1', 'sell_price','sell_date', 'report_no_2', 'gains_losses'];
} 
