<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailTemplates extends Model
{
    const PAYMENT_DETAIL_PAGE='PT784668A';
    const ADMIN_PAYMENT_DETAIL_PAGE='ADM4256';
    protected $fillable = ['template_name', 'template_key', 'template',  'created_at', 'updated_at','deleted_at'];
}
