<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gateway extends Model
{
    use HasFactory,SoftDeletes;
    protected $table='gateway';
    protected $fillable = [
        'name','pkey','skey','deleted_at','created_at','updated_at'
    ];

}
