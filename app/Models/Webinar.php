<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Webinar extends Model
{
    protected $fillable = [ 'title', 'path', 'reviews', 'created_at', 'updated_at','deleted_at'];
}
