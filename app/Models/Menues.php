<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menues extends Model
{
    use HasFactory,SoftDeletes;
    protected $table='menues';
    protected $fillable = [
        'title','parent','order','slug','meta_title ','meta','meta_desc','report_category'
    ];

    public function childs(){
        return $this->hasMany(Menues::class,'parent','id');
    }
}
