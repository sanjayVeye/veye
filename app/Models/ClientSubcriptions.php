<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientSubcriptions extends Model
{
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'post_code', 'color_code','user_agent','http_connection','comment','note','colorstatus','readed','subscribe', 'status', 'created_at', 'updated_at','deleted_at'];
}
