<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $fillable = ['title', 'slug','description','date','article_img','attachment', 'reporthtml', 'meta_keywords','meta_description','asx_code', 'created_at', 'updated_at','deleted_at'];
}
