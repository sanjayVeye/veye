<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subscriptions;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class SubscriptionsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-subscriptions|create-subscriptions|edit-subscriptions|delete-subscriptions', ['only' => ['index','show']]);
        $this->middleware('permission:create-subscriptions', ['only' => ['create','store']]);
        $this->middleware('permission:edit-subscriptions', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-subscriptions', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.subscriptions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.subscriptions.add',compact('reports','sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'reports_id'=>'required',
            'sale_price'=>'required'
        ]);

        $input = $request->all();

        if ($request->hasFile('icon_image')) {
            $icon_image = $request->file('icon_image');         
            $fileName = $icon_image->getClientOriginalName();
            $fileExtension = $icon_image->getClientOriginalExtension();
            $iconimageName = time().rand().'.'.$icon_image->getClientOriginalExtension();
            $request->file('icon_image')->move(base_path().'/public/uploads/offer_image',$iconimageName);
            $input['icon_image'] = $iconimageName;
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/offer_image',$imageName);
            $input['image'] = $imageName;
        } 
        $input['reports_id'] = implode(",", $request->reports_id);

        $user = Subscriptions::create($input);
        return redirect()->route('subscriptions.index')
                        ->with('success','Subscribe Page created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Subscriptions::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$subscriptions = Subscriptions::where('id',$id)->first();
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.subscriptions.edit',compact('subscriptions','reports','sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'reports_id'=>'required',
            'sale_price'=>'required',
        ]);
        $reportsId = implode(",", $request->reports_id);

        $user =Subscriptions::find($id);
        if ($request->hasFile('icon_image')) {
            $icon_image = $request->file('icon_image');         
            $fileName = $icon_image->getClientOriginalName();
            $fileExtension = $icon_image->getClientOriginalExtension();
            $iconimageName = time().rand().'.'.$icon_image->getClientOriginalExtension();
            $request->file('icon_image')->move(base_path().'/public/uploads/offer_image',$iconimageName);
            $user->icon_image = $iconimageName;
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/offer_image',$imageName);
            $user->image = $imageName;
        } 

        $user->title = $request->title;
        $user->reports_id = $reportsId; 
        $user->subscribe_title = $request->subscribe_title;
        $user->yearly_description = $request->yearly_description;   
        $user->red_strap_text = $request->red_strap_text; 
        $user->plan_information = $request->plan_information; 
        $user->plan_price = $request->plan_price; 
        $user->slug = $request->slug; 
        $user->plan_cut_price = $request->plan_cut_price; 
        $user->sale_information = $request->sale_information; 
        $user->offer_price = $request->offer_price; 
        $user->sale_price = $request->sale_price; 
        $user->report_type = $request->report_type; 
        $user->display_order = $request->display_order;        
        $user->offer_start = date("Y-m-d h:i:s", strtotime($request->offer_start_date)); 
        $user->offer_end = date("Y-m-d h:i:s", strtotime($request->offer_end_date)); 
        $user->description = $request->description; 
        $user->save();      
        return redirect()->route('subscriptions.index')
                        ->with('success','Subscribe Page updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subscriptions::find($id)->delete();
        return redirect()->route('subscriptions.index')
                        ->with('success','Subscribe Page deleted successfully');
    }

    public function datatable(Request $request){
        $users=Subscriptions::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-subscriptions')){
                            $action .='<a href="'.route("subscriptions.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-subscriptions')){
                            $action .='<a href="'.route("subscriptions.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
