<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Lead;
use App\Models\Admin\Client;
use App\Models\Admin\ClientCredential;
use App\Models\Admin\ClientDetails;
use App\Models\Admin\ClientDetailsPlan;
use App\Models\Subscriptions;
use App\Models\Reports;
use App\Models\CustomerProductReport;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth; 
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class LeadController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-lead|create-lead|edit-lead|delete-lead', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-lead', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-lead', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-lead', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.lead.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.lead.add', compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|email|unique:client_credentials,username',
            'password' => 'required|min:6',
            'client_id'=>'required',
            'start_date'=>'required|date',
            'end_date'  =>'required|date'
        ]);

        DB::beginTransaction();

        try {
            
            ClientCredential::create([
                'username'=>$request->username,
                'password'=>Hash::make($request->password),
                'client_id'=>$request->client_id,
                'subscription_start_date'=>date('Y-m-d H:i:s', strtotime($request->start_date)),
                'subscription_end_date'=>date('Y-m-d H:i:s', strtotime($request->end_date))
            ]);
    
            $details=ClientDetails::create([
                'client_id'                 =>$request->client_id,
                'product_name'              =>$request->product_name,
                'subscription_start_date'   =>date('Y-m-d H:i:s', strtotime($request->start_date)),
                'subscription_end_date'     =>date('Y-m-d H:i:s', strtotime($request->end_date)),
                'invoice_no'                =>$request->invoice_no,
                'amount'                    =>$request->amount_paid,
                'comment'                   =>$request->comment,
                'product_type_text'         =>$request->product_type,
                'report_access'             =>strtotime($request->end_date) > strtotime(date('Y-m-d')) ? 1 : 0
            ]);
    
    
            foreach($request->subscribe_id as $id){
    
                $report=Reports::find($id);
                ClientDetailsPlan::create([
                    'client_id' =>$request->client_id,
                    'client_details_id'=>$details->id,
                    'subscription_start_date'   =>date('Y-m-d H:i:s', strtotime($request->start_date)),
                    'subscription_end_date'     =>date('Y-m-d H:i:s', strtotime($request->end_date)),
                    'access'    =>1,
                    'comment'   =>$request->comment,
                    'report_name'=>$report->title,
                    'report_key'=>$report->unique_key,
                    'report_id' =>$report->id
                ]);
    
            } 
            DB::commit();
            return redirect()->route('lead.index')
                        ->with('success','Client created successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('lead.index')
                            ->with('error','Failed');
        } 
    }
      

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clients = Client::select(DB::raw('concat(first_name," ",last_name) as client_name'), 'id')
            ->whereNotIn('id', ClientCredential::pluck('client_id')->toArray())->get();
        $reports = Reports::where('deleted_at', NULL)->get();

        $leadData = Lead::where('id', $id)->first();
        // $clientCredential = $leadData;
        $customeReportData = CustomerProductReport::where('lead_id', @$id)->first();
        // print_r($customeReportData);die;
        $clientCredential = ClientCredential::where('id', @$customeReportData->customer_id)->first();
        $clientData = ClientDetails::where('client_id', @$clientCredential->client_id)->first();
        $client = Client::where('id', @$clientCredential->client_id)->first();
        $userData = User::where('id', @$clientCredential->user_id)->first();
        // print_r($client->id);die;
        return view('admin.lead.view_lead', compact('clients','reports','leadData','customeReportData','client','clientCredential','clientData','userData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'    => 'required|unique:Reports,title',
        ]);
        $slug = $request->title;
        function cleans($slug)
        {
            $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
            return preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        }
        $slugData = strtolower(cleans($slug));
        $reports = Lead::where('id', $id)->update([
            'title'     => $request->title,
            'order'     => $request->order,
            'slug'      => $slugData,
        ]);

        return redirect()->route('reports.index')
            ->with('success', 'Reports updated successfully');
    }

    public function statusUpdate(Request $request, $id, $status)
    {
        $reports = Lead::where('id', $id)->update([
            'access'     => $status
        ]);

        return redirect()->back()
            ->with('success', 'Status updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $reports = Lead::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('index')
            ->with('success', 'Reports deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = Lead::get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('name', function ($row) {
                $name = $row->fname . ' ' . $row->lname;
                return $name;
            })

            ->addColumn('status', function ($row) {
                if ($row->access=='1') {
                    $status = '<button type="button" class="btn btn-primary conv-pop"  data-toggle="modal" data-target="#modal-oldconversion">&nbsp; Accept</button>';
                }else{
                    $status = '<button type="button" class="btn btn-danger conv-pop"  data-toggle="modal" data-target="#modal-oldconversion">&nbsp; Reject</button>'; 
                }
                return $status;
            })  

            ->addColumn('action', function ($row) {
                $action = '';
                if ($row->access=='1') {
                $action .= '<a href="' . route("lead.edit", $row->id) . '"><button type="button" class="btn btn-success conv-pop"  data-toggle="modal" data-target="#modal-oldconversion">&nbsp; Conversion Details</button></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('edit-lead')) {
                    $action .= '<a href="' . route("lead.status", $row->id) . '/1"><button type="button" class="btn btn-primary conv-pop"  data-toggle="modal" data-target="#modal-oldconversion">&nbsp; Accept</button></a>
                    <a href="' . route("lead.status", $row->id) . '/0"><button type="button" class="btn btn-danger conv-pop"  data-toggle="modal" data-target="#modal-oldconversion">&nbsp; Reject</button></a>';
                }
                // if (Auth::user()->can('delete-lead')) {
                //     $action .= '<a href="' . route("lead.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                // }
                return $action;
            })->make();
    }
}
