<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Reports;
use App\Models\Enquiry;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use App\Models\ContactUs;
use Session;
use Yajra\DataTables\DataTables;

class EnquiryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-enquiry|create-enquiry|edit-enquiry|delete-enquiry', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-enquiry', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-enquiry', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-enquiry', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.enquiry.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.veye-details.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'veye_acn_no' => 'required',
            'veye_abn_no' => 'required'
        ]);

        $input = $request->all();
        $user = Enquiry::create($input);
        return redirect()->route('veye.index')
            ->with('success', 'Veye Detail created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = Enquiry::find($id);
        $user->group_id = @$request->group_id;
        $user->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
        $user = Enquiry::where('id', $id)->first();
        return view('admin.veye-details.edit', compact('user', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'veye_acn_no' => 'required',
            'veye_abn_no' => 'required'
        ]);

        $user = Enquiry::find($id);

        $user->veye_acn_no = $request->veye_acn_no;
        $user->veye_abn_no = $request->veye_abn_no;
        $user->veye_afsl_no = $request->veye_afsl_no;
        $user->veye_ar_no = $request->veye_ar_no;
        $user->global_acn_no = $request->global_acn_no;
        $user->global_abn_no = $request->global_abn_no;
        $user->global_afsl_no = $request->global_afsl_no;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->post_code = $request->post_code;

        $user->save();
        return redirect()->route('veye.index')
            ->with('success', 'Veye Detail updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Enquiry::find($id)->delete();
        $report = Enquiry::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');
        $report->save();
        return redirect()->route('enquiry.index')
            ->with('success', 'Enquiry deleted successfully');
    }

    public function datatable(Request $request)
    {
        $enquiry = ContactUs::where('deleted_at', NULL)->orderBy('created_at','desc')->get();
        return DataTables::of($enquiry)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('action', function ($row) {
                $action = '';
                // if(Auth::user()->can('edit-enquiry')){
                //     $action .='<a href="'.route("enquiry.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                // }
                if (Auth::user()->can('delete-enquiry')) {
                    $action .= '<a href="' . route("enquiry.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
