<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\LogoEdit;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class LogoController extends Controller
{
    function __construct()
    {
        // $this->middleware('permission:manage-logo|create-logo|edit-logo|delete-logo', ['only' => ['index', 'show']]);
        // $this->middleware('permission:create-logo', ['only' => ['create', 'store']]);
        // $this->middleware('permission:edit-logo', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:delete-logo', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.logo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.logo.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, []);

        $input = $request->all();

        if ($request->hasFile('header_logo')) {
            $header_logo = $request->file('header_logo');
            $fileName = $header_logo->getClientOriginalName();
            $fileExtension = $header_logo->getClientOriginalExtension();
            $imageName = time() . rand() . '.' . $header_logo->getClientOriginalExtension();
            $request->file('header_logo')->move(base_path() . '/public/frontend/assets/images', $imageName);
            $input['header_logo'] = $imageName;
        }

        if ($request->hasFile('footer_logo')) {
            $footer_logo = $request->file('footer_logo');
            $fileName = $footer_logo->getClientOriginalName();
            $fileExtension = $footer_logo->getClientOriginalExtension();
            $imageName = time() . rand() . '.' . $footer_logo->getClientOriginalExtension();
            $request->file('footer_logo')->move(base_path() . '/public/frontend/assets/images', $imageName);
            $input['footer_logo'] = $imageName;
        }

        $user = LogoEdit::create($input);
        Alert::success('Success', 'logo created successfully');
        return redirect()->route('logo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $sector = LogoEdit::find($id);
        $sector->group_id = @$request->group_id;
        $sector->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
        $user = LogoEdit::where('id', $id)->first();
        return view('admin.logo.edit', compact('user', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, []);

        $user = LogoEdit::find($id);

        if ($request->hasFile('header_logo')) {
            $header_logo = $request->file('header_logo');
            $fileName = $header_logo->getClientOriginalName();
            $fileExtension = $header_logo->getClientOriginalExtension();
            $imageName = time() . rand() . '.' . $header_logo->getClientOriginalExtension();
            $request->file('header_logo')->move(base_path() . '/public/frontend/assets/images', $imageName);
            $insdata['slider_img'] = $imageName;
            $user->header_logo = $imageName;
        }

        if ($request->hasFile('footer_logo')) {
            $footer_logo = $request->file('footer_logo');
            $fileName = $footer_logo->getClientOriginalName();
            $fileExtension = $footer_logo->getClientOriginalExtension();
            $imageName = time() . rand() . '.' . $footer_logo->getClientOriginalExtension();
            $request->file('footer_logo')->move(base_path() . '/public/frontend/assets/images', $imageName);
            $insdata['slider_img'] = $imageName;
            $user->footer_logo = $imageName;
        }

        $user->save();
        Alert::success('Success', 'logo updated successfully');
        return redirect()->route('logo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Sectors::find($id)->delete();
        $sector = LogoEdit::find($id);
        $sector->deleted_at = date('Y-m-d h:m:i');
        $sector->save();
        return redirect()->route('logo.index')
            ->with('success', 'logo deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = LogoEdit::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])


            ->addColumn('header_logo', function ($row) {
                $header_logo = '<img src="' . asset("frontend/assets/images/") . '/' . $row->header_logo . '" alt="" width="100" height="100">&nbsp;&nbsp;';
                return $header_logo;
            })


            ->addColumn('footer_logo', function ($row) {
                $footer_logo = '<img src="' . asset("frontend/assets/images/") . '/' . $row->footer_logo . '" alt="" width="100" height="100">&nbsp;&nbsp;';
                return $footer_logo;
            })

            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-logo')) {
                    $action .= '<a href="' . route("logo.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-logo')) {
                    $action .= '<a href="' . route("logo.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
