<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\daytoday_mails;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class DaytodayMailController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-daytodaymail|create-daytodaymail|edit-daytodaymail|delete-daytodaymail', ['only' => ['index','show']]);
        $this->middleware('permission:create-daytodaymail', ['only' => ['create','store']]);
        $this->middleware('permission:edit-daytodaymail', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-daytodaymail', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.daytodaymail.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.daytodaymail.add',compact('reports','sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content'=>'required',
            'report_type'=>'required'
        ]);

        $input = $request->all();

        $user = daytoday_mails::create($input);
        return redirect()->route('daytodaymail.index')
                        ->with('success','Send Day To Day Mail created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=daytoday_mails::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$daytodaymail = daytoday_mails::where('id',$id)->first();
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.daytodaymail.edit',compact('daytodaymail','reports','sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content'=>'required',
        ]);

        $user =daytoday_mails::find($id);
        $user->title = $request->first_name;
        $user->content = $request->content;
        $user->report_type = $request->report_type;
        $user->save();      
        return redirect()->route('daytodaymail.index')
                        ->with('success','Send Day To Day Mail updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        daytoday_mails::find($id)->delete();
        return redirect()->route('daytodaymail.index')
                        ->with('success','Send Day To Day Mail deleted successfully');
    }

    public function datatable(Request $request){
        $users=daytoday_mails::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-daytodaymail')){
                            $action .='<a href="'.route("daytodaymail.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-daytodaymail')){
                            $action .='<a href="'.route("daytodaymail.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
