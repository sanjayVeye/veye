<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Editorial;
use App\Models\Articles;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class EditorialController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-editorial|create-editorial|edit-editorial|delete-editorial', ['only' => ['index','show']]);
        $this->middleware('permission:create-editorial', ['only' => ['create','store']]);
        $this->middleware('permission:edit-editorial', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-editorial', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.editorial.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.editorial.add',compact('roles'));
    }

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            // 'position' => 'required'
        ]);

        $input = $request->all();
        if ($request->hasFile('article_img')) {
            $article_img = $request->file('article_img');         
            $fileName = $article_img->getClientOriginalName();
            $fileExtension = $article_img->getClientOriginalExtension();
            $imageName = time().rand().'.'.$article_img->getClientOriginalExtension();
            $request->file('article_img')->move(base_path().'/public/uploads/article/',$imageName);
            $imageNames = 'uploads/article/'.$imageName.'';
            $input['article_img'] = $imageNames;
            $input[created_at]=$request->created_at;
             $input[attachment]=$request->attachment;
            
        } 

        $user = Articles::create($input);
        return redirect()->route('editorial.index')
                        ->with('success','editorial created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Articles::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$data = Articles::where('ID',$id)->first();
        return view('admin.editorial.edit',compact('data','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $user = Articles::find($id);
   
        if ($request->hasFile('article_img')) {
            $article_img = $request->file('article_img');         
            $fileName = $article_img->getClientOriginalName();
            $fileExtension = $article_img->getClientOriginalExtension();
            $imageName = time().rand().'.'.$article_img->getClientOriginalExtension();
            $request->file('article_img')->move(base_path().'/public/uploads/article/',$imageName);
            //$insdata['article_img']= $imageName;
            $imageNames = 'uploads/article/'.$imageName.'';
            $user->article_img = $imageNames;
            
        }  

        $user->title = $request->title;
        $user->slug = $request->slug;
        $user->created_at = $request->created_at;
        $user->description = $request->description;
        $user->reporthtml = $request->reporthtml;
        $user->attachment = $request->attachment;
    
        $user->save();
        return redirect()->route('editorial.index')
                        ->with('success','editorial updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Blogs::find($id)->delete();
        $report =Articles::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('editorial.index')
                        ->with('success','editorial  deleted successfully');
    }

    public function datatable(Request $request){
        $users=Articles::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('article_img', function ($row) {
                            $article_img ='<img src="'.asset("/").'/'.$row->article_img.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                        return $article_img;
                    })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-editorial')){
                            $action .='<a href="'.route("editorial.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-editorial')){
                            $action .='<a href="'.route("editorial.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
