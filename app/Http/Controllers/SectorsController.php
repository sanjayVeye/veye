<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class SectorsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-sector|create-sector|edit-sector|delete-sector', ['only' => ['index','show']]);
        $this->middleware('permission:create-sector', ['only' => ['create','store']]);
        $this->middleware('permission:edit-sector', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-sector', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.sectors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.sectors.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required'
        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Sectors',$imageName);
            $input['image'] = $imageName;
            
        }

        $user = Sectors::create($input);
        Alert::success('Success', 'sector created successfully');
        return redirect()->route('sector.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $sector=Sectors::find($id);
        $sector->group_id=@$request->group_id;
        $sector->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = Sectors::where('id',$id)->first();
        return view('admin.sectors.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $user =Sectors::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Sectors',$imageName);$insdata['slider_img']= $imageName;
            $user->image = $imageName;
        }  
        
        $user->title = $request->title;
        $user->slug = $request->slug;
        $user->description = $request->description;
        $user->meta_title = $request->meta_title;
        $user->meta_keywords = $request->meta_keywords;
        $user->meta_description = $request->meta_description; 

        $user->save();
        Alert::success('Success', 'Sector updated successfully');
        return redirect()->route('sector.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Sectors::find($id)->delete();
        $sector =Sectors::find($id);
        $sector->deleted_at = date('Y-m-d h:m:i');    
        $sector->save();
        return redirect()->route('sector.index')
                        ->with('success','Sector deleted successfully');
                        
    }

    public function datatable(Request $request){
        $users=Sectors::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  

                    ->addColumn('image', function ($row) {
                        $image ='<img src="'.asset("uploads/Sectors/").'/'.$row->image.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                    return $image;
                })

                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-sector')){
                            $action .='<a href="'.route("sector.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-sector')){
                            $action .='<a href="'.route("sector.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
