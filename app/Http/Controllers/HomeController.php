<?php

namespace App\Http\Controllers;

use App\Models\DividendReports;
use App\Models\ContactUs;
use App\Models\Enquiry;
use App\Models\ClientSubcriptions;
use Yajra\DataTables\DataTables;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
  

    public function index()
    {
        $dateTime = date('Y-m-d H:i:s');
        $dailyReports=DividendReports::where('report_id','2')->count();
        $dividentReports=DividendReports::where('report_id','6')->count();
        $stockWeekReports=DividendReports::where('report_id','7')->count();
        $masterReports=DividendReports::where('report_id','3')->count();
        $pennyStockReports=DividendReports::where('report_id','4')->count();
        $count2 =ClientSubcriptions::where('subscribe',1)->count();
        $count =ClientSubcriptions::where('subscribe',0)->count();
        return view('home', compact('dailyReports','dividentReports','stockWeekReports','masterReports','pennyStockReports','count2','count'));
        
    }

    public function freeReportEnq(Request $request)
    {
        $users = ContactUs::where('form_type','free_report')->where('deleted_at',null)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('name', function ($row) {
                $name = $row->name;
                return $name;
            })

            ->addColumn('action', function ($row) {
                $action = '';
               
                    $action .= '<a href="' . route("dashboard.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                
                return $action;
            })->make();
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = ContactUs::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->back()->with('success', 'register deleted successfully');
        // return redirect()->route('webinarfrontend')
        //     ->with('success', 'register deleted successfully');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Editorial()
    {
        return view('Editorial');
    }
}
