<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ContactUs;
use App\Helpers\Helper;
use Spatie\Permission\Models\Role;
use DB;
use Hash; 
use Auth;
use App\Branch;
use App\Model\Roles;
use App\Models\EmailSubcription;
use App\Models\Enquiry;
use App\Models\Lead;
use Illuminate\Support\Facades\Redirect;
use Session;
use Yajra\DataTables\DataTables;
use RealRashid\SweetAlert\Facades\Alert;

class ContactUsController extends Controller
{
    // function __construct()
    // {
    //     $this->middleware('permission:manage-register|create-register|edit-register|delete-register', ['only' => ['index', 'show']]);
    //     $this->middleware('permission:create-register', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:edit-register', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:delete-register', ['only' => ['destroy']]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.contactus.index');
    }

    /**

     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'required',
        ]);

        $menues = ContactUs::create([
            'name'     => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'form_type'     => $request->form_type,
            'post_code'     => $request->post_code??'0',
            'message'     => $request->message??'NIL',
        ]);

        Enquiry::create([
            'name'     => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'form_type'     => $request->form_type,
            'post_code'     => $request->post_code??'0',
            'message'     => $request->message??'NIL',
        ]);

        $clientData = array('first_name' => $request->name, 'email' => $request->email, 'phone' => $request->phone);

        Helper::subscribeMailChimp($clientData, '73a463764b', 'subscribed');

        $data = array('name' => $request->name, 'email' => $request->email, 'phone' => $request->phone, 'post_code' => $request->post_code);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://crm.veyeaustralia.com.au/api/add-lead?data=' . urlencode(json_encode($data)), // your preferred link
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));

if($request->form_type=='contact_us'){
}else{
    $result = curl_exec ($curl);
}     
        curl_close ($curl);

        //dd($result);
        if(@$request->free_report=='free_report'){
            $_SESSION["free_report"] = "free_report";
            $urls = 'https://www.veye.com.au/public/uploads/5 HIGH QUALITY ASX DIVIDEND STOCKS FOR 2023.pdf';
            // $urls = 'https://freereport.veye.com.au/10-high-quality-asx-dividend-stocks-report/';
            return Redirect::to($urls);
        }else{
        return redirect()->route('thankyou')
            ->with('success', 'thankyou deleted successfully');
        }
    }

    public function emailSubscribe(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required',
        ]);

        EmailSubcription::create([
            'email' => $request->email
        ]);
        Alert::success('Success', 'Subscribe successfully');
        return redirect()->back()->with('success', 'Subscribe successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = ContactUs::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('contact-us')
            ->with('success', 'contact-us deleted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $menues = ContactUs::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->back()
            ->with('success', 'Deleted successfully');
    }



    public function datatable(Request $request)
    {
        $users = ContactUs::where('deleted_at', null)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('name', function ($row) {
                $name = $row->fname . ' ' . $row->lname;
                return $name;
            })

            ->addColumn('action', function ($row) {
                $action = '';

                $action .= '<a href="' . route("contactus.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';

                return $action;
            })->make();
    }
}
