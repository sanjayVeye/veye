<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\WatchLists;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class WatchListController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('frontend.registration.register');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'company' => 'required',
            'buy_qty'=>'required'
        ]);

        $input = $request->all();
        $input['buy_date'] = date("Y-m-d", strtotime($request->buy_date));
        $user = WatchLists::create($input);
        Alert::success('Success', 'Added successfully');
        return redirect()->back();
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
