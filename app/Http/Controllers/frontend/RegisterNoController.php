<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Enquiry;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
 use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class RegisterNoController extends Controller
{
    // function __construct()
    // {
    //     $this->middleware('permission:manage-register|create-register|edit-register|delete-register', ['only' => ['index', 'show']]);
    //     $this->middleware('permission:create-register', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:edit-register', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:delete-register', ['only' => ['destroy']]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.webinarfrontend-register.index');
    }

    /**

     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'name'    => 'required',
            'email'    => 'required',
        ]);
    
        $menues = Enquiry::create([
            'name'     => $request->name,
            'last_name'     => $request->last_name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'post_code'     => $request->post_code,
        ]);
		
		
		 $data = array('name' => $request->name, 'email' => $request->email, 'phone' => $request->phone, 'post_code' => $request->post_code);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://web.trademyapp.com/public/api/add-lead?data=' . urlencode(json_encode($data)), // your preferred link
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);

        return redirect()->back()
            ->with('success', 'webinarfrontend-register created successfully');
    }

   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = Enquiry::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('webinarfrontend')
            ->with('success', 'register deleted successfully');
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $menues = Enquiry::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->back()
        ->with('success', 'Deleted successfully');
    }



    public function datatable(Request $request)
    {
        $users = Enquiry::where('deleted_at',null)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('name', function ($row) {
                $name = $row->fname . ' ' . $row->lname;
                return $name;
            })

            ->addColumn('action', function ($row) {
                $action = '';
               
                    $action .= '<a href="' . route("webinarfrontend-register.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                
                return $action;
            })->make();
    }
}
