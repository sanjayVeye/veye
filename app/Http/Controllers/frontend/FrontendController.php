<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Admin\Client;
use App\Models\Admin\ClientDetailsPlan;
use App\Models\Admin\ClientSubcriptions;
use App\Models\DividendReports;
use App\Models\Menues;
use App\Models\Reports;
use App\Models\ReportCategories;
use App\Models\Sectors;
use App\Models\Companies;
use App\Models\PastRecommendations;
use App\Models\PersonalRecommendations;
use App\Models\Pages;
use App\Models\Faq;
use App\Models\Frontend\ClientRegistration;
use App\Models\Testimonials;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;

class FrontendController extends Controller
{

    public function manage_menues($parent, $child = null)
    {
        $menues = Menues::where('slug', $parent)->first();

        if (!$menues)
            return back()->with('error', 'Menu not found.');

        if ($child) {
            $child_row = Menues::where('slug', $child)->first();
            if (!$child_row)
                return back()->with('error', 'Sub menu not found.');
        }

       
        if (in_array($child, config('constant.fix_slugs')))
            return $this->fixSlugs($child);

        $results_per_page = 50;
        $result = DB::table('dividend_reports');
        if (isset($child_row))
            $result = $result->where('report_id', $child_row->report_category);
       
        $result = $result->where('deleted_at', null)->paginate(10);

        if (!isset($_GET['page']))
            $page = 50;
        else
            $page = $_GET['page'];

        $page_first_result = ($page - 1) * $results_per_page;


        $result = DividendReports::query();

        if (isset($child_row)){
            if ($parent == 'stock-advisory') { 
                Session::put('url.intended',URL::previous());
                $slugdata = Reports::where('slug', $child)->first();

                $result = $result->where('dividend_reports.report_id', $child_row->report_category);
                $result = $result->leftJoin('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code')->groupBy('id')->orderBy('dividend_reports.schedule', 'desc')->orderBy('dividend_reports.created_at', 'desc')->orderBy('dividend_reports.report_no', 'desc');
            }
        }
            
        if ($parent == 'special-reports') {
            Session::put('url.intended',URL::previous());
            $slugdata = Sectors::where('slug', $child)->first();

            $result = $result->orWhere('dividend_reports.report_id', $child_row->report_category);
            $result = $result->orWhere('dividend_reports.report_id', 18);
            $result = $result->leftJoin('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code');
        }

        $access=0;

        if ($parent == 'opportunity-stock') {
            Session::put('url.intended',URL::previous());
            $access=1;
            $slugdata = ReportCategories::where('slug', $child)->first();

            $result = $result->where('dividend_reports.reportcategory', $slugdata->id);
            $result = $result->leftJoin('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code');
        }


        if ($parent == 'sector-specific') {
            $access=1;
            $slugdata = Sectors::where('slug', $child)->first();
            if($slugdata){
                $companies=Companies::sectorSpecficSearch($slugdata->id)->pluck('asx_code')->toArray();
                $result = $result->asxCodeFilter($companies);
            }
            
        }
        // print_r($result);die;
        $dateTime = date('Y-m-d H:i:s');
        // print_r($result);die;
        $result = $result->select('dividend_reports.*')
        ->where(function($q) use($dateTime){
            $q->where('dividend_reports.schedule','<=', $dateTime)
                ->orWhere('dividend_reports.schedule',NULL);
        })
        
        ->orderBy('dividend_reports.schedule', 'desc')->orderBy('dividend_reports.created_at', 'desc')->orderBy('dividend_reports.report_no', 'desc');

        $result = $result->where('dividend_reports.deleted_at', null)->paginate(10);

        $clentData = Client::where('email', @Auth::user()->email)->first();
       
        $dateTime = date('Y-m-d H:i:s');
       // dd($child_row->report_category);
        $reportAccess = ClientDetailsPlan::with('report')->where('client_id', @$clentData->id)
            ->where('report_id', @$child_row->report_category)
            ->where('access', 1)
            ->where('subscription_start_date', '<=', $dateTime)
            ->where('subscription_end_date', '>=', $dateTime)
            ->first();

        // //testing view
        // return view('frontend.menues', [
        //     'child' => $child ? $child : $parent,
        //     'parent' => $parent,
        //     'results' => $result,
        //     'page' => $page,
        //     'child_row' => $child_row
        // ]);
        
        if (@Auth::user()->user_type == '0') {
            return view('frontend.menues', [
                'child' => $child ? $child : $parent,
                'parent' => $parent,
                'results' => $result,
                'page' => $page,
                'slugdata' => $slugdata,
                'child_row' => $child_row
            ]);
        } elseif ($reportAccess || $access==1) {
            return view('frontend.menues', [
                'child' => $child ? $child : $parent,
                'parent' => $parent,
                'results' => $result,
                'slugdata' => $slugdata,
                'page' => $page,
                'child_row' => $child_row
            ]);
        } else {  

            
            if (@Auth::user()->user_type) {         
            return redirect()->route('subscriptionMenu');
        }else{

            Session::put('url.intended',$_SERVER['REQUEST_URI']);
            return redirect()->route('registration.login');
        }
        }
   
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'reporthtml' => 'required',
        ]);
        $user = DividendReports::find($id);
        $user->reporthtml = $request->reporthtml;
        $user->save();
        Alert::success('Success', 'Report updated successfully');
        return back();
    }


    public function manage_footer($parent)
    {
        $menues = Pages::where('tag', $parent)->first();
       

        if ($parent == 'privacy-policy') {
            Session::put('url.intended',URL::previous());
            return view('frontend.privacypolicies', compact('menues'));
        } elseif ($parent == 'about-us') {
            $pagesdata = pages::where('deleted_at', NULL)->where('tag', 'about-us')->first();
            $faqdata = Faq::where('deleted_at', null)->get();
            $testmanag = Testimonials::limit(6)->where('deleted_at', null)->get();
            Session::put('url.intended',URL::previous());
            return view('frontend.about-us', compact('menues', 'pagesdata', 'faqdata', 'testmanag'));
        } elseif ($parent == 'past-performance') {
            $result = PastRecommendations::take(20)->get();
            Session::put('url.intended',URL::previous());
            return view('frontend.pastperformance', compact('result'));
        } elseif ($parent == 'past-recommendations') {
            $result=DB::select(DB::raw('SELECT a.*,b.slug as slug1, c.slug as slug2 FROM `past_recommendations` a left  join dividend_reports b on a.report_no1 =b.report_no left  join dividend_reports c on a.report_no_2=c.report_no'));
            //$result = PersonalRecommendations::orderBy('sell_date', 'desc')->get();
            $recommendations = 'past_recommendations';
            Session::put('url.intended',URL::previous());
            return view('frontend.pastrecommendations', compact('result', 'recommendations'));
        } elseif ($parent == 'terms-conditions') {
            Session::put('url.intended',URL::previous());
            return view('frontend.termscondition', compact('menues'));
        } elseif ($parent == 'financial-services-guide') {
            Session::put('url.intended',URL::previous());
            return view('frontend.financial_services_guide', compact('menues'));
        } else {

         if(!$menues){
           return  redirect('/404'); 
        }
            // dd($menues);
            $pagesdata = $menues;
            //$pagesdata = pages::where('deleted_at',NULL)->where('tag',$menues)->first();
            Session::put('url.intended',URL::previous());
            return view('frontend.otherpages', compact('pagesdata'));
        }
    }

    private function fixSlugs($child)
    {
        if ($child == 'stock-advisory') {
            $resultdata = Reports::get();
            return view('frontend.stockadvisory', compact('resultdata'));
        }
        if ($child == 'sector-specific') {
            $sectors = Sectors::get();
            $company = Companies::get();
            return view('frontend.sectorspecific', compact('sectors', 'company'));
        }
    }

 public function report_read_more_nf(){
     return redirect('404');
 }
    public function report_read_more($parent)
    {
        $ids = $parent;
       //dd($ids);
        $report = DividendReports::with('sectorName')->where('dividend_reports.slug',$ids)->first();
      
        $child_row = Menues::where('id', $report->report_id)->first();


        $clentData = Client::where('email', @Auth::user()->email)->first();
        // print_r($clentData);die;
        $dateTime = date('Y-m-d H:i:s');
       //dd($report->report_id);
        $reportAccess = ClientDetailsPlan::with('report')->where('client_id', @$clentData->id)
            ->where('report_id', @$report->report_id)
            ->where('access', 1)
            ->where('subscription_start_date', '<=', $dateTime)
            ->where('subscription_end_date', '>=', $dateTime)
           // dd($reportAccess->dump());
            ->first();

        // dd($reportAccess);
        //return view('frontend.menues_details', compact('report', 'child_row'));
      
        if (@Auth::user()->user_type == '0') {
            if ($report->report_id == 2) {
                return view('frontend.menues_details_daily', compact('report', 'child_row'));
            } else {
                return view('frontend.menues_details', compact('report', 'child_row'));
            }
        } elseif ($reportAccess) {
            if ($report->report_id == 2) {
                return view('frontend.menues_details_daily', compact('report', 'child_row'));
            } else {
                return view('frontend.menues_details', compact('report', 'child_row'));
            }
        } else {
            if (@Auth::user()->user_type) {         
                return redirect()->route('subscriptionMenu');
            }else{
                Session::put('url.intended',URL::previous());
                return redirect()->route('registration.login');
            }          
        }





        //	if($report->report_id==2)
        //{
        //	return view('frontend.menues_details_daily',compact('report','child_row'));
        //}
        //else{
        //	return view('frontend.menues_details',compact('report','child_row'));
        //}


    }
}
