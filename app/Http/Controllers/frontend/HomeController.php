<?php
namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\ClientRegistration;
use App\Models\Subscriptions;
use App\Models\Checkout;
use App\Models\MasterDtl;
use App\Models\TemporaryPayment;
use App\Models\Editorial;
use App\Models\PersonalRecommendations;
use Hash;
use Illuminate\Support\Facades\DB;
// use Session;
use Cookie;
use App\Helpers;
use App\Models\AddPopup;
use App\Models\Admin\Client;
use App\Models\Admin\ClientCredential;
use App\Models\Articles;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use App\Models\DividendReports;
use App\Models\ReportCategories;
use App\Models\Sectors;
use App\Models\Companies;
use App\Models\Reports;
use App\Models\Webinar;
use App\Models\PastRecommendations;
use App\Models\Testimonials;
use App\Models\Homes;
use App\Models\Faq;
use App\Models\Cart;
use App\Models\Pages;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        $dateTime = date('Y-m-d H:i:s');
       
        $daily = DividendReports::leftJoin('reports', 'reports.id', '=', 'dividend_reports.report_id')
        ->take(8)->where('dividend_reports.report_id', 2)
        ->orderBy('dividend_reports.schedule', 'asc')
        ->orderBy('dividend_reports.created_at', 'desc')->get();
        $latest = DividendReports::where('report_id',2)
        ->where(function($q) use($dateTime){
            $q->where('dividend_reports.schedule','<=', $dateTime)
                ->orWhere('dividend_reports.schedule',NULL);
        })
        ->take(5)
        ->orderBy('dividend_reports.schedule', 'desc')
        ->orderBy('dividend_reports.created_at', 'desc')
        ->orderBy('dividend_reports.report_no', 'desc')
        ->get();

        
        //$test = DividendReports::where('id','8313')->first();
        //if()
       //dd($dateTime . '='. $test->schedule);
        $reports = Reports::limit(4)->get();
		
        $divReports1 = DividendReports::where('report_id',6)->limit(2)
        ->where(function($q) use($dateTime){
            $q->where('dividend_reports.schedule','<=', $dateTime)
                ->orWhere('dividend_reports.schedule',NULL);
        })
        ->orderBy('id','desc');
		$divReports2 = DividendReports::where('report_id',7)->limit(2)
        ->where(function($q) use($dateTime){
            $q->where('dividend_reports.schedule','<=', $dateTime)
                ->orWhere('dividend_reports.schedule',NULL);
        })
        ->orderBy('created_at','desc')->union($divReports1)->orderBy('created_at','desc')->get();
		$divReports  = $divReports2;
        //$divReports=$divReports->orderBy('created_at','desc');
        $reportscat = ReportCategories::where('deleted_at', null)->get();
        $sector = Sectors::limit(10)->get();
        $blog = Articles::limit(4)->orderBy('created_at','desc')->where('deleted_at', null)->get();
        $testmanag = Testimonials::limit(6)->where('deleted_at', null)->get();
        $result = PersonalRecommendations::limit(6)->orderBy('sell_date','desc')->get();
        $homesdata = Homes::first();
        $popupStatus = AddPopup::where('deleted_at', NULL)->first();

        $homepopup=false;
       
        if($request->cookie('homepopup')=='blockpopup'){
            $homepopup=true;
        }
        else{
          $min = 14400;
          Cookie::queue(Cookie::make('homepopup','blockpopup',$min));
        }

        return view('frontend.index', compact('daily', 'reports','divReports', 'reportscat', 'sector', 'blog', 'testmanag', 'result', 'latest', 'homesdata','homepopup','popupStatus'));
    }

    public function pageMotFound(Request $request){
        return view('frontend.404');
    }

    public function search(Request $request){
        $asxCode=$request->q;
        $asxCodedisplay=$asxCode;
		$compasxCode=explode("-",$asxCodedisplay);
		if(strpos($asxCode,':',1) > 0){
		    $compname=trim($compasxCode[0]);
		
            $asxCodedisplay=str_replace(" ","",$asxCodedisplay);
            $asxCodedisplay=str_replace("(","",$asxCodedisplay);
            $asxCodedisplay=str_replace(")","",$asxCodedisplay);
            $nasval=explode(":",$asxCodedisplay);
            $asxname=$nasval[1];
		    $asxname=explode("-",$nasval[0]);
            $nasdaqname= $asxname[0];
			$asxval=$asxname[1];
		}else{
			$compname=$asxCode;
			$nasdaqname=$asxCode;
			$asxval=$asxCode;
            $asxcode=$asxCode;
		}

        $company=Companies::where('title','like',"$compname")->first();
        if(!$company){

            $company=Companies::where('asx_code','like',"$nasdaqname")->first();
        }
        if(!$company){

            $company=Companies::where('code_type','like',"$asxval")->first();
        }

        
        $divident_reports=DividendReports::asxCodeFilter([@$company->asx_code])
                                        ->orderBy('dividend_reports.created_at', 'desc')
                                        ->paginate(10);

        return view('frontend.search_reports',compact('company','asxCode','divident_reports'));
    }

    public function sectorSearch(Request $request,$id){
        $asxCode=str_replace('-',' ',$id);
        $asxCodedisplay=$asxCode;
		$compasxCode=explode("-",$asxCodedisplay);
		if(strpos($asxCode,':',1) > 0){
		    $compname=trim($compasxCode[0]);
		
            $asxCodedisplay=str_replace(" ","",$asxCodedisplay);
            $asxCodedisplay=str_replace("(","",$asxCodedisplay);
            $asxCodedisplay=str_replace(")","",$asxCodedisplay);
            $nasval=explode(":",$asxCodedisplay);
            $asxname=$nasval[1];
		    $asxname=explode("-",$nasval[0]);
            $nasdaqname= $asxname[0];
			$asxval=$asxname[1];
		}else{
			$compname=$asxCode;
			$nasdaqname=$asxCode;
			$asxval=$asxCode;
            $asxcode=$asxCode;
		}

        $company=Companies::where('title','like',"$compname")->first();
        if(!$company){

            $company=Companies::where('asx_code','like',"$nasdaqname")->first();
        }
        if(!$company){

            $company=Companies::where('code_type','like',"$asxval")->first();
        }

        
        $divident_reports=DividendReports::asxCodeFilter([@$company->asx_code])
                                        ->orderBy('dividend_reports.created_at', 'desc')
                                        ->paginate(10);

        return view('frontend.search_reports',compact('company','asxCode','divident_reports'));
    }
 
    public function _search(Request $request)
    {
        if ($request->session()->get('key') == @$_GET['q']) {
            $asxCode = @$_GET['q'];
        } else {
            $asxCode = @$_GET['q'];
            session('key', @$_GET['q']);
        }
		$asxCodedisplay=$asxCode;
		$compasxCode=explode("-",$asxCodedisplay);
		if(strpos($asxCode,':',1)>0)
		{
		$compname=trim($compasxCode[0]);
		
		$asxCodedisplay=str_replace(" ","",$asxCodedisplay);
		$asxCodedisplay=str_replace("(","",$asxCodedisplay);
		$asxCodedisplay=str_replace(")","",$asxCodedisplay);
		$nasval=explode(":",$asxCodedisplay);
		$nasdaqname=$nasval[1];
		$asxname=explode("-",$nasval[0]);
			$asxval=$asxname[1];
		}
		else{
			$compname=$asxCode;
			$nasdaqname=$asxCode;
			$asxval=$asxCode;
		}
		//dd($asxval);
        $countDiviReport = DividendReports::join('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code')
			->join('asx_type', 'asx_type.id', '=', 'companies.asx_type_id')
			->where('companies.title', 'LIKE', '%' . $compname . '%')
            ->where('asx_type.title', 'LIKE', '%' . $nasdaqname . '%')
			->where('dividend_reports.asx_code', 'LIKE', '%' . $asxval . '%')
			//dd($countDiviReport->dump());
            ->count();
			//dd($countDiviReport);
        $reportDatas = DividendReports::select('dividend_reports.*','companies.title')->join('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code')
			->join('asx_type', 'asx_type.id', '=', 'companies.asx_type_id')
			->where('companies.title', 'LIKE', '%' . $compname . '%')
            ->where('asx_type.title', 'LIKE', '%' . $nasdaqname . '%')
			->where('dividend_reports.asx_code', 'LIKE', '%' . $asxval . '%')
			->orderBy('dividend_reports.created_at', 'desc')
			->paginate(10);
			//dd($reportDatas->dump());
             

            // print_r($countDiviReport);die;

        $reportsValue = '';
        foreach ($reportDatas as $v) {
            $reportsValue = $v;
			//dd($reportsValue);
        }
        if ($reportsValue) {
            $reportData = $reportDatas;
            $page = 1;
        } else {
            $reportData = DividendReports::orderBy('id', 'desc')->limit(5)->get();
            $page = '';
        }
        $companyData = Companies::where('asx_code', $asxCode)
            ->orWhere('title', 'LIKE', '%' . $compname . '%')
            ->first();
		
        // print_r($reportData);die;
        return view('frontend.search_reports', compact('reportData', 'page', 'asxCode', 'companyData','countDiviReport'));
    }
    public function sectorSpecific()
    {
        $sectors = Sectors::get();
        $company = Companies::get();
        $masterDlt = MasterDtl::where('deleted_at',NULL)->get();

          $metas=Pages::where('tag','sector-specific')->first();
        
        return view('frontend.sectorspecific', compact('sectors', 'company','masterDlt','metas'));
    }

    public function subscriptionMenu()
    {
        $faqdata = Faq::where('deleted_at',null)->get();
        $session_id = Session::getId('product_id');
        $subscriptionmenu = Subscriptions::with(['reportMenu'])->where('deleted_at', null)->orderBy('display_order', 'asc')->get();
        $cart = Cart::where('session_id', $session_id)->where('delated_at', null)->with(['productName'])->get();
        date_default_timezone_set('Asia/Kolkata');
        $currentDate = date('Y-m-d h-i-s');

        Session::put('url.intended',URL::previous());
        return view('frontend.subscriptionmenu', compact('subscriptionmenu', 'cart','faqdata','currentDate'));
    }

    public function editorial()
    {
        $editorial = Articles::where('deleted_at', null)->orderBy('created_at','desc')->paginate(12);
        Session::put('url.intended',URL::previous());
        return view('frontend.editorial', compact('editorial'));
    }

     public function article($slug){
        Session::put('url.intended',URL::previous());
         return redirect('/editorial-details/'.$slug);

     }

     public function articles(){
        return view('frontend.404');

     }
     

    public function contact_us()
    {
        Session::put('url.intended',URL::previous());
        return view('frontend.contact_us');
    }

    public function thankyou()
    {
        return view('frontend.thankyou');
    }

    public function offer($slug)
    {
        //dd($slug);
        $subs=Subscriptions::where('slug',$slug)->first();
        //dd($subs->slug);

        $faqdata = Faq::where('deleted_at',null)->get();
        $session_id = Session::getId('product_id');
        $subscriptionmenu = Subscriptions::with(['reportMenu'])->where('deleted_at', null)->orderBy('display_order', 'asc')->get();
        $cart = Cart::where('session_id', $session_id)->where('delated_at', null)->with(['productName'])->get();
        date_default_timezone_set('Asia/Kolkata');
        $currentDate = date('Y-m-d h-i-s');
        $subDatedate=date('Y-m-d h-i-s',strtotime($subs->offer_end));
        $price=0;
        $months=0;

        if($currentDate < @$subs->offer_end){
        $price=$subs->offer_price;
        $months=$subs->plan_information;
         }
        else{
        $price=$subs->sale_price;
        $months=$subs->sale_information;
       
        }
        //dd($currentDate. "=".$subDatedate);
        if($currentDate > $subDatedate)
        {
            return view('frontend.offer');
        }
        else{
            return redirect('/checkout?currentDate&'.$currentDate.'&price='.$price.'&months='.$months.'&quantity=1&title='.$subs->title. '&id='.Crypt::encrypt($subs->id));
        }
      
       
       
        
    }

    public function more_report_details()
    {
        return view('frontend.more_report_details');
    }
    public function report_details($id)
    {
        $ids = decrypt($id);
        $details = Reports::find($ids);
        $reports = Reports::where('deleted_at', null)->take(5)->orderBy('id', 'desc')->get();
        return view('frontend.report_details', compact('details', 'reports'));
    }

    public function editorial_details($id)
    {
        $ids = $id;
       
        $details = Articles::where('slug',$ids)->first();
        if(!$details){
            return redirect('404');
        }
        
        $editorial = Articles::where('deleted_at', null)->take(6)->orderBy('id', 'desc')->get();

        return view('frontend.editorial-details', compact('details', 'editorial'));
    }

        public function editorial_details_blog($id)
    {
        $ids = $id;
       
        //$details = Articles::where('slug',$ids)->first();
        //$editorial = Articles::where('deleted_at', null)->take(6)->orderBy('id', 'desc')->get();

        return redirect('/blog/'.$id);
    }

    public function editorialdetails()
    {
        return view('frontend.editorialdetails');
    }
    
    public function stockadvisory()
    {
        $resultdata = Reports::where('deleted_at', null)->get();
        $metas=Pages::where('tag','stock-advisory')->first();
        
        return view('frontend.stockadvisory', compact('resultdata','metas'));
    }

    public function sector_specific()
    {
        $sector = sectors::where('deleted_at', null)->get();
        $metas=Pages::where('tag','sector-specific')->first();
        //dd($metas);
        return view('frontend.sectorspecific', compact('sector','metas'));
    }


    public function webinarfrontend()
    {
        $linkwebinar = Webinar::where('deleted_at', null)->orderBy('id', 'desc')->paginate(10);
        $dateTime = date('Y-m-d H:i:s');  
        $clientData = Client::join('client_credentials as clientdtl', 'clients.id', '=', 'clientdtl.client_id')
        ->where('email', @Auth::user()->email)
        ->where('deleted_at', null)
        ->where('clientdtl.subscription_start_date','<=',$dateTime)
        ->where('clientdtl.subscription_end_date','>=',$dateTime)
        ->first();
        Session::put('url.intended',URL::previous());
        return view('frontend.webinarfrontend', compact('linkwebinar','clientData'));
    }

    public function checkout(Request $request)
    {
        // dd(encrypt(''));
        
        function generateTransactionId($prefix)
        {
            $str_len = 8;
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            $result = '';
            for ($i = 0; $i < $str_len; $i++) {
                $result .= $characters[mt_rand(0, 61)];
            }
            $transactionId = $prefix . $result;
            return strtolower($transactionId);
        }
        if ($request->id) {
            $sucriptionEndDate = Subscriptions::where('id', decrypt($request->id))->first();
        } else {
            $sucriptionEndDate = '';
        }
        
        $session_id = Session::getId('product_id');
        if ($request->title) {  
              
            $cart = "";
            $title = $request->title;
            $price = str_replace('$', '', $request->price);
            $quantity = $request->quantity;
            $months = $request->months;
            $planInformation = '';
        } else {
            
            $cart = Cart::where('session_id', $session_id)->where('delated_at', null)->with(['productName'])->get();            
            $sum = 0;
            $quantites = 0;
            foreach ($cart as $v) {                
                $prices = str_replace('$', '', $v->price);
                $sum += $prices;
                $quantites += $v->quantity;
                $name = $v->name;
                $plan_information = $v->productName->plan_information;
            }
            $title = $name;
            $planInformation = $plan_information;
            $price = $sum;
            $quantity = $quantites;
            $months = 12;
        }
        // print_r($cart);die;
        $resultPast = PersonalRecommendations::limit(6)->orderBy('sell_date','desc')->get();

        $transaction_id = generateTransactionId('VEYE');
        //date_default_timezone_set('Austrailia/Kolkata');
        $currentDate = date('Y-m-d h-i-s');

        $input['unique_session_id'] = $session_id;
        $input['amount'] = $price;
        $input['payment_status'] = 'Pending';
        // $input['plan_id'] = decrypt($request->id);         
        // $input['created_at'] = $currentDate;  
        TemporaryPayment::create($input);
        // dd(decrypt($request->id));  
        $testmanag = Testimonials::limit(6)->where('deleted_at', null)->get();
        $checkutdata = Checkout::first();
        $homesdata = Homes::first();
        
        $getdata =
            Cart::where('session_id', $session_id)->where('product_id', decrypt($request->id))->exists();
        //dd($getdata);
        if ($getdata == true) {
            //alert('Product is already Added to shopping cart');
        } else {
            if(decrypt($request->id) !='custom'){
            $cart = new Cart();
            $cart->name = $sucriptionEndDate->title;
            $cart->product_id = $sucriptionEndDate->id;
            $cart->price   = $sucriptionEndDate->sale_price;
            $cart->quantity   = 1;
            $cart->session_id   = $session_id;
            //dd($cart);
            $cart->save();
            }
        }
        

        return view('frontend.checkout', compact('resultPast', 'title', 'price', 'quantity', 'months', 'transaction_id', 'session_id', 'cart', 'sucriptionEndDate', 'currentDate','testmanag','homesdata','checkutdata','planInformation'));
    }

    public function buynow(Request $request)
    {
        // dd($request->all());
        $productId = $request->id;

        $inputs = array();
        $inputs['product_id'] = $request->id;
        $inputs['name'] = $request->title;
        $inputs['price'] = $request->price;
        $inputs['quantity'] = $request->quantity;
        $inputs['delated_at'] = 0;
        //dd($inputs);
        Cart::create($inputs);

        Alert::success('Success', 'Registeration successfully');
        return redirect()->route('checkout')->with('productId', $productId);
        //return view('frontend.checkout',compact('checkout','productId'));

    }
 
    public function addToCart(Request $request)
    {

        $product = Subscriptions::findOrFail($request->product_id);
        // $product = Subscriptions::find($request->product_id);

        $session_id = Session::getId('product_id');
        $getdata =
            Cart::where('session_id', $session_id)->where('product_id', $request->product_id)->exists();
        //dd($getdata);
        if ($getdata == true) {
            alert('Product is already Added to shopping cart');
        } else {
            $cart = new Cart();
            $cart->name = $product->title;
            $cart->product_id = $request->product_id;
            $cart->price   = $product->sale_price;
            $cart->quantity   = $request->quantity;
            $cart->session_id   = $session_id;
            //dd($cart);
            $cart->save();
        }
        $getdatas =
            Cart::where('session_id', $session_id)->with(['productName'])->get();
        $cartdata = '<h5>Plan details</h5>

               <div class="planDetaScr">';
        $total = 0;
        foreach ($getdatas as $key => $data) {

            $cartdata .= '  <div class="DevidenReportBoxSecondBox">
                                   <h5>' . $data->productName->title . ' <span><b>' . $data->price . '</b><a href="#0"
                                   class="remove-cart"
                                    data-cart_id="' . $data->id . '"> 
                               <i class="fas fa-trash-alt remove-cart"></i></a></span></h5>
                                   <p>Membership duration: ' . $data->productName->plan_information . '</p>
                                </div>';
            $price = str_replace('$', '', $data->price);
            $total += $price;
        }
        $cartdata .= '</div>
               <div class="DevidenReportBoxSecondBox">
                  <h5>Total amount <span><b> $' . $total . '</b></span></h5>
               </div>';
        echo $cartdata;
    }

    public function removeCart(Request $request)
    {
        //dd($request->cart_id);
        //$cartdata = Cart::findOrFail($request->cart_id);

        //dd($cartdata);

        $session_id = Session::getId('product_id');
        //dd($session_id);
        Cart::where('session_id', $session_id)->where('id', $request->cart_id)->delete();

        $getdatas =
            Cart::where('session_id', $session_id)->with(['productName'])->get();
        $cartdata = '<h5>Plan details</h5>

               <div class="planDetaScr">';
        $total = '0';
        foreach ($getdatas as $key => $data) {

            $cartdata .= '  <div class="DevidenReportBoxSecondBox">
                                   <h5>' . $data->productName->title . ' <span><b>' . $data->price . '</b><a href="#0"
                                   class="remove-cart"
                                    data-cart_id="' . $data->id . '"> 
                               <i class="fas fa-trash-alt remove-cart"></i></a></span></h5>
                                   <p>Membership duration: ' . $data->productName->plan_information . '</p>
                                </div>';
                                $price = str_replace('$', '', $data->price);
                                $total += $price;
        }
        $cartdata .= '</div>
               <div class="DevidenReportBoxSecondBox">
                  <h5>Total amount <span><b> $' . $total . '</b></span></h5>
               </div>';
        echo $cartdata;
    }


    public function search_company(Request $request){
        $term=$request->term;

        if(strlen($term)>=3){
            $result= Companies::leftJoin('asx_type', 'asx_type.id', '=', 'companies.asx_type_id')
                ->where('companies.asx_code','like',"$term%")->orWhere('asx_type.title','like',"$term%")->orWhere('companies.title','like',"$term%")
 ->select(DB::raw('concat(companies.title," - (", asx_code,": ",asx_type.title,")") as label'),DB::raw('concat(companies.title," - (", asx_code,": ",asx_type.title,")")  as value'))->limit(20)->get();
				//select('asx_code as label','asx_code as value')
			//dd($result->dump());
				
        }else{
            $result= Companies::where(function($q) use($term){
                $q->where('title','like',"$term%");
            })->select('title as label','title as value')->limit(20)->get();
        }

       return $result;
    }
}
