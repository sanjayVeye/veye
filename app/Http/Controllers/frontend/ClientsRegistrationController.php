<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Frontend\ClientRegistration;
use App\Models\Admin\ClientDetailsPlan;
use App\Models\Admin\ClientDetails;
use App\Models\Admin\Client;
use App\Models\WatchLists;
use App\Models\Articles;
use App\Models\DividendReports;
use App\Models\Menues;
use App\Models\User;
// use Hash;
use Spatie\Permission\Models\Role;
use App\Providers\SendEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Cookie;
use Illuminate\Support\Str;
use App\Models\Sectors;
use App\Models\Companies;
use App\Models\InvoiceOrder;
use ReportCategories;
use PDF;

class ClientsRegistrationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('frontend.registration.register');
    }

    public function watchlist(Request $request)
    {



        //          echo $response."rrrr";
        // $err = curl_error($curl);
        // curl_close($curl);

        // if ($err) {
        //     echo "cURL Error #:" . $err;
        // } else {
        //    dd($response);
        // }

        $watchlist = WatchLists::where('deleted_at', NULL)->orderBy('id', 'DESC')->paginate(10);
        return view('frontend.dashboard.watchlist', compact('watchlist'));
    }

    public function watchlistApi($qry)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/auto-complete?q=' . $qry . '&region=US', // your preferred link
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'X-RapidAPI-Key:8cb14b9729mshea195bebe7e06f5p123002jsn6c7a40eb0128',
                'X-RapidAPI-Hostt:yh-finance.p.rapidapi.com',
                //  'Content-Type:application/octet-stream',
            ),
        ));

        $response = curl_exec($curl);

        return $response;
    }

    public function company_name(Request $request)
    {

        $term = $request->term;
        $result = $this->watchlistApi($term);
        $result = str_replace('shortname', 'label', $result);
        $result = str_replace('exchange', 'value', $result);
        $temp = json_decode($result, true);
        return array_slice($temp['quotes'], 2);
    }

    public function latestbuy(Request $request)
    {
        $clientData = Client::where('user_id', Auth::user()->id)->first();
        $latestbuy = DividendReports::with('companyName')->where('recommendation', 2)->where('created_at', '>=', Carbon::now()->subMonths(2))->where('deleted_at', NULL)->orderBy('id', 'DESC')->paginate(10);
        // print_r(Carbon::now()->subMonths(2));die; 
        // $latestbuy = ClientDetails::where('client_id',@$clientData->id)->orderBy('id','DESC')->get();
        return view('frontend.dashboard.latestbuy', compact('latestbuy'));
    }

    public function latestsell(Request $request)
    {
        $latestsell = DividendReports::with('companyName')->where('recommendation', 1)->where('created_at', '>=', Carbon::now()->subMonths(2))->where('deleted_at', NULL)->orderBy('id', 'DESC')->paginate(10);
        return view('frontend.dashboard.latestsell', compact('latestsell'));
    }

    public function updateEmail(Request $request)
    {
        $latestsell = WatchLists::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        return view('frontend.dashboard.updateEmail', compact('latestsell'));
    }

    public function updatePass(Request $request)
    {
        $latestsell = WatchLists::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        return view('frontend.dashboard.updatePass', compact('latestsell'));
    }

    public function subscriptionGeneratePDFS($id)
    {
        $datas = InvoiceOrder::find($id);

        $data = [
            'orderid' => $id,
            'order_receiver_name' => $datas->order_receiver_name,
            'username' => $datas->username,
            'cust_email' => $datas->Cust_email,
            'created_at' => $datas->created_at,
            'phone' => $datas->phone,
            'post_code' => $datas->post_code,
            'order_total_after_tax' => $datas->order_total_after_tax,
            'order_total_tax' => $datas->order_total_tax,
            'order_total_before_tax' => $datas->order_total_before_tax,
            'date' => date('m/d/Y')
        ];

        $pdf = PDF::loadView('myPDF', $data);

        return $pdf->download('invoice.pdf');
    }


    public function mySubscription(Request $request)
    {
        date_default_timezone_set('Asia/Kolkata');
        $currentdate = date('Y-m-d H:i:s');
        // print_r($currentdate);die;
        $clientData = Client::where('email', Auth::user()->email)->first();
        $subscription = ClientDetailsPlan::with('invoice_no', 'report')->where('client_id', @$clientData->id)->where('subscription_start_date', '<=', $currentdate)->where('subscription_end_date', '>=', $currentdate)->where('access', 1)->paginate(10);
        $subscriptionDeactive = ClientDetailsPlan::with('invoice_no', 'report')->where('client_id', @$clientData->id)->where('subscription_end_date', '<=', $currentdate)->paginate(10);
        // print_r($subscription);die; 
        return view('frontend.dashboard.mySubscription', compact('subscription', 'subscriptionDeactive'));
    }

    public function myInvoice(Request $request)
    {
        $clientData = Client::where('email', Auth::user()->email)->first();
        $invoiceDetail = ClientDetails::where('client_id', @$clientData->id)
            //dd($invoiceDetail->dump());
            ->get();

        return view('frontend.dashboard.myInvoice', compact('invoiceDetail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     // dd($request->all());
    //     $request->validate([
    //         'first_name'    =>'required',
    //         // 'last_name'     =>'nullable',
    //         // 'post_code'     =>'required|digits:6',
    //         // 'phone'         =>'required|digits:10',
    //         // 'email'         =>'required|email',
    //         // 'password'      =>'required',
    //     ]);
    //     if($request->password ==  $request->cnf_password) {
    //         $input = $request->all();
    //         // ClientRegistration::create($input);
    //         User::create($input);
    //     } else {
    //         return redirect()->route('registration.index')->with('success','Password does not match.');
    //     }

    //     return redirect()->route('registration.login')->with('success','Registeration successfully.');
    // }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'mobile' => 'required'
        ]);

        $input = $request->all();
        $roles = 2;
        $input['name'] = $request->first_name . ' ' . $request->last_name;
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $lastId = $user->id;
        // print_r($LastInsertId);die;
        $inputs = $request->all();
        $inputs['user_id'] = $lastId;
        $inputs['phone'] = $request->mobile;
        ClientRegistration::create($inputs);

        $role = Role::find($roles);
        $user->assignRole($role);
        Alert::success('Success', 'Registeration successfully');
        return redirect()->route('registration.login');
    }

    public function login(Request $request)
    {
        return view('frontend.registration.login');
    }

    public function loginMatch(Request $request)
    {
        $request->validate([
            'email' => 'required|email|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required'
        ], ['email.email' => 'Please enter valid email address']);

        // if($request->ajax())
        //     return response()->json(array('message'=>'validation complete.'),200);

        $user = User::where('email', $request->email)->first();

        // dd(md5($request->password));
        if ($request->email == 'admin@gmail.com') {
            return redirect()->route('registration.login')->with('fail', 'Your login credentials don`t match.');
        } else {
            if (md5(@$request->password) == @$user->password) {
                $user->password = Hash::make(@$request->password);
                $user->user_type = '1';
                $updateUser = $user->update();
                if ($updateUser) {
                    $credentials = $request->only('email', 'password');

                    if (Auth::attempt($credentials)) {
                        if (Session::has('BACKURL')) {
                            $bUrl = Session::get('BACKURL');
                            session()->forget('BACKURL');
                            return redirect($bUrl);
                        } else {
                            if ($request->login_type == '1') {

                                return back()->with('success', 'Welcome ' . Auth::user()->name);
                            } else {
                                return Redirect::to(Session::get('url.intended'))->with('success', 'Welcome ' . Auth::user()->name);
                                // return redirect()->route('dashboard.home')->with('success', 'Welcome ' . Auth::user()->name);
                            }
                        }
                    } else {
                        return redirect()->route('registration.login')->with('fail', 'Your login credentials don`t match.');
                    }
                }
                // return redirect()->route('forgot.password');
            } else {

                $credentials = $request->only('email', 'password');
// print_r($credentials['email']);die;
                if (Auth::attempt($credentials)) {

                    if($request->remember){
                        $min = 14400;
                       Cookie::queue(Cookie::make('email',$credentials['email'],$min));
                       Cookie::queue(Cookie::make('password',$request->password,$min));
                    }else{
                        Cookie::queue(Cookie::make('email',''));
                        Cookie::queue(Cookie::make('password',''));
                    }

                    if (Session::has('BACKURL')) {
                        $bUrl = Session::get('BACKURL');
                        session()->forget('BACKURL');
                        return redirect($bUrl);
                    } else {
                        if ($request->login_type == '1') {
                            return back()->with('success', 'Welcome ' . Auth::user()->name);
                        } else {
                            $user->user_type = '1';
                            $user->update();
                            return Redirect::to(Session::get('url.intended'))->with('success', 'Welcome ' . Auth::user()->name);
                            // return redirect()->route('dashboard.home')->with('success', 'Welcome ' . Auth::user()->name);
                        }
                    }
                } else {
                    return redirect()->route('registration.login')->with('fail', 'Your login credentials don`t match.');
                }
            }
        }
    }


    public function dashboard(Request $request)
    {
        $dateTime = date('Y-m-d H:i:s');
        $dailyReports = DividendReports::where('report_id', '2')->count();
        $dividentReports = DividendReports::where('report_id', '3')->count();
        $stockWeekReports = DividendReports::where('report_id', '4')->count();
        $masterReports = DividendReports::where('report_id', '5')->count();
        $pennyStockReports = DividendReports::where('report_id', '6')->count();
        $hotReports = DividendReports::where('report_id', '7')->count();
        $articles = Articles::count();

        $clentData = db::table('clients')->where('email', Auth::user()->email)->first();

        $reportAccess = ClientDetailsPlan::with('report')->where('client_id', @$clentData->id)->where('access', 1)
            ->where('subscription_start_date', '<=', $dateTime)
            ->where('subscription_end_date', '>=', $dateTime)
            ->groupBy('report_id')
            ->get();
        // dd($reportAccess);
 
        return view('frontend.dashboard.home', compact('dailyReports', 'dividentReports', 'stockWeekReports', 'masterReports', 'pennyStockReports', 'hotReports', 'articles', 'reportAccess', 'clentData'));
    }

    public function forgotPassword(Request $request)
    {
        $keyId = '';
        $email = '';
        return view('frontend.registration.forgot_password', compact('keyId', 'email'));
    }
    public function forgotPasswordSet(Request $request, $id)
    {
        $keyId = $id;
        $email = $_GET['email'];
        return view('frontend.registration.forgot_password', compact('keyId', 'email'));
    }

    public function send_pass_reset_link(Request $request)
    {
        $validator = validator($request->all(), [
            'email' => 'required|email|regex:/(.+)@(.+)\.(.+)/i||exists:users'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with($validator->errors()->toArray());
            // return response()->json(array('errors'=>$validator->errors()->toArray()),400);
        }

        //Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(40),
            'created_at' => date('Y-m-d H:i:s')
        ]);
        //Get the token just created above
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();
           

        if ($this->sendResetEmail($request->email, $tokenData->token)) {
            return redirect()->back()->with('success', 'A reset link has been sent to your email address.');
        } else {
            $validator = validator($request->all(), [
                'network' => 'required'
            ], ['network.required' => 'A Network Error occurred. Please try again.']);

            if ($validator->fails()) {
                return redirect()->back()->with($validator->errors()->toArray());
                // return response()->json(array('errors'=>$validator->errors()->toArray()),400);
            }
            else{
                 return redirect()->back()->with('success', "Registered Email Does not exists.");
            }
        }
    }

    public function resetPassword(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'token' => 'required',
            'password'  => 'required',
        ]);

        $password = $request->password;
        $tokenData = DB::table('password_resets')
            ->where('token', $request->token)->first();

        if (!$tokenData)
            return redirect()->back()->with('error', 'Token expired.');

        $user = User::where('email', $tokenData->email)->first();
        if (!$user) return redirect()->back()->with(['error' => 'Email not found']);

        $user->password = Hash::make($password);
        $user->user_type = 1;
        $user->update();
        Auth::login($user);
        //Delete the token
        DB::table('password_resets')->where('email', $user->email)
            ->delete();
        return redirect('/')->with('success', 'Your password has been reset successfully.');
        // dd($request->all());
        // $oldPass = $request->old_password;
        // $pass    = $request->new_password;
        // $clients = ClientRegistration::where('password', $oldPass)->first();
        // $password = $clients->password;
        // // dd($clients->password);
        // if($request->old_password ==  $password) {
        //     // dd("1111");
        //     if($request->new_password ==  $request->cnf_password) {
        //         $update = ClientRegistration::where('password', $oldPass)->update([
        //             'password' => $pass,
        //         ]);
        //         Alert::success('Success', 'New Password Reset successfully');
        //         return redirect()->route('registration.login');
        //     } else {
        //         Alert::error('Failed', 'Confirm Password does not match');
        //         return redirect()->route('forgot.password');
        //     }
        // } else {
        //     Alert::error('Failed', 'Old Password does not match');
        //     return redirect()->route('forgot.password');
        // }

    }

    public function emailupdate(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . $id,
        ]);

        $user = User::find($id);
        $user->email = $request->email;
        $user->save();
        Alert::success('Success', 'Email updated successfully');
        return redirect()->route('my.email');
    }

    public function passwordupdate(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required',
        ]);

        $user = User::find($id);
        $user->password = bcrypt($request->password);
        $user->save();
        Alert::success('Success', 'Password updated successfully');
        return redirect()->route('my.password');
    }


    public function userReports($parent, $child = null)
    {

        $menues = Menues::where('slug', $parent)->first();
        if (!$menues)
            return back()->with('error', 'Menu not found.');

        if ($child) {
            $child_row = Menues::where('slug', $child)->first();
            if (!$child_row)
                return back()->with('error', 'Sub menu not found.');
        }

        if (in_array($child, config('constant.fix_slugs')))
            return $this->fixSlugs($child);

        $results_per_page = 50;
        $result = DB::table('dividend_reports');
        if (isset($child_row))
            //dd($child_row->report_category);
            $result = $result->where('report_id', $child_row->report_category);
        // dd($result->dump());
        $result = $result->where('deleted_at', null)->paginate(10);

        if (!isset($_GET['page']))
            $page = 50;
        else
            $page = $_GET['page'];

        $page_first_result = ($page - 1) * $results_per_page;

        $result = DB::table('dividend_reports');

        if (isset($child_row))
            if ($parent == 'stock-advisory') {
                $slugdata = Sectors::where('slug', $child)->first();

                $result = $result->where('dividend_reports.report_id', $child_row->report_category);
                $result = $result->leftJoin('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code')->orderBy('dividend_reports.schedule', 'desc')->orderBy('dividend_reports.created_at', 'desc')->orderBy('dividend_reports.report_no', 'desc');
            }
        if ($parent == 'opportunity-stock') {
            $slugdata = ReportCategories::where('slug', $child)->first();

            $result = $result->where('dividend_reports.reportcategory', $slugdata->id);
            $result = $result->leftJoin('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code')->orderBy('dividend_reports.schedule', 'desc')->orderBy('dividend_reports.created_at', 'desc')->orderBy('dividend_reports.report_no', 'desc');
        }
        if ($parent == 'sector-specific') {

            $slugdata = Sectors::where('slug', $child)->first();
            //dd($slugdata);
            $result = $result->leftJoin('companies', 'companies.asx_code', '=', 'dividend_reports.asx_code')->where('companies.sector_id', @$slugdata->id)->orderBy('dividend_reports.schedule', 'desc')->orderBy('dividend_reports.created_at', 'desc')->orderBy('dividend_reports.report_no', 'desc');
        }
        // print_r($result);die;
        $result = $result->select('dividend_reports.*')->orderBy('dividend_reports.schedule', 'desc')->orderBy('dividend_reports.created_at', 'desc')->orderBy('dividend_reports.report_no', 'desc');

        $result = $result->where('dividend_reports.deleted_at', null)->paginate(10);

        $clentData = Client::where('email', @Auth::user()->email)->first();
        // print_r($clentData);die;
        $dateTime = date('Y-m-d H:i:s');
        $reportAccess = ClientDetailsPlan::with('report')->where('client_id', $clentData->id)
            ->where('report_id', @$child_row->report_category)
            ->where('access', 1)
            ->where('subscription_start_date', '<=', $dateTime)
            ->where('subscription_end_date', '>=', $dateTime)
            ->first();

        // dd($reportAccess);

        if (@Auth::user()->user_type == '0') {
            return view('frontend.dashboard.reports', [
                'child' => $child ? $child : $parent,
                'parent' => $parent,
                'results' => $result,
                'page' => $page,
                'child_row' => $child_row
            ]);
        } elseif ($reportAccess) {
            return view('frontend.dashboard.reports', [
                'child' => $child ? $child : $parent,
                'parent' => $parent,
                'results' => $result,
                'page' => $page,
                'child_row' => $child_row
            ]);
        } else {
            // return redirect()->route('subscriptionMenu');
            return redirect()->route('registration.login');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
