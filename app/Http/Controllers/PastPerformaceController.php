<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PastPerformace;
use DB;
use Hash;
use Auth;
use Session;
use Yajra\DataTables\DataTables;

class PastPerformaceController extends Controller
{
    // function __construct()
    // {
    //     $this->middleware('permission:manage-past-performace|create-past-performace|edit-past-performace|delete-past-performace', ['only' => ['index', 'show']]);
    //     $this->middleware('permission:create-past-performace', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:edit-past-performace', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:delete-past-performace', ['only' => ['destroy']]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.past-performace.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.past-performace.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'template'    => 'required', 
        ]);

        $menues = PastPerformace::create([
            'template'     => $request->template,
        ]);
        return redirect()->route('past-performace.index')
            ->with('success', 'past-performace created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $role = PastPerformace::where('id', $id)->first();
        return view('admin.past-performace.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'template'    => 'required',
        ]);

        $menues = PastPerformace::where('id', $id)->update([
            'template'     => $request->template,
        ]);

        return redirect()->route('past-performace.index')
            ->with('success', 'past-performace updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = PastPerformace::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('past-performace.index')
            ->with('success', 'past-performace deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = PastPerformace::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-past-performace')) {
                    $action .= '<a href="' . route("past-performace.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-past-performace')) {
                    $action .= '<a href="' . route("past-performace.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
  