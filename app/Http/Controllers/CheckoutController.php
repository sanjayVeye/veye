<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Checkout;
use DB;
use Hash;
use Auth;
use Session;
use Yajra\DataTables\DataTables;

class CheckoutController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-checkout|create-checkout|edit-checkout|delete-checkout', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-checkout', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-checkout', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-checkout', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.checkout.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.checkout.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'template'    => 'required', 
        ]);

        $menues = Checkout::create([
            'template'     => $request->template,
        ]);
        return redirect()->route('checkout.index')
            ->with('success', 'checkout created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data  = Checkout::where('id', $id)->first();
        return view('admin.checkout.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'template'    => 'required',
        ]);

        $menues = Checkout::where('id', $id)->update([
            'template'     => $request->template,
        ]);

        return redirect()->route('checkout.index')
            ->with('success', 'checkout updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = Checkout::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('checkout.index')
            ->with('success', 'checkout deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = Checkout::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-checkout')) {
                    $action .= '<a href="' . route("checkout.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-checkout')) {
                    $action .= '<a href="' . route("checkout.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
  