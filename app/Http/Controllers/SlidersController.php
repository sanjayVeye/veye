<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Reports;
use App\Models\Sliders;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class SlidersController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-sliders|create-sliders|edit-sliders|delete-sliders', ['only' => ['index','show']]);
        $this->middleware('permission:create-sliders', ['only' => ['create','store']]);
        $this->middleware('permission:edit-sliders', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-sliders', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.sliders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.sliders.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required'
        ]);

        $input = $request->all();
        if ($request->hasFile('slider_img')) {
            $image = $request->file('slider_img');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('slider_img')->move(base_path().'/public/uploads/sliders',$imageName);$insdata['slider_img']= $imageName;
            $input['slider_img'] = $imageName;
        }        

        $user = Sliders::create($input);
        // $role=Role::find('7');
        // $user->assignRole($role);
        return redirect()->route('sliders.index')
                        ->with('success','Slider created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Sliders::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = Sliders::where('id',$id)->first();
        return view('admin.sliders.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $user =Sliders::find($id);

        if ($request->hasFile('slider_img')) {
            $image = $request->file('slider_img');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('slider_img')->move(base_path().'/public/uploads/sliders',$imageName);$insdata['slider_img']= $imageName;
            $user->slider_img = $imageName;
        }  

        $user->title = $request->title;
        $user->description = $request->description;
    
        $user->save();
        return redirect()->route('sliders.index')
                        ->with('success','Slider updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Sliders::find($id)->delete();
        $report =Sliders::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('sliders.index')
                        ->with('success','Slider deleted successfully');
    }

    public function datatable(Request $request){
        $users=Sliders::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('image', function ($row) {                     
                            $image ='<img src="'.asset("uploads/sliders/").'/'.$row->slider_img.'" alt="" width="100" height="100">&nbsp;&nbsp;';                        
                        return $image;
                    })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-sliders')){
                            $action .='<a href="'.route("sliders.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-sliders')){
                            $action .='<a href="'.route("sliders.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
