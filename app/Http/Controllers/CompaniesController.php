<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Companies;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use App\Models\AsxType;
use Session;
use Yajra\DataTables\DataTables;

class CompaniesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-companies|create-companies|edit-companies|delete-companies', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-companies', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-companies', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-companies', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        $sectors = Sectors::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        $asxType = AsxType::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        return view('admin.companies.add', compact('reports', 'sectors', 'asxType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'asx_code' => 'required'
        ]);
        $input = $request->all();

        $input['report_type'] = implode(",", $request->report_type);
         $input['code_type']  = $request->asx_type_id;
        $user = Companies::create($input);
        return redirect()->route('companies.index')
            ->with('success', 'Company created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = Companies::find($id);
        $user->group_id = @$request->group_id;
        $user->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Companies::where('id', $id)->first();
        $reports = Reports::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        $sectors = Sectors::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        $asxType = AsxType::where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
        return view('admin.companies.edit', compact('companies', 'reports', 'sectors', 'asxType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'asx_code' => 'required',
        ]);

        $user = Companies::find($id);
        $user->sector_id = $request->sector_id;
        $user->title = $request->title;
        $user->asx_code = $request->asx_code;
        $user->report_type = $request->report_type;
        $user->code_type  = $request->asx_type_id;
        $user->description = $request->description;
        $user->save();
        return redirect()->route('companies.index')
            ->with('success', 'Company updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Companies::find($id)->delete();
        return redirect()->route('companies.index')
            ->with('success', 'Company deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = Companies::with('asxtype')->where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])
            ->addColumn('asx_type', function ($row) {
              $asx_type = @$row->code_type;
                return $asx_type;
            })

            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-companies')) {
                    $action .= '<a href="' . route("companies.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-companies')) {
                    $action .= '<a href="' . route("companies.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
