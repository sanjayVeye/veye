<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\AsxType;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class AsxTypeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-asx-type|create-asx-type|edit-asx-type|delete-asx-type', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-asx-type', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-asx-type', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-asx-type', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.asx-type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.asx-type.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title'    => 'required',
        ]);

        $menues = AsxType::create([
            'title'     => $request->title,
        ]);
        return redirect()->route('asx-type.index')
            ->with('success', 'asx-type created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $role = AsxType::where('id', $id)->first();
        return view('admin.asx-type.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'    => 'required',
        ]);

        $menues = AsxType::where('id', $id)->update([
            'title'     => $request->title,
        ]);

        return redirect()->route('asx-type.index')
            ->with('success', 'asx-type updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = AsxType::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('asx-type.index')
            ->with('success', 'asx-type deleted successfully');
    }




    public function datatable(Request $request)
    {
        $users = AsxType::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-asx-type')) {
                    $action .= '<a href="' . route("asx-type.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-asx-type')) {
                    $action .= '<a href="' . route("asx-type.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
