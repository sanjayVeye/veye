<?php

namespace App\Http\Controllers;

use App\Models\EmailTemplates;
use App\Providers\SendEmail;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function sendOtp($mobile,$otp){
        $smsmessage =urlencode("Dear Customer, Your OTP is ".$otp." Use this OTP to complete your transaction. Thank you. Team Royal Makers.");
        $sms_url1 = "http://sms.osdigital.in/V2/http-api.php?apikey=5lHZy3OikbZrntsk&senderid=RYLMKR&number=".$mobile."&message=".$smsmessage."&format=json";
        file_get_contents($sms_url1);
        return 'true';
    }
    public function sendResetEmail($email, $token){
        $user = DB::table('clients')->where('email', $email)->select('first_name','last_name', 'email')->first();
        $link = url('password/reset').'/'. $token . '?email=' . urlencode($user->email);
        try {
            $template ='<h3>Hi '.$user->first_name.' '.$user->last_name. ',</h3>
                <p>There was a request to change your password!</p>
                <p>If you did not make this request then please ignore this email.</p>
                <p>Otherwise, please click this link to change your password: '.$link.'</p>
            ';
            Event(new SendEmail('Reset your password',$user->email,$template));
            return true;
        } catch (\Exception $e) {
            dd($e);
            return false;
        }
    }

    public function parseEmailTemplate($template_key,$array=[]){
        $parseStr='';
        $templte=EmailTemplates::where('template_key',$template_key)->first();
        if($templte){
            $parseStr= $templte->template;
            foreach($array as $key=>$values){
                $parseStr=str_replace($key,$values,$parseStr);
            }
        }
        return  $parseStr;
    }

}
