<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\Admin\Client;
use App\Models\TemporaryPayment;
use App\Models\admin\ClientDetailsPlan;
use App\Models\admin\ClientSubcriptions;
use App\Models\Cart;
use App\Models\PaymentDetails;
use App\Models\EmailTemplates;
use App\Providers\SendEmail;
use App\Models\User;
use App\Models\Frontend\ClientRegistration;
use App\Models\Admin\ClientDetails;
use Exception;
use Session;
use Stripe;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Stripe\Exception\CardException;
use Stripe\StripeClient;

class PaymentController extends Controller
{

    private $stripe;
    public function __construct()
    {
        $this->stripe = new StripeClient(config('stripe.api_keys.secret_key'));
    }

    public function index()
    {
        return view('frontend.payment');
    }

    public function payment(Request $request)
    {

        // $validator = Validator::make($request->all(), [
        //     'fullName' => 'required',
        //     'cardNumber' => 'required',
        //     'month' => 'required',
        //     'year' => 'required',
        //     'cvv' => 'required'
        // ]);


        // if ($validator->fails()) {
        //     Alert::error('Sorry!', $validator->errors()->first());
        //     return response()->redirectTo('/');
        // }

        // $token = $this->createToken($request);

        // if (!empty($token['error'])) {
        //     Alert::error('Sorry!', $token['error']);
        //     return response()->redirectTo('/');
        // }
        // if (empty($token['id'])) {
        //     Alert::error('Sorry!', 'Payment failed.');
        //     return response()->redirectTo('/');
        // }

        // $charge = $this->createCharge($token['id'], 2000);
        // // dd( $charge['status']);
        // if (!empty($charge) && $charge['status'] == 'succeeded') {
        //     $session_id = Session::getId('product_id');
        //     $cart = Cart::where('session_id', $session_id)->where('delated_at', null)->with(['productName'])->get();
        //     $sum = 0;
        //     $quantites = 0;
        //     foreach ($cart as $v) {
        //         $sum += $v->price;
        //     }
        //     $paymentRecieved = $sum;
        //     $token = $token['id'];

        //     Alert::success('Success', 'Payment completed.');
        //     return view('frontend.your_detail', compact('token', 'paymentRecieved'));
        // } else {
        //     Alert::error('Sorry!', 'Payment failed');
        // }
        // return response()->redirectTo('/');
        // dd($request);
        // $token = $this->createToken($request);

        // $charge = $this->createCharge($token['id'], 2000);

        // if (!empty($charge) && $charge['status'] == 'succeeded') {

        $session_id = Session::getId('product_id');
        $cart = Cart::where('session_id', $session_id)->where('delated_at', null)->with(['productName'])->get();
        $sum = 0;
        $quantites = 0;
        foreach ($cart as $v) {
            $prices = str_replace('$', '', $v->price);
            $sum += $prices;
        }
        $paymentRecieved = $sum;
        // $token = $token['id'];
        $token = $request['stripeToken'];

        Alert::success('Success', 'Payment completed.');
        return view('frontend.your_detail', compact('token', 'paymentRecieved'));
        //     } else {
        //     Alert::error('Sorry!', 'Payment failed');
        // }

    }

    private function createToken($cardData)
    {
        $token = null;
        try {
            $token = $this->stripe->tokens->create([
                'card' => [
                    'number' => $cardData['cardNumber'],
                    'exp_month' => $cardData['month'],
                    'exp_year' => $cardData['year'],
                    'cvc' => $cardData['cvv']
                ]
            ]);
        } catch (CardException $e) {
            $token['error'] = $e->getError()->message;
        } catch (Exception $e) {
            $token['error'] = $e->getMessage();
        }
        return $token;
    }

    private function createCharge($tokenId, $amount)
    {
        $charge = null;
        try {
            $charge = $this->stripe->charges->create([
                'amount' => $amount,
                'currency' => 'aud',
                'source' => $tokenId,
                'description' => 'Veye Service Subscription Payment'
            ]);
        } catch (Exception $e) {
            $charge['error'] = $e->getMessage();
        }
        return $charge;
    }


    public function storeYourPaymentDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullName' => 'required',
            'cardNumber' => 'required',
            'month' => 'required',
            'year' => 'required',
            'cvv' => 'required'
        ]);

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        Stripe\Charge::create ([
            "amount" =>100,
            "currency" => "AUD",
            "source" => $request->stripeToken,
            "description" => "Making test payment." 
        ]);

        // $api_key = '07aa56da04c0a17a963dc24ae3f13753-us17';

        // $userData = User::where('email', $request->email)->first();
        // if ($userData) {
        //     $clientData = ClientRegistration::where('email', $userData->email)->first();
        //     $client_id = @$clientData->id;
        //     $username = $clientData->email;

        //     $client['first_name'] = $clientData->first_name;
        //     $client['last_name'] = $clientData->last_name;
        //     $client['phone'] = $clientData->phone;
        //     $client['page_refrance'] = 0;
        //     $client['ip_address'] = $request->ip();
        //     $client['lat_lng'] = 0;
        //     $client['user_agent'] = 0;
        //     $client['http_connection'] = 0;
        //     $client['note'] = 0;
        //     $client['colorstatus'] = 0;
        //     $client['email'] = $clientData->email;
        //     $client['post_code'] = $clientData->post_code;
        //     $client['subscribe'] = 1;
        //     $last_id = ClientSubcriptions::create($client)->id;
        // } else {
        //     $username = $request->email;

        //     $user['name'] = $request->fname . ' ' . $request->lname;
        //     $user['email'] = $request->email;
        //     $user['mobile'] = $request->phone;
        //     $user['post_code'] = $request->post_code;
        //     $user['password'] = Hash::make($request->fname);
        //     $user['user_type'] = 1;
        //     $user_id = User::create($user)->id;

        //     $client['user_id'] = $user_id;
        //     $client['first_name'] = $request->fname;
        //     $client['last_name'] = $request->lname;
        //     $client['email'] = $request->email;
        //     $client['phone'] = $request->phone;
        //     $client['post_code'] = $request->post_code;
        //     $client['password'] = Hash::make($request->fname);
        //     $client_id = ClientRegistration::create($client)->id;


        //     $subcription['first_name'] = $request->fname;
        //     $subcription['last_name'] = $request->lname;
        //     $subcription['phone'] = $request->phone;
        //     $subcription['page_refrance'] = 0;
        //     $subcription['ip_address'] = $request->ip();
        //     $subcription['lat_lng'] = 0;
        //     $subcription['user_agent'] = 0;
        //     $subcription['http_connection'] = 0;
        //     $subcription['note'] = 0;
        //     $subcription['colorstatus'] = 0;
        //     $subcription['email'] = $request->email;
        //     $subcription['post_code'] = $request->post_code;
        //     $subcription['subscribe'] = 1;
        //     $subcription_id = ClientSubcriptions::create($subcription)->id;
        // }

        // $session_id = Session::getId('product_id');

        // $cart = Cart::where('session_id', $session_id)->where('delated_at', null)->with(['productName'])->get();
        // // dd($cart);
        // $clientinput['client_id'] = $client_id;
        // $clientinput['username'] = $username;
        // $clientinput['password'] = Hash::make('123456');
        // $clientinput['product_name'] = @$cart[0]->name;
        // $clientinput['subscription_start_date'] = date('Y-m-d H:i:s');
        // $month = str_replace(" Months", "", @$cart[0]->name);
        // if ($request->paymentAmountRecieved == '900') {
        //     $dates = strtotime(date('Y-m-d H:i:s'));
        //     $new_dates = strtotime('+ 1 year', $dates);
        // } else {
        //     $dates = strtotime(date('Y-m-d H:i:s'));
        //     $new_dates = strtotime('+ ' . $month . ' months', $dates);
        // }
        // $clientinput['subscription_end_date'] = date('Y-m-d H:i:s', $new_dates);
        // $clientinput['active'] = 1;

        // ClientDetails::create($clientinput);

        // $sum = 0;
        // $quantites = 0;

        // $paymentData = '';
        // $subcriptionData = '';
        // if ($cart) {
        //     foreach ($cart as $v) {
        //         $prices = str_replace('$', '', $v->price);
        //         $sum += $prices;
        //         $quantites += $v->quantity;
        //         $name = $v->name;
        //         $product_id = $v->product_id;

        //         // $input['report_id'] = $product_id;
        //         $input['report_id'] = $request->report_id;
        //         $input['client_id'] = $clientData->id;
        //         $input['client_details_id'] = $last_id;
        //         $input['subscription_start_date'] = date('Y-m-d H:i:s');
        //         $months = str_replace(" Months", "", $v->name);
        //         if ($request->paymentAmountRecieved == '900') {
        //             $date = strtotime(date('Y-m-d H:i:s'));
        //             $new_date = strtotime('+ 1 year', $date);
        //         } else {
        //             $date = strtotime(date('Y-m-d H:i:s'));
        //             $new_date = strtotime('+ ' . $months . ' months', $date);
        //         }
        //         $input['subscription_end_date'] = date('Y-m-d H:i:s', $new_date);
        //         $input['access'] = 1;
        //         $input['comment'] = $request->comment;
        //         $input['report_name'] = $name;
        //         $list_id = ClientDetailsPlan::create($input)->id;

        //         $inputs['client_id'] = $clientData->id;
        //         $inputs['payment_amount'] = $request->paymentAmountRecieved;
        //         $inputs['payment_type'] = 'Online Payment';
        //         $inputs['payment_token'] = $request->paymentToken;


        //         /**send data on mailchimp************/
        //         $postData = json_encode([
        //             "email_address" => $clientData->email ?? '',
        //             "status"        => "subscribed",
        //             "merge_fields"  => [
        //                 "FNAME" => $clientData->first_name ?? '',
        //                 "LNAME" => $clientData->last_name ?? '',
        //                 "PHONE" =>  $clientData->phone ?? '',
        //             ]
        //         ]);

        //         $ch = curl_init();

        //         $ch = curl_init('https://us17.api.mailchimp.com/3.0/lists/' . $list_id . '/members/');
        //         // curl_setopt($ch, CURLOPT_URL,"https://omsspices.com/contact.php");
        //         curl_setopt_array($ch, array(
        //             CURLOPT_POST => TRUE,
        //             CURLOPT_RETURNTRANSFER => TRUE,
        //             CURLOPT_HTTPHEADER => array(
        //                 'Authorization: apikey ' . $api_key,
        //                 'Content-Type: application/json'
        //             ),
        //             CURLOPT_CUSTOMREQUEST => "POST",
        //             CURLOPT_POSTFIELDS => $postData
        //         ));
        //         $response = curl_exec($ch);
        //         $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //         curl_close($ch);

        //         // PaymentDetails::create($inputs);
        //         $paymentData .= '
        //     <tr>
        //     <th>' . $v->price . '</th>
        //     <th>Online Payment</th>
        //     <th>' . $request->paymentToken . '</th>
        //     <th>' . date('Y-m-d H:i:s') . '</th>
        // </tr>';

        //         $subcriptionData .= '
        //     <tr>
        //     <th>' . $name . '</th>
        //     <th>' . date('Y-m-d H:i:s') . '</th>
        //     <th>' . $new_date . '</th>
        //     <th>' . date('Y-m-d H:i:s') . '</th>
        // </tr>';

        //         $cartData = Cart::find($v->id);
        //         @$cartData->delete();
        //     }
        // }


        Alert::success('Success', 'Payment completed.');
        return view('frontend.thankyou');
        // return view('frontend.your_detail');
    }

    public function store_payment_details(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullName' => 'required',
            'cardNumber' => 'required',
            'month' => 'required',
            'year' => 'required',
            'cvv' => 'required'
        ]);

        $api_key = '07aa56da04c0a17a963dc24ae3f13753-us17';

        $clientData = ClientRegistration::where('user_id', Auth::user()->id)->first();
        $client['first_name'] = $clientData->first_name;
        $client['last_name'] = $clientData->last_name;
        $client['phone'] = $clientData->phone;
        $client['page_refrance'] = 0;
        $client['ip_address'] = $request->ip();
        $client['lat_lng'] = 0;
        $client['user_agent'] = 0;
        $client['http_connection'] = 0;
        $client['note'] = 0;
        $client['colorstatus'] = 0;
        $client['email'] = $clientData->email;
        $client['post_code'] = $clientData->post_code;
        $client['subscribe'] = 1;
        $last_id = ClientSubcriptions::create($client)->id;

        $session_id = Session::getId('product_id');
        $cart = Cart::where('session_id', $session_id)->where('delated_at', null)->with(['productName'])->get();

        $clientinput['client_id'] = $clientData->id;
        $clientinput['username'] = $clientData->email;
        $clientinput['password'] = Hash::make('123456');
        $clientinput['product_name'] = $cart[0]->name;
        $clientinput['subscription_start_date'] = date('Y-m-d H:i:s');
        $month = str_replace(" Months", "", $cart[0]->name);
        if ($request->paymentAmountRecieved == '900') {
            $dates = strtotime(date('Y-m-d H:i:s'));
            $new_dates = strtotime('+ 1 year', $dates);
        } else {
            $dates = strtotime(date('Y-m-d H:i:s'));
            $new_dates = strtotime('+ ' . $month . ' months', $dates);
        }
        $clientinput['subscription_end_date'] = date('Y-m-d H:i:s', $new_dates);
        $clientinput['active'] = 1;

        ClientDetails::create($clientinput);

        $sum = 0;
        $quantites = 0;

        $paymentData = '';
        $subcriptionData = '';
        foreach ($cart as $v) {
            $sum += $v->price;
            $quantites += $v->quantity;
            $name = $v->name;
            $product_id = $v->product_id;

            // $input['report_id'] = $product_id;
            $input['report_id'] = $request->report_id;
            $input['client_id'] = $clientData->id;
            $input['client_details_id'] = $last_id;
            $input['subscription_start_date'] = date('Y-m-d H:i:s');
            $months = str_replace(" Months", "", $v->name);
            if ($request->paymentAmountRecieved == '900') {
                $date = strtotime(date('Y-m-d H:i:s'));
                $new_date = strtotime('+ 1 year', $date);
            } else {
                $date = strtotime(date('Y-m-d H:i:s'));
                $new_date = strtotime('+ ' . $months . ' months', $date);
            }
            $input['subscription_end_date'] = date('Y-m-d H:i:s', $new_date);
            $input['access'] = 1;
            $input['comment'] = $request->comment;
            $input['report_name'] = $name;
            $list_id = ClientDetailsPlan::create($input)->id;

            $inputs['client_id'] = $clientData->id;
            $inputs['payment_amount'] = $request->paymentAmountRecieved;
            $inputs['payment_type'] = 'Online Payment';
            $inputs['payment_token'] = $request->paymentToken;


            /**send data on mailchimp************/
            $postData = json_encode([
                "email_address" => $clientData->email ?? '',
                "status"        => "subscribed",
                "merge_fields"  => [
                    "FNAME" => $clientData->first_name ?? '',
                    "LNAME" => $clientData->last_name ?? '',
                    "PHONE" =>  $clientData->phone ?? '',
                ]
            ]);

            $ch = curl_init();

            $ch = curl_init('https://us17.api.mailchimp.com/3.0/lists/' . $list_id . '/members/');
            // curl_setopt($ch, CURLOPT_URL,"https://omsspices.com/contact.php");
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: apikey ' . $api_key,
                    'Content-Type: application/json'
                ),
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $postData
            ));
            $response = curl_exec($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // PaymentDetails::create($inputs);
            $paymentData .= '
            <tr>
            <th>' . $v->price . '</th>
            <th>Online Payment</th>
            <th>' . $request->paymentToken . '</th>
            <th>' . date('Y-m-d H:i:s') . '</th>
        </tr>';

            $subcriptionData .= '
            <tr>
            <th>' . $name . '</th>
            <th>' . date('Y-m-d H:i:s') . '</th>
            <th>' . $new_date . '</th>
            <th>' . date('Y-m-d H:i:s') . '</th>
        </tr>';

            $cartData = Cart::find($v->id);
            $cartData->delete();
        }

        DB::commit();

        $parsedTemplate = $this->parseEmailTemplate(EmailTemplates::PAYMENT_DETAIL_PAGE, [
            '__username__' => $clientData->first_name . ' ' . $clientData->last_name,
            '__emailid__' => $clientData->email,
            '__password__' => '123456',
            '__paymentdetail__' => $paymentData,
            '__subscription__' => $subcriptionData,
        ]);
        Event(new SendEmail('Thankyou for Subscription', $clientData->email, $parsedTemplate));

        $adminPaymentData = '<table cellspacing="0" style="border-collapse:collapse; width:551px">
        <tbody>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Post Code</th>
                <th>Paid Amount</th>
                <th>Payment Type</th>
                <th>Payment Token</th>
                <th>Payment Date(as per sydney standard time)</th>
            </tr>
            <tr>
            <th>' . $clientData->first_name . ' ' . $clientData->last_name . '</th>
            <th>' . $clientData->phone . '</th>
            <th>' . $clientData->email . '</th>
            <th>' . $clientData->post_code . '</th>
            <th>' . $v->price . '</th>
            <th>Online Payment</th>
            <th>' . $request->paymentToken . '</th>
            <th>' . date('Y-m-d H:i:s') . '</th>
        </tr>
        </tbody>
    </table>
       ';
        $adminTemplate = $this->parseEmailTemplate(EmailTemplates::ADMIN_PAYMENT_DETAIL_PAGE, [
            '__paymentdetail__' => $adminPaymentData,
        ]);

        Event(new SendEmail('New Subscription', 'babloowebabridge4120@gmail.com', $adminTemplate));

        Alert::success('Success', 'Payment completed.');
        return view('frontend.thankyou');
        // return view('frontend.your_detail');
    }

    function generateTransactionId($prefix)
    {
        $str_len = 8;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $result = '';
        for ($i = 0; $i < $str_len; $i++) {
            $result .= $characters[mt_rand(0, 61)];
        }
        $transactionId = $prefix . $result;
        return strtolower($transactionId);
    }

    public function notifyPayment(Request $request)
    {
        try {
            // Change not required
            define('PAYPAL_URL', (PAYPAL_SANDBOX == true) ? "https://www.sandbox.paypal.com/cgi-bin/webscr" : "https://www.paypal.com/cgi-bin/webscr");

            $raw_post_data = file_get_contents('php://input');
            error_log(date('[Y-m-d H:i e] ') . "curl response 1: " . $raw_post_data . "" . PHP_EOL, 3, LOG_FILE);
            $raw_post_array = explode('&', $raw_post_data);

            $myPost = array();
            foreach ($raw_post_array as $keyval) {
                $keyval = explode('=', $keyval);
                if (count($keyval) == 2)
                    $myPost[$keyval[0]] = urldecode($keyval[1]);
            }

            // Read the post from PayPal system and add 'cmd'
            $req = 'cmd=_notify-validate';
            foreach ($myPost as $key => $value) {
                $value = urlencode($value);
                $req .= "&$key=$value";
            }

            /*
 * Post IPN data back to PayPal to validate the IPN data is genuine
 * Without this step anyone can fake IPN data
 */
            $paypalURL = PAYPAL_URL;
            $ch = curl_init($paypalURL);
            if ($ch == FALSE) {
                return FALSE;
            }

            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
            curl_setopt($ch, CURLOPT_SSLVERSION, 6);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: company-name'));
            $res = curl_exec($ch);

            // error_log(date('[Y-m-d H:i e] '). "curl response IPN: ".json_encode($_POST)."" . PHP_EOL, 3, LOG_FILE);

            error_log(date('[Y-m-d H:i e] ') . "curl response IPN: " . json_encode($res) . "" . PHP_EOL, 3, LOG_FILE);
            /*
 * Inspect IPN validation result and act accordingly
 * Split response headers and payload, a better way for strcmp
 */
            $tokens = explode("\r\n\r\n", trim($res));
            $res = trim(end($tokens));
            if (strcmp($res, "VERIFIED") == 0 || strcasecmp($res, "VERIFIED") == 0) {

                // Retrieve transaction info from PayPal
                $item_number    = $_POST['item_number'];
                $txn_id         = $_POST['txn_id'];
                $payment_gross     = $_POST['mc_gross'];
                $currency_code     = $_POST['mc_currency'];
                $payment_status = $_POST['payment_status'];
                $payer_email = $_POST['payer_email'];
                $randomID = $_POST['custom'];
                // $on['unique_session_id'] = $_POST['custom'];
                $date = date('Y-m-d H:i:s');

                echo 'Payement Success';
                // error_log(date('[Y-m-d H:i e] '). "Custom variable check: ".json_encode($_POST)."" . PHP_EOL, 3, LOG_FILE);

                // Check if transaction data exists with the same TXN ID
                // $prevPayment = $function->select('temporary_payment', array('id'), 'WHERE unique_session_id = "'.$randomID.'"');

                // if(count($prevPayment) > 0){
                // exit();
                // }else{
                // Insert transaction data into the database
                // $update = $function->BuildAndRunUpdateQuery('temporary_payment', array('txn_id' => $txn_id, 'email' => $payer_email, 'amount' => $payment_gross, 'payment_status' => $payment_status, 'created_at' => $date), array('unique_session_id' => $randomID));

                // $insert = $function->BuildAndRunInsertQuery('temporary_payment', array('payment_token' => $txn_id, 'payment_amount' => $payment_gross, 'payment_type' => $curren cy_code));

                // }

            }
        } catch (\Throwable $th) {
        }
    }
    public function cancelPayment(Request $request)
    {
        // print_r($request->item_name);die;
        try {

            return view('frontend.thankyou');
        } catch (\Throwable $th) {
        }
    }

    public function yourPaymentStatus(Request $request)
    {
        // print_r($request->item_name);die;

    }

    public function returnPayment(Request $request)
    {

        try {
            return view('frontend');
        } catch (\Throwable $th) {
        }
    }
}
