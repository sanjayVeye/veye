<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Menus;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class MenuController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-menu|create-menu|edit-menu|delete-menu', ['only' => ['index','show']]);
        $this->middleware('permission:create-menu', ['only' => ['create','store']]);
        $this->middleware('permission:edit-menu', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-menu', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.menu.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $input = $request->all();

        $user = Menus::create($input);
        // $role=Role::find('7');
        // $user->assignRole($role);
        return redirect()->route('menu.index')
                        ->with('success','Report Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Menus::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = Menus::where('id',$id)->first();
        return view('admin.menu.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $user =Menus::find($id);
        $user->title = $request->title;
    
        $user->save();
        return redirect()->route('menu.index')
                        ->with('success','Report Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Reports::find($id)->delete();
        $report =Menus::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('reports.index')
                        ->with('success','Report deleted successfully');
    }

    public function datatable(Request $request){
        $users=Menus::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-menu')){
                            $action .='<a href="'.route("menu.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-reports')){
                            $action .='<a href="'.route("menu.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
