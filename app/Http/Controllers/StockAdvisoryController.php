<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Reports;
use App\Models\StockAdvisories;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class StockAdvisoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-stock-advisory|create-stock-advisory|edit-stock-advisory|delete-stock-advisory', ['only' => ['index','show']]);
        $this->middleware('permission:create-stock-advisory', ['only' => ['create','store']]);
        $this->middleware('permission:edit-stock-advisory', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-stock-advisory', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.stock-advisory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.stock-advisory.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'position' => 'required'
        ]);

        $input = $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/stock_advisory',$imageName);
            $input['image'] = $imageName;
        }  
        $user = StockAdvisories::create($input);
        // $role=Role::find('7');
        // $user->assignRole($role);
        return redirect()->route('stock-advisory.index')
                        ->with('success','Stock Advisory created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=StockAdvisories::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$data = StockAdvisories::where('id',$id)->first();
        return view('admin.stock-advisory.edit',compact('data','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'position' => 'required',
        ]);

        $user =StockAdvisories::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/stock_advisory',$imageName);$insdata['slider_img']= $imageName;
            $user->image = $imageName;
        }  

        $user->title = $request->title;
        $user->position = $request->position;
        $user->date = $request->date;
        $user->desciption = $request->desciption;
    
        $user->save();
        return redirect()->route('stock-advisory.index')
                        ->with('success','Stock Advisory updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // StockAdvisories::find($id)->delete();
        $report =StockAdvisories::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('stock-advisory.index')
                        ->with('success','Stock Advisory deleted successfully');
    }

    public function datatable(Request $request){
        $users=StockAdvisories::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('image', function ($row) {
                            $image ='<img src="'.asset("uploads/stock_advisory/").'/'.$row->image.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                        return $image;
                    })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-stock-advisory')){
                            $action .='<a href="'.route("stock-advisory.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-stock-advisory')){
                            $action .='<a href="'.route("stock-advisory.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
