<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\FooterMenu;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class FooterController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-footermenu|create-footermenu|edit-footermenu|delete-footermenu', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-footermenu', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-footermenu', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-footermenu', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.footer-menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.footer-menu.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title'    => 'required',
        ]);
        $slug = $request->title;
        function clean($slug)
        {
            $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
            return preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        }
        $slugData = strtolower(clean($slug));
        // dd($request->all());
        $menues = FooterMenu::create([
            'title'     => $request->title,
            'type'     => $request->type,
            'order'     => $request->order,
            'slug'      => $slugData,
        ]);
        return redirect()->route('footermenu.index')
            ->with('success', 'Footer created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $menuData = FooterMenu::where('id', $id)->first();
        return view('admin.footer-menu.edit', compact('menuData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'    => 'required',
        ]);
        $slug = $request->title;
        function cleans($slug)
        {
            $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
            return preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        }
        $slugData = strtolower(cleans($slug));
        $menues = FooterMenu::where('id', $id)->update([
            'title'     => $request->title,
            'order'     => $request->order,
            'type'     => $request->type,
            'slug'      => $request->slug,
        ]);

        return redirect()->route('footermenu.index')
            ->with('success', 'Footer updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = FooterMenu::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('footermenu.index')
            ->with('success', 'Footer deleted successfully');
    }




    public function datatable(Request $request)
    {
        $users = FooterMenu::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->addColumn('type', function ($row) {
                if ($row->type == '1') {
                    $type = 'Important Links';
                } else {
                    $type = 'Quick Links';
                }
                return $type;
            })
            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-footermenu')) {
                    $action .= '<a href="' . route("footermenu.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-footermenu')) {
                    $action .= '<a href="' . route("footermenu.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
