<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PersonalRecommendations;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use App\Providers\SendEmail;
// use DB;
use Event;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class PersonalRecommendationController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-personalrecommendation|create-personalrecommendation|edit-personalrecommendation|delete-sendmail', ['only' => ['index','show']]);
        $this->middleware('permission:create-personalrecommendation', ['only' => ['create','store']]);
        $this->middleware('permission:edit-personalrecommendation', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-personalrecommendation', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.personal-recommendation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.personal-recommendation.add',compact('reports','sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company' => 'required',
            'buy_price'=>'required', 
           
        ]);

        $input = $request->all();

        $user = PersonalRecommendations::create($input);
       
        return redirect()->route('personalrecommendation.index')
                        ->with('success','Personal Recommendation created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=PersonalRecommendations::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$data = PersonalRecommendations::where('id',$id)->first();
        return view('admin.personal-recommendation.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company' => 'required',
            'buy_price'=>'required'
        ]);

        $user =PersonalRecommendations::find($id);
        $user->company = $request->company;
        $user->buy_price = $request->buy_price;
        $user->buy_date = $request->buy_date;
        $user->report_no1 = $request->report_no1;
        $user->sell_price = $request->sell_price;
        $user->sell_date = $request->sell_date;
        $user->report_no_2 = $request->report_no_2;
        $user->gains_losses = $request->gains_losses;
        $user->save();      
        return redirect()->route('personalrecommendation.index')
                        ->with('success','Personal Recommendation updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PersonalRecommendations::find($id)->delete();
        return redirect()->route('personalrecommendation.index')
                        ->with('success','Personal Recommendation deleted successfully');
    }

    public function datatable(Request $request){
        $users=PersonalRecommendations::get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-personalrecommendation')){
                            $action .='<a href="'.route("personalrecommendation.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-personalrecommendation')){
                            $action .='<a href="'.route("personalrecommendation.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
