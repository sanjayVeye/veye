<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Models\Roles;
use App\Models\Faq;
use App\Models\Others\PrivacyPolicies;
use Session;
use Yajra\DataTables\DataTables;
class FaqController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-faq|create-faq|edit-faq|delete-faq', ['only' => ['index','show']]);
        $this->middleware('permission:create-faq', ['only' => ['create','store']]);
        $this->middleware('permission:edit-faq', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-faq', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $roles = Role::all();
        return view('admin.faq.add',compact('roles'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'details' => 'required'
        ]);

        $input = $request->all();

        $user = Faq::create($input);
        return redirect()->route('faq.index')
                        ->with('success','FAQ created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Pages::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$data = Faq::where('id',$id)->first();
       
        return view('admin.faq.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'details' => 'required'
        ]);

        $user =Faq::find($id);
        $user->metatitle = $request->metatitle;
        $user->metakeywords = $request->metakeywords;
        $user->metadescription = $request->metadescription;
        $user->title = $request->title;
        $user->details = $request->details;
        $user->save();      
        return redirect()->route('faq.index')
                        ->with('success','Faq updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq::find($id)->delete();
        return redirect()->route('faq.index')
                        ->with('success','Faq deleted successfully');
    }

    public function datatable(Request $request){
        $users= Faq::get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-faq')){
                            $action .='<a href="'.route("faq.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-faq')){
                            $action .='<a href="'.route("faq.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
