<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Models\Roles;
use App\Models\Homes;
use App\Models\Others\PrivacyPolicies;
use Session;
use Yajra\DataTables\DataTables;
class homesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-homes|create-homes|edit-homes|delete-homes', ['only' => ['index','show']]);
        $this->middleware('permission:create-homes', ['only' => ['create','store']]);
        $this->middleware('permission:edit-homes', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-homes', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.homes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $roles = Role::all();
        return view('admin.homes.add',compact('roles'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        ]);

        $input = $request->all();

        $user = Homes::create($input);
        return redirect()->route('homes.index')
                        ->with('success','homes created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Pages::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$data = Homes::where('id',$id)->first();
       
        return view('admin.homes.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
    
        ]);

        $user =Homes::find($id);
        $user->title = $request->title;
        $user->details = $request->details;
        $user->catagories = $request->catagories;
        $user->metatitle = $request->metatitle;
        $user->metakeywords = $request->metakeywords;
        $user->metadescription = $request->metadescription;
        $user->save();      
        return redirect()->route('homes.index')
                        ->with('success','homes updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        homes::find($id)->delete();
        return redirect()->route('homes.index')
                        ->with('success','homes deleted successfully');
    }

    public function datatable(Request $request){
        $users= Homes::get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-homes')){
                            $action .='<a href="'.route("homes.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-homes')){
                            $action .='<a href="'.route("homes.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
