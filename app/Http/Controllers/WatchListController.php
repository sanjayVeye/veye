<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WatchLists;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class WatchListController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-watchlist|create-watchlist|edit-watchlist|delete-watchlist', ['only' => ['index','show']]);
        $this->middleware('permission:create-watchlist', ['only' => ['create','store']]);
        $this->middleware('permission:edit-watchlist', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-watchlist', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.watchlist.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.watchlist.add',compact('reports','sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'reports_id'=>'required'
        ]);

        $input = $request->all();

        $user = WatchLists::create($input);
        Alert::success('Success', 'Subscribe Page created successfully');
        return redirect()->route('watchlist.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=WatchLists::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$watchlist = WatchLists::where('id',$id)->first();
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.watchlist.edit',compact('watchlist','reports','sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'reports_id'=>'required',
        ]);

        $user =WatchLists::find($id);
        $user->title = $request->title;
        $user->reports_id = $request->reports_id;
        $user->subscribe_title = $request->subscribe_title;
        $user->yearly_description = $request->yearly_description;   
        $user->red_strap_text = $request->red_strap_text; 
        $user->plan_information = $request->plan_information; 
        $user->plan_price = $request->plan_price; 
        $user->sale_price = $request->sale_price; 
        $user->report_type = $request->report_type; 
        $user->display_order = $request->display_order; 
        $user->save();     
        Alert::success('Success', 'Subscribe Page updated successfully'); 
        return redirect()->route('watchlist.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WatchLists::find($id)->delete();
        Alert::success('Success', 'Subscribe Page deleted successfully'); 
        return redirect()->route('watchlist.index');
    }

    public function datatable(Request $request){
        $users=WatchLists::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-watchlist')){
                            $action .='<a href="'.route("watchlist.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-watchlist')){
                            $action .='<a href="'.route("watchlist.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
