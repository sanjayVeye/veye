<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use PDF;
use Auth;
use App\Branch;
use App\Models\Roles;
use App\Models\Reports;
use App\Models\InvoiceOrder;
use App\Models\InvoiceOrderItem;
use Session;
use Yajra\DataTables\DataTables;

class ReportMonthController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-report-month|create-report-month|edit-report-month|delete-report-month', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-report-month', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-report-month', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-report-month', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $a = 24;
        // $bb = (!empty($a))?$a:1;
        // print_r($bb);die;
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.report-month.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /* function used for storing data */

    public function store(Request $request)
    {
        //$this->validate($request, [
        //  'title' => 'required',
        //'details' => 'required'
        //]);
        $orderData = InvoiceOrder::orderBy('id', 'DESC')->first();
        // print_r($orderData->id);die;
        $num = 4075;
        $invo = "2023-" . ($num + $orderData->id + 1);
        $year = "22-23";

        $input['user_id'] = 0;
        $input['username'] = $invo;
        $input['user'] = 0;

        $input['order_total_before_tax'] = 0;
        $input['order_total_tax'] = 0;
        $input['order_tax_per'] = 0;
        $input['order_total_after_tax'] = 0;
        $input['order_amount_paid'] = 0;
        $input['order_total_amount_due'] = 0;
        $input['year'] = $year;

        $input['order_receiver_name'] = $request->order_receiver_name;
        $input['Cust_email'] = $request->Cust_email;
        $input['post_code'] = $request->post_code;
        $input['phone'] = $request->phone;
        $input['mop'] = $request->mop;
        $input['card'] = $request->card;
        $input['digit'] = $request->digit;
        $input['subscription'] = $request->subscription;
        $input['subsstartdate'] = $request->subsstartdate;
        $input['subsenddate'] = $request->subsenddate;
        $input['months'] = $request->months;
        $input['note'] = $request->note;
        $order = InvoiceOrder::create($input);
        $orderId = $order->id;

        foreach ($request->productName as $key => $item) {
            $inputs['order_item_id'] = 0;
            $inputs['order_id'] = $orderId;
            $inputs['username'] = $invo;
            $inputs['item_code'] = 0;
            $inputs['item_name'] = $request->productName[$key];
            $inputs['subsstartdate'] = date('Y-m-d', strtotime($request->subsstartdate[$key]));
            $inputs['subsenddate'] = date('Y-m-d', strtotime($request->subsenddate[$key]));
            $inputs['month'] = $request->month[$key];
            $inputs['order_item_quantity'] = $request->quantity[$key];
            $inputs['order_item_price'] = $request->price[$key];
            $inputs['order_item_final_amount'] = $request->quantity[$key] * $request->price[$key];
            //    print_r($request->subsstartdate[$key]);
            InvoiceOrderItem::create($inputs);
        }
        // die;
        // dd($input);

        // $user = InvoiceOrder::create($input);
        return redirect()->route('report-month.index')
            ->with('success', 'report-month created successfully');
    }


    public function generatePDF($id)
    {
        $datas = InvoiceOrder::find($id);

        $data = [
            'orderid' => $id,
            'order_receiver_name' => $datas->order_receiver_name,
            'username' => $datas->username,
            'cust_email' => $datas->Cust_email,
            'created_at' => $datas->created_at,
            'phone' => $datas->phone,
            'post_code' => $datas->post_code,
            'order_total_after_tax' => $datas->order_total_after_tax,
            'order_total_tax' => $datas->order_total_tax,
            'order_total_before_tax' => $datas->order_total_before_tax,
            'date' => date('m/d/Y')
        ];

        $pdf = PDF::loadView('myPDF', $data);

        return $pdf->download('report-month.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = Pages::find($id);
        $user->group_id = @$request->group_id;
        $user->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Reports::where('id', $id)->first();

        return view('admin.report-month.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'title' => 'required',
    //         'details' => 'required'
    //     ]);

    //     $user = InvoiceOrder::find($id);
    //     $user->metatitle = $request->metatitle;
    //     $user->metakeywords = $request->metakeywords;
    //     $user->metadescription = $request->metadescription;
    //     $user->title = $request->title;
    //     $user->details = $request->details;
    //     $user->save();
    //     return redirect()->route('reports.index')
    //         ->with('success', 'report-month updated successfully');
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reports::find($id)->delete();
        return redirect()->route('report-month.index')
            ->with('success', 'report-month deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = InvoiceOrder::orderBy('id', 'DESC')->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('month', function ($row) {
                $a = (!empty($row->months)) ? $row->months : 1;
                $month = number_format(($row->order_total_before_tax / $a), 2);
                return $month;
            })
            ->addColumn('print', function ($row) {
                $print = '<a href="' . route("report-month.generatePDF", $row->id) . '"><i class="fa fa-print" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                return $print;
            })
            ->addColumn('downloadpdf', function ($row) {
                $downloadpdf = '<a href="' . route("report-month.generatePDF", $row->id) . '"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                return $downloadpdf;
            })

            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('report-month-reports')) {
                    $action .= '<a href="' . route("report-month.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-report-month')) {
                    $action .= '<a href="' . route("report-month.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
