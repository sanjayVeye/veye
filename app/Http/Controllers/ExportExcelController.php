<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use PDF;
use Auth;
use App\Models\Reports;
use App\Models\InvoiceOrder;
use Yajra\DataTables\DataTables;
use App\Models\InvoiceOrderItem;

class ExportExcelController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $a = 24;
        // $bb = (!empty($a))?$a:1;
        // print_r($bb);die;
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.export-excel.index');
    }
 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = InvoiceOrderItem::find($id);
        $user->group_id = @$request->group_id;
        $user->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InvoiceOrderItem::find($id)->delete();
        return redirect()->route('export-excel.index')
            ->with('success', 'export-excel deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = InvoiceOrderItem::orderBy('id', 'DESC')->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('month', function ($row) {
                $a = (!empty($row->months)) ? $row->months : 1;
                $month = number_format(($row->order_total_before_tax / $a), 2);
                return $month;
            })
            ->addColumn('print', function ($row) {
                $print = '<a href=""><i class="fa fa-print" aria-hidden="true"></i></a>&nbsp;&nbsp;';                     
            return $print;
        })
        ->addColumn('unpaidinvoice', function ($row) {
            $unpaidinvoice = '<a href=""><i class="fa fa-ban" aria-hidden="true"></i></a>&nbsp;&nbsp;';                     
        return $unpaidinvoice;
    })

            ->addColumn('action', function ($row) {
                $action = '';
                    $action .= '<a href="' . route("export-excel.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';

                return $action;
            })->make();
    }
}
