<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ClientSubcriptions;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class ClientSubscriptionsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-subscription|create-subscription|edit-subscription|delete-subscription', ['only' => ['index','show']]);
        $this->middleware('permission:create-subscription', ['only' => ['create','store']]);
        $this->middleware('permission:edit-subscription', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-subscription', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.client-subscription.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.client-subscription.add',compact('reports','sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name'=>'required',
            'email'=>'required'
        ]);

        $input = $request->all();

        $user = ClientSubcriptions::create($input);
        return redirect()->route('subscription.index')
                        ->with('success','Client subscription created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=ClientSubcriptions::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$subscriptions = ClientSubcriptions::where('id',$id)->first();
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.client-subscription.edit',compact('subscriptions','reports','sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name'=>'required',
        ]);

        $user =ClientSubcriptions::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;   
        $user->post_code = $request->post_code; 
        $user->note = $request->note;
        $user->save();      
        return redirect()->route('subscription.index')
                        ->with('success','Client subscription updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ClientSubcriptions::find($id)->delete();
        return redirect()->route('subscription.index')
                        ->with('success','Client subscription deleted successfully');
    }

    public function datatable(Request $request){
        $users=ClientSubcriptions::where('deleted_at',NULL)->orderBy('id', 'DESC')->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-subscription')){
                            $action .='<a href="'.route("subscription.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-subscription')){
                            $action .='<a href="'.route("subscription.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
