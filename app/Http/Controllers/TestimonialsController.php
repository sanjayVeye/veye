<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Testimonials;
use App\Models\Articles;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class TestimonialsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-testimonials|create-testimonials|edit-testimonials|delete-testimonials', ['only' => ['index','show']]);
        $this->middleware('permission:create-testimonials', ['only' => ['create','store']]);
        $this->middleware('permission:edit-testimonials', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-testimonials', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.testimonials.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.testimonials.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'company_name' => 'required'
        ]);

        $input = $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Test_Management',$imageName);
            $input['image'] = $imageName;
        } 

        $user = Testimonials::create($input);
        Alert::success('Success', 'testimonials created successfully');
        return redirect()->route('testimonials.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Testimonials::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = Testimonials::where('id',$id)->first();
        return view('admin.testimonials.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'desciption' => 'required',
        ]);

        $user =Testimonials::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Test_Management',$imageName);$insdata['slider_img']= $imageName;
            $user->image = $imageName;
        }  

        $user->title = $request->title;
        $user->company_name = $request->company_name;
        $user->desciption = $request->desciption;
    
        $user->save();
        Alert::success('Success', 'testimonials updated successfully');
        return redirect()->route('testimonials.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Blogs::find($id)->delete();
        $report =Testimonials::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        Alert::success('Success', 'testimonials deleted successfully');
        return redirect()->route('testimonials.index');
    }

    public function datatable(Request $request){
        $users=Testimonials::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('image', function ($row) {
                            $image ='<img src="'.asset("uploads/Test_Management/").'/'.$row->image.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                        return $image;
                    })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-testimonials')){
                            $action .='<a href="'.route("testimonials.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-testimonials')){
                            $action .='<a href="'.route("testimonials.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
