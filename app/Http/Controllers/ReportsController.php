<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Reports;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class ReportsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-reports|create-reports|edit-reports|delete-reports', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-reports', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-reports', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-reports', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.reports.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.reports.add', compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title'    => 'required',
            'slug'      => 'required',
        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Reports',$imageName);
            $input['image'] = $imageName;
            
        }
        
        $user = Reports::create($input);
        return redirect()->route('reports.index')
            ->with('success', 'Reports created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
        $user = Reports::where('id', $id)->first();
        return view('admin.reports.edit', compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug'      => 'required',
        ]);

        $user =Reports::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Reports',$imageName);$insdata['slider_img']= $imageName;
            $user->image = $imageName;
        }  


        $user->title = $request->title;
        $user->slug = $request->slug;
        $user->description = $request->description;
        $user->meta_title = $request->meta_title;
        $user->meta_keywords = $request->meta_keywords;
        $user->meta_description = $request->meta_description; 
        $user->mailchimp_id = $request->mailchimp_id; 
        $user->save();
        return redirect()->route('reports.index')
            ->with('success', 'Reports updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $reports = Reports::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('reports.index')
            ->with('success', 'Reports deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = Reports::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('image', function ($row) {
                $image ='<img src="'.asset("uploads/Reports/").'/'.$row->image.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
            return $image;
        })

            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-reports')) {
                    $action .= '<a href="' . route("reports.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-reports')) {
                    $action .= '<a href="' . route("reports.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
