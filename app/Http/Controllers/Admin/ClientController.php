<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ClientController extends Controller
{
    function __construct(){
        $this->middleware('permission:manage-clients|create-clients|edit-clients|delete-clients', ['only' => ['index','show']]);
        $this->middleware('permission:create-clients', ['only' => ['create','store']]);
        $this->middleware('permission:edit-clients', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-clients', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request)){
            if($request->has('info'))
                return $this->info_datatable($request);
            else
                return $this->datatable($request);
        }
            
        return view('admin.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'nullable',
            'post_code'=>'required',
            'phone'     =>'required',
            'email'     =>'required|email'
        ]);

        $client=new Client();
        $client->first_name =$request->first_name;
        $client->last_name  =$request->last_name;
        $client->email      =$request->email;
        $client->phone      =$request->phone;
        $client->post_code  =$request->post_code;
        $client->save();

        return redirect()->route('clients.index')->with('success','Created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.clients.edit',[
            'client'=>Client::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'nullable',
            'post_code'=>'required',
            'phone'     =>'required',
            'email'     =>'required|email'
        ]);

        $client=Client::find($id);
        $client->first_name =$request->first_name;
        $client->last_name  =$request->last_name;
        $client->email      =$request->email;
        $client->phone      =$request->phone;
        $client->post_code  =$request->post_code;
        $client->save();

        return redirect()->route('clients.index')->with('success','Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client=Client::find($id);
        $client->delete();

        return redirect()->route('clients.index')->with('success','Deleted successfully.');
    }

    public function datatable($request){
        $users=Client::select('*',DB::raw('concat(first_name," ",last_name) as client_name'));
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->editColumn('created_at',function($row){
                        return date('d-M-Y H:i:s',strtotime($row->created_at));
                    })
                    ->editColumn('updated_at',function($row){
                        return date('d-M-Y H:i:s',strtotime($row->updated_at));
                    })
                    ->addColumn('type',function($row){
                        $action='';
                        if(Auth::user()->can('info-clients')){
                            $action .='<a href="javascript:void(0)" onclick="infoModal('.$row->id.')"><i class="fa fa-info" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('service-clients')){
                            $action .='<a href="javascript:void(0)" onclick="serviceModal('.$row->id.')"><i class="btn btn-success">Service</i></a>';
                        }
                        return $action;
                    })
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-clients')){
                            $action .='<a href="'.route("clients.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-clients')){
                            $action .='<a href="'.route("clients.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })
                    ->make();
    }

    public function info_datatable($request){
        return [];
    }

    public function type_update(Request $request){
        $client=Client::find($request->_client_id);
    
        if($request->has('_is_type_change')){
            $client->client_type=$request->_type;
            $client->save();

            return ['message'=>'Update successfully.'];
        }

        return ['message'=>'success.','client_type'=>$client->client_type];
    }
}
