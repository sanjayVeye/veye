<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Providers\SendEmail;
use App\Models\Admin\Client;
use App\Models\Admin\ClientCredential;
use App\Models\Admin\ClientDetails;
use App\Models\Admin\ClientDetailsPlan;
use App\Models\InvoiceOrder;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Subscriptions;
use App\Models\Products;
use App\Models\Reports;
// use Hash;
use Auth;
use PDF;
use Help; // Important
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class ClientCredentialController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:manage-clients|create-clients|edit-clients|delete-clients', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-clients', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-clients', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-clients', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.client_credentials.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::select(DB::raw('concat(first_name," ",last_name) as client_name'), 'id')
            ->whereNotIn('id', ClientCredential::pluck('client_id')->toArray())->get();
        // $subscriptions = Subscriptions::where('deleted_at',NULL)->get();
        $subscriptions = Reports::where('deleted_at', NULL)->get();
        return view('admin.client_credentials.add', compact('clients', 'subscriptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|email|unique:client_credentials,username',
            'password' => 'required|min:1',
            'client_id' => 'required',
            'start_date' => 'required|date',
            'end_date'  => 'required|date'
        ]);

        DB::beginTransaction();

        $clientData = Client::find($request->client_id);

        try {

            $user = User::create([
                'name' => $clientData->first_name . ' ' . $clientData->last_name,
                'email' => $request->username,
                'password' => Hash::make($request->password),
                'mobile' => $clientData->phone,
                'post_code' => $clientData->post_code,
                'user_type' => 1
            ]);
            $lastId = $user->id;

            ClientCredential::create([
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'client_id' => $request->client_id,
                'subscription_start_date' => date('Y-m-d H:i:s', strtotime($request->start_date)),
                'subscription_end_date' => date('Y-m-d H:i:s', strtotime($request->end_date))
            ]);

            $details = ClientDetails::create([
                'client_id'                 => $request->client_id,
                'product_name'              => $request->product_name,
                'subscription_start_date'   => date('Y-m-d H:i:s', strtotime($request->start_date)),
                'subscription_end_date'     => date('Y-m-d H:i:s', strtotime($request->end_date)),
                'invoice_no'                => $request->invoice_no,
                'amount'                    => $request->amount_paid,
                'comment'                   => $request->comment,
                'product_type_text'         => $request->product_type,
                'report_access'             => strtotime($request->end_date) > strtotime(date('Y-m-d')) ? 1 : 0
            ]);

            Helper::subscribeMailChimp($clientData, '73a463764b', 'unsubscribed');

            foreach ($request->subscribe_id as $id) {

                $report = Reports::find($id);
                ClientDetailsPlan::create([
                    'client_id' => $request->client_id,
                    'client_details_id' => $details->id,
                    'subscription_start_date'   => date('Y-m-d H:i:s', strtotime($request->start_date)),
                    'subscription_end_date'     => date('Y-m-d H:i:s', strtotime($request->end_date)),
                    'access'    => 1,
                    'comment'   => $request->comment,
                    'report_name' => $report->title,
                    'report_key' => $report->unique_key,
                    'report_id' => $report->id
                ]);
                Helper::subscribeMailChimp($clientData, $report->mailchimp_id, 'subscribed');
            }
            DB::commit();
            return redirect()->route('client-credentials.index')
                ->with('success', 'Client created successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('client-credentials.index')
                ->with('error', 'Failed');
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = User::find($id);
        $user->group_id = @$request->group_id;
        $user->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = ClientCredential::where('client_id', $id)->first();
        $userData = User::where('email', $user->username)->first();
        // dd($user->username);
        return view('admin.client_credentials.edit', compact('user', 'userData'));
    }

    public function addProduct($id, Request $request)
    {
        if ($request->ajax())
            return $this->clientDetailsDatatable($request, $id);

        $user = Client::where('id', $id)->first();
        $subscriptions = Reports::where('deleted_at', NULL)->get();
        $products = Products::where('user_id', $user->id)->get();
        $userId = $user->id;
        return view('admin.client_credentials.addMoreProducts', compact('user', 'products', 'subscriptions', 'userId'));
    }

    public function storeProduct(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
        ]);

        // $input = $request->all();
        $input['user_id'] = $request->user_id;
        $input['subscribe_id'] = implode(',', $request->subscribe_id);
        $input['product_name'] = $request->product_name;
        $input['start_date'] = $request->start_date;
        $input['end_date'] = $request->end_date;
        $input['invoice_no'] = $request->invoice_no;
        $input['amount_paid'] = $request->amount_paid;
        $input['product_type'] = $request->product_type;
        $input['comment'] = $request->comment;
        Products::create($input);
        return redirect()->route('client-credentials.addProduct')
            ->with('success', 'Product created successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required|email|unique:client_credentials,username,' . $id,
        ]);

        $client = ClientCredential::find($id);
        $client->username = $request->username;
        if ($request->password) {
            $this->validate($request, [
                'password'  =>  'required|confirmed',
            ]);
            $client->password = Hash::make($request->password);
        }
        $client->save();
        // dd($request->username);
        if ($request->user_id) {
            $user = User::where('email', $request->username)->first();
            $user->email = $request->username;
            $user->password = Hash::make($request->password);
            $user->save();
        } else {
            $input['email'] = $request->username;
            $input['password'] = Hash::make($request->password);
            User::create($input);
        }
        return redirect()->route('client-credentials.index')
            ->with('success', 'Client updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::find($id)->delete();
        return redirect()->route('client-credentials.index')
            ->with('success', 'Client deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = Client::select('*', DB::raw('concat(first_name," ",last_name) as client_name'))->orderBy('created_at', 'desc')
            ->whereIn('id', ClientDetailsPlan::pluck('client_id')->toArray());
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])
            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-clients')) {
                    $action .= '<a href="' . route("client-credentials.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                return $action;
            })
            ->addColumn('access', function ($row) {
                return '<p style="cursor:pointer;"><a href="javascript:void(0)" onclick="clientInfoModal(' . $row->id . ')"><span style="background-color:#00c0ef;padding:0 5px;color:white;">' . $row->all_plan . '</span></a>|<span style="background-color:green;padding:0 5px;color:white;">' . $row->enable_plan . '</span>|<span style="background-color:red;padding:0 5px;color:white;">' . $row->disable_plan . '</span></p>';
            })
            ->addColumn('add_more', function ($row) {
                $add_more = '';
                $add_more .= '<a href="' . route("client-credentials.addProduct", $row->id) . '"><i class="fa fa-user-plus" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';

                return $add_more;
            })->make();
    }

    function invoiceOrder($id)
    {
        $invoiceOrder = InvoiceOrder::where('client_details_id', $id)->first();
        if ($invoiceOrder) {
            $result = $invoiceOrder->id;
        } else {
            $result = '';
        }
        return $result;
    }

    private function clientDetailsDatatable($request, $id)
    {
        $clients = ClientDetails::select('*');
        $clients = $clients->where('deleted_at', NULL);
        if ($request->has('client_pk'))
            $clients = $clients->where('client_id', $request->client_pk);
        else
            $clients = $clients->where('client_id', $id);

        $clients = $clients->orderBy('created_at', 'desc');
        return DataTables::of($clients)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->addColumn('downloadpdf', function ($row) {
                $downloadpdf = '<a href="' . route("invoiceorder.generatePDFS", $this->invoiceOrder($row->id)) . '"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                return $downloadpdf;
            })
            ->editColumn('sendmail', function ($row) {
                $sendmail = '<a class="btn btn-success" href="javascript:void(0)" onclick="viewSendMailModal(' . $row->id . ',\'' . $row->comment . '\')">Send Mail</a>&nbsp;&nbsp;';
                return $sendmail;
            })
            ->rawColumns(['input' => true, 'html' => true])
            ->editColumn('subscription_start_date', function ($row) {
                return date('Y-m-d', strtotime($row->subscription_start_date));
            })
            ->editColumn('subscription_end_date', function ($row) {
                return date('Y-m-d', strtotime($row->subscription_end_date));
            })
            ->addColumn('reports', function ($row) {
                $html = '';
                foreach ($row->plans as $row) {
                    $html .= '<li>' . @$row->report->title . '</li>';
                }
                return '<ul>' . $html . '</ul>';
            })
            ->addColumn('invoice', function ($row) {
                $invoice = '<a href="' . route("invoiceorder.createinvoice", $row->id) . '"><i class="fa fa-plus" style="color:red;font-size: 24px;"></i></a>';
                return $invoice;
            })
            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-clients')) {
                    $action .= '<a onclick="callOthers(' . $row->id . ')" href="javascript:void(0)"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-clients')) {
                    $action .= '<a href="' . route("client-credentials.deleteProduct", $row->id) . '" onclick="confirmation()"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })
            ->editColumn('comment', function ($row) {
                $add_more = '';
                $add_more .= '<a class="btn btn-success" href="javascript:void(0)" onclick="viewCommentModal(' . $row->id . ',\'' . $row->comment . '\')">View More</a>&nbsp;&nbsp;';
                return $add_more;
            })->make();
    }

    public function deleteProduct($id)
    {
        // dd($id);
        $menues = ClientDetails::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->back()->with('success', 'register deleted successfully');
    }

    public function editProduct(Request $request)
    {
        $client = ClientDetails::find($request->client_detail_id);
        $client->comment = $request->comment;
        $client->save();

        return back()->with('success', 'Updated successfully');
    }

    public function addMoreOther(Request $request)
    {
        $user = Client::where('id', $request->client_id)->first();
        $subscriptions = Reports::where('deleted_at', NULL)->get();
        $products = Products::where('user_id', $user->id)->get();
        $userId = $user->id;

        $clientDetails = null;
        if ($request->client_details_id)
            $clientDetails = ClientDetails::find($request->client_details_id);
        return view('admin.client_credentials.subscription', compact('user', 'products', 'subscriptions', 'userId', 'clientDetails'));
    }

    public function manageAddMore(Request $request)
    {
        $this->validate($request, [
            'subscribe_id' => 'required',
            'start_date' => 'required|date',
            'end_date'  => 'required|date',
            'client_id' => 'required'
        ]);

        DB::beginTransaction();

        try {
            $arr = [
                'client_id'                 => $request->client_id,
                'product_name'              => $request->product_name,
                'subscription_start_date'   => date('Y-m-d H:i:s', strtotime($request->start_date)),
                'subscription_end_date'     => date('Y-m-d H:i:s', strtotime($request->end_date)),
                'invoice_no'                => $request->invoice_no,
                'amount'                    => $request->amount_paid,
                'comment'                   => $request->comment,
                'product_type_text'         => $request->product_type,
                'report_access'             => strtotime($request->end_date) > strtotime(date('Y-m-d')) ? 1 : 0
            ];

            $details_pk = 0;
            if ($request->has('client_details_pk')) {
                ClientDetails::where('id', $request->client_details_pk)
                    ->update($arr);
                $details_pk = $request->client_details_pk;
            } else {
                $details = ClientDetails::create($arr);
                $details_pk = $details->id;
            }

            ClientDetailsPlan::where([
                'client_id' => $request->client_id,
                'client_details_id' => $details_pk,
            ])->delete();

            foreach ($request->subscribe_id as $id) {

                $report = Reports::find($id);
                ClientDetailsPlan::create([
                    'client_id' => $request->client_id,
                    'client_details_id' => $details_pk,
                    'subscription_start_date' => date('Y-m-d H:i:s', strtotime($request->start_date)),
                    'subscription_end_date' => date('Y-m-d H:i:s', strtotime($request->end_date)),
                    'access' => strtotime($request->end_date) > strtotime(date('Y-m-d')) ? 1 : 0,
                    'comment' => $request->comment,
                    'report_name' => $report->title,
                    'report_key' => $report->unique_key,
                    'report_id' => $report->id
                ]);
            }
            DB::commit();
            return back()->with('success', 'Client updated successfully');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return back()->with('error', 'Failed');
        }
    }

    public function clientInfo(Request $request)
    {

        $client = ClientDetailsPlan::with('report')->where('client_id', $request->id)->get();
        // dd($client);
        //  return response()->json($client);
        $html = '';
        foreach ($client as $v) {
            if ($v->access == 0) {
                $access = '<button class="btn btn-xs btn-danger report_access" id="' . $v->id . '" data-id="' . $v->id . '" data-value="1" style="padding:0px auto;font-size: 15px;margin:0px;"><i class="fa fa-toggle-off text-white" style="padding-top: 2.5px;"></i> <small>Deny</small></button>';
            } elseif ($v->access == 1) {
                $access = '<button class="btn btn-xs btn-success report_access" id="' . $v->id . '" data-id="' . $v->id . '" data-value="0" style="padding:0px auto;font-size: 15px;margin:0px;"><i class="fa fa-toggle-on text-white" style="padding-top: 2.5px;"></i> <small>Grant</small></button>';
            }
            date_default_timezone_set('Asia/Kolkata');
            $currentDate = date('Y-m-d h:i:s');
            if (date("d-M-Y", strtotime($currentDate)) < date("d-M-Y", strtotime($v->subscription_start_date))) {
                $exStatus = '<p style="color:green;">Extension</p>';
            } else {
                $exStatus = '';
            }

            $html .= '<tr>
        <td>' . $v->report->title . '' . $exStatus . '</td> 
        <td>' . date("d-M-Y", strtotime($v->subscription_start_date)) . '</td>
        <td>' . date("d-M-Y", strtotime($v->subscription_end_date)) . '</td>
        <td>
            ' . $access . '
        </td>
        <td><textarea class="form-control" name="comment" id="comment' . $v->id . '">' . $v->comment . '</textarea></td>
        <td><input type="submit" name="update_comment" class="btn btn-info update_comment" data-id="' . $v->id . '"></td>
    </tr>';
        }
        echo $html;
    }

    public function updateComment(Request $request)
    {
        $client = ClientDetailsPlan::find($request->rowid);
        $client->comment = $request->comment;
        $aa = $client->save();
        return $aa;
    }

    public function reportAccess(Request $request)
    {
        $client = ClientDetailsPlan::find($request->id);
        $client->access = $request->value;
        $aa = $client->save();
        if ($aa == 1) {
            $clientDetail = ClientDetailsPlan::with('report')->where('client_id', $client->client_id)->get();
            $html = '';
            foreach ($clientDetail as $v) {
                $clientData = Client::find($v->client_id);
                $report = Reports::find($v->report_id);
                if ($v->access == 0) {
                    Helper::subscribeMailChimp($clientData, '73a463764b', 'subscribed');
                    Helper::subscribeMailChimp($clientData, $report->mailchimp_id, 'unsubscribed');
                    $access = '<button class="btn btn-xs btn-danger report_access" id="' . $v->id . '" data-id="' . $v->id . '" data-value="1" style="padding:0px auto;font-size: 15px;margin:0px;"><i class="fa fa-toggle-off text-white" style="padding-top: 2.5px;"></i> <small>Deny</small></button>';
                } elseif ($v->access == 1) {
                    $access = '<button class="btn btn-xs btn-success report_access" id="' . $v->id . '" data-id="' . $v->id . '" data-value="0" style="padding:0px auto;font-size: 15px;margin:0px;"><i class="fa fa-toggle-on text-white" style="padding-top: 2.5px;"></i> <small>Grant</small></button>';
                }
                $html .= '<tr>
                <td>' . $v->report->title . '</td>
                <td>' . date("d-M-Y", strtotime($v->subscription_start_date)) . '</td>
                <td>' . date("d-M-Y", strtotime($v->subscription_end_date)) . '</td>
                <td>
                    ' . $access . '
                </td>
                <td><textarea class="form-control" name="comment" id="comment' . $v->id . '">' . $v->comment . '</textarea></td>
                <td><input type="submit" name="update_comment" class="btn btn-info update_comment" data-id="' . $v->id . '"></td>
            </tr>';
            }
            return $html;
        }
    }


    public function invoiceSend(Request $request)
    {

        $clientDetail = ClientDetails::where('id', $request->client_id)->first();
        $user = Client::where('id', $clientDetail->client_id)->first();

        $invoiceData= InvoiceOrder::where('client_details_id',$clientDetail->id)->where('unpaid',$request->invoice_type)->first();

        $data = [
            'orderid' => $invoiceData->id,
            'order_receiver_name' => $invoiceData->order_receiver_name,
            'username' => $invoiceData->username,
            'cust_email' => $invoiceData->Cust_email,
            'created_at' => $invoiceData->created_at,
            'phone' => $invoiceData->phone,
            'post_code' => $invoiceData->post_code,
            'order_total_after_tax' => $invoiceData->order_total_after_tax,
            'order_total_tax' => $invoiceData->order_total_tax,
            'order_total_before_tax' => $invoiceData->order_total_before_tax,
            'date' => date('m/d/Y')
        ];
                  
        $pdf = PDF::loadView('myPDF', $data);
        // dd($pdf);
        // $attachment = $pdf->download('invoice.pdf');
        $attachment = $pdf->output('invoice.pdf');
        // dd($attachment);

        if ($request->mail_type == 'normal') {

            $templates = '<!doctype html>
        <html lang="en-US">
        <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Normal Invoice</title>
        <meta name="description" content="Payment Template">
        <style type="text/css">
        a:hover {text-decoration: underline !important;}
        </style>
        </head>

        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        <p style="margin-left:0cm; margin-right:0cm; text-align:justify">Hi&nbsp;<strong>' . $user->first_name . ',</strong></p>

        <p>Thanks for upgrading your subscription.Please find attached your fully paid invoice.</p>
        
        <p>You can reach out to your Account Manager directly at&nbsp;<strong>(02) 72019080</strong>&nbsp;or email him on&nbsp;<a href="mailto:assist@veye.com.au" target="_blank">assist@veye.com.au</a>. And, in case his number is busy, please leave a voicemail he will call you back on priority.</p>
        
        <p><strong>Thanks &amp; Regards,</strong></p>
        
        <p>Team Veye</p>
        
        <p>Veye Pty Ltd</p>
        
        <p>Level 21, 207 Kent Street</p>
        
        <p>Sydney, NSW, 2000</p>
        
        <p>Tel.:&nbsp;<strong>(02) 72019080</strong>&nbsp;(D)</p>
        
        <p>Tel.: (02) 9052 4957</p>
        
        <p>Web.:<a href="http://www.veye.com.au/" target="_blank">http://www.veye.com.au</a></p>
        
        <p>Please refer to our&nbsp;<a href="http://www.veye.com.au/fsg.php" target="_blank">www.veye.com.au/fsg.php</a>&nbsp;as per your convenience.</p>
        
        <p><strong>DISCLAIMER</strong>&nbsp;Veye Pty Ltd(ABN 58 623 120 865), holds (AFSL No. 523157 ). All information provided by Veye Pty Ltd through its website, reports, and newsletters is general financial product advice only and should not be considered a personal recommendation to buy or sell any asset or security. Before acting on the advice, you should consider whether it&acirc;&euro;&trade;s appropriate to you, in light of your objectives, financial situation, or needs. You should look at the Product Disclosure Statement or other offer document associated with the security or product before making a decision on acquiring the security or product. You can refer to our Terms &amp; Conditions and Financial Services Guide for more information. Any recommendation contained herein may not be suitable for all investors as it does not take into account your personal financial needs or investment objectives. Although Veye takes the utmost care to ensure accuracy of the content and that the information is gathered and processed from reliable resources, we strongly recommend that you seek professional advice from your financial advisor or stockbroker before making any investment decision based on any of our recommendations. All the information we share represents our views on the date of publishing as stocks are subject to real time changes and therefore may change without notice. Please remember that investments can go up and down and past performance is not necessarily indicative of future returns. We request our readers not to interpret our reports as direct recommendations. To the extent permitted by law, Veye Pty Ltd excludes all liability for any loss or damage arising from the use of this website and any information published (including any indirect or consequential loss, any data loss, or data corruption) (as mentioned on the website&nbsp;<a href="http://www.veye.com.au/" target="_blank">www.veye.com.au</a>), and confirms that the employees and/or associates of Veye Pty Ltd do not hold positions in any of the financial products covered on the website on the date of publishing this report. Veye Pty Ltd hereby limits its liability, to the extent permitted by law to the resupply of services.</p>
        </body>
        </html>';

            Event(new SendEmail('Veye Subscription Invoice - Fully Paid and Login Details', $user->email, $templates, $attachment));
        } elseif ($request->mail_type == 'premium') {

            $templates = '<!doctype html>
            <html lang="en-US">
            <head>
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
            <title>Premium Invoice</title>
            <meta name="description" content="Payment Template">
            <style type="text/css">
            a:hover {text-decoration: underline !important;}
            </style>
            </head>
    
            <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
            <p>Hi&nbsp;<strong>' . $user->first_name . ',</strong></p>

            <p>Thank you for making the payment and subscribing to Veye. We welcome you on board.</p>
            
            <p>Please find attached a copy of your fully paid invoice.</p>
            
            <p>Your Username and password are as follows:</p>
            
            <p>Username:&nbsp;<strong><a href="mailto:jsav.arc@outlook.com" target="_blank">jsav.arc@outlook.com</a></strong></p>
            
            <p>Password:&nbsp;<strong>joanne</strong></p>
            
            <p>If you face any issues, please reach out to us at&nbsp;<strong>(02) 290729924</strong>.</p>
            
            <p><strong>Thanks &amp; Regards,</strong></p>
            
            <p>Team Veye</p>
            
            <p>Veye Pty Ltd</p>
            
            <p>Level 21, 207 Kent Street</p>
            
            <p>Sydney, NSW, 2000</p>
            
            <p>Tel.:&nbsp;<strong>(02) 290729924</strong>&nbsp;(D)</p>
            
            <p>Tel.: (02) 9052 4957</p>
            
            <p>Web.:<a href="http://www.veye.com.au/" target="_blank">http://www.veye.com.au</a></p>
            
            <p>Please refer to our&nbsp;<a href="http://www.veye.com.au/fsg.php" target="_blank">www.veye.com.au/fsg.php</a>&nbsp;as per your convenience.</p>
            
            <p><strong>DISCLAIMER</strong>&nbsp;Veye Pty Ltd(ABN 58 623 120 865), holds (AFSL No. 523157). All information provided by Veye Pty Ltd through its website, reports, and newsletters is general financial product advice only and should not be considered a personal recommendation to buy or sell any asset or security. Before acting on the advice, you should consider whether it&acirc;&euro;&trade;s appropriate to you, in light of your objectives, financial situation, or needs. You should look at the Product Disclosure Statement or other offer document associated with the security or product before making a decision on acquiring the security or product. You can refer to our Terms &amp; Conditions and Financial Services Guide for more information. Any recommendation contained herein may not be suitable for all investors as it does not take into account your personal financial needs or investment objectives. Although Veye takes the utmost care to ensure accuracy of the content and that the information is gathered and processed from reliable resources, we strongly recommend that you seek professional advice from your financial advisor or stockbroker before making any investment decision based on any of our recommendations. All the information we share represents our views on the date of publishing as stocks are subject to real time changes and therefore may change without notice. Please remember that investments can go up and down and past performance is not necessarily indicative of future returns. We request our readers not to interpret our reports as direct recommendations. To the extent permitted by law, Veye Pty Ltd excludes all liability for any loss or damage arising from the use of this website and any information published (including any indirect or consequential loss, any data loss, or data corruption) (as mentioned on the website&nbsp;<a href="http://www.veye.com.au/" target="_blank">www.veye.com.au</a>), and confirms that the employees and/or associates of Veye Pty Ltd do not hold positions in any of the financial products covered on the website on the date of publishing this report. Veye Pty Ltd hereby limits its liability, to the extent permitted by law to the resupply of services.</p>
            </body>
            </html>';

            // Event(new SendEmail('Veye Subscription Invoice - Fully Paid and Login Details', $user->email, $templates,$attachment));
            Event(new SendEmail('Veye Subscription Invoice - Fully Paid and Login Details', $user->email, $templates,$attachment));
        }
        return back()->with('success', 'Invoice Send Mail successfully');
    }
}
