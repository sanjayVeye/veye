<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Reports;
use App\Models\Articles;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class ArticlesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-article|create-article|edit-article|delete-article', ['only' => ['index','show']]);
        $this->middleware('permission:create-article', ['only' => ['create','store']]);
        $this->middleware('permission:edit-article', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-article', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.articles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.articles.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'asx_code' => 'required'
        ]);

        $input = $request->all();
        if ($request->hasFile('article_img')) {
            $image = $request->file('article_img');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('article_img')->move(base_path().'/public/uploads/articles',$imageName);
            $input['article_img'] = $imageName;
        }  
        
        if ($request->hasFile('thumbnail_img')) {
            $image = $request->file('thumbnail_img');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageNames = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('thumbnail_img')->move(base_path().'/public/uploads/thumbnail',$imageNames);
            $input['thumbnail_img'] = $imageNames;
        } 

        $user = Articles::create($input);
        // $role=Role::find('7');
        // $user->assignRole($role);
        return redirect()->route('article.index')
                        ->with('success','Article created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Articles::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = Articles::where('id',$id)->first();
        return view('admin.articles.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'asx_code' => 'required',
        ]);

        $user =Articles::find($id);

        if ($request->hasFile('article_img')) {
            $image = $request->file('article_img');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('article_img')->move(base_path().'/public/uploads/articles',$imageName);$insdata['article_img']= $imageName;
            $user->article_img = $imageName;
        }  

        if ($request->hasFile('thumbnail_img')) {
            $image = $request->file('thumbnail_img');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('thumbnail_img')->move(base_path().'/public/uploads/thumbnail',$imageName);$insdata['thumbnail_img']= $imageName;
            $user->thumbnail_img = $imageName;
        } 

        $user->title = $request->title;
        $user->asx_code = $request->asx_code;
    
        $user->save();
        return redirect()->route('article.index')
                        ->with('success','Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Articles::find($id)->delete();
        $report =Articles::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('article.index')
                        ->with('success','Article deleted successfully');
    }

    public function datatable(Request $request){
        $users=Articles::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('image', function ($row) {
                            $image ='<img src="'.asset("uploads/articles/").'/'.$row->article_img.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                        return $image;
                    })
                    ->addColumn('thumb', function ($row) {
                        $thumb ='<img src="'.asset("uploads/thumbnail/").'/'.$row->thumbnail_img.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                    return $thumb;
                })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-article')){
                            $action .='<a href="'.route("article.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-article')){
                            $action .='<a href="'.route("article.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
