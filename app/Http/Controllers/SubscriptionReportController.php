<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Menus;
use App\Models\Admin\ClientDetailsPlan;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class SubscriptionReportController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-subscription-report|create-subscription-report|edit-subscription-report|delete-subscription-report', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-subscription-report', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-subscription-report', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-subscription-report', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        if ($request->ajax($request))
            return $this->datatable($request);

        return view('admin.subscription-report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     $menues = Menus::where('deleted_at',NULL)->get();
    //     return view('admin.subscription-report.add',compact('menues'));
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $this->validate($request, [
    //         'title' => 'required'
    //     ]);

    //     $input = $request->all();

    //     $user = SubMenu::create($input);
    //     // $role=Role::find('7');
    //     // $user->assignRole($role);
    //     return redirect()->route('subscription-report.index')
    //                     ->with('success','Report Category created successfully');
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = ClientDetailsPlan::find($id);
        $user->group_id = @$request->group_id;
        $user->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = ClientDetailsPlan::where('id', $id)->first();
        return view('admin.subscription-report.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $user = ClientDetailsPlan::find($id);
        $user->title = $request->title;

        $user->save();
        return redirect()->route('subscription-report.index')
            ->with('success', 'Report Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Reports::find($id)->delete();
        $report = ClientDetailsPlan::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');
        $report->save();
        return redirect()->route('subscription-report.index')
            ->with('success', 'Report deleted successfully');
    }

    public function datatable(Request $request)
    {
          

        $search = $_GET['search'];
       // dd($_GET['search']);
        $startFrom = @$_GET['subscription_start_date'];
        $startTo = @$_GET['subscription_start_date_to'];
        $endFrom = @$_GET['subscription_end_date'];
        $endTo = @$_GET['subscription_end_date_to'];
        // $users = ClientDetailsPlan::with(['client','invoice_no']);
     
        $result = DB::table('client_details_plans')
        ->select('client_details_plans.*','client.first_name','client.last_name','client.email','client.phone','invoice_no.invoice_no','invoice_no.product_name','invoice_no.amount');
        $result = $result->Join('clients as client', 'client.id', '=', 'client_details_plans.client_id');
        $result = $result->Join('client_details as invoice_no', 'invoice_no.id', '=', 'client_details_plans.client_details_id');
        if($startFrom!=''){
       
        $result = $result->whereBetween('invoice_no.subscription_start_date', [$startFrom.' 00:00:00',$startTo.' 23:59:59']);
        }
        if($endFrom!=''){ 
                $result = $result->whereBetween('invoice_no.subscription_end_date', [$endFrom.' 00:00:00',$endTo.' 23:59:59']);
        }
        //dd($search);
        if($search!=''){  
                   //$result = $result->orWhere('client.first_name', 'LIKE', $search)->orWhere('client.email', 'LIKE', $search);


            $result = $result->where(function($q) use($search){
                $q->where('client.first_name', 'LIKE', '%'.$search.'%')
            ->orWhere('client.email', 'LIKE', '%'.$search.'%')

            ->orWhere('client.phone', 'LIKE', '%'.$search.'%');
        });
        
        }
        $result->groupBy('client.id');
//dd($result->dump());
        $result = $result->get();
        return DataTables::of($result)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('client_name', function ($row) {
                $client_name = @$row->first_name . ' ' . @$row->last_name;
                return $client_name;
            })

            ->addColumn('email', function ($row) {
                $email = @$row->email;
                return $email;
            })
            ->addColumn('phone', function ($row) {
                $phone = @$row->phone;
                return $phone;
            })

            ->addColumn('invoice_no', function ($row) {
                $invoice_no = @$row->invoice_no;
                return $invoice_no;
            })

            ->addColumn('product_name', function ($row) {
                $product_name = @$row->product_name;
                return $product_name;
            })

            ->addColumn('amount', function ($row) {
                $amount = @$row->amount;
                return $amount;
            })

            ->addColumn('access', function ($row) {
                if ($row->access=='1') {
                    $access = '<button type="button" class="btn btn-primary conv-pop"  data-toggle="modal" data-target="#modal-oldconversion">&nbsp; Yes</button>';
                }else{
                    $access = '<button type="button" class="btn btn-danger conv-pop"  data-toggle="modal" data-target="#modal-oldconversion">&nbsp; No</button>'; 
                }
                return $access;
            })

            ->addColumn('action', function ($row) {
                // $action = '';
                // if (Auth::user()->can('edit-subscription-report')) {
                //     $action .= '<a href="' . route("subscription-report.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                // }
                // if (Auth::user()->can('delete-subscription-report')) {
                //     $action .= '<a href="' . route("subscription-report.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                // }
                // return $action;
            })->make();
    }
}
