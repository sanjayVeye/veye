<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
class AjaxController extends Controller
{
    function populateState($countryID,$selected=null){
        $state=State::where(['CountryID'=>$countryID])->get();
        return view('admin.ajax.state',[
            'states'=>$state,
            'selected'=>$selected
        ]);
    }
}
