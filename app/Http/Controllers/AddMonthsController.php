<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\AddMonths;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class AddMonthsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-add-months|create-add-months|edit-add-months|delete-add-months', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-add-months', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-add-months', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-add-months', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.add-months.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.add-months.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title'    => 'required',
        ]);

        $menues = AddMonths::create([
            'title'     => $request->title,
        ]);
        return redirect()->route('add-months.index')
            ->with('success', 'add-months created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $role = AddMonths::where('id', $id)->first();
        return view('admin.add-months.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'    => 'required',
        ]);

        $menues = AddMonths::where('id', $id)->update([
            'title'     => $request->title,
        ]);

        return redirect()->route('add-months.index')
            ->with('success', 'add-months updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function destroy($id)
    {
        AddMonths::find($id)->delete();
        return redirect()->route('add-months.index')
            ->with('success', 'add-months deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = AddMonths::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-add-months')) {
                    $action .= '<a href="' . route("add-months.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-add-months')) {
                    $action .= '<a href="' . route("add-months.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
