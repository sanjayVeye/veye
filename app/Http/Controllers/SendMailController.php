<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SendMail;
use App\Models\Reports;
use App\Models\Sectors;
use App\Models\EmailTemplates;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use App\Providers\SendEmail;
// use DB;
use Event;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class SendMailController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-sendmail|create-sendmail|edit-sendmail|delete-sendmail', ['only' => ['index','show']]);
        $this->middleware('permission:create-sendmail', ['only' => ['create','store']]);
        $this->middleware('permission:edit-sendmail', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-sendmail', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.send-mail.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.send-mail.add',compact('reports','sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_type' => 'required',
            'email'=>'required'
        ]);

        $input = $request->all();

        $templateData = EmailTemplates::where('template_id','EG784668H')->first();

        $user = SendMail::create($input);
        DB::commit();
        // $templates='<!doctype html>
        // <html lang="en-US">
        // <head>
        // <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        // <title>Reset Password</title>
        // <meta name="description" content="Payment Template">
        // <style type="text/css">
        // a:hover {text-decoration: underline !important;}
        // </style>
        // </head>

        // <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        // <!--100% body table-->
        // <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        // style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: "Open Sans", sans-serif;">
        // <tr>
        // <td>
        // <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
        // align="center" cellpadding="0" cellspacing="0">
        // <tr>
        // <td style="height:80px;">&nbsp;</td>
        // </tr>
        // <tr>
        // <td style="text-align:center;">
        // <a href="" title="logo" target="_blank">
        // <img width="100" src="frontendmain/img/updatelogo.png" title="VEYE" alt="VEYE">
        // </a>
        // </td>
        // </tr>
        // <tr>
        // <td style="height:20px;">&nbsp;</td>
        // </tr>
        // <tr>
        // <td>
        // <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
        // style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
        // <tr>
        // <td style="height:40px;">&nbsp;</td>
        // </tr>
        // <tr>
        // <td style="padding:0 35px;">
        // <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:"Rubik",sans-serif;">You have
        // requested to payment by click on this link</h1>
        // <span
        // style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
        // <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        // Testing.
        // </p>
        // <a href=""
        // style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Click For Payment</a>
        // </td>
        // </tr>
        // <tr>
        // <td style="height:40px;">&nbsp;</td>
        // </tr>
        // </table>
        // </td>
        // <tr>
        // <td style="height:20px;">&nbsp;</td>
        // </tr>
        // <tr>
        // <td style="text-align:center;">
        // <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>veye.com.au</strong></p>
        // </td>
        // </tr>
        // <tr>
        // <td style="height:80px;">&nbsp;</td>
        // </tr>
        // </table>
        // </td>
        // </tr>
        // </table>
        // <!--/100% body table-->
        // </body>
        // </html>';
        Event(new SendEmail('Email Send',$request->email,$templateData->template));
        return redirect()->route('sendmail.index')
                        ->with('success','Send Mail created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=SendMail::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$sendmail = SendMail::where('id',$id)->first();
        return view('admin.send-mail.edit',compact('sendmail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_type' => 'required',
            'email'=>'required'
        ]);

        $user =SendMail::find($id);
        $user->user_type = $request->user_type;
        $user->email = $request->email;
        $user->amount = $request->amount;
        $user->plan = $request->plan;
        $user->save();      
        return redirect()->route('sendmail.index')
                        ->with('success','Send Mail updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SendMail::find($id)->delete();
        return redirect()->route('sendmail.index')
                        ->with('success','Send Mail deleted successfully');
    }

    public function datatable(Request $request){
        $users=SendMail::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-sendmail')){
                            $action .='<a href="'.route("sendmail.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-sendmail')){
                            $action .='<a href="'.route("sendmail.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
