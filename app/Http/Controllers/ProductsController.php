<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subscriptions;
use App\Models\Products;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class ProductsController extends Controller
{
    // function __construct()
    // {
    //     $this->middleware('permission:manage-products|create-products|edit-products|delete-products', ['only' => ['index','show']]);
    //     $this->middleware('permission:create-products', ['only' => ['create','store']]);
    //     $this->middleware('permission:edit-products', ['only' => ['edit','update']]);
    //     $this->middleware('permission:delete-products', ['only' => ['destroy']]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.clients.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
        ]);

        // $input = $request->all();
        $input['user_id'] = $request->user_id;
        $input['subscribe_id'] = implode(',', $request->subscribe_id);
        $input['product_name'] = $request->product_name;
        $input['start_date'] = $request->start_date;
        $input['end_date'] = $request->end_date;
        $input['invoice_no'] = $request->invoice_no;
        $input['amount_paid'] = $request->amount_paid;
        $input['product_type'] = $request->product_type;
        $input['comment'] = $request->comment;
        $user = Products::create($input);  
        return redirect()->route('clients.addProduct',$request->user_id)
                        ->with('success','Product created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Products::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$product = Products::where('id',$id)->first();
        $subscriptions = Subscriptions::where('deleted_at',NULL)->get();
        return view('admin.clients.editProduct',compact('product','subscriptions'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => 'required',
        ]);

        $user =Products::find($id);
        $user->user_id = $request->user_id;
        $user->subscribe_id = implode(',', $request->subscribe_id);
        $user->product_name = $request->product_name;
        $user->start_date = $request->start_date;
        $user->end_date = $request->end_date;
        $user->invoice_no = $request->invoice_no;  
        $user->amount_paid = $request->amount_paid;  
        $user->product_type = $request->product_type;  
        $user->comment = $request->comment;  
        $user->save();
        return redirect()->route('clients.index')
                        ->with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Products::find($id)->delete();
        return redirect()->route('clients.index')
                        ->with('success','Product deleted successfully');
    }

    public function datatable(Request $request){
        $users=Products::where('user_type',1);
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('role',function($row){
                        return (count($row->roles()->pluck('name')) > 0) ? $row->roles()->pluck('name')[0] : '';
                    })
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-clients')){
                            $action .='<a href="'.route("clients.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-clients')){
                            $action .='<a href="'.route("clients.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })
                    ->addColumn('add_more', function ($row) {
                        $add_more='';
                            $add_more .='<a href="'.route("clients.addProduct",$row->id).'"><i class="fa fa-user-plus" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                     
                        return $add_more;
                    })->make();
    }
}
