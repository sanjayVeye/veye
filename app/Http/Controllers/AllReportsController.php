<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\DividendReports;
use App\Models\AsxCodes;
use App\Models\Companies;
use App\Models\Reports;
use App\Models\Sectors;
use App\Models\ReportCategories;
use App\Models\Recommendations;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Hash;
use Helper;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class AllReportsController extends Controller
{
    function __construct()
    {
        // $this->middleware('permission:manage-allreports|create-allreports|edit-allreports|delete-allreports', ['only' => ['index','show']]);
        // $this->middleware('permission:create-allreports', ['only' => ['create','store']]);
        // $this->middleware('permission:edit-allreports', ['only' => ['edit','update']]);
        // $this->middleware('permission:delete-allreports', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // if($request->ajax($request))
        //     return $this->datatable($request);
		if(isset($_GET['rid'])){
			$id = $_GET['rid'];
			$users=DividendReports::where('report_id',$id)->where('deleted_at',NULL)->orderBy('id','desc')->limit(400)->get();
		}
		else{
			
			 $users=DividendReports::where('deleted_at',NULL)->paginate(10);
		}
        
       
        return view('admin.all-reports.index',compact('users'));
    }

    public function searchReports(Request $request)
    {
        $users = DB::table('dividend_reports');
        $title = ''; 
		if(isset($_GET['title'])){
			$title = $_GET['title'];
			$users=$users->where('title', 'LIKE', '%' . $title . '%');
		}
        $asx_code = ''; 
		if(isset($_GET['asx_code'])){
			$asx_code = $_GET['asx_code'];
			$users = $users->where('asx_code', 'LIKE', '%' . $asx_code . '%');
		}
        $users = $users->where('deleted_at', null)->paginate(10);

		// else{			
		// 	 $users=DividendReports::where('deleted_at',NULL)->paginate(10);
		// } 

        return view('admin.all-reports.search_reports',compact('users','title','asx_code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $roles = Role::all();
        $asxCodes = Companies::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $reportsCategory = ReportCategories::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sector = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $recommendationData = Recommendations::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.all-reports.add',compact('roles','asxCodes','reports','reportsCategory','recommendationData','sector'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'report_id' => 'required'
        ]);

        $input = $request->all();
        if ($request->hasFile('attachment')) {
            $attachment = $request->file('attachment');         
            $fileName = $attachment->getClientOriginalName();
            $fileExtension = $attachment->getClientOriginalExtension();
            $attachmentName = time().rand().'.'.$attachment->getClientOriginalExtension();
            $request->file('attachment')->move(base_path().'/public/uploads/reports',$attachmentName);
            $attachmentNames = 'uploads/reports/'.$attachmentName.'';
            $input['attachment'] = $attachmentNames;
        } 

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');         
            $fileName = $thumbnail->getClientOriginalName();
            $fileExtension = $thumbnail->getClientOriginalExtension();
            $thumbnailName = time().rand().'.'.$thumbnail->getClientOriginalExtension();
            $request->file('thumbnail')->move(base_path().'/public/uploads/reports',$thumbnailName);
            $thumbnailNames = 'uploads/reports/'.$thumbnailName.'';
            $input['thumbnail'] = $thumbnailNames;
        } 
        
        $input['recommendation'] = $request->recommendation??'0';
        $input['schedule'] = $request->schedule .' ' . $request->report_time.':00';
       
        if(is_array($request->asx_code)){
            $input['asx_code'] =implode(",", $request->asx_code); 
        }
        else{
            $input['asx_code'] =$request->asx_code;
        }
        $input['report_no']=$request->report_no;
        $lastId = DividendReports::create($input)->id;

        $divireport =DividendReports::find($lastId);    
        $divireport->report_no = 'VEYE-'.$lastId.'';    
        $divireport->save();  
        // $role=Role::find('7');
        // $user->assignRole($role);
        return redirect()->route('allreports.index','rid='.$request->rid.'')
                        ->with('success','Report created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=DividendReports::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$datas = DividendReports::where('id',$id)->first();
        $asxCodes = Companies::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $reportsCategory = ReportCategories::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sector = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $recommendationData = Recommendations::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.all-reports.edit',compact('datas','role','asxCodes','reports','reportsCategory','recommendationData','sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'report_id' => 'required',
        ]);

        $user =DividendReports::find($id);       

        if ($request->hasFile('attachment')) {
            $attachment = $request->file('attachment');         
            $fileName = $attachment->getClientOriginalName();
            $fileExtension = $attachment->getClientOriginalExtension();
            $attachmentName = time().rand().'.'.$attachment->getClientOriginalExtension();
            $request->file('attachment')->move(base_path().'/public/uploads/reports',$attachmentName);
            $attachmentNames = 'uploads/reports/'.$attachmentName.'';
            $user->attachment = $attachmentNames;
        } 

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');         
            $fileName = $thumbnail->getClientOriginalName();
            $fileExtension = $thumbnail->getClientOriginalExtension();
            $thumbnailName = time().rand().'.'.$thumbnail->getClientOriginalExtension();
            $request->file('thumbnail')->move(base_path().'/public/uploads/reports',$thumbnailName);
            $thumbnailNames = 'uploads/reports/'.$thumbnailName.'';
            $user->thumbnail = $thumbnailNames;
        } 
       $asxcode="";
       //dd($request->asx_code);
        if(is_array($request->asx_code)){
            $asxcode=implode(",", $request->asx_code); 
        }
        else{
            $asxcode=$request->asx_code;
        }
        

        $user->title = $request->title;
        $user->slug = $request->slug;
        $user->asx_code =  $asxcode; 
        $user->report_id = $request->report_id;
        $user->report_no = $request->report_no;
        $user->reportcategory = $request->reportcategory;
        $user->schedule = $request->schedule .' ' . $request->report_time.':00';
        $user->report_time = $request->report_time;
        $user->recommendation = $request->recommendation??'0';
        $user->current_price = $request->current_price;
        $user->reporthtml = $request->reporthtml;
    
        $user->save();
       // return back()->with('success','Report updated successfully');

         return redirect()->route('allreports.index','rid='.$request->rid.'')
                        ->with('success','Report updated successfully');
        // return redirect()->route('allreports.index')->with('success','Report updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Reports::find($id)->delete();
        $report =DividendReports::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('allreports.index')
                        ->with('success','Report deleted successfully');
    }

    public function datatable(Request $request){
            $users=DividendReports::where('deleted_at',NULL)->get();
      
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('thumbnail', function ($row) {
                        $image ='<img src="'.asset("uploads/reports/").'/'.$row->thumbnail.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                    return $image;
                })
                //     ->addColumn('thumbnail', function ($row) {
                //         $recommendations =getRecommendationsData($row->recommendation)->title ?? '';                     
                //     return $recommendations;
                // })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-allreports')){
                            $action .='<a href="'.route("allreports.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-allreports')){
                            $action .='<a href="'.route("allreports.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        $action .='<a href="'.route("allreports.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        $action .='<a href="'.route("allreports.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        return $action;
                    })->make();
    }
}
