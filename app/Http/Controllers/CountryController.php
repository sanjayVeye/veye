<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Country;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class CountryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-country|create-country|edit-country|delete-country', ['only' => ['index','show']]);
        $this->middleware('permission:create-country', ['only' => ['create','store']]);
        $this->middleware('permission:edit-country', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-country', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.country.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.country.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Country',$imageName);
            $input['image'] = $imageName;
            
        }

        $user = Country::create($input);
        Alert::success('Success', 'country created successfully');
        return redirect()->route('country.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $sector=Country::find($id);
        $sector->group_id=@$request->group_id;
        $sector->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = country::where('id',$id)->first();
        return view('admin.country.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $user =country::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/Country',$imageName);$insdata['slider_img']= $imageName;
            $user->image = $imageName;
        }  
        
        $user->title = $request->title;
        $user->save();
        Alert::success('Success', 'country updated successfully');
        return redirect()->route('country.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Sectors::find($id)->delete();
        $sector =country::find($id);
        $sector->deleted_at = date('Y-m-d h:m:i');    
        $sector->save();
        return redirect()->route('country.index')
                        ->with('success','country deleted successfully');
                        
    }

    public function datatable(Request $request){
        $users=country::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  

                    ->addColumn('image', function ($row) {
                        $image ='<img src="'.asset("uploads/Country/").'/'.$row->image.'" alt="" width="70" height="50">&nbsp;&nbsp;';                     
                    return $image;
                })

                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-country')){
                            $action .='<a href="'.route("country.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-country')){
                            $action .='<a href="'.route("country.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
