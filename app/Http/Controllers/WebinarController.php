<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Webinar;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;

use Session;
use Yajra\DataTables\DataTables;


class WebinarController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-webinar|create-webinar|edit-webinar|delete-webinar', ['only' => ['index','show']]);
        $this->middleware('permission:create-webinar', ['only' => ['create','store']]);
        $this->middleware('permission:edit-webinar', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-webinar', ['only' => ['destroy']]);
    }
     public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.webinar.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.webinar.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'path'=>'required',
            'reviews'=>'required',
        ]);

        $input = $request->all();

        $user = Webinar::create($input);
        Alert::success('Success', 'Webinar created successfully');
        return redirect()->route('webinar.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Webinar::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$webinar = Webinar::where('id',$id)->first();
        
        return view('admin.webinar.edit',compact('webinar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'path'=>'required',
            'reviews'=>'required',
        ]);

        $user =webinar::find($id);
        $user->title = $request->title;
        $user->path = $request->path;
        $user->reviews = $request->reviews;
        
       
        $user->save();     
        Alert::success('Success', 'Webinar Page updated successfully'); 
        return redirect()->route('webinar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Webinar::find($id)->delete();
        Alert::success('Success', 'Webinar Page deleted successfully'); 
        return redirect()->route('webinar.index');
    }

    public function datatable(Request $request){
        $users=Webinar::where('deleted_at',NULL)->orderBy('id','desc')->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-webinar')){
                            $action .='<a href="'.route("webinar.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-webinar')){
                            $action .='<a href="'.route("webinar.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
