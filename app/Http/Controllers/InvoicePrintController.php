<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Reports;
use App\Models\Sectors;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use PDF;
use Auth;
use App\Branch;
use App\Models\Admin\Client;
use App\Models\Admin\ClientDetails;
use App\Models\Admin\ClientDetailsPlan;
use App\Models\Roles;
use App\Models\Faq;
use App\Models\InvoiceOrder;
use App\Models\InvoiceOrderItem;
use App\Models\Subscriptions;
use App\Models\Others\PrivacyPolicies;
use Session;
use Yajra\DataTables\DataTables;
class InvoicePrintController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-invoiceorder|create-invoiceorder|edit-invoiceorder|delete-invoiceorder', ['only' => ['index','show']]);
        $this->middleware('permission:create-invoiceorder', ['only' => ['create','store']]);
        $this->middleware('permission:edit-invoiceorder', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-invoiceorder', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $a = 24;
        // $bb = (!empty($a))?$a:1;
        // print_r($bb);die;
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.invoiceorder.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function create()
    {
         $roles = Role::all();
         $subscriptions= Subscriptions::all();
        return view('admin.invoiceorder.add',compact('roles','subscriptions'));
        
    }

    public function createinvoice(Request $request,$id)
    {
         $roles = Role::all();
         $subscriptions= Subscriptions::all();
         $clientData= ClientDetails::with('client')->where('id',$id)->first();
         $reportData= ClientDetailsPlan::with('report')->where('client_details_id',$clientData->id)->get();
        
        return view('admin.invoiceorder.add',compact('roles','subscriptions','clientData','reportData'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


   /* function used for storing data */

    public function store(Request $request)
    {
        //$this->validate($request, [
          //  'title' => 'required',
            //'details' => 'required'
        //]);
        $orderData = InvoiceOrder::orderBy('id', 'DESC')->first();
        // print_r($orderData->id);die;
        $num = 4075;
        $invo = "2023-".($num + $orderData->id + 1);
        $year = "22-23";
       
        $input['user_id'] = 0;
        $input['client_details_id'] = $request->client_details_id;
        $input['username'] = $invo;
        $input['user'] = 0;

        $input['order_total_before_tax'] = 0;
        $input['order_total_tax'] = 0;
        $input['order_tax_per'] = 0;
        $input['order_total_after_tax'] = 0;
        $input['order_amount_paid'] = 0;
        $input['order_total_amount_due'] = 0;
        $input['year'] = $year;

        $input['order_receiver_name'] = $request->order_receiver_name;
        $input['Cust_email'] = $request->Cust_email;
        $input['post_code'] = $request->post_code;
        $input['phone'] = $request->phone;
        $input['mop'] = $request->mop;
        $input['card'] = $request->card;
        $input['digit'] = $request->digit;      
        $input['subscription'] = $request->subscription;
        $input['subsstartdate'] = $request->subsstartdate;
        $input['subsenddate'] = $request->subsenddate;
        $input['months'] = $request->months;
        $input['note'] = $request->note;
        $order = InvoiceOrder::create($input);
        $orderId = $order->id;

        foreach($request->productName as $key=>$item){
        $inputs['order_item_id'] = 0;
        $inputs['order_id'] = $orderId;
        $inputs['username'] = $invo;
        $inputs['item_code'] = 0;
        $inputs['item_name'] = $request->productName[$key];
        $inputs['subsstartdate'] = date('Y-m-d', strtotime($request->subsstartdate[$key]));
        $inputs['subsenddate'] = date('Y-m-d', strtotime($request->subsenddate[$key]));
        $inputs['month'] = $request->month[$key];
        $inputs['order_item_quantity'] = $request->quantity[$key];
        $inputs['order_item_price'] = $request->price[$key];
        $inputs['order_item_final_amount'] = $request->quantity[$key]*$request->price[$key];
        //    print_r($request->subsstartdate[$key]);
        InvoiceOrderItem::create($inputs);
     
    }
    // die;
        // dd($input);

        // $user = InvoiceOrder::create($input);
        return redirect()->route('invoiceorder.index')
                        ->with('success','Invoice created successfully');
    }


    public function generatePDFS($id)
    {
        $datas= InvoiceOrder::find($id);

        $data = [
            'orderid' => $id,
            'order_receiver_name' => $datas->order_receiver_name,
            'username' => $datas->username,
            'cust_email' => $datas->Cust_email,
            'created_at' => $datas->created_at,
            'phone' => $datas->phone,
            'post_code' => $datas->post_code,
            'order_total_after_tax' => $datas->order_total_after_tax,
            'order_total_tax' => $datas->order_total_tax,
            'order_total_before_tax' => $datas->order_total_before_tax,
            'date' => date('m/d/Y')
        ];
          
        $pdf = PDF::loadView('myPDF', $data);
    
        return $pdf->download('invoice.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Pages::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $subscriptions= Subscriptions::all();
    	$data = InvoiceOrder::where('id',$id)->first();       
    	$invoceOrder = InvoiceOrderItem::where('order_id',$data->id)->get();       
        return view('admin.invoiceorder.edit',compact('data','roles','subscriptions','invoceOrder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'order_receiver_name' => 'required',
            'Cust_email' => 'required'
        ]);

        $user =InvoiceOrder::find($id);
        $user->order_total_before_tax = !empty($request->order_total_before_tax)?$request->order_total_before_tax:0;
        $user->order_total_tax = !empty($request->order_total_tax)?$request->order_total_tax:0;
        $user->order_total_after_tax = !empty($request->order_total_after_tax)?$request->order_total_after_tax:0;
        $user->order_receiver_name = $request->order_receiver_name;
        $user->Cust_email = $request->Cust_email;
        $user->post_code = $request->post_code;
        $user->phone = $request->phone;
        $user->mop = $request->mop;
        $user->card = $request->card;
        $user->digit = $request->digit;
        $user->subscription = $request->subscription;
        $user->subsstartdate = $request->subsstartdate;
        $user->subsenddate = $request->subsenddate;
        $user->months = $request->months;
        $user->note = $request->note;
        $user->digit = $request->digit;

        $user->save(); 
       
        foreach($request->order_item_id as $key=>$item){
            $item =InvoiceOrderItem::find($item);
            $item->order_id = $request->order_id;
            $item->item_name = $request->productName[$key];
            $item->subsstartdate = date('Y-m-d', strtotime($request->subsstartdate[$key]));
            $item->subsenddate = date('Y-m-d', strtotime($request->subsenddate[$key]));
            $item->month = $request->month[$key];
            $item->order_item_quantity = $request->quantity[$key];
            $item->order_item_price = $request->price[$key];
            $item->order_item_final_amount = $request->quantity[$key]*$request->price[$key];
            $item->save();
         
        }  
        return redirect()->route('invoiceorder.index')
                        ->with('success','Invoice updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq::find($id)->delete();
        return redirect()->route('invoiceorder.index')
                        ->with('success','Invoice deleted successfully');
    }

    public function datatable(Request $request){
        if(@$_GET['unpaid_order']=='unpaid'){
            $users= InvoiceOrder::where('unpaid', '0')->orderBy('id', 'DESC')->get();
        }elseif(@$_GET['unpaid_order']=='paid') {
            $users= InvoiceOrder::where('unpaid', '1')->orderBy('id', 'DESC')->get();
        }else{
            $users= InvoiceOrder::orderBy('id', 'DESC')->get();
        }
       
        return DataTables::of($users)
                    ->addColumn('sno',function($row){
                        $count ='';
                        STATIC $counts=1;
                        $count .=$counts++;
                        $count .= '<a href="'.route("invoiceorder.generatePDFS",$row->id).'"><i class="fa fa-print" aria-hidden="true"></i></a>&nbsp;&nbsp;';
                         return $count;
                        })
                    ->rawColumns(['input'=>true,'html'=>true])

                    ->addColumn('month', function ($row) {
                        $a = (!empty($row->months))?$row->months:1;
                        $month = number_format(($row->order_total_before_tax / $a),2);                     
                    return $month;
                })
                //     ->addColumn('print', function ($row) {
                //         $print = '<a href="'.route("invoiceorder.generatePDFS",$row->id).'"><i class="fa fa-print" aria-hidden="true"></i></a>&nbsp;&nbsp;';                     
                //     return $print;
                // })
                ->addColumn('downloadpdf', function ($row) {
                    $downloadpdf = '<a href="'.route("invoiceorder.generatePDFS",$row->id).'"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>&nbsp;&nbsp;';                     
                return $downloadpdf;
            })
        //     ->addColumn('unpaidinvoice', function ($row) {
        //         $unpaidinvoice = '<a href="'.route("invoiceorder.generatePDFS",$row->id).'"><i class="fa fa-ban" aria-hidden="true"></i></a>&nbsp;&nbsp;';                     
        //     return $unpaidinvoice;
        // })

                 ->addColumn('unpaidinvoice', function ($row) {
                    if($row->unpaid==1){
                $unpaidinvoice = 'Unpaid';                     
                    }elseif($row->unpaid==0){
                        $unpaidinvoice = 'Paid';    
                    }
                return $unpaidinvoice;
        })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-invoiceorder')){
                            $action .='<a href="'.route("invoiceorder.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-invoiceorder')){
                            $action .='<a href="'.route("invoiceorder.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }


    public function togglepaid(Request $request){
        //print_r($request);
         $id = $request->sThisVal;

          $unpaid = $request->unpaid;       


         if($unpaid == "unpaid"){
             foreach($id as $value){
                $value = stripcslashes($value);
                $users= InvoiceOrder::where('id', $value) ->update(['unpaid' => "1"]);
                echo "unpaid";
                echo $value;
         }
         }else{
             foreach($id as $value){
                $value = stripcslashes($value);
                 $users= InvoiceOrder::where('id', $value) ->update(['unpaid' => "0"]);
                   echo "updated id ";
                   echo $value;
             }
         }

        
       
        // if(@$_GET['order']=='unpaid'){
        //     $users= InvoiceOrder::where('id', $value) ->update(['unpaid' => "1"]);
        // }else{
        //    $users= InvoiceOrder::where('id', $value) ->update(['unpaid' => "0"]);
        // }  

       // echo ($user);  

        exit;
       
    }
}
