<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Banner;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class BannerController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-banner|create-banner|edit-banner|delete-banner', ['only' => ['index','show']]);
        $this->middleware('permission:create-banner', ['only' => ['create','store']]);
        $this->middleware('permission:edit-banner', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-banner', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.banner.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required'
        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/BannerAd',$imageName);
            $input['image'] = $imageName;
            
        }

        $user = Banner::create($input);
        Alert::success('Success', 'banner created successfully');
        return redirect()->route('banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $sector=Banner::find($id);
        $sector->group_id=@$request->group_id;
        $sector->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = Banner::where('id',$id)->first();
        return view('admin.banner.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $user =Banner::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/BannerAd',$imageName);$insdata['slider_img']= $imageName;
            $user->image = $imageName;
        }  
        
        $user->title = $request->title;
        $user->slug = $request->slug;
        $user->save();
        Alert::success('Success', 'banner updated successfully');
        return redirect()->route('banner.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Sectors::find($id)->delete();
        $sector =Banner::find($id);
        $sector->deleted_at = date('Y-m-d h:m:i');    
        $sector->save();
        return redirect()->route('banner.index')
                        ->with('success','banner deleted successfully');
                        
    }

    public function datatable(Request $request){
        $users=Banner::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  

                    ->addColumn('image', function ($row) {
                        $image ='<img src="'.asset("uploads/BannerAd/").'/'.$row->image.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                    return $image;
                })

                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-banner')){
                            $action .='<a href="'.route("banner.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-banner')){
                            $action .='<a href="'.route("banner.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
