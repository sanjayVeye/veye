<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Providers\SendOtp;
use Illuminate\Http\Request;
use Event;
//coming data from crm models
use App\Models\Lead;
use App\Models\CustomerProductReport;
use App\Models\Customer;

use Illuminate\Support\Facades\Auth;

class ApiController
{
    public function login(Request $request){
        $validator=validator($request->all(),[
            'email'=>'required|exists:users,email',
            'password'=>'required'
        ]);

        if($validator->fails()){
            return response()->json(['status' => 400, 'errors' => $validator->errors()->toArray()]);
        }

        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            $token=Auth()->user()->createToken(Auth::user()->name)->plainTextToken;
            return response()->json(['status' => 200, 'message' => 'Successfully logged in.','token'=>$token,'user'=>Auth::user(),'is_verified'=>Auth::user()->customers->is_verified]);
        }
        return response()->json(['status' => 400, 'message' => 'Credential not matched our records.']);
    }
    public function userVerified(){
        return response()->json(['status'=>Auth::user()->customers->is_verified ? 200 : 400,'user'=>Auth::user(),'is_verified'=>Auth::user()->customers->is_verified],200);
    }

    public function clientLead(request $request){
       
        $alldata=json_decode($request->data);
	
      
        $product=$alldata->product;
       
        $customer=$alldata->customer;
        //dd($alldata);
         $lead=$alldata->lead;
         // dd($lead[0]);
         $lead=$lead[0];
         $customer=$customer[0];
         //$product=$product[0];
        //dd($lead);
        $leaddata['fname'] = $lead->fname;
        $leaddata['lname'] = $lead->lname;
        $leaddata['email'] = $lead->email;
        $leaddata['phone'] = $lead->phone;
        $leaddata['page_reference'] = $lead->page_reference;
        $leaddata['ip_address'] = $lead->ip_address;
        $leaddata['lat_lng'] = $lead->lat_lng;
        $leaddata['day_time_phone'] = $lead->day_time_phone;
        $leaddata['post_code'] = $lead->post_code;
        $leaddata['user_agent'] = $lead->user_agent;
        $leaddata['http_connection'] = $lead->http_connection;
        $leaddata['comment'] = $lead->comment;
        $leaddata['note'] = $lead->note;
        $leaddata['colorstatus'] = $lead->colorstatus;
        $leaddata['readed'] = $lead->readed;

        $leaddata['subscribe'] = $lead->subscribe;
        $leaddata['lead_comment_id'] = $lead->lead_comment_id;
        $leaddata['status'] = $lead->status;
        $leaddata['rawID'] = $lead->rawID;
        $leaddata['lead_attend_status'] = $lead->lead_attend_status;
        $leaddata['upload_method'] = $lead->upload_method;
        $leaddata['is_crm_updated'] = $lead->is_crm_updated;
        $leaddata['lead_created_at'] = $lead->lead_created_at;
        $leaddata['lead_updated_at'] = $lead->lead_updated_at;
        $leaddata['created_at'] = $lead->created_at;
        $leaddata['updated_at'] = $lead->updated_at;

         $leaddata['type'] = $lead->type;
        $leaddata['term_condition'] = $lead->term_condition;
        $leaddata['consent'] = $lead->consent;
        $leaddata['page_url'] = $lead->page_url;
        $leaddata['external_lead_id'] = $lead->external_lead_id;
        $leaddata['called'] = $lead->called;
        $leaddata['not_interested_on'] = $lead->not_interested_on;

         $leaddata['deal_of_day'] = $lead->deal_of_day;
        $leaddata['report'] = $lead->report;
        $leaddata['last_updated_by'] = $lead->last_updated_by;
        $leaddata['sub_status'] = $lead->sub_status;
        $leaddata['amount'] = $lead->amount;
        $leaddata['months'] = $lead->months;
        $leaddata['latest_comment'] = $lead->latest_comment;

        $leaddata['lead_status'] = $lead->lead_status;
        $leaddata['lead_type'] = $lead->lead_type;
        $leaddata['lead_source'] = $lead->lead_source;
        $leaddata['source'] = $lead->source;
        $leaddata['membership_type'] = $lead->membership_type;
        $leaddata['condition'] = $lead->condition;
        $leaddata['condition_color'] = $lead->condition_color;
        $leaddata['customer_service'] = $lead->customer_service;
        $leaddata['check_date'] = $lead->check_date;
        $leaddata['user_id'] = $customer->user_id;
        $leaddata['covertedtocustomer'] = 0;
        //$leaddata['updatedata'] = $lead->updatedata; 
        // to be used when data from API hit is in update mode (0=new, 1= update)
        
        

        Lead::create($leaddata); 

        foreach($product as $key=>$products){
            //dd($product);
            $productdata['lead_id'] = $products->lead_id;
            $productdata['customer_id'] = $products->customer_id;
            $productdata['productreport_id'] = $products->productreport_id;
            $productdata['start_date'] = $products->start_date;
            $productdata['end_date'] = $products->end_date;
            $productdata['created_at'] = $products->created_at;
            $productdata['updated_at'] = $products->updated_at;

            CustomerProductReport::create($productdata); 
        }


        $customerdata['lead_id'] = $customer->lead_id;
        $customerdata['membership_id'] = $customer->membership_id;
        $customerdata['custom_plan_month'] = $customer->custom_plan_month;
        $customerdata['productreport_id'] = $customer->productreport_id;
        $customerdata['price'] = $customer->price;
        $customerdata['user_id'] = $customer->user_id;
        $customerdata['status'] = $customer->status;
        $customerdata['created_at'] = $customer->created_at;
        $customerdata['updated_at'] = $customer->updated_at;

         Customer::create($customerdata); 
       
 return $customer->lead_id;

        //return response()->json(['status'=>Auth::user()->customers->is_verified ? 200 : 400,'user'=>Auth::user(),'is_verified'=>Auth::user()->customers->is_verified],200);
    }

    public function getLead(request $request){
       
        $lead=Lead::where('id',43)->get();
        $product=CustomerProductReport::where('lead_id',43)->get();
        $customer=Customer::where('lead_id',43)->get();
        $data = array('lead'=>$lead,'product'=>$product,'customer'=>$customer);
        //dd(json_encode($data));



        return redirect('http://localhost/veye/public/api/client-lead?data='.json_encode($data));
        //return response()->json(['status'=>Auth::user()->customers->is_verified ? 200 : 400,'user'=>Auth::user(),'is_verified'=>Auth::user()->customers->is_verified],200);
    }

    public function addlead(Request $request){
        $alldata=json_decode($request->data);
// dd($alldata);
        Lead::create([
            'fname'     => $alldata->name,
            'email'     => $alldata->email,
            'phone'     => $alldata->phone,
            'post_code'     => $alldata->post_code,
            'months'     => 0,
        ]);
        $message = 'contactus created successfully';
        return $message;
    }
    
}
