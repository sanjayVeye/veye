<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ClientType;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class ClientTypeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-client-type|create-client-type|edit-client-type|delete-client-type', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-client-type', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-client-type', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-client-type', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.client-type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.client-type.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title'    => 'required',
        ]);
    
        $menues = ClientType::create([
            'title'     => $request->title,
        ]);
        return redirect()->route('client-type.index')
            ->with('success', 'client-type created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $role = ClientType::where('id', $id)->first();
        return view('admin.client-type.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'    => 'required',
        ]);
       
        $menues = ClientType::where('id', $id)->update([
            'title'     => $request->title,
        ]);

        return redirect()->route('client-type.index')
            ->with('success', 'client-type updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = ClientType::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('client-type.index')
            ->with('success', 'client-type deleted successfully');
    }




    public function datatable(Request $request)
    {
        $users = ClientType::get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])

            ->addColumn('name', function ($row) {
                $name = $row->fname . ' ' . $row->lname;
                return $name;
            })

            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-client-type')) {
                    $action .= '<a href="' . route("client-type.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                if (Auth::user()->can('delete-client-type')) {
                    $action .= '<a href="' . route("client-type.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                }
                return $action;
            })->make();
    }
}
