<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Subscriptions;
use App\Models\Products;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class ClientsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-clients|create-clients|edit-clients|delete-clients', ['only' => ['index','show']]);
        $this->middleware('permission:create-clients', ['only' => ['create','store']]);
        $this->middleware('permission:edit-clients', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-clients', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $subscriptions = Subscriptions::where('deleted_at',NULL)->get();
        return view('admin.clients.add',compact('roles','subscriptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
            'mobile'=>'required',
            'post_code'=>'required'
        ]);

        // $input = $request->all();
        $input['name'] = $request->name;
        $input['email'] = $request->email;
        $input['mobile'] = $request->mobile;
        $input['post_code'] = $request->post_code;
        $input['password'] = Hash::make($request->password);
        $input['user_type'] = '1';
        $user = User::create($input);     
        $role=Role::find('7');
        $user->assignRole($role);

        $product['user_id'] = $user->id;
        $product['subscribe_id'] = $request->subscribe_id;
        $product['product_name'] = $request->product_name;
        $product['start_date'] = $request->start_date;
        $product['end_date'] = $request->end_date;
        $product['invoice_no'] = $request->invoice_no;
        $product['amount_paid'] = $request->amount_paid;
        $product['product_type'] = $request->product_type;
        $product['comment'] = $request->comment;
        Products::create($product); 
        return redirect()->route('clients.index')
                        ->with('success','Client created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=User::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = User::where('id',$id)->first();
        return view('admin.clients.edit',compact('user','role'));
    }

    public function addProduct($id)
    {
    	$user = User::where('id',$id)->first();
    	$subscriptions = Subscriptions::where('deleted_at',NULL)->get();
    	$products = Products::where('user_id',$user->id)->get();
        $userId=$user->id;
        return view('admin.clients.addProducts',compact('user','products','subscriptions','userId'));
    }

    public function storeProduct(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
        ]);

        // $input = $request->all();
        $input['user_id'] = $request->user_id;
        $input['subscribe_id'] = implode(',', $request->subscribe_id);
        $input['product_name'] = $request->product_name;
        $input['start_date'] = $request->start_date;
        $input['end_date'] = $request->end_date;
        $input['invoice_no'] = $request->invoice_no;
        $input['amount_paid'] = $request->amount_paid;
        $input['product_type'] = $request->product_type;
        $input['comment'] = $request->comment;
        $user = Products::create($input);  
        return redirect()->route('clients.addProduct')
                        ->with('success','Product created successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
        ]);

        $user =User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->post_code = $request->post_code;
        $user->user_type = '1';  
        if($request->password){
            $this->validate($request,[
                'password'  =>  'required|confirmed',
                'password_confirmation' => 'required',
            ]);
            $user->password = bcrypt($request->password);
        }  
        $user->save();
        DB::table('model_has_roles')->where('model_id',$user->id)->delete();
        $role=Role::find('7');
        $user->assignRole($role);
        return redirect()->route('clients.index')
                        ->with('success','Client updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('clients.index')
                        ->with('success','User deleted successfully');
    }

    public function datatable(Request $request){
        $users=User::where('user_type',1);
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('role',function($row){
                        return (count($row->roles()->pluck('name')) > 0) ? $row->roles()->pluck('name')[0] : '';
                    })
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-clients')){
                            $action .='<a href="'.route("clients.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-clients')){
                            $action .='<a href="'.route("clients.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })
                    ->addColumn('add_more', function ($row) {
                        $add_more='';
                            $add_more .='<a href="'.route("clients.addProduct",$row->id).'"><i class="fa fa-user-plus" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                     
                        return $add_more;
                    })->make();
    }
}
