<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Auth;
use Spatie\Permission\Models\Role;
use DB;
class PermissionController extends Controller
{
    function __construct(){
        $this->middleware('permission:manage-permission|edit-permission', ['only' => ['show','update']]);
        $this->middleware('permission:edit-permission', ['only' => ['edit','update']]);
    }
    
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aPermission=DB::table('role_has_permissions')
                            ->where('role_id',$id)
                            ->pluck('permission_id')->toArray();

        $pPermission=Permission::get();

        $arr=[];
        foreach($pPermission as $row){
            $isPermission=in_array($row->id,$aPermission) ? 1 : 0;
            array_push($arr,array("id" =>(string)$row->id, "parent" =>($row->parent==0) ? '#': (string)$row->parent  , "text" => $row->title,
                'state'       => array(
                    'opened'    => 1,
                    'disabled'  => 0,
                    'selected'  =>$isPermission
                ),'icon'=> "fa fa-lock" 
            ));
        }
        return view('admin/permission/index',[
            'tree'=>$arr,
            'role'=>Role::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role=Role::find($id);
        $role->syncPermissions($request->permission);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
