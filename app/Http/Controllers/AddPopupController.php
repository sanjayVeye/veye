<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\AddPopup;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;

class AddPopupController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-add-popup|create-add-popup|edit-add-popup|delete-add-popup', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-add-popup', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-add-popup', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-add-popup', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax($request))
            return $this->datatable($request);
        return view('admin.add-popup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.add-popup.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, []);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            $request->file('image')->move(base_path() . '/public/uploads/popup', $imageName);
            $input['image'] = $imageName;
        }

        $user = AddPopup::create($input);
        Alert::success('Success', 'add-popup created successfully');
        return redirect()->route('add-popup.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $sector = AddPopup::find($id);
        $sector->group_id = @$request->group_id;
        $sector->save();
        return response()->json(array('status' => 1, 'message' => 'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
        $user = AddPopup::where('id', $id)->first();
        return view('admin.add-popup.edit', compact('user', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, []);

        $user = AddPopup::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time() . rand() . '.' . $image->getClientOriginalExtension();
            $request->file('image')->move(base_path() . '/public/uploads/popup', $imageName);
            $insdata['slider_img'] = $imageName;
            $user->image = $imageName;
        }
        $user->status = $request->status;

        $user->save();
        Alert::success('Success', 'add-popup updated successfully');
        return redirect()->route('add-popup.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Sectors::find($id)->delete();
        $sector = AddPopup::find($id);
        $sector->deleted_at = date('Y-m-d h:m:i');
        $sector->save();
        return redirect()->route('add-popup.index')
            ->with('success', 'add-popup deleted successfully');
    }

    public function datatable(Request $request)
    {
        $users = AddPopup::where('deleted_at', NULL)->get();
        return DataTables::of($users)
            ->addColumn('sno', function () {
                static $count = 1;
                return $count++;
            })
            ->rawColumns(['input' => true, 'html' => true])


            ->addColumn('image', function ($row) {
                $image = '<img src="' . asset("uploads/popup/") . '/' . $row->image . '" alt="" width="100" height="100">&nbsp;&nbsp;';
                return $image;
            })

            ->addColumn('status', function ($row) {
                if($row->status==1){
                    $status = 'Active';
                }else{
                    $status = 'Inactive'; 
                }
                return $status;
            })

            ->addColumn('action', function ($row) {
                $action = '';
                if (Auth::user()->can('edit-add-popup')) {
                    $action .= '<a href="' . route("add-popup.edit", $row->id) . '"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                }
                // if (Auth::user()->can('delete-add-popup')) {
                //     $action .= '<a href="' . route("add-popup.destroy", $row->id) . '" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                // }
                return $action;
            })->make();
    }
}
