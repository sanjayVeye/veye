<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menues;
use App\Models\Reports;
use Yajra\DataTables\DataTables;
class MenuesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-menues|create-menues|edit-menues|delete-menues', ['only' => ['index','show']]);
        $this->middleware('permission:create-menues', ['only' => ['create','store']]);
        $this->middleware('permission:edit-menues', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-menues', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.menues.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menues = Menues::where('parent', 0)
                    ->orderBy('id', 'desc')
                    ->get();
        return view('admin.menues.add',compact('menues'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|unique:menues,title',
            'slug'      => 'required|unique:menues,slug',
            'parent'    => 'required',
        ]);
        Menues::create([
            'title'     => $request->title,
            'parent'    => $request->parent,
            'order'     => $request->order,
            'slug'      => $request->slug,
            'meta_title'=>$request->meta_title,
            'meta'      =>$request->meta,
            'meta_desc' =>$request->meta_desc
        ]);
        return redirect()->route('menues.index')
                        ->with('success','Menues created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parents   = Menues::where('parent', 0)
                                ->orderBy('id', 'desc')->get();
        $menu= Menues::find($id);
        return view('admin.menues.edit',compact('parents', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'     => 'required|unique:menues,title,'.$id,
            'slug'      => 'required|unique:menues,slug,'.$id,
            'parent'    => 'required',
        ]);

        Menues::where('id',$id)->update([
            'title'     => $request->title,
            'parent'    => $request->parent,
            'order'     => $request->order,
            'slug'      => $request->slug,
            'meta_title'=>$request->meta_title,
            'meta'      =>$request->meta,
            'meta_desc' =>$request->meta_desc
        ]);
        
        return redirect()->route('menues.index')
                        ->with('success','Menues updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $menues = Menues::where('id', $id)->update([
            'deleted_at'     => date('Y-m-d h:m:i'),
        ]);
        return redirect()->route('menues.index')
                        ->with('success','Menues deleted successfully');
    }

    public function show($id,Request $request){
        $menu=Menues::find($id);
        $menu->report_category=$request->report_category;
        $menu->save();

        return ['message'=>'change successfully.'];
    }
    public function datatable(Request $request){
        $menues = Menues::orderBy('id', 'desc')
                        ->withoutTrashed()
                        ->get();

        $reports=Reports::all();
        return DataTables::of($menues)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('category',function($row) use($reports) {
                        $action='';
                        if($row->parent) {
                            $tmp='<option value="">-select-</option>';
                            foreach($reports as $rw){
                                $sel=$row->report_category==$rw->id ? 'selected' :'';
                                $tmp .='<option value="'.$rw->id.'" '.$sel.'>'.$rw->title.'</option>';
                            }
                            $action='<select class="form-control" data-id="'.$row->id.'" onchange="funChangeReport(this)">'.$tmp.'</select>';
                        } 
                        return $action;
                    })
                    ->addColumn('parent',function($row) {
                        if($row->parent == 0) {
                            $parent = "Parent"; 
                        } else {
                            $parent = $row->title;
                        }
                        return $parent;
                    })
                    ->addColumn('action', function ($row) {
                        $action='';
                            $action .='<a href="'.route("menues.edit",$row->id).'" title="edit"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                            $action .='<a href="'.route("menues.destroy",$row->id).'" onclick="return confirmDelete();" title="delete"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        return $action;
                    })
                    ->make(true);
    }
}
