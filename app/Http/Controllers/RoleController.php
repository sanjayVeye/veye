<?php
    
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Yajra\DataTables\DataTables; 
use Auth;
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:manage-roles|create-roles|edit-roles|delete-roles', ['only' => ['index','store']]);
        $this->middleware('permission:create-roles', ['only' => ['create','store']]);
        $this->middleware('permission:edit-roles', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-roles', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        $role = Role::get();
    	return view('admin/role/index')->with(['role'=>$role]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.role.add');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role();
    	$role->name = $request->name;
        $role->guard_name = 'web';
    	$role->save();
    	return redirect()->route('roles.index')->with('success','Role Added successfully!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
    	return view('admin/role/edit')->with(['role'=>$role]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array('name'	=>	$request->name);
    	Role::where('id',$id)->update($data);
    	return redirect()->route('roles.index')->with('success','Edited successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Role::find($id);
    	$delete->delete();
    	return redirect()->route('roles.index')->with('success','Role Deleted successfully!');
    }

    public function datatable(Request $request){
        $roles=Role::all();
         return DataTables::of($roles)
         ->addColumn('sno',function(){STATIC $count=1; return $count++;})
         ->rawColumns(['input'=>true,'html'=>true])
         ->addColumn('permission', function ($row) {
            $action='';
            if(Auth::user()->can('manage-permission')){
                $action .='<a href="'.route("permission.show",$row->id).'">Manage Permission</a>&nbsp;&nbsp;';
            }
            return $action;
        })
         ->addColumn('action', function ($row) {
             $action='';
             if(Auth::user()->can('edit-roles')){
                 $action .='<a href="'.route("roles.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
             }
             if(Auth::user()->can('delete-roles')){
                 $action .='<a href="'.route("roles.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
             }
             return $action;
         })->make();
     }
}