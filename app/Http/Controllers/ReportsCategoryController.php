<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ReportCategories;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class ReportsCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-reports-category|create-reports-category|edit-reports-category|delete-reports-category', ['only' => ['index','show']]);
        $this->middleware('permission:create-reports-category', ['only' => ['create','store']]);
        $this->middleware('permission:edit-reports-category', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-reports-category', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.report-category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.report-category.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title'    => 'required',
            'slug'      => 'required',
        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/editorial',$imageName);
            $input['image'] = $imageName;
            
        }
        
        $user = ReportCategories::create($input);
            return redirect()->route('reports-category.index')
                            ->with('success','Report-Category created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=ReportCategories::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = ReportCategories::where('id',$id)->first();
        return view('admin.report-category.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $user =ReportCategories::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');         
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $request->file('image')->move(base_path().'/public/uploads/editorial',$imageName);$insdata['slider_img']= $imageName;
            $user->image = $imageName;
        }  
        
        $user->title = $request->title;
        $user->slug = $request->slug;
        $user->desciption = $request->desciption;
        $user->meta_title = $request->meta_title;
        $user->meta_keywords = $request->meta_keywords;
        $user->meta_description = $request->meta_description; 

        $user->save();
        Alert::success('Success', 'reports-category updated successfully');
        return redirect()->route('reports-category.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Reports::find($id)->delete();
        $report =ReportCategories::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('reports-category.index')
                        ->with('success','Report deleted successfully');
    }

    public function datatable(Request $request){
        $users=ReportCategories::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])

                    ->addColumn('image', function ($row) {
                        $image ='<img src="'.asset("uploads/editorial/").'/'.$row->image.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                    return $image;
                })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-reports-category')){
                            $action .='<a href="'.route("reports-category.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-reports-category')){
                            $action .='<a href="'.route("reports-category.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
