<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Reports;
use App\Models\VeyeDetails;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use App\Branch;
use App\Model\Roles;
use Session;
use Yajra\DataTables\DataTables;
class VeyeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-veye|create-veye|edit-veye|delete-veye', ['only' => ['index','show']]);
        $this->middleware('permission:create-veye', ['only' => ['create','store']]);
        $this->middleware('permission:edit-veye', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-veye', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
            $veyeDetail = VeyeDetails::first();
        return view('admin.veye-details.add',compact('veyeDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();        
        return view('admin.veye-details.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'veye_acn_no' => 'required',
            'veye_abn_no' => 'required'
        ]);

        $input = $request->all(); 
        $user = VeyeDetails::create($input);
        return redirect()->route('veye.index')
                        ->with('success','Veye Detail created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=VeyeDetails::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::get();
    	$user = VeyeDetails::where('id',$id)->first();
        return view('admin.veye-details.edit',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'veye_acn_no' => 'required',
            'veye_abn_no' => 'required'
        ]);

        $user =VeyeDetails::find($id); 

        $user->veye_acn_no = $request->veye_acn_no;
        $user->veye_abn_no = $request->veye_abn_no;
        $user->veye_afsl_no = $request->veye_afsl_no;
        $user->veye_ar_no = $request->veye_ar_no;
        $user->global_acn_no = $request->global_acn_no;
        $user->global_abn_no = $request->global_abn_no;
        $user->global_afsl_no = $request->global_afsl_no;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->post_code = $request->post_code;
    
        $user->save();
        return redirect()->route('veye.index')
                        ->with('success','Veye Detail updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VeyeDetails::find($id)->delete();
        $report =VeyeDetails::find($id);
        $report->deleted_at = date('Y-m-d h:m:i');    
        $report->save();
        return redirect()->route('article.index')
                        ->with('success','Article deleted successfully');
    }

    public function datatable(Request $request){
        $users=VeyeDetails::where('deleted_at',NULL)->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                    ->addColumn('image', function ($row) {
                            $image ='<img src="'.asset("uploads/articles/").'/'.$row->article_img.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                        return $image;
                    })
                    ->addColumn('thumb', function ($row) {
                        $thumb ='<img src="'.asset("uploads/thumbnail/").'/'.$row->thumbnail_img.'" alt="" width="100" height="100">&nbsp;&nbsp;';                     
                    return $thumb;
                })
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-article')){
                            $action .='<a href="'.route("article.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-article')){
                            $action .='<a href="'.route("article.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
