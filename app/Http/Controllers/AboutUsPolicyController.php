<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Reports;
use App\Models\Sectors;
use DB;
use Hash;
use Auth;
use Session;
use Yajra\DataTables\DataTables;
class AboutUsPolicyController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:manage-aboutus|create-aboutus|edit-aboutus|delete-aboutus', ['only' => ['index','show']]);
        $this->middleware('permission:create-aboutus', ['only' => ['create','store']]);
        $this->middleware('permission:edit-aboutus', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-aboutus', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax($request))
            return $this->datatable($request);
        return view('admin.about-us.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.about-us.add',compact('reports','sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'template' => 'required'
        ]);

        $input = $request->all();

        $user = Pages::create($input);
        return redirect()->route('aboutus.index')
                        ->with('success','About Us created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=Pages::find($id);
        $user->group_id=@$request->group_id;
        $user->save();
        return response()->json(array('status'=>1,'message'=>'Successfully updated.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$data = Pages::where('id',$id)->first();
        $reports = Reports::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        $sectors = Sectors::where('deleted_at',NULL)->orderBy('id','DESC')->get();
        return view('admin.about-us.edit',compact('data','reports','sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'template' => 'required'
        ]);

        $user =Pages::find($id);
        $user->meta_title = $request->meta_title;
        $user->meta_keywords = $request->meta_keywords;
        $user->meta_description = $request->meta_description;
        $user->tag = $request->tag;
        $user->template = $request->template;
        $user->save();      
        return redirect()->route('aboutus.index')
                        ->with('success','About Us updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pages::find($id)->delete();
        return redirect()->route('aboutus.index')
                        ->with('success','About Us deleted successfully');
    }

    public function datatable(Request $request){
        $users=Pages::where('deleted_at',NULL)->where('tag','about-us')->get();
        return DataTables::of($users)
                    ->addColumn('sno',function(){STATIC $count=1; return $count++;})
                    ->rawColumns(['input'=>true,'html'=>true])
                  
                    ->addColumn('action', function ($row) {
                        $action='';
                        if(Auth::user()->can('edit-aboutus')){
                            $action .='<a href="'.route("aboutus.edit",$row->id).'"><i class="fa fa-edit" style="font-size: 24px;"></i></a>&nbsp;&nbsp;';
                        }
                        if(Auth::user()->can('delete-aboutus')){
                            $action .='<a href="'.route("aboutus.destroy",$row->id).'" onclick="return confitm(\'Are you sure??????.\')"><i class="fa fa-trash" style="color:red;font-size: 24px;"></i></a>';
                        }
                        return $action;
                    })->make();
    }
}
