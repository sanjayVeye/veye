<?php

namespace App\Helpers;

use App\Models\DividendReports;
use App\Models\Gateway;
use App\Models\Menues;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Helper
{
    public static function list(){
        Artisan::call('cache:clear');
        $parent=DB::table('nav_masters')
                                        ->where('parent_id',0)
                                        ->orderBy('orders','ASC')->get();
        $nav=[];
        foreach($parent as $row){
            if(Auth::user()->canany(explode(',',$row->permissions))){
                $child=DB::table('nav_masters')
                                                ->where('parent_id',$row->id)
                                                ->orderBy('orders','ASC')->get();

                $tmp=[];
                foreach($child as $cRow){
                    if(Auth::user()->canany(explode(',',$cRow->permissions))){
                        array_push($tmp,array('name'=>$cRow->name,'parent_id'=>$cRow->parent_id,'path'=>$cRow->path));
                    }
                }

                if(count($tmp) > 0){
                    array_push($nav,array('name'=>$row->name,'parent_id'=>$row->parent_id,'path'=>$row->path,'childs'=>$tmp));
                }else{
                    array_push($nav,array('name'=>$row->name,'parent_id'=>$row->parent_id,'path'=>$row->path,'childs'=>[]));
                }
            }
        }

        return $nav;

    }

    public static function frontend_menues(){
        return Menues::with('childs')->where('parent',0)
                        ->orderBy('order','asc')->get();
    }

    public static function subscribeMailChimp($clientData,$mailchimp_id,$subscribe){
    $api_key = '07aa56da04c0a17a963dc24ae3f13753-us17';
    /**send data on mailchimp************/
    $postData = json_encode([
        "email_address" => $clientData->email ?? '',
        "status"        => $subscribe,
        "merge_fields"  => [
            "FNAME" => $clientData->first_name ?? '',
            "LNAME" => $clientData->last_name ?? '',
            "PHONE" =>  $clientData->phone ?? '',
        ]
    ]);

    $ch = curl_init();

    $ch = curl_init('https://us17.api.mailchimp.com/3.0/lists/'.$mailchimp_id.'/members/');
    // curl_setopt($ch, CURLOPT_URL,"https://omsspices.com/contact.php");
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: apikey ' . $api_key,
            'Content-Type: application/json'
        ),
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $postData
    ));
    $response = curl_exec($ch);
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    // dd($ch);
}

    public static function menuesByParent($parent_id){
        return Menues::where('parent',$parent_id)
                        ->where('deleted_at',null)->get();
    }

    public static function getDividendReports(){
        return DividendReports::orderBy('dividend_reports.created_at', 'desc')
                                            ->paginate(4);
    }

    public static function getGatewayData(){
        return Gateway::first();
    }
}
