<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class mailChimpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function handle(MailChimp $event)
    {
        $api_key = '07aa56da04c0a17a963dc24ae3f13753-us17';
        $list_id = $event->id;

		/**send data on mailchimp************/
		$postData = json_encode([
			"email_address" => $_POST['email'] ?? '',
			"status"        => "subscribed",
			"merge_fields"  => [
				"FNAME" => $_POST['Lname'] ?? '',
				"LNAME" => $_POST['Fname'] ?? '',
				"PHONE"=>  $_POST['phone'] ?? '',
			]
		]);

		$ch = curl_init();

		$ch = curl_init('https://us17.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
		// curl_setopt($ch, CURLOPT_URL,"https://omsspices.com/contact.php");
		curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array(
				'Authorization: apikey '.$api_key,
				'Content-Type: application/json'
			),
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $postData
		));
		$response = curl_exec($ch);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
    }
}
