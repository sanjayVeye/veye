<?php

namespace App\Providers;

use App\Providers\SendEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmail  $event
     * @return void
     */
    public function handle(SendEmail $event)
    {
        $subject=$event->subject;
        $emails=$event->emails;
        $attachment=$event->attachment;
        $body=$event->body;
        $data=[];
        Mail::send([],$data,function($message) use ($subject,$emails,$attachment,$body){
            $message->to($emails)->subject($subject);
            $message->from('resetpassword@veyeptyltd.com.au',$subject);
            $message->setBody($body, 'text/html');
            if($attachment){
                        // $message->attach($attachment);
                        $message->attachData($attachment, 'factuur.pdf', ['mime' => 'application/pdf']);
                    }
        });






        // Mail::send('send_email',[],function($message) use ($subject,$emails,$attachment,$body){
        //     $message->to($emails)->subject($subject);
        //     $message->from('testingemail@veye.co.in','Veye');
        //     $message->setBody('<h1>Testing</h1>', 'text/html');
        //     if($attachment){
        //         $message->attach($attachment);
        //     }
        //  });
    }
}
