<?php

namespace App\Providers;

use App\Http\Controllers\Controller;
use App\Providers\SendOtp;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendOtpNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendOtp  $event
     * @return void
     */
    public function handle(SendOtp $event)
    {
        $smsmessage =urlencode('Dear '.$event->name.', Welcome to GUPTA MARBLE AND PAINTS HOUSE. Your Mobile OTP '.$event->otp);
        $sms_url1 = "http://admin.bulksmslogin.com/api/sendhttp.php?authkey=".env('AUTHKEY')."&mobiles=".$event->mobile."&message=".$smsmessage."&sender=".env('SENDERID')."&route=4&DLT_TE_ID=".env('DLTID');
        file_get_contents($sms_url1);
    }
}
