<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\frontend\ClientsRegistrationController;
use App\Http\Controllers\frontend\HomeController;
use App\Http\Controllers\frontend\FrontendController;
use Illuminate\Support\Facades\Auth;



/* 
    Clients Registration
    Dated 06-02-2023
    */

Route::resource('registration', ClientsRegistrationController::class)->except(['destroy']);

Route::get('client-login', [ClientsRegistrationController::class, 'login'])->name('registration.login');

Route::post('client-login-match', [ClientsRegistrationController::class, 'loginMatch'])->name('registration.login.match');

Route::get('dashboard', [ClientsRegistrationController::class, 'dashboard'])->name('dashboard.home');

Route::get('forgot-password', [ClientsRegistrationController::class, 'forgotPassword'])->name('forgot.password');

Route::post('reset-password', [ClientsRegistrationController::class, 'resetPassword'])->name('reset.password');

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('sectorSpecific', [HomeController::class, 'sectorSpecific'])->name('sectorSpecific');
Route::get('subscribe', [HomeController::class, 'subscriptionMenu'])->name('subscriptionMenu');
//changed subscriptionMenu to subscribe
Route::get('editorial', [HomeController::class, 'Editorial'])->name('Editorial');
Route::get('report/stock-advisory/stock-advisory',[HomeController::class,'stockadvisory'])->name('stockadvisory');
Route::get('reports',[HomeController::class,'reports'])->name('reports');
Route::get('report/sector-specific/airlines',[HomeController::class,'airlines'])->name('airlines');
Route::get('checkout', [HomeController::class, 'checkout'])->name('checkout');
Route::get('editorialdetails', [HomeController::class, 'editorialdetails'])->name('editorialdetails');
//Webinar / frontend
Route::get('webinarfrontend', [HomeController::class, 'webinarfrontend'])->name('webinarfrontend');
// Auth::routes();
// Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('my-watchlist', [ClientsRegistrationController::class, 'watchlist'])->name('my.watchlist');
    Route::get('latest-buy', [ClientsRegistrationController::class, 'latestbuy'])->name('latest.buy');
    Route::get('latest-sell', [ClientsRegistrationController::class, 'latestsell'])->name('latest.sell');
    Route::get('my-email', [ClientsRegistrationController::class, 'updateEmail'])->name('my.email');
    Route::get('my-password', [ClientsRegistrationController::class, 'updatePass'])->name('my.password');
    Route::get('my-subscription', [ClientsRegistrationController::class, 'mySubscription'])->name('my.subscription');
    Route::get('my-invoice', [ClientsRegistrationController::class, 'myInvoice'])->name('my.invoice');

    Route::post('email-update/{id}', [ClientsRegistrationController::class, 'emailupdate'])->name('useremail.emailupdate');
    Route::post('password-update/{id}', [ClientsRegistrationController::class, 'passwordupdate'])->name('userpassword.passwordupdate');
    // Route::get('registration/destroy/{id}',[ClientsRegistrationController::class,'destroy'])->name('registration.destroy');
    Route::get('report/{parent}/{child?}', [FrontendController::class, 'manage_menues'])->name('manage.menues');
    Route::get('pages/{parent}', [FrontendController::class, 'manage_footer'])->name('manage.footer');
    // Route::get('privacypolicies', [FrontendController::class, 'Privacypolicies'])->name('Privacypolicies');
    // Route::get('about-us',[FrontendController::class,'about-us'])->name('about-us');
    // Route::get('terms-condition',[FrontendController::class,'terms-condition'])->name('terms-condition');
    // Route::get('about-us',[FrontendController::class,'about-us'])->name('about-us');
});
